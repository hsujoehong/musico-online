<?php

/* newsletter/templates/blocks/social/widget.hbs */
class __TwigTemplate_1f7a4cc3cff92c891ec7af14345c355e62fb6971c9baa0d4dd7b15674a5a05ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"mailpoet_widget_icon\"><span class=\"dashicons dashicons-facebook\"></span></div>
<div class=\"mailpoet_widget_title\">";
        // line 2
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Social");
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "newsletter/templates/blocks/social/widget.hbs";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "newsletter/templates/blocks/social/widget.hbs", "/var/www/musico.demo.lohaslife.cc/httpdocs/wp-content/plugins/mailpoet/views/newsletter/templates/blocks/social/widget.hbs");
    }
}
