jQuery(function($){
    let $customJsField = $('textarea[name="betheme[custom-js]"]');
    let $customJsDiv = $('<div>')
        .attr('id', 'betheme[custom-js]')
        .height(500)
        .insertAfter($customJsField);
    $customJsField.hide();

    if($customJsField.length) {
        var editorJs = ace.edit('betheme[custom-js]');

        editorJs.getSession().setValue($customJsField.val());
        editorJs.getSession().on('change', function(){
            $customJsField.val(editorJs.getSession().getValue());
        });
        editorJs.setTheme("ace/theme/monokai");
        editorJs.session.setMode("ace/mode/javascript");
    }


    let $customCssField = $('textarea[name="betheme[custom-css]"]');
    let $customCssDiv = $('<div>')
        .attr('id', 'betheme[custom-css]')
        .height(500)
        .insertAfter($customCssField);
    $customCssField.hide();

    if($customCssField.length) {
        var editorCss = ace.edit('betheme[custom-css]');

        editorCss.getSession().setValue($customCssField.val());
        editorCss.getSession().on('change', function(){
            $customCssField.val(editorCss.getSession().getValue());
        });
        editorCss.setTheme("ace/theme/monokai");
        editorCss.session.setMode("ace/mode/css");
    }
});