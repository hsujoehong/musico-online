<?php

function musico_index_events_shortcode_function($atts){
    ob_start();
    $args = array(
        'post_type' => 'events',
        'meta_key' => '_start_time',
        'orderby' => 'meta_value',
        'order' => 'ASC',
        'post_status'   => 'publish',
        'posts_per_page' => 6,
        'offset'=> 0,
        'meta_query' => array(
            array(
                'key'     => '_end_time',
                'value'   => time(),
                'compare' => '>=',
            ),
        ),
        );
    $events = new WP_Query( $args );
    $output = '<div class="index-events-section">';
    foreach($events->get_posts() as $event){
        $post_meta = get_post_meta($event->ID);
        $post_category = get_the_terms( $event->ID, 'events_category' );
        $wpseo_primary_term = new WPSEO_Primary_Term( 'events_category', $event->ID );
        $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
        $Primary_term = get_term( $wpseo_primary_term );
        $category_name = '音樂活動 / ';
        if($Primary_term->name){
            $category_name .= $Primary_term->name;
        }else{
            $category_name .= $post_category[0]->name;
        }
        $output .= '<div class="index-events-area">';
            $output .= '<div class="index-events-warp">';
                $output .= '<div class="post-events-img">';
                    $output .= '<div class="image_wrapper">';
                        $output .= '<a href="'.$event->guid.'">';
                            $output .= get_the_post_thumbnail($event->ID,'medium');
                        $output .= '</a>';
                    $output .= '</div>';
                $output .= '</div>';
            $output .= '</div>';
            // desc ---------------------------------------------------------------------------
            $output .= '<div class="index-event-desc-wrapper">';
                $output .= '<div class="index-event-desc">';
                    $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $event->guid .'">'. $event->post_title .'</a></h2>';
                    $output .= '<h5 class="second-title" ><span class="events-time">'.date('Y/m/d',$post_meta['_start_time'][0]).' / </span><span class="events-class">'. $category_name .'</span></h5>';
                    if(has_excerpt($event->ID)) {
                        $post_excerpt = mb_substr( $event->post_excerpt, 0, 40 ) . '...<a class="read-all" href="'.$event->guid.'">詳全文</a>';
                        $output .= '<div class="post-excerpt">' .$post_excerpt . '</div>';
                    }
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';
    }
    $output .= '</div>';
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_index_portcolio_shortcode_function($atts){
    ob_start();
    $terms_list = array();
    $terms = get_terms( array('taxonomy' => 'portfolio-types','hide_empty' => false,) );
    //shortcode 預設參數
    extract(shortcode_atts(array(
        "cat" => 'all',
        "num" => '3',
    ), $atts));
    if($cat == 'all'){
        foreach ($terms as $term){
            array_push($terms_list,$term->term_id);
        }
    }else{
        $cat_array = explode(',',$cat);
        foreach($cat_array as $cat_arrays){
            array_push($terms_list,$cat_arrays);
        }
    }
    $args = array(
        'post_type' => 'portfolio',
        'orderby' => 'date',
        'post_status'   => 'publish',
        'order' => 'DESC',
        'posts_per_page' => $num,
        'offset'=> 0,
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'portfolio-types',
                'field' => 'term_id',
                'terms' => $terms_list,
            ),
        ));
    $portfolios = new WP_Query( $args );
    $output = '<div class="index-portfolio-section">';
    foreach($portfolios->get_posts() as $portfolio){
        $post_meta = get_post_meta($portfolio->ID);
        $post_category = get_the_terms( $portfolio->ID, 'portfolio-types' );
        $wpseo_primary_term = new WPSEO_Primary_Term( 'portfolio-types', $portfolio->ID );
        $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
        $Primary_term = get_term( $wpseo_primary_term );
        $category_name = '音樂專欄 / ';
        if($Primary_term->name){
            $category_name .= $Primary_term->name;
        }else{
            $category_name .= $post_category[0]->name;
        }
        $output .= '<div class="index-portfolio-area">';
            $output .= '<div class="index-portfolio-warp">';
                $output .= '<div class="post-portfolio-img">';
                    $output .= '<div class="image_wrapper">';
                        $output .= '<a href="'.$portfolio->guid.'">';
                            $output .= get_the_post_thumbnail($portfolio->ID,'medium');
                        $output .= '</a>';
                    $output .= '</div>';
                $output .= '</div>';
            $output .= '</div>';
        // desc ---------------------------------------------------------------------------
            $output .= '<div class="index-event-desc-wrapper">';
                $output .= '<div class="index-event-desc">';
                    $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $portfolio->guid .'">'. $portfolio->post_title .'</a></h2>';
                    $output .= '<h5 class="second-title" ><span class="portfolio-time">'.date('Y/m/d',strtotime($portfolio->post_date)).' / </span><span class="events-class">'. $category_name .'</span></h5>';
                if(has_excerpt($portfolio->ID)) {
                    $post_excerpt = mb_substr( $portfolio->post_excerpt, 0, 40 ) . '...<a class="read-all" href="'.$portfolio->guid.'">詳全文</a>';
                    $output .= '<div class="post-excerpt">' .$post_excerpt . '</div>';
                }
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';
    }
    $output .= '</div>';
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_events_up_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    // Get posts
    global $post;
    $post_slug = $post->post_name;
    $slug = get_term_by('name',urldecode($post_slug),'events_category');
    if ($slug == ''){
        $slug = get_term_by('slug',urldecode($post_slug),'events_category');
    }
    $event_list = array();
    if($slug !=''){
        $event_list = array($slug->term_id);
    }else{
        $events_category = get_terms( array('taxonomy' => 'events_category','hide_empty' => false) );
        foreach ($events_category as $events_categorys){
            array_push($event_list,$events_categorys->term_id);
        }
    }

    $args = array(
        'post_type' => 'events',
        'meta_key' => '_start_time',
        'orderby' => 'meta_value',
        'order' => 'ASC',
        'post_status'   => 'publish',
        'posts_per_page' => 3,
        'offset'=>((($paged-1)*13)),
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'events_category',
                'field' => 'term_id',
                'terms' => $event_list,
            ),
        ),
        'meta_query' => array(
            array(
                'key'     => '_end_time',
                'value'   => time(),
                'compare' => '>=',
            ),
        ),
    );
    $events = new WP_Query( $args );
    foreach($events->get_posts() as $event){
        $post_meta = get_post_meta($event->ID);
        $post_category = get_the_terms( $event->ID, 'events_category' );
        $wpseo_primary_term = new WPSEO_Primary_Term( 'events_category', $event->ID );
        $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
        $Primary_term = get_term( $wpseo_primary_term );
        if($slug !=''){
            $category_name = $slug ->name;
        }else{
            if($Primary_term->name){
                $category_name = $Primary_term->name;
            }else{
                $category_name = $post_category[0]->name;
            }
        }
        $weeklist = array('日', '一', '二', '三', '四', '五', '六');
        $output = '<div class="post-list-wrap">';
            $output .= '<div class="post-events-img">';
                $output .= '<div class="image_wrapper">';
                    $output .= '<a href="'.$event->guid.'">';
                        $output .= get_the_post_thumbnail($event->ID,'medium');
                    $output .= '</a>';
                $output .= '</div>';
            $output .= '</div>';
            // desc ---------------------------------------------------------------------------
            $output .= '<div class="post-event-desc-wrapper">';
                $output .= '<div class="events-title" ><span class="events-time" >'.date('Y/m/d',$post_meta['_start_time'][0]).' ( '.$weeklist[date('w',$post_meta['_start_time'][0])].' ) </span><span class="events-class">'. $category_name .'</span></div>';
                $output .= '<div class="post-event-desc">';
                        // title -------------------------------------
                    $output .= '<div class="post-title">';
                        // default ----------------------------
                        $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $event->guid .'">'. $event->post_title .'</a></h2>';
                        $output .= '<div class="button-icon">';
                            $output .= '<div class="button-favorites">';//收藏按鈕
                                $output .= get_favorites_button($event->ID);
                            $output .= '</div>';
                            $output .= '<div class="button-love">';//按讚按鈕
                                $output .= '<span class="icons-wrapper">';
                                    $output .= '<img class="thumb-up-icon" src="'.wp_get_attachment_url('566').'">';
                                $output .= '</span>';
                                $output .= '<span class="label">'.$post_meta['mfn-post-love'][0].'</span>';
                            $output .= '</div>';
                            $output .= '<div class="button-view-count">';//觀看次數
                                $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($event->ID));
                            $output .= '</div>';
                        $output .= '</div>';
                    $output .= '</div>';
                    $output .= '<div class="post-content">';
                        $output .= '<p>活動縣市：'.$post_meta['_city'][0].'</p>';
                        $output .= '<p>活動場館：'.$post_meta['_area'][0].'</p>';
                        $output .= '<p>活動類型：'.$post_meta['_class'][0].'| 活動對象：'.$post_meta['_person'][0].'| 活動門票：'.(($post_meta['_cost'][0]=='付費')?"付費":"免費").'</p>';
                    $output .= '</div>';
                if(has_excerpt($event->ID)) {
                    $post_excerpt = mb_substr( $event->post_excerpt, 0, 80 ) . '...<a class="read-all" href="'.$event->guid.'">詳全文</a>';
                    $output .= '<div class="post-excerpt">' .$post_excerpt . '</div>';
                }
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';
        echo $output;
    }
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_events_middle_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    // Get posts
    global $post;
    $post_slug = $post->post_name;
    $slug = get_term_by('name',urldecode($post_slug),'events_category');
    if ($slug == ''){
        $slug = get_term_by('slug',urldecode($post_slug),'events_category');
    }
    $event_list = array();
    if($slug !=''){
        $event_list = array($slug->term_id);
    }else{
        $events_category = get_terms( array('taxonomy' => 'events_category','hide_empty' => false,) );
        foreach ($events_category as $events_categorys){
            array_push($event_list,$events_categorys->term_id);
        }
    }
    $args = array(
        'post_type' => 'events',
        'post_status'   => 'publish',
        'meta_key' => '_start_time',
        'orderby' => 'meta_value',
        'order' => 'ASC',
        'posts_per_page' => 5,
        'offset'=>((($paged-1)*13)+3),
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'events_category',
                'field' => 'term_id',
                'terms' => $event_list,
            ),
        ),
        'meta_query' => array(
            array(
                'key'     => '_end_time',
                'value'   => time(),
                'compare' => '>=',
            ),
        ),
    );
    $events = new WP_Query( $args );
    $output = '<div id="meddle" data-meddle-num="'.$events->post_count.'">';
    foreach($events->get_posts() as $event){
        $post_meta = get_post_meta($event->ID);
        $post_category = get_the_terms( $event->ID, 'events_category' );
        $wpseo_primary_term = new WPSEO_Primary_Term( 'events_category', $event->ID );
        $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
        $Primary_term = get_term( $wpseo_primary_term );
        if($slug !=''){
            $category_name = $slug ->name;
        }else{
            if($Primary_term->name){
                $category_name = $Primary_term->name;
            }else{
                $category_name = $post_category[0]->name;
            }
        }
        $output .= '<div class="post-list-wrap">';
            $output .= '<div class="post-events-img">';
                $output .= '<div class="image_wrapper">';
                $output .= '<a href="'.$event->guid.'">';
                    $output .= get_the_post_thumbnail($event->ID,'thumbnail');
                $output .= '</a>';
                $output .= '</div>';
            $output .= '</div>';
        // desc ---------------------------------------------------------------------------
            $output .= '<div class="post-event-desc-wrapper">';
                $output .= '<div class="post-event-desc">';
                    // title -------------------------------------
                    $output .= '<div class="post-title">';
                    // default ----------------------------
                    $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $event->guid .'">'. $event->post_title .'</a></h2>';
                    $output .= '<div class="button-icon">';
                        $output .= '<div class="button-favorites">';//收藏按鈕
                            $output .= get_favorites_button($event->ID);
                        $output .= '</div>';
                        $output .= '<div class="button-love">';//按讚按鈕
                            $output .= '<span class="icons-wrapper">';
                                $output .= '<img class="thumb-up-icon" src="'.wp_get_attachment_url('566').'">';
                            $output .= '</span>';
                            $output .= '<span class="label">'.$post_meta['mfn-post-love'][0].'</span>';
                        $output .= '</div>';
                        $output .= '<div class="button-view-count">';//觀看次數
                            $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($event->ID));
                        $output .= '</div>';
                    $output .= '</div>';
                $output .= '</div>';
                $output .= '<h5 class="second-title" >'.date('Y.m.d',$post_meta['_start_time'][0]).' / 音樂活動 / '. $category_name .'</h5>';
                $output .= '<div class="post-content">';
                    $output .= '<p>活動縣市：'.$post_meta['_city'][0].'</p>';
                    $output .= '<p>活動場館：'.$post_meta['_area'][0].'</p>';
                    $output .= '<p>活動類型：'.$post_meta['_class'][0].'| 活動對象：'.$post_meta['_person'][0].'| 活動門票：'.(($post_meta['_cost'][0]=='付費')?"付費":"免費").'</p>';
                $output .= '</div>';
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';
    }
    $output .= '</div>';
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_events_down_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    // Get posts
    global $post;
    $post_slug = $post->post_name;
    $slug = get_term_by('name',urldecode($post_slug),'events_category');
    if ($slug == ''){
        $slug = get_term_by('slug',urldecode($post_slug),'events_category');
    }
    $event_list = array();
    if($slug !=''){
        $event_list = array($slug->term_id);
    }else{
        $events_category = get_terms( array('taxonomy' => 'events_category','hide_empty' => false) );
        foreach ($events_category as $events_categorys){
            array_push($event_list,$events_categorys->term_id);
        }
    }
    $args = array(
        'post_type' => 'events',
        'meta_key' => '_start_time',
        'orderby' => 'meta_value',
        'post_status'   => 'publish',
        'order' => 'ASC',
        'posts_per_page' => 5,
        'offset'=>((($paged-1)*13)+8),
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'events_category',
                'field' => 'term_id',
                'terms' => $event_list,
            ),
        ),
        'meta_query' => array(
            array(
                'key'     => '_end_time',
                'value'   => time(),
                'compare' => '>=',
            ),
        ),
    );
    $events = new WP_Query( $args );
    $output = '';
    foreach($events->get_posts() as $event){
        $post_meta = get_post_meta($event->ID);
        $post_category = get_the_terms( $event->ID, 'events_category' );
        $wpseo_primary_term = new WPSEO_Primary_Term( 'events_category', $event->ID );
        $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
        $Primary_term = get_term( $wpseo_primary_term );
        if($slug !=''){
            $category_name = $slug ->name;
        }else{
            if($Primary_term->name){
                $category_name = $Primary_term->name;
            }else{
                $category_name = $post_category[0]->name;
            }
        }
            $output .= '<div  class="post-list-wrap">';
                $output .= '<div class="post-events-img">';
                    $output .= '<div class="image_wrapper">';
                        $output .= '<a href="'.$event->guid.'">';
                            $output .= get_the_post_thumbnail($event->ID,'thumbnail');
                        $output .= '</a>';
                    $output .= '</div>';
                $output .= '</div>';
                // desc ---------------------------------------------------------------------------
                $output .= '<div class="post-event-desc-wrapper">';
                    $output .= '<div class="post-event-desc">';
                        // title -------------------------------------
                        $output .= '<div class="post-title">';
                        // default ----------------------------
                            $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $event->guid .'">'. $event->post_title .'</a></h2>';
                            $output .= '<div class="button-icon">';
                                $output .= '<div class="button-favorites">';//收藏按鈕
                                    $output .= get_favorites_button($event->ID);
                                $output .= '</div>';
                                $output .= '<div class="button-love">';//按讚按鈕
                                    $output .= '<span class="icons-wrapper">';
                                        $output .= '<img class="thumb-up-icon" src="'.wp_get_attachment_url('566').'">';
                                    $output .= '</span>';
                                    $output .= '<span class="label">'.$post_meta['mfn-post-love'][0].'</span>';
                                $output .= '</div>';
                                $output .= '<div class="button-view-count">';//觀看次數
                                    $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($event->ID));
                                $output .= '</div>';
                            $output .= '</div>';
                        $output .= '</div>';
                        $output .= '<h5 class="second-title" >'.date('Y.m.d',$post_meta['_start_time'][0]).' / 音樂活動 / '. $category_name .'</h5>';
                        $output .= '<div class="post-content">';
                            $output .= '<p>活動縣市：'.$post_meta['_city'][0].'</p>';
                            $output .= '<p>活動場館：'.$post_meta['_area'][0].'</p>';
                            $output .= '<p>活動類型：'.$post_meta['_class'][0].'| 活動對象：'.$post_meta['_person'][0].'| 活動門票：'.(($post_meta['_cost'][0]=='付費')?"付費":"免費").'</p>';
                        $output .= '</div>';
                    $output .= '</div>';
                $output .= '</div>';
            $output .= '</div>';
    }
    echo $output;
    ?>
    <div class="pagination">
        <?php
        $args = array(
            'post_type' => 'events',
            'post_status'   => 'publish',
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'OR',
                array(
                    'taxonomy' => 'events_category',
                    'field' => 'term_id',
                    'terms' => $event_list,
                ),
            ),
            'meta_query' => array(
                array(
                    'key'     => '_end_time',
                    'value'   => time(),
                    'compare' => '>=',
                ),
            ),
            );
        $even = new WP_Query( $args );
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => ceil($even->found_posts/13),
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => '<i class="icon-left-open"></i>',
            'next_text'    => '<i class="icon-right-open"></i>',
            'add_args'     => false,
            'add_fragment' => '',
        ) );
        ?>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_index_search_box_shortcode_function($atts){
    ob_start();
    $states = array('基隆市' => '基隆市','台北市' => '台北市','新北市' => '新北市','宜蘭縣' => '宜蘭縣','桃園市' => '桃園市','新竹市' => '新竹市','新竹縣' => '新竹縣',
        '苗栗縣' => '苗栗縣','台中市' => '台中市','彰化縣' => '彰化縣','南投縣' => '南投縣','雲林縣' => '雲林縣','嘉義市' => '嘉義市','嘉義縣' => '嘉義縣','台南市' => '台南市',
        '高雄市' => '高雄市','屏東縣' => '屏東縣','花蓮縣' => '花蓮縣','台東縣' => '台東縣','澎湖縣' => '澎湖縣','金門縣' => '金門縣','連江縣' => '連江縣',
    );
    $post_types = get_terms( array('taxonomy' => 'events_category','hide_empty' => false,) );
    $post_areas = get_terms( array('taxonomy' => 'post_area','hide_empty' => false,) );
    $post_classs = get_terms( array('taxonomy' => 'post_class','hide_empty' => false,) );
    $post_persons = get_terms( array('taxonomy' => 'post_person','hide_empty' => false,) );
    $post_natures = get_terms( array('taxonomy' => 'post_nature','hide_empty' => false,) );
?>
    <div class="search-box-section">
        <form method="post" id="events-searchform" action="<?php echo esc_url( home_url( '/events-search/' ) ); ?>">
            <div>
                <h1 class="search-box-title">搜尋音樂活動</h1>
                <div class="keyword-div">
                    <input class="keyword" type="text" name="keyword" placeholder="關鍵字">
                </div>
            </div>
            <div>
                <div class="datepicker-div">
                    <input class="datepicker-input datepicker-input-start" type="text" name="start_time" placeholder="起訖日期設定">
                    <input class="datepicker-input datepicker-input-end" type="text" name="end_time" placeholder="起訖日期設定">
                </div>
            </div>
            <div class="all-select-div">
                <div class="select-div">
                    <select class="search-box-select" name="city">
                        <option value="" disabled selected hidden>活動縣市</option>
                        <?php foreach($states as $state){ ?>
                            <option value="<?=$state?>" ><?=$state?></option>
                        <?php }?>
                    </select>
                    <select class="search-box-select" name="person">
                        <option value="" disabled selected hidden>活動對象</option>
                        <?php foreach($post_persons as $post_person){ ?>
                            <option value="<?=$post_person->name?>"><?=$post_person->name?></option>
                        <?php }?>

                    </select>
                </div>
                <div class="select-div">
                    <select class="search-box-select" name="type">
                        <option value="" disabled selected hidden>音樂類型</option>
                        <?php foreach($post_types as $post_type){ ?>
                            <option value="<?=$post_type->name?>"><?=$post_type->name?></option>
                        <?php }?>
                    </select>
                    <select class="search-box-select" name="class">
                        <option value="" disabled selected hidden>活動類型</option>
                        <?php foreach($post_classs as $post_class){ ?>
                            <option value="<?=$post_class->name?>"><?=$post_class->name?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="select-div">
                    <select class="search-box-select" name="nature">
                        <option value="" disabled selected hidden>活動性質</option>
                        <?php foreach($post_natures as $post_nature){ ?>
                            <option value="<?=$post_nature->name?>"><?=$post_nature->name?></option>
                        <?php }?>
                    </select>
                    <select class="search-box-select" name="cost">
                        <option value="" disabled selected hidden>活動門票</option>
                        <option value="付費" >售票</option>
                        <option value="免費" >免費</option>
                    </select>
                </div>
                <div class="select-div-one">
                    <select class="search-box-select" name="area">
                        <option value="" disabled selected hidden>活動場館</option>
                        <?php foreach($post_areas as $post_area){ ?>
                            <option value="<?=$post_area->name?>" ><?=$post_area->name?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <div class="search-box-btn">
                <button type="submit" class="search-btn">開始搜尋</button>
                <button type="button" class="clean-btn">清空重搜</button>
            </div>
        </form>
    </div>
<?php
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_index_top10_shortcode_function($atts){
    ob_start();
    $event_list = array();
    $events_category = get_terms( array('taxonomy' => 'events_category','hide_empty' => false) );
    foreach ($events_category as $events_categorys){
        array_push($event_list,$events_categorys->term_id);
    }
    $args = array(
        'post_type' => 'events',
        'meta_key' => 'views',
        'orderby' => 'meta_value_num',
        'post_status'   => 'publish',
        'order' => 'DESC',
        'posts_per_page' => 10,
        'offset'=>0,
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'events_category',
                'field' => 'term_id',
                'terms' => $event_list,
            ),
        ),
        'meta_query' => array(
            array(
                'key'     => '_end_time',
                'value'   => time(),
                'compare' => '>=',
            ),
        ),
        );
    $top10 = new WP_Query( $args );
    $i = 1;
    $output = '<div id="top-section">';
        $output .= '<div class="title-bar" >';
            $output .= '<h1>熱門活動<span class="top10-title-span">Top10</span></h1>';
        $output .= '</div>';
        $output .= '<div id="top10-area-wrapper">';
        foreach($top10->get_posts() as $top10s){
            $output .= '<div class="top10-area">';
                $output .= '<div class="top10-detail">';
                    $output .= '<span class="top10-num">'.$i.'</span><div class="top10-name"><a href="'.$top10s->guid.'" >' . mb_substr( $top10s->post_title, 0, 40 ) . '</a></div>';
                $output .= '</div>';
            $output .= '</div>';
            $i++;
        }
        $output .= '</div>';
    $output .= '</div>';
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_portfolio_author_shortcode_function($atts){
    ob_start();
    $portfolio_author = $_GET['portfolio_author'];
    extract(shortcode_atts(array("cat" => '57'), $atts));
    $tax_args = array(
        'taxonomy' => 'portfolio_author',
        'hide_empty' => false,
        'meta_query' => array(
            array(
                'key'       => 'author_belong_class',
                'value'     => $cat,
                'compare'   => '='
            )
        ),
        'parent' => 0,
    );
    $catsArray = array();
    $author_belong_class = get_terms($tax_args);
    foreach($author_belong_class as $author_belong){
        $author_array = get_term_children($author_belong->term_id,'portfolio_author');
        $catsArray = array_merge($catsArray,$author_array);
    }

    $author_categorys = get_terms(array(
        'taxonomy' => 'portfolio_author',
        'hide_empty' => false,
        'include' => $catsArray,
    ));
    $change_id = '';
    if($portfolio_author !=''){
        foreach($author_categorys as $key => $author_category){
            if($author_category->term_id == $portfolio_author && $key != 0 ){
                $change_id = $key;
            }
        }
    }
    if($change_id != ''){
        $new_author_array = array($author_categorys[$change_id]);
        unset($author_categorys[$change_id]);
        $author_categorys = array_merge($new_author_array,$author_categorys);
    }


    // default ----------------------------
    $output ='<div class="every-author">';
        $output .= '<a class="author-link" href="./">';
            $output .='<div class="all-author">';
                $output .= '<span class="en-title">ALL</span>';
                $output .= '<span class="tw-title">所有文章</span>';
            $output .='</div>';
        $output .= '</a>';
        $output .='<div class="author-area" data-col="'.count($author_categorys).'">';
            foreach($author_categorys as $author_category) {
                if($author_category->parent != 0){
                    $author_class = get_term_by('term_id',$author_category->parent,'portfolio_author');
                    $author_name = $author_class->name . ' | ' .$author_category->name;
                    $active = ($portfolio_author == $author_category->term_id)?"author_active":"";
                        $output .= '<div class="author-detail">';
                            $output .= '<a class="author-link" href="?portfolio_author='.$author_category->term_id.'">';
                                $output .= '<div class="author-image">';
                                    $output .= ttw_thumbnail_image($author_category->term_id,'thumbnail');
                                    $output .= '<div class="author-title '.$active.'">';
                                        $output .= $author_name;
                                    $output .= '</div>';
                                $output .= '</div>';
                            $output .= '</a>';
                        $output .= '</div>';
                }
            }
        $output .='</div>';
    $output .='</div>';
    $author_description = get_term_by('term_id',$portfolio_author,'portfolio_author');
    if(!empty($author_description)){
        $output .='<div class="discription-detail">';
            $output .= '<div class="author-name ">';
                $output .= $author_description->name;
            $output .= '</div>';
            $output .= '<div class="author-description ">';
                $output .= $author_description->description;
            $output .= '</div>';
        $output .= '</div>';
    }
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_portfolio_up_shortcode_function($atts){
    ob_start();

    extract(shortcode_atts(array("cat" => '57'), $atts));
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    // Get posts
    $portfolio_author = $_GET['portfolio_author'];
    $author_list = array($cat);
    if($portfolio_author !=''){
        $args = array(
            'post_type' => 'portfolio',
            'post_status'   => 'publish',
            'orderby' => 'date',
            'order' => 'D   ESC',
            'posts_per_page' => 3,
            'offset'=>((($paged-1)*8)),
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'portfolio_author',
                    'field' => 'term_id',
                    'terms' => array($portfolio_author),
                ),
                array(
                    'taxonomy' => 'portfolio-types',
                    'field' => 'term_id',
                    'terms' => $author_list,
                ),
            ));
    }else{
        $tax_args = array(
            'taxonomy' => 'portfolio_author',
            'hide_empty' => false,
            'meta_query' => array(
                array(
                    'key'       => 'author_belong_class',
                    'value'     => $cat,
                    'compare'   => '='
                )
            ),
            'parent' => 0,
        );
        $catsArray = array();
        $author_belong_class = get_terms($tax_args);
        foreach($author_belong_class as $author_belong){
            $author_array = get_term_children($author_belong->term_id,'portfolio_author');
            $catsArray = array_merge($catsArray,$author_array);
        }
        $args = array(
            'post_type' => 'portfolio',
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => 3,
            'offset'=>((($paged-1)*8)),
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'portfolio_author',
                    'field' => 'term_id',
                    'terms' => $catsArray,
                ),
                array(
                    'taxonomy' => 'portfolio-types',
                    'field' => 'term_id',
                    'terms' => $author_list,
                ),
            ));
    }
    $output = '';
    $portfoliios = new WP_Query( $args );
    foreach($portfoliios->get_posts() as $portfoliio){
            $post_meta = get_post_meta($portfoliio->ID);
            $post_author = get_the_terms( $portfoliio->ID, 'portfolio_author' )[0];
            $author_class = get_term_by( 'id',$post_author->parent, 'portfolio_author' );
            $output .= '<div class="post-list-wrap">';
            $output .= '<div class="post-events-img">';
            $output .= '<div class="image_wrapper">';
            $output .= '<a href="'.$portfoliio->guid.'">';
            $output .= get_the_post_thumbnail($portfoliio->ID,'medium');
            $output .= '</a>';
            $output .= '</div>';
            $output .= '</div>';
            // desc ---------------------------------------------------------------------------
            $output .= '<div class="post-event-desc-wrapper">';
            $output .= '<div class="portfolio-title" ><span class="portfolio-time">'.date('Y.m.d',strtotime($portfoliio->post_date)).'</span>  <span class="portfolio-class">'. $author_class->name .' | '.$post_author->name.'</span></div>';
            $output .= '<div class="post-event-desc">';
            // title -------------------------------------
            $output .= '<div class="post-title">';
            // default ----------------------------
            $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $portfoliio->guid .'">'. $portfoliio->post_title .'</a></h2>';
            $output .= '<div class="button-icon">';
            $output .= '<div class="button-favorites">';//收藏按鈕
            $output .= get_favorites_button($portfoliio->ID);
            $output .= '</div>';
            $output .= '<div class="button-love">';//按讚按鈕
            $output .= '<span class="icons-wrapper">';
            $output .= '<img class="thumb-up-icon" src="'.wp_get_attachment_url('566').'">';
            $output .= '</span>';
            $output .= '<span class="label">'.$post_meta['mfn-post-love'][0].'</span>';
            $output .= '</div>';
            $output .= '<div class="button-view-count">';//觀看次數
            $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($portfoliio->ID));
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            if(has_excerpt($portfoliio->ID)) {
                $post_excerpt = mb_substr( $portfoliio->post_excerpt, 0, 300 ) . '...<a class="read-all" href="'.$portfoliio->guid.'">詳全文</a>';
                $output .= '<div class="post-excerpt">' .$post_excerpt . '</div>';
            }
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
    }
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_portfolio_down_shortcode_function($atts){
    ob_start();
    extract(shortcode_atts(array("cat" => '57'), $atts));
    $portfolio_author = $_GET['portfolio_author'];
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    // Get posts
    $author_list = array($cat);
    if($portfolio_author !=''){
        $args = array(
            'post_type' => 'portfolio',
            'post_status'   => 'publish',
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => 5,
            'offset'=>((($paged-1)*8)+3),
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'portfolio_author',
                    'field' => 'term_id',
                    'terms' => array($portfolio_author),
                ),
                array(
                    'taxonomy' => 'portfolio-types',
                    'field' => 'term_id',
                    'terms' => $author_list,
                ),
            ));
    }else{
        $tax_args = array(
            'taxonomy' => 'portfolio_author',
            'hide_empty' => false,
            'meta_query' => array(
                array(
                    'key'       => 'author_belong_class',
                    'value'     => $cat,
                    'compare'   => '='
                )
            ),
            'parent' => 0,
        );
        $catsArray = array();
        $author_belong_class = get_terms($tax_args);
        foreach($author_belong_class as $author_belong){
            $author_array = get_term_children($author_belong->term_id,'portfolio_author');
            $catsArray = array_merge($catsArray,$author_array);
        }
        $args = array(
            'post_type' => 'portfolio',
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => 5,
            'offset'=>((($paged-1)*8)+3),
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'portfolio_author',
                    'field' => 'term_id',
                    'terms' => $catsArray,
                ),
                array(
                    'taxonomy' => 'portfolio-types',
                    'field' => 'term_id',
                    'terms' => $author_list,
                ),
            ));
    }
    $output = '';
    $portfoliios = new WP_Query( $args );
    foreach($portfoliios->get_posts() as $portfoliio){
            $post_meta = get_post_meta($portfoliio->ID);
            $post_author = get_the_terms( $portfoliio->ID, 'portfolio_author' )[0];
            $author_class = get_term_by( 'id',$post_author->parent, 'portfolio_author' );
            $output .= '<div class="post-list-wrap">';
            $output .= '<div class="post-events-img">';
            $output .= '<div class="image_wrapper">';
            $output .= '<a href="'.$portfoliio->guid.'">';
            $output .= get_the_post_thumbnail($portfoliio->ID,'thumbnail');
            $output .= '</a>';
            $output .= '</div>';
            $output .= '</div>';
            // desc ---------------------------------------------------------------------------
            $output .= '<div class="post-event-desc-wrapper">';
            $output .= '<div class="post-event-desc">';
            // title -------------------------------------
            $output .= '<div class="post-title">';
            // default ----------------------------
            $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $portfoliio->guid .'">'. $portfoliio->post_title .'</a></h2>';
            $output .= '<div class="button-icon">';
            $output .= '<div class="button-favorites">';//收藏按鈕
            $output .= get_favorites_button($portfoliio->ID);
            $output .= '</div>';
            $output .= '<div class="button-love">';//按讚按鈕
            $output .= '<span class="icons-wrapper">';
            $output .= '<img class="thumb-up-icon" src="'.wp_get_attachment_url('566').'">';
            $output .= '</span>';
            $output .= '<span class="label">'.$post_meta['mfn-post-love'][0].'</span>';
            $output .= '</div>';
            $output .= '<div class="button-view-count">';//觀看次數
            $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($portfoliio->ID));
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '<h5 class="second-title" >'.date('Y.m.d',strtotime($portfoliio->post_date)).' / '. $author_class->name .'/'.$post_author->name .'</h5>';
            if(has_excerpt($portfoliio->ID)) {
                $post_excerpt = mb_substr( $portfoliio->post_excerpt, 0, 40 ) . '...<a class="read-all" href="'.$portfoliio->guid.'">詳全文</a>';
                $output .= '<div class="post-excerpt">' .$post_excerpt . '</div>';
            }
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
    }
    $output .= '</div>';
    echo $output;
    ?>
    <div class="pagination">
        <?php
        if($portfolio_author !=''){
            $args = array(
                'post_type' => 'portfolio',
                'post_status'   => 'publish',
                'orderby' => 'date',
                'order' => 'DESC',
                'posts_per_page' => -1,
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'portfolio_author',
                        'field' => 'term_id',
                        'terms' => array($portfolio_author),
                    ),
                    array(
                        'taxonomy' => 'portfolio-types',
                        'field' => 'term_id',
                        'terms' => $author_list,
                    ),
                ));
        }else {
            $tax_args = array(
                'taxonomy' => 'portfolio_author',
                'hide_empty' => false,
                'meta_query' => array(
                    array(
                        'key'       => 'author_belong_class',
                        'value'     => $cat,
                        'compare'   => '='
                    )
                ),
                'parent' => 0,
            );
            $catsArray = array();
            $author_belong_class = get_terms($tax_args);
            foreach($author_belong_class as $author_belong){
                $author_array = get_term_children($author_belong->term_id,'portfolio_author');
                $catsArray = array_merge($catsArray,$author_array);
            }
            $args = array(
                'post_type' => 'portfolio',
                'orderby' => 'date',
                'order' => 'DESC',
                'posts_per_page' => -1,
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'portfolio_author',
                        'field' => 'term_id',
                        'terms' => $catsArray,
                    ),
                    array(
                        'taxonomy' => 'portfolio-types',
                        'field' => 'term_id',
                        'terms' => $author_list,
                    ),
                ));
        }
        $portfolios = new WP_Query( $args );
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => ceil($portfolios->found_posts/8),
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => '<i class="icon-left-open"></i>',
            'next_text'    => '<i class="icon-right-open"></i>',
            'add_args'     => false,
            'add_fragment' => '',
        ) );
        ?>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_special_portfolio_up_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    // Get posts
    global $post;
    $post_slug = $post->post_name;
    $slug = get_term_by('name',urldecode($post_slug),'portfolio-types');
    if ($slug == ''){
        $slug = get_term_by('slug',urldecode($post_slug),'portfolio-types');
    }
    $author_list = array($slug->term_id);
    $args = array(
        'post_type' => 'portfolio',
        'post_status'   => 'publish',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => 3,
        'offset'=>((($paged-1)*8)),
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'portfolio-types',
                'field' => 'term_id',
                'terms' => $author_list,
            ),
        ));
    $portfoliios = new WP_Query( $args );
    $output = '';
    foreach($portfoliios->get_posts() as $portfoliio){
        $post_meta = get_post_meta($portfoliio->ID);
        $post_author = get_the_terms( $portfoliio->ID, 'portfolio_author' )[0];
        $author_class = get_term_by( 'id',$post_author->parent, 'portfolio_author' );
        $output .= '<div class="special-portfolio-list-wrap">';
            $output .= '<div class="special-portfolio-img">';
                $output .= '<div class="image_wrapper">';
                    $output .= '<a href="'.$portfoliio->guid.'">';
                        $output .= get_the_post_thumbnail($portfoliio->ID,'medium');
                    $output .= '</a>';
                $output .= '</div>';
            $output .= '</div>';
            // desc ---------------------------------------------------------------------------
            $output .= '<div class="special-portfolio-desc-wrapper">';
                $output .= '<div class="portfolio-title" ><span class="portfolio-time">'.date('Y.m.d',strtotime($portfoliio->post_date)).'</span>  <span class="portfolio-class">'. $author_class->name .'|'.$post_author->name.'</span></div>';
                $output .= '<div class="post-event-desc">';
                    // title -------------------------------------
                    $output .= '<div class="post-title">';
                        // default ----------------------------
                        $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $portfoliio->guid .'">'. $portfoliio->post_title .'</a></h2>';
                    $output .= '</div>';
                if(has_excerpt($portfoliio->ID)) {
                    $post_excerpt = mb_substr( $portfoliio->post_excerpt, 0, 120 ) . '...<a class="read-all" href="'.$portfoliio->guid.'">詳全文</a>';
                    $output .= '<div class="post-excerpt">' .$post_excerpt . '</div>';
                }
                $output .= '</div>';
                $output .= '<div class="button-icon">';
                    $output .= '<div class="button-favorites">';//收藏按鈕
                        $output .= get_favorites_button($portfoliio->ID);
                    $output .= '</div>';
                    $output .= '<div class="button-love">';//按讚按鈕
                        $output .= '<span class="icons-wrapper">';
                            $output .= '<img class="thumb-up-icon" src="'.wp_get_attachment_url('566').'">';
                        $output .= '</span>';
                        $output .= '<span class="label">'.$post_meta['mfn-post-love'][0].'</span>';
                    $output .= '</div>';
                    $output .= '<div class="button-view-count">';//觀看次數
                        $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($portfoliio->ID));
                    $output .= '</div>';
                $output .= '</div>';
                $output .= '<div class="special-portfolio-button-area"><a href="'. $portfoliio->guid .'"><button class="special-portfolio-button">閱讀完整文章</button></a></div>';
            $output .= '</div>';
        $output .= '</div>';
    }
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_special_portfolio_down_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    // Get posts
    global $post;
    $post_slug = $post->post_name;
    $slug = get_term_by('name',urldecode($post_slug),'portfolio-types');
    if ($slug == ''){
        $slug = get_term_by('slug',urldecode($post_slug),'portfolio-types');
    }
    $author_list = array($slug->term_id);
    $args = array(
        'post_type' => 'portfolio',
        'post_status'   => 'publish',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => 5,
        'offset'=>((($paged-1)*8)+3),
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'portfolio-types',
                'field' => 'term_id',
                'terms' => $author_list,
            ),
        ));
    $portfoliios = new WP_Query( $args );
    $output = '';
    foreach($portfoliios->get_posts() as $portfoliio){
        $post_meta = get_post_meta($portfoliio->ID);
        $category_name = get_the_terms( $portfoliio->ID, 'portfolio-types' )[0];
        $output .= '<div class="post-list-wrap">';
        $output .= '<div class="post-events-img">';
        $output .= '<div class="image_wrapper">';
        $output .= '<a href="'.$portfoliio->guid.'">';
        $output .= get_the_post_thumbnail($portfoliio->ID,'thumbnail');
        $output .= '</a>';
        $output .= '</div>';
        $output .= '</div>';
        // desc ---------------------------------------------------------------------------
        $output .= '<div class="post-event-desc-wrapper">';
        $output .= '<div class="post-event-desc">';
        // title -------------------------------------
        $output .= '<div class="post-title">';
        // default ----------------------------
        $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $portfoliio->guid .'">'. $portfoliio->post_title .'</a></h2>';
        $output .= '<div class="button-icon">';
        $output .= '<div class="button-favorites">';//收藏按鈕
        $output .= get_favorites_button($portfoliio->ID);
        $output .= '</div>';
        $output .= '<div class="button-love">';//按讚按鈕
        $output .= '<span class="icons-wrapper">';
        $output .= '<img class="thumb-up-icon" src="'.wp_get_attachment_url('566').'">';
        $output .= '</span>';
        $output .= '<span class="label">'.$post_meta['mfn-post-love'][0].'</span>';
        $output .= '</div>';
        $output .= '<div class="button-view-count">';//觀看次數
        $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($portfoliio->ID));
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '<h5 class="second-title" >'.date('Y.m.d',strtotime($portfoliio->post_date)).' / 音樂專欄 / '.$category_name->name .'</h5>';
        if(has_excerpt($portfoliio->ID)) {
            $post_excerpt = mb_substr( $portfoliio->post_excerpt, 0, 40 ) . '...<a class="read-all" href="'.$portfoliio->guid.'">詳全文</a>';
            $output .= '<div class="post-excerpt">' .$post_excerpt . '</div>';
        }
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
    }
    $output .= '</div>';
    echo $output;
    ?>
    <div class="pagination">
        <?php
        $args = array(
            'post_type' => 'portfolio',
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'OR',
                array(
                    'taxonomy' => 'portfolio-types',
                    'field' => 'term_id',
                    'terms' => $author_list,
                ),
            ));
        $portfolios = new WP_Query( $args );
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => ceil($portfolios->found_posts/8),
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => '<i class="icon-left-open"></i>',
            'next_text'    => '<i class="icon-right-open"></i>',
            'add_args'     => false,
            'add_fragment' => '',
        ) );
        ?>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_post_list_shortcode_function($atts){
    ob_start();
    $cat_id = $_GET['cat_id'];
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    // Get posts
    global $post;
    $post_slug = $post->post_name;
    $slug = get_term_by('name',urldecode($post_slug),'category');
    if ($slug == ''){
        $slug = get_term_by('slug',urldecode($post_slug),'category');
    }
    $children = get_term_children($slug->term_id,'category');
    $cat_list = array($slug->term_id);
    if($cat_id !=''){
        $cat_list = array($cat_id);
    }else{
        foreach ($children as $child){
            array_push($cat_list,$child);
        }
    }
    $args = array(
        'post_type' => 'post',
        'post_status'   => 'publish',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => 10,
        'offset'=>(($paged-1)*10),
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $cat_list,
            ),
        ));
    $output = '';
    $get_post = new WP_Query( $args );
    $output .= '<div class="post-cat-list-area">';
    $output .= '<ul>';
    foreach($children as $child){
        $cat = get_term($child);
        $active=($cat_id ==$cat->term_id)?'cat-list-active':'';
        $output .= '<a href="?cat_id='.$cat->term_id.'" class="'.$active.'"><li>'.$cat->name.'</li></a>';
    }
    $output .= '</ul>';
    $output .= '</div>';
    $output .= '<div class="post-list-section-custom-area">';
    foreach($get_post->get_posts() as $posts){
        $post_meta = get_post_meta($posts->ID);
        if($cat_id ==''){
            $post_category = get_the_terms( $posts->ID, 'category' );
            $wpseo_primary_term = new WPSEO_Primary_Term( 'category', $posts->ID );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $Primary_term = get_term( $wpseo_primary_term );
            if($Primary_term->name){
                if($Primary_term->parent !='0'){
                    $parent_term = get_term_by('term_id',$Primary_term->parent,'category');
                    $category_name = $parent_term->name.' / '.$Primary_term->name;
                }else{
                    $category_name = $Primary_term->name;
                }
            }else{
                if($post_category[0]->parent !='0'){
                    $parent_term = get_term_by('term_id',$post_category[0]->parent,'category');
                    $category_name = $parent_term->name.' / '.$post_category[0]->name;
                }else{
                    $category_name = $post_category[0]->name;
                }
            }
        }else{
            $category_name = get_term_by('term_id',$cat_id,'category')->name;
        }
            $output .= '<div class="post-list-wrap">';
                $output .= '<div class="post-events-img">';
                    $output .= '<div class="image_wrapper">';
                        $output .= '<a href="'.$posts->guid.'">';
                            $output .= get_the_post_thumbnail($posts->ID,'thumbnail');
                        $output .= '</a>';
                    $output .= '</div>';
                $output .= '</div>';
            // desc ---------------------------------------------------------------------------
            $output .= '<div class="post-event-desc-wrapper">';
                $output .= '<div class="post-event-desc">';
                // title -------------------------------------
                    $output .= '<div class="post-title">';
                        // default ----------------------------
                        $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $posts->guid .'">'. $posts->post_title .'</a></h2>';
                        $output .= '<div class="button-icon">';
                            $output .= '<div class="button-favorites">';//收藏按鈕
                                $output .= get_favorites_button($posts->ID);
                            $output .= '</div>';
                            $output .= '<div class="button-love">';//按讚按鈕
                                $output .= '<span class="icons-wrapper">';
                                    $output .= '<img class="thumb-up-icon" src="'.wp_get_attachment_url('566').'">';
                                $output .= '</span>';
                                $output .= '<span class="label">'.$post_meta['mfn-post-love'][0].'</span>';
                            $output .= '</div>';
                            $output .= '<div class="button-view-count">';//觀看次數
                                $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($posts->ID));
                            $output .= '</div>';
                        $output .= '</div>';
                    $output .= '</div>';
                    $output .= '<h5 class="second-title" >'.date('Y.m.d',strtotime($posts->post_date)).' / '.$category_name.'</h5>';
                    if(has_excerpt($posts->ID)) {
                        $post_excerpt = mb_substr( $posts->post_excerpt, 0, 83 ) . '...<a class="read-all" href="'.$posts->guid.'">詳全文</a>';
                        $output .= '<div class="post-excerpt">' .$post_excerpt . '</div>';
                    }
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';
    }
    $output .= '</div>';
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_post_paginate_shortcode_function($atts){
    ob_start();
    $cat_id = $_GET['cat_id'];
    // Get posts
    global $post;
    $post_slug = $post->post_name;
    $slug = get_term_by('name',urldecode($post_slug),'category');
    if ($slug == ''){
        $slug = get_term_by('slug',urldecode($post_slug),'category');
    }
    $children = get_term_children($slug->term_id,'category');
    $cat_list = array($slug->term_id);
    if($cat_id !=''){
        $cat_list = array($cat_id);
    }else{
        foreach ($children as $child){
            array_push($cat_list,$child);
        }
    }
    ?>
    <div class="post-pagination">
        <div class="pagination">
            <?php
            $args = array(
                'post_type' => 'post',
                'post_status'   => 'publish',
                'orderby' => 'date',
                'order' => 'DESC',
                'posts_per_page' => -1,
                'offset'=>0,
                'tax_query' => array(
                    'relation' => 'OR',
                    array(
                        'taxonomy' => 'category',
                        'field' => 'term_id',
                        'terms' => $cat_list,
                    ),
                ));
            $post = new WP_Query( $args );
            echo paginate_links( array(
                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                'total'        => ceil($post->found_posts/10),
                'current'      => max( 1, get_query_var( 'paged' ) ),
                'format'       => '?paged=%#%',
                'show_all'     => false,
                'type'         => 'plain',
                'end_size'     => 2,
                'mid_size'     => 1,
                'prev_next'    => true,
                'prev_text'    => '<i class="icon-left-open"></i>',
                'next_text'    => '<i class="icon-right-open"></i>',
                'add_args'     => false,
                'add_fragment' => '',
            ) );
            ?>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_blog_left_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    // Get posts
    $args = array(
        'post_type' => 'custom_blog',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => 3,
        'offset'=>((($paged-1)*8)),
        );
    $blogs = new WP_Query( $args );
    $output = '<div class="blog-section">';
    foreach($blogs->get_posts() as $blog){
        $post_meta = get_post_meta($blog->ID);
        $nick_name = bbp_get_user_nicename($blog->post_author);
        $author_img = get_user_meta( $blog->post_author ,'pcg_custom_gravatar')[0];
        $output .= '<div class="blog-list-wrap">';
            $output .= '<div class="blog-img">';
                $output .= '<div class="image_wrapper">';
                    $output .= '<a href="'.$blog->guid.'">';
                        $img_id = ($blog->ID)?($blog->ID):'778';
                        $output .= get_the_post_thumbnail('778',array('600','400'));
                    $output .= '</a>';
                $output .= '</div>';
            $output .= '</div>';
            // desc ---------------------------------------------------------------------------
            $output .= '<div class="blog-desc-wrapper">';
                $output .= '<div class="blog-desc">';
                    // title -------------------------------------
                    $output .= '<div class="blog-title">';
                        // default ----------------------------
                        $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $blog->guid .'">'. $blog->post_title .'</a></h2>';
                        $output .= '<h5 class="second-title" >'.date('Y.m.d',strtotime($blog->post_date)).'</h5>';
                        $output .= '<div class="author-img"><img src="'.wp_get_attachment_url($author_img).'"><h5 class="blog-author" >'.$nick_name.'</h5></div>';
                    $output .= '</div>';
                    $post_content  = mb_substr( strip_tags($blog->post_content), 0, 200 ) . '...<a class="read-all" href="'.$blog->guid.'">詳全文</a>';
                    $output .= '<div class="post-excerpt">' .$post_content  . '</div>';
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';
    }
    $output .= '</div>';
    echo $output;
    ?>
    <div class="pagination">
        <?php
        $args = array(
            'post_type' => 'custom_blog',
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => -1,
        );
        $portfolios = new WP_Query( $args );
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => ceil($portfolios->found_posts/8),
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => '<i class="icon-left-open"></i>',
            'next_text'    => '<i class="icon-right-open"></i>',
            'add_args'     => false,
            'add_fragment' => '',
        ) );
        ?>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_blog_right_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    // Get posts
    $args = array(
        'post_type' => 'custom_blog',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => 5,
        'offset'=>((($paged-1)*8)+3),
        );
    $blogs = new WP_Query( $args );
    $i = 1;
    $output = '<div class="blog-list-section">';
    foreach($blogs->get_posts() as $blog){
        $post_meta = get_post_meta($blog->ID);
        $nick_name = bbp_get_user_nicename($blog->post_author);
        $author_img = get_user_meta( $blog->post_author ,'pcg_custom_gravatar')[0];
        $output .= '<div class="blog-list-wrap">';
        if($i == 1){
            $output .= '<div class="blog-list-img">';
                $output .= '<div class="image_wrapper">';
                    $output .= '<a href="'.$blog->guid.'">';
                        $output .= get_the_post_thumbnail($blog->ID,array('600','400'));
                    $output .= '</a>';
                $output .= '</div>';
            $output .= '</div>';
        }
            // desc ---------------------------------------------------------------------------
            $output .= '<div class="blog-list-desc-wrapper">';
                $output .= '<div class="blog-list-desc">';
                // title -------------------------------------
                    $output .= '<div class="blog-list-title">';
                        // default ----------------------------
                        $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $blog->guid .'">'. $blog->post_title .'</a></h2>';
                        $output .= '<h5 class="second-title" >'.date('Y.m.d',strtotime($blog->post_date)).'</h5>';
                        $output .= '<div class="author-img"><img src="'.wp_get_attachment_url($author_img).'"><h5 class="blog-author" >'.$nick_name.'</h5></div>';
                    $output .= '</div>';
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';
        $i++;
    }
    $output .= '</div>';
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_newset_protfoilo_shortcode_function($atts){
    ob_start();
    extract(shortcode_atts(array("cat" => '57'), $atts));
    $portfolio_author = $_GET['portfolio_author'];
    if($portfolio_author =='' ||$portfolio_author =='all'){
        $myvariable = ob_get_clean();
        return $myvariable;
    }
    $output = '<div class="newest-three-post-area">';
    $output .= '<div class="title-bar"><h1>推薦閱讀</h1></div>';
    $output .= '<div class="newest-three-post small-list">';
    $portfolio_types = array($cat);

    $tax_args = array(
        'taxonomy' => 'portfolio_author',
        'hide_empty' => false,
        'meta_query' => array(
            array(
                'key'       => 'author_belong_class',
                'value'     => $cat,
                'compare'   => '='
            )
        ),
        'parent' => 0,
    );
    $catsArray = array();
    $author_belong_class = get_terms($tax_args);
    foreach($author_belong_class as $author_belong){
        $author_array = get_term_children($author_belong->term_id,'portfolio_author');
        $catsArray = array_merge($catsArray,$author_array);
    }

    $args = array(
        'post_type' => 'portfolio',
        'post_status'   => 'publish',
        'orderby' => 'date',
        'order' => 'DESC',
//        'orderby' => 'rand',
        'posts_per_page' => 3,
        'tax_query' => array(
            'relation' => 'AND',
            array(
                'taxonomy' => 'portfolio_author',
                'field' => 'term_id',
                'terms' => $catsArray,
            ),
//            array(
//                'taxonomy' => 'portfolio_author',
//                'field' => 'term_id',
//                'terms' => array($portfolio_author),
//            ),
            array(
                'taxonomy' => 'portfolio-types',
                'field' => 'term_id',
                'terms' => $portfolio_types,
            ),
        ));
    $portfoliios = new WP_Query($args);
    foreach($portfoliios->get_posts() as $portfoliio){
        $post_meta = get_post_meta($portfoliio->ID);
        $post_author = get_the_terms( $portfoliio->ID, 'portfolio_author' )[0];
        $author_class = get_term_by( 'id',$post_author->parent, 'portfolio_author' );
        $output .= '<div class="post-list-wrap">';
        $output .= '<div class="post-events-img">';
        $output .= '<div class="image_wrapper">';
        $output .= '<a href="'.$portfoliio->guid.'">';
        $output .= get_the_post_thumbnail($portfoliio->ID,'thumbnail');
        $output .= '</a>';
        $output .= '</div>';
        $output .= '</div>';
        // desc ---------------------------------------------------------------------------
        $output .= '<div class="post-event-desc-wrapper">';
        $output .= '<div class="post-event-desc">';
        // title -------------------------------------
        $output .= '<div class="post-title">';
        // default ----------------------------
        $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $portfoliio->guid .'">'. $portfoliio->post_title .'</a></h2>';
        $output .= '<div class="button-icon">';
        $output .= '<div class="button-favorites">';//收藏按鈕
        $output .= get_favorites_button($portfoliio->ID);
        $output .= '</div>';
        $output .= '<div class="button-love">';//按讚按鈕
        $output .= '<span class="icons-wrapper">';
        $output .= '<img class="thumb-up-icon" src="'.wp_get_attachment_url('566').'">';
        $output .= '</span>';
        $output .= '<span class="label">'.$post_meta['mfn-post-love'][0].'</span>';
        $output .= '</div>';
        $output .= '<div class="button-view-count">';//觀看次數
        $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($portfoliio->ID));
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '<h5 class="second-title" >'.date('Y.m.d',strtotime($portfoliio->post_date)).' / '. $author_class->name .'/'.$post_author->name .'</h5>';
        if(has_excerpt($portfoliio->ID)) {
            $post_excerpt = mb_substr( $portfoliio->post_excerpt, 0, 40 ) . '...<a class="read-all" href="'.$portfoliio->guid.'">詳全文</a>';
            $output .= '<div class="post-excerpt">' .$post_excerpt . '</div>';
        }
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
    }
    $output .= '</div>';
    $output .= '</div>';
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}
?>