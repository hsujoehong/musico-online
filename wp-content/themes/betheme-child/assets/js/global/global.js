(function ($) {
    $(document).ready(function () {
        if ($("#index-header-banner-div").size() > 0 && $('#index-header-banner-div').html() == '') { // slider 搬至header 之上
            var header_html = $('#ad-section').html();
            $('#index-header-banner-div').html(header_html);
            $('#ad-section').remove();
        }

        $(window).scroll(function () { // 下滑隱藏上方slider 並多出 ▼ btn
            if ($(document).scrollTop() >= 300) {
                $('#index-header-banner-div').hide();
                $('.index-header-banner-icon').show();
            }
        });
        $('#header-banner-icon').click(function () { //點擊 ▼ btn 後的動作
            if ($('#index-header-banner-div')[0].style.display == 'none') { // slider若隱藏則顯示
                $('#index-header-banner-div').show();
                $('.index-header-banner-icon').hide();
            }
        });
        var meddle_num = $('#meddle').attr('data-meddle-num'); // 活動列表中間段數量(分三段)
        if (meddle_num != '5') { //若中間段不滿5則 去掉一則廣告
            $('.last-ad').remove();
        }

        $('.read-more-btn').click(function () { //閱讀更多
            $('.more-content').show();
            $('.read-more-btn').remove();

        });
    });

    $('.datepicker-input-start').datepicker({ // 搜活動時間選擇器-開始時間
        language: 'en',
        position : "bottom left",

        onSelect: function (select_date) {
            $('.datepicker-input-end').datepicker({ //若開始時間選取，結束時間的最小時間限制
                language: 'en',
                minDate: new Date(select_date),
                position : "bottom right",
            });
        }
    });
    $('.datepicker-input-end').datepicker({// 搜活動時間選擇器-結束時間
        language: 'en',
        position : "bottom right",
        onSelect: function (select_date) {
            $('.datepicker-input-start').datepicker({//若結束時間選取，開始時間的最大時間限制
                language: 'en',
                position : "bottom left",
                maxDate: new Date(select_date),
            });
        }
    });

    $('.favorited-cancel').click(function () { // 會員中心-取消收藏
        $.post('../../wp-admin/admin-ajax.php', {
            action: 'favorited_cancel_action', // 自取一個action的名稱
            user_id: $(this).attr('data-user'),
            post_id: $(this).attr('data-id'),
            db_type: $(this).attr('data-type'),
        }, function (data) {
            location.reload();
        });
    })
    $('#user-gravatar-image').click(function () { //使用者大頭貼
        media_uploader = wp.media({
            frame: "select",
            multiple: false,
            title: '上傳大頭貼',
            library: {
                type: 'image' // limits the frame to show only images
            },
            button: {
                text: '選取圖片'
            }
        });
        media_uploader.open();
        media_uploader.on("select", function () {
            var selectedImages = media_uploader.state().get('selection').first().toJSON();
            $.post('../../wp-admin/admin-ajax.php', {
                action: 'change_user_image_action', // 自取一個action的名稱
                user_id: $('#user-gravatar-image').attr('data-user'),
                img_id: selectedImages.id,
            }, function (data) {
                location.reload();
            });
        });
    });
    $('#favorite-select').change(function () { //會員中心-收藏分類選項
        if ($('select[name="cats"]').val() == 'all') {
            window.location = '/my-account/favorite/';
        } else {
            window.location = '?cats=' + $('select[name="cats"]').val();
        }
    })
    $('#other-favorite-select').change(function () { //其他會員中心-收藏分類選項
        if ($('select[name="cats"]').val() == 'all') {
            window.location = '/musician/favorite/';
        } else {
            window.location = '?user_id=' + $('#user-gravatar-image').attr('data-user') + '&cats=' + $('select[name="cats"]').val();
        }
    })

    if($('.author-area').attr('data-col') <6){
        var num_full = $('.author-area').attr('data-col');
    }else{
        var num_full = 6;
    }

    if($('.author-area').attr('data-col') <4){
        var num_1422 = $('.author-area').attr('data-col');
    }else{
        var num_1422 = 4;
    }
    if($('.author-area').attr('data-col') <3){
        var num_960 = $('.author-area').attr('data-col');
    }else{
        var num_960 = 3;
    }

    $('.author-area').slick({ //專欄作者slider
        infinite: true,
        speed: 300,
        slidesToShow: num_full,
        draggable: true,
        nextArrow: '<i class="icon-right-open-big"></i>',
        prevArrow: '<i class="icon-left-open-big"></i>',
        responsive: [{
            breakpoint: 1422,
            settings: {
                slidesToShow: num_1422,
                slidesToScroll: 1
            }
        },
            {
                breakpoint: 960,
                settings: {
                    slidesToShow: num_960,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]

    });


    $('.clean-btn').click(function () {
        $('.keyword').val('');
        $('.datepicker-input').val('');
        $('.search-box-select').val('');
        $('.all_check').prop("checked",false);

    });

})(jQuery)