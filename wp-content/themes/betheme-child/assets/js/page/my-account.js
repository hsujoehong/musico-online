

(function($){
    $('.user-blog-Feature-img').click(function () { //網誌文章封面圖
        media_uploader = wp.media({
            frame:    "select",
            multiple: false,
            title: '上傳封面圖',
            library: {
                type: 'image' // limits the frame to show only images
            },
            button: {
                text : '選取圖片'
            }
        });
        media_uploader.open();
        media_uploader.on("select", function(){
            var selectedImages = media_uploader.state().get( 'selection' ).first().toJSON();
            $('#feature_img_input').val(selectedImages.id);
            $('#feature_img_src').attr('src',selectedImages.url);
        });
    });

    $('#submit-blog').click(function () { //送出網誌內容
        if($('#feature_img_input').val()==''){ //判斷封面圖是否選取
            alert('請選擇封面圖片!');
            $('#user-blog-Feature-img').focus();
            return false;
        }
        if($('#title').val()==''){ //判斷網誌標題
            alert('請輸入網誌標題!');
            $('#title').focus();
            return false;
        }
        var content = document.getElementById('blog_content_ifr').contentWindow.document.getElementById('tinymce').innerHTML;

        if(content==''){ //判斷網誌內容
            alert('請輸入網誌內容!');
            $('#blog_content').focus();
            return false;
        }
        $.post('../../wp-admin/admin-ajax.php' , { //送出網誌資料至ajax 進行儲存
            action: 'user_blog_update_action', // 自取一個action的名稱
            blog_action: $('#blog_action').val(),
            post_id: $('#blog_action').attr('data-post'),
            user_id: $('#user-gravatar-image').attr('data-user'),
            feature_img_id: $('#feature_img_input').val(),
            title: $('#title').val(),
            content: content,
        }, function(data) {
            if(data == '1'){
                window.location='/my-account/user-blog/';
                alert('網誌發布成功!');
            }else if(data == '2'){
                window.location='/my-account/user-blog/';
                alert('網誌修改成功!');
            }else{
                alert('網誌發布/修改失敗!');
                return false;
            }
        });

    });

    $('.item-display-select').change(function () { //隱私設定
        $.post('../../wp-admin/admin-ajax.php' , {
            action: 'user_privacy_action', // 自取一個action的名稱
            user_id: $('#user-gravatar-image').attr('data-user'),
            privacy_type: $(this).attr('data-type'),
            privacy_value: $(this).val(),
        }, function(data) {

        });
    });


})(jQuery)