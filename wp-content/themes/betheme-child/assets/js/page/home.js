jQuery(function ($) {
    $('.set-slick .mcb-wrap-inner').slick({
        // dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 6,
        draggable: true,
        autoplay: true,
        nextArrow: '<i class="icon-right-open-big"></i>',
        prevArrow: '<i class="icon-left-open-big"></i>',
        responsive: [{
                breakpoint: 2000,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 1422,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 960,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    });
    $(".set-slick .column").addClass('slider-item').removeClass('column');

});