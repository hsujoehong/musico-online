<?php
/**
 * The search template file.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */

get_header();

$translate['search-title'] = mfn_opts_get('translate') ? mfn_opts_get('translate-search-title','Ooops...') : __('Ooops...','betheme');
$translate['search-subtitle'] = mfn_opts_get('translate') ? mfn_opts_get('translate-search-subtitle','No results found for:') : __('No results found for:','betheme');

$translate['published'] 	= mfn_opts_get('translate') ? mfn_opts_get('translate-published','Published by') : __('Published by','betheme');
$translate['at'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-at','at') : __('at','betheme');
$translate['readmore'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-readmore','Read more') : __('Read more','betheme');
?>

    <div id="Content">
        <div class="content_wrapper clearfix">


            <!-- .sections_group -->
            <div class="sections_group">

                <div class="section">
                    <div class="section_wrapper clearfix">

                        <?php if( have_posts() && trim( $_GET['s'] ) ): ?>

                            <div class="column one column_blog">
                                <div class="blog_wrapper isotope_wrapper">

                                    <div class="posts_group classic ">
                                        <div class="small-list">
                                        <?php
                                        while ( have_posts() ):
                                            the_post();
                                            if(get_post_type() != 'product'):
                                                $post_author = get_the_terms( get_the_ID(), 'portfolio_author' )[0];
                                            ?>
                                                <div class="post-list-wrap">
                                                    <div class="post-events-img">
                                                        <div class="image_wrapper">
                                                            <a href="<?=get_the_guid()?>">
                                                                <?=get_the_post_thumbnail(get_the_ID(),'thumbnail');?>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="post-event-desc-wrapper">
                                                        <div class="post-event-desc">
                                                            <div class="post-title">
                                                                <h2 class="entry-title" itemprop="headline"><a href="<?=get_the_guid()?>"> <?=get_the_title();?></a></h2>
                                                                <div class="button-icon">
                                                                    <div class="button-favorites"><!--收藏按鈕-->
                                                                        <?=get_favorites_button(get_the_ID());?>
                                                                    </div>
                                                                    <div class="button-love"><!--按讚按鈕-->
                                                                        <?=mfn_love(get_the_ID());?>
                                                                    </div>
                                                                    <div class="button-view-count"><!--觀看次數-->
                                                                        <img class="heart-empty-icon" src="<?=wp_get_attachment_url('565')?>"> <?=number_format(getPostViews(get_the_ID()));?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <h5 class="second-title" ><?=date('Y.m.d',strtotime(get_the_date(get_the_ID())))?></h5>
                                                            <?php
                                                            if(has_excerpt(get_the_ID())):
                                                                $post_excerpt = mb_substr( get_the_excerpt(), 0, 40 ) . '...<a class="read-all" href="'.get_the_guid().'">詳全文</a>';
                                                            ?>
                                                            <div class="post-excerpt"><?=$post_excerpt?></div>
                                                            <?php endif;?>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php
                                            endif;
                                        endwhile;
                                        ?>
                                        </div>

                                    </div>
                                    <?php
                                        mfn_builder_print( '888', true );
                                    ?>
                                    <?php
                                    // pagination
                                    if(function_exists( 'mfn_pagination' )):
                                        echo mfn_pagination();
                                    else:
                                        ?>
                                        <div class="nav-next"><?php next_posts_link(__('&larr; Older Entries', 'betheme')) ?></div>
                                        <div class="nav-previous"><?php previous_posts_link(__('Newer Entries &rarr;', 'betheme')) ?></div>
                                    <?php
                                    endif;
                                    ?>

                                </div>
                            </div>

                        <?php else: ?>

                            <div class="column one search-not-found">

                                <div class="snf-pic">
                                    <i class="themecolor <?php mfn_opts_show( 'error404-icon', 'icon-traffic-cone' ); ?>"></i>
                                </div>

                                <div class="snf-desc">
                                    <h2><?php echo $translate['search-title']; ?></h2>
                                    <h4><?php echo $translate['search-subtitle'] .' '. esc_html( $_GET['s'] ); ?></h4>
                                </div>

                            </div>

                        <?php endif; ?>

                    </div>
                </div>

            </div>


            <!-- .four-columns - sidebar -->
            <?php if( is_active_sidebar( 'mfn-search' ) ):  ?>
                <div class="sidebar four columns">
                    <div class="widget-area clearfix <?php mfn_opts_show( 'sidebar-lines' ); ?>">
                        <?php dynamic_sidebar( 'mfn-search' ); ?>
                    </div>
                </div>
            <?php endif; ?>


        </div>
    </div>

<?php get_footer();

// Omit Closing PHP Tags