<?php
function musico_search_list_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $post_type_list = array();//文章類型
    $author_list = array();//cat->portfolio_author
    $keyword = $_POST['keyword'];//關鍵字
    if(isset($_POST['post-keyword'])){
        $keyword_array = $_POST['post-keyword'];//勾選關鍵字 (關鍵字陣列)
    }else{
        $keyword_array = array();
    }
    foreach($keyword_array as $keyword_check){//三個關鍵字勾選保留
        if($keyword_check == '親子'){
            $postkeyword1_check= 'checked';
        }
        if($keyword_check == '公益'){
            $postkeyword2_check= 'checked';
        }
        if($keyword_check == '免費'){
            $postkeyword3_check= 'checked';
        }
    }
    if($keyword != ''){
        array_push($keyword_array , $keyword); //將手動輸入關鍵字放入陣列中
    }
    $andSQL = '';
    $keywordSQL = '';
    $post_cat_SQL = '';
    if(isset($_POST['portfolio-cat'])){//是否勾選專欄條件
        $portfolio_check = 'checked';//checkbox保留勾選
        array_push($post_type_list,'portfolio');//存入post-type 搜尋條件
        $portfolio_cats_list = array();//專欄分類array
        $terms = get_terms(array('taxonomy' => 'portfolio-types','hide_empty' => false,));//專欄的分類
        foreach ($terms as $term){
            array_push($portfolio_cats_list,$term->term_id);//分類ID存入專欄分類array
        }
    }
    if(isset($_POST['events-cat'])){//是否勾選活動條件
        $events_check = 'checked';//checkbox保留勾選
        array_push($post_type_list,'events');//存入post-type 搜尋條件
        $events_cats_list = array();// 活動分類array
        $terms = get_terms(array('taxonomy' => 'events_category','hide_empty' => false,));//活動的分類
        foreach ($terms as $term){
            array_push($events_cats_list,$term->term_id);//分類ID存入活動分類array
        }
    }
    if(isset($_POST['post-cat'])){//是否勾選文章條件
        array_push($post_type_list,'post');//存入 post-type 搜尋條件
        $post_cats_list = array();//文章分類array
        foreach($_POST['post-cat'] as $post_cat){
            ${'post'.$post_cat.'_check'} = 'checked';//checkbox 保留勾選
            if($post_cat != '92'){//判斷勾選不是會員獨享(子分類)
                array_push($post_cats_list,$post_cat);//分類ID存入文章分類array
                $terms = get_terms(array('taxonomy' => 'category','hide_empty' => false,'parent' => $post_cat,));//勾選的post分類下 子分類
                foreach ($terms as $term){
                    array_push($post_cats_list,$term->term_id);//分類ID存入文章分類array
                }
            }else{
                array_push($post_cats_list,'92');//勾選會員獨享則將會員獨享加入分類array
            }
        }
    }
    $post_cats_list = array_unique($post_cats_list);
    if(!empty($post_cats_list)||!empty($portfolio_cats_list)||!empty($events_cats_list)){//有勾選文章分類，將分類array加入SQL搜尋條件
        $post_cat_str = '';
        foreach ($post_cats_list as $search_post_cat){
            $post_cat_str .= "'".$search_post_cat."'".',';
        }
        foreach ($portfolio_cats_list as $search_post_cat){
            $post_cat_str .= "'".$search_post_cat."'".',';
        }
        foreach ($events_cats_list as $search_post_cat){
            $post_cat_str .= "'".$search_post_cat."'".',';
        }
        $post_cat_SQL = "AND wp_term_relationships.term_taxonomy_id IN (".rtrim($post_cat_str,',').")" ;
    }
    if(!isset($_POST['portfolio-cat'])&&!isset($_POST['post-cat'])&&!isset($_POST['events-cat'])){
        $post_type_list = array('portfolio','post','events'); // 沒選擇分類則post type 是全類別
    }
    $extend_post_type= '';
    $termSQL =  '';
    $author_sql ='';
    $events_term_sql ='';

    foreach ($post_type_list as $search_post_type){
        $extend_post_type .= "'".$search_post_type."'".',';
    }
    $extend_post_type = rtrim($extend_post_type,',');
    if(!empty($keyword_array) ){//關鍵字陣列
        if(in_array('portfolio',$post_type_list)){//如果搜尋條件有專欄
            $termSQL .=  'LEFT JOIN wp_term_relationships AS author_term ON (wp_posts.ID = author_term.object_id)';//活動要的term 資料庫加入SQL
            foreach ($keyword_array as $keyword_each){//撈取符合關鍵字的分類
                $terms_author_d = get_terms( array('taxonomy' => 'portfolio_author','hide_empty' => false,'description__like' => $keyword_each));//cat 簡介 LIKE 關鍵字
                $terms_author_n = get_terms( array('taxonomy' => 'portfolio_author','hide_empty' => false,'name__like' => $keyword_each));//cat name LIKE 關鍵字
                foreach ($terms_author_d as $author){//符合關鍵字之簡介的作者ID存入陣列
                    if($author->parent != '0'){
                        array_push($author_list,$author->term_id);
                    }else{//撈到父級分類
                        $terms_author_d_chlid = get_terms( 'portfolio_author', array('hide_empty' => 0,'parent' => $author->term_id));
                        foreach ($terms_author_d_chlid as $author_chlid){//父級分類下所有子分類
                            array_push($author_list,$author_chlid->term_id);
                        }
                    }
                }
                foreach ($terms_author_n as $author){//符合關鍵字之name的作者ID存入陣列
                    if($author->parent != '0'){
                        array_push($author_list,$author->term_id);
                    }else{//撈到父級分類
                        $terms_author_n_chlid = get_terms( 'portfolio_author', array('hide_empty' => 0,'parent' => $author->term_id));
                        foreach ($terms_author_n_chlid as $author_chlid){//父級分類下所有子分類
                            array_push($author_list,$author_chlid->term_id);
                        }
                    }
                }
            }
            $author_list = array_unique($author_list);//清除重複ID
            $search_term = implode(',',$author_list);// 作者分類array 轉 字串
            if($search_term !=''){
                $author_sql .= 'OR(author_term.term_taxonomy_id IN ('.$search_term.'))'; //作者分類搜尋SQL
            }
        }
        if(in_array('events',$post_type_list)){//搜尋條件有活動
            foreach ($keyword_array as $keyword_each){//關鍵字搜尋活動自訂內容
                $keywordSQL .= "( wp_postmeta.meta_key = '_city' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_area' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_address' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_class' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_person' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_nature' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_host' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_actor' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR";
            }
        }
        foreach ($keyword_array as $keyword_each){//keyword 搜尋 要搜尋分類的 標題 簡介 內文 mfn 小工具中 符合keyword
            $keywordSQL .= "(post_title like '%".$keyword_each."%') OR 
            (post_content like '%".$keyword_each."%') OR 
            (post_excerpt like '%".$keyword_each."%') OR 
            ( wp_postmeta.meta_key = 'mfn-page-items' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR";
        }
        $keywordSQL = rtrim($keywordSQL,'OR'); //清除最後一個OR 符號 (避免錯誤)
    }

    if($keywordSQL !='' || $author_sql !=''||$events_term_sql!=''){
        $andSQL = 'AND ('.$keywordSQL.$author_sql.$events_term_sql.')';//將有的自訂SQL 加入搜尋SQL
    }
    $strSQL = "SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts
                LEFT JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id)
                ".$termSQL."
                INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id )
                WHERE 1=1  AND (wp_posts.post_type IN (".$extend_post_type.")) ".$post_cat_SQL." AND(wp_posts.post_status = 'publish') 
                ".$andSQL."GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT ".(($paged-1)*10).", 10" ;
    global $wpdb;
    $found_posts = $wpdb->get_results( $strSQL ,ARRAY_N );

    $output = '';
    $output .= '<div class="search-custom-area">';
    $output .='<form method="post" class="searchform" id="all-searchform" action="'.esc_url( home_url( '/all-search/' ) ).'">';
    $output .= '<div class="search-all-form-title">';
        $output .= '<div class="search-keyword-area">';
            $output .= '<input type="text" name="keyword" id="keyword" value="'.$_POST['keyword'].'"><i class="icon_search icon-search-fine"></i>';
        $output .= '</div>';
        $output .= '<div class="search-clean-area">';
            $output .= '<button type="button" class="clean-btn">清除</button>';
        $output .= '</div>';
    $output .= '</div>';
    $output .= '<div class="search-checkbox-area">';
    $output .= '<div class="search-checkbox-list-div">';
    $output .= '<label class="search-box-container">音樂活動<input type="checkbox" class="all_check" name="events-cat" id="events" value="events" '.$events_check.'><span class="checkmark '.$events_check.'"></span></label>';
    $output .= '<label class="search-box-container">音樂專欄<input type="checkbox" class="all_check" name="portfolio-cat" id="portfolio" value="portfolio" '.$portfolio_check.'><span class="checkmark '.$portfolio_check.'"></span></label>';
    $output .= '<label class="search-box-container">音樂新聞<input type="checkbox" class="all_check" name="post-cat[]" id="post-parent1" value="36" '.$post36_check.'><span class="checkmark '.$post36_check.'"></span></label>';
    $output .= '<label class="search-box-container">資源媒合<input type="checkbox" class="all_check" name="post-cat[]" id="post-parent2" value="37" '.$post37_check.'><span class="checkmark '.$post37_check.'"></span></label>';
    $output .= '<label class="search-box-container">好康分享<input type="checkbox" class="all_check" name="post-cat[]" id="post-parent3" value="38" '.$post38_check.'><span class="checkmark '.$post38_check.'"></span></label>';
    $output .= '<label class="search-box-container">會員專屬<input type="checkbox" class="all_check" name="post-cat[]" id="post-child" value="92" '.$post92_check.'><span class="checkmark '.$post92_check.'"></span></label>';
    $output .= '<label class="search-box-container">親子<input type="checkbox" class="all_check" name="post-keyword[]" id="post-keyword1" value="親子" '.$postkeyword1_check.'><span class="checkmark '.$postkeyword1_check.'"></span></label>';
    $output .= '<label class="search-box-container">公益<input type="checkbox" class="all_check" name="post-keyword[]" id="post-keyword2" value="公益" '.$postkeyword2_check.'><span class="checkmark '.$postkeyword2_check.'"></span></label>';
    $output .= '<label class="search-box-container">免費<input type="checkbox" class="all_check" name="post-keyword[]" id="post-keyword3" value="免費" '.$postkeyword3_check.'><span class="checkmark '.$postkeyword3_check.'"></span></label>';
    $output .= '</div>';
    $output .= '<div class="search-btn-list-diva">';
    $output .= '<button type="submit" id="sub-btn">重新搜尋</button>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</form>';
    $output .= '</div>';
    $output .= '<div class="post-list-section-custom-area">';
    if(count($found_posts) ==0){
        $output .= '<div class="column one search-not-found">';
        $output .= '<div class="snf-pic">';
        $output .= '<i class="themecolor icon-traffic-cone"></i>';
        $output .= '</div>';
        $output .= '<div class="snf-desc">';
        $output .= '<h2>Ooops...</h2>';
        $output .= '<h4>目前沒有該類別資料</h4>';
        $output .= '</div>';
        $output .= '</div>';
    }else{
        foreach($found_posts as $found_post){
            $posts = get_post($found_post[0]);
            $post_meta = get_post_meta($posts->ID);
            $category_name = '';
            if(get_post_type($posts->ID) == 'events'){
                $taxonmy = 'events_category';
                $category_name = '音樂活動';
            }else if(get_post_type($posts->ID) == 'portfolio'){
                $taxonmy = 'portfolio-types';
                $category_name = '音樂專欄';
            }else if(get_post_type($posts->ID) == 'post'){
                $taxonmy = 'category';
            }
            $post_category = get_the_terms( $posts->ID, $taxonmy );
            $wpseo_primary_term = new WPSEO_Primary_Term( $taxonmy, $posts->ID );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $Primary_term = get_term( $wpseo_primary_term );
            if(get_post_type($posts->ID) == 'post'){
                if($Primary_term->parent != '0'){
                    $category_name = get_term($Primary_term->parent)->name;
                }else{
                    $category_name = '';
                }
            }
            if($Primary_term->name){
                if($category_name !=''){
                    $category_name .= '/'.$Primary_term->name;
                }else{
                    $category_name .= $Primary_term->name;
                }
            }else{
                if($category_name !=''){
                    $category_name .= '/'.$post_category[0]->name;
                }else{
                    if($post_category[0]->parent != '0'){
                        $category_name = get_term($post_category[0]->parent)->name . '/'. $post_category[0]->name;
                    }else{
                        $category_name = $post_category[0]->name;
                    }
                }
            }
            $output .= '<div class="post-list-wrap">';
            $output .= '<div class="post-events-img">';
            $output .= '<div class="image_wrapper">';
            $output .= '<a href="'.$posts->guid.'">';
            $output .= get_the_post_thumbnail($posts->ID,'thumbnail');
            $output .= '</a>';
            $output .= '</div>';
            $output .= '</div>';
            // desc ---------------------------------------------------------------------------
            $output .= '<div class="post-event-desc-wrapper">';
            $output .= '<div class="post-event-desc">';
            // title -------------------------------------
            $output .= '<div class="post-title">';
            // default ----------------------------
            $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $posts->guid .'">'. $posts->post_title .'</a></h2>';
            $output .= '<div class="button-icon">';
            $output .= '<div class="button-favorites">';//收藏按鈕
            $output .= get_favorites_button($posts->ID);
            $output .= '</div>';
            $output .= '<div class="button-love">';//按讚按鈕
            $output .= '<span class="icons-wrapper">';
            $output .= '<img class="thumb-up-icon" src="'.wp_get_attachment_url('566').'">';
            $output .= '</span>';
            $output .= '<span class="label">'.$post_meta['mfn-post-love'][0].'</span>';
            $output .= '</div>';
            $output .= '<div class="button-view-count">';//觀看次數
            $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($posts->ID));
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '<h5 class="second-title" >'.date('Y.m.d',strtotime($posts->post_date)).'/'.$category_name.'</h5>';
            if(has_excerpt($posts->ID)) {
                $post_excerpt = mb_substr( $posts->post_excerpt, 0, 83 ) . '...<a class="read-all" href="'.$posts->guid.'">詳全文</a>';
                $output .= '<div class="post-excerpt">' .$post_excerpt . '</div>';
            }
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
        }
    }
    $output .= '</div>';
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_search_paginate_shortcode_function($atts){
    ob_start();
    $post_type_list = array();//文章類型
    $author_list = array();//cat->portfolio_author
    $keyword = $_POST['keyword'];//關鍵字
    if(isset($_POST['post-keyword'])){
        $keyword_array = $_POST['post-keyword'];//勾選關鍵字 (關鍵字陣列)
    }else{
        $keyword_array = array();
    }
    if($keyword != ''){
        array_push($keyword_array , $keyword); //將手動輸入關鍵字放入陣列中
    }
    $andSQL = '';
    $keywordSQL = '';
    if(isset($_POST['portfolio-cat'])){//是否勾選專欄條件
        array_push($post_type_list,'portfolio');//存入post-type 搜尋條件
        $portfolio_cats_list = array();//專欄分類array
        $terms = get_terms(array('taxonomy' => 'portfolio-types','hide_empty' => false,));//專欄的分類
        foreach ($terms as $term){
            array_push($portfolio_cats_list,$term->term_id);//分類ID存入專欄分類array
        }
    }
    if(isset($_POST['events-cat'])){//是否勾選活動條件
        array_push($post_type_list,'events');//存入post-type 搜尋條件
        $events_cats_list = array();// 活動分類array
        $terms = get_terms(array('taxonomy' => 'events_category','hide_empty' => false,));//活動的分類
        foreach ($terms as $term){
            array_push($events_cats_list,$term->term_id);//分類ID存入活動分類array
        }
    }
    if(isset($_POST['post-cat'])){//是否勾選文章條件
        array_push($post_type_list,'post');//存入 post-type 搜尋條件
        $post_cats_list = array();//文章分類array
        foreach($_POST['post-cat'] as $post_cat){
            if($post_cat != '92'){//判斷勾選不是會員獨享(子分類)
                $terms = get_terms(array('taxonomy' => 'category','hide_empty' => false,'parent' => $post_cat,));//勾選的post分類下 子分類
                foreach ($terms as $term){
                    array_push($post_cats_list,$term->term_id);//分類ID存入文章分類array
                }
            }else{
                array_push($post_cats_list,'92');//勾選會員獨享則將會員獨享加入分類array
            }
        }
    }
    if(!empty($post_cats_list)||!empty($portfolio_cats_list)||!empty($events_cats_list)){
        $post_cat_str = '';
        foreach ($post_cats_list as $search_post_cat){
            $post_cat_str .= "'".$search_post_cat."'".',';
        }
        foreach ($portfolio_cats_list as $search_post_cat){
            $post_cat_str .= "'".$search_post_cat."'".',';
        }
        foreach ($events_cats_list as $search_post_cat){
            $post_cat_str .= "'".$search_post_cat."'".',';
        }
        $post_cat = "wp_term_relationships.term_taxonomy_id IN (".rtrim($post_cat_str,',').")" ;
    }else{
        $post_cat = "1=1";
    }
    if(!isset($_POST['portfolio-cat'])&&!isset($_POST['post-cat'])&&!isset($_POST['events-cat'])){
        $post_type_list = array('portfolio','post','events');
    }
    $extend_post_type= '';
    $termSQL =  '';
    $author_sql ='';
    $events_term_sql ='';
    foreach ($post_type_list as $search_post_type){
        $extend_post_type .= "'".$search_post_type."'".',';
    }
    $extend_post_type = rtrim($extend_post_type,',');
    if(!empty($keyword_array) ){
        if(in_array('portfolio',$post_type_list)){
            $search_term = '';
            $termSQL .=  'LEFT JOIN wp_term_relationships AS author_term ON (wp_posts.ID = author_term.object_id)';
            foreach ($keyword_array as $keyword_each){
                $terms_author_d = get_terms( array('taxonomy' => 'portfolio_author','hide_empty' => false,'description__like' => $keyword_each));//cat 簡介 LIKE 關鍵字
                $terms_author_n = get_terms( array('taxonomy' => 'portfolio_author','hide_empty' => false,'name__like' => $keyword_each));//cat name LIKE 關鍵字
                foreach ($terms_author_d as $author){//符合關鍵字之簡介的作者ID存入陣列
                    if($author->parent != '0'){
                        array_push($author_list,$author->term_id);
                    }else{//撈到父級分類
                        $terms_author_d_chlid = get_terms( 'portfolio_author', array('hide_empty' => 0,'parent' => $author->term_id));
                        foreach ($terms_author_d_chlid as $author_chlid){//父級分類下所有子分類
                            array_push($author_list,$author_chlid->term_id);
                        }
                    }
                }
                foreach ($terms_author_n as $author){//符合關鍵字之name的作者ID存入陣列
                    if($author->parent != '0'){
                        array_push($author_list,$author->term_id);
                    }else{//撈到父級分類
                        $terms_author_n_chlid = get_terms( 'portfolio_author', array('hide_empty' => 0,'parent' => $author->term_id));
                        foreach ($terms_author_n_chlid as $author_chlid){//父級分類下所有子分類
                            array_push($author_list,$author_chlid->term_id);
                        }
                    }
                }
            }
            $author_list = array_unique($author_list);//清除重複ID
            foreach ($author_list as $author_id){
                $search_term .= "'".$author_id."'".',';
            }
            $search_term = rtrim($search_term,',');
            if($search_term !=''){
                $author_sql .= 'OR(author_term.term_taxonomy_id IN ('.$search_term.'))';
            }
        }
        if(in_array('events',$post_type_list)){
            foreach ($keyword_array as $keyword_each){
                $keywordSQL .= "( wp_postmeta.meta_key = '_city' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_area' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_address' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_class' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_person' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_nature' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_host' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR
                ( wp_postmeta.meta_key = '_actor' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR";
            }
        }
        foreach ($keyword_array as $keyword_each){
            $keywordSQL .= "(post_title like '%".$keyword_each."%') OR 
            (post_content like '%".$keyword_each."%') OR 
            (post_excerpt like '%".$keyword_each."%') OR 
            ( wp_postmeta.meta_key = 'mfn-page-items' AND wp_postmeta.meta_value LIKE '%".$keyword_each."%' )OR";
        }

        $keywordSQL = rtrim($keywordSQL,'OR');
    }

    if($keywordSQL !='' || $author_sql !=''||$events_term_sql!=''){
        $andSQL = 'AND ('.$keywordSQL.$author_sql.$events_term_sql.')';
    }
    $strSQL = "SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts
                LEFT JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id)
                ".$termSQL."
                INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id )
                WHERE 1=1 AND (wp_posts.post_type IN (".$extend_post_type."))AND ".$post_cat." AND(wp_posts.post_status = 'publish') 
                ".$andSQL."GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC " ;

    global $wpdb;
    $found_posts = $wpdb->get_results( $strSQL ,ARRAY_N );
    // Get posts
    ?>
    <div class="post-pagination">
        <div class="pagination">
            <?php
            echo paginate_links( array(
                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                'total'        => ceil(count($found_posts)/10),
                'current'      => max( 1, get_query_var( 'paged' ) ),
                'format'       => '?paged=%#%',
                'show_all'     => false,
                'type'         => 'plain',
                'end_size'     => 2,
                'mid_size'     => 1,
                'prev_next'    => true,
                'prev_text'    => '<i class="icon-left-open"></i>',
                'next_text'    => '<i class="icon-right-open"></i>',
                'add_args'     => false,
                'add_fragment' => '',
            ) );
            ?>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}



function musico_events_search_list_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    if(isset($_POST)){
        foreach($_POST as $kk => $vv){
            $$kk = $vv;
        }
    }
    $states = array('基隆市' => '基隆市','台北市' => '台北市','新北市' => '新北市','宜蘭縣' => '宜蘭縣','桃園市' => '桃園市','新竹市' => '新竹市','新竹縣' => '新竹縣',
        '苗栗縣' => '苗栗縣','台中市' => '台中市','彰化縣' => '彰化縣','南投縣' => '南投縣','雲林縣' => '雲林縣','嘉義市' => '嘉義市','嘉義縣' => '嘉義縣','台南市' => '台南市',
        '高雄市' => '高雄市','屏東縣' => '屏東縣','花蓮縣' => '花蓮縣','台東縣' => '台東縣','澎湖縣' => '澎湖縣','金門縣' => '金門縣','連江縣' => '連江縣',
    );
    $post_types = get_terms( array('taxonomy' => 'events_category','hide_empty' => false,) );
    $post_areas = get_terms( array('taxonomy' => 'post_area','hide_empty' => false,) );
    $post_classs = get_terms( array('taxonomy' => 'post_class','hide_empty' => false,) );
    $post_persons = get_terms( array('taxonomy' => 'post_person','hide_empty' => false,) );
    $post_natures = get_terms( array('taxonomy' => 'post_nature','hide_empty' => false,) );

    $keywordSQL = '';
    $termSQL = '';
    $time_SQL = '';
    $term_SQL = '';
    $custom_SQL = '';
    $customSQL = '';
    if($_POST['start_time']!=''){
        $end_SQL = 'INNER JOIN wp_postmeta AS events_end_time ON ( wp_posts.ID = events_end_time.post_id )';
        $time_SQL .= "( events_end_time.meta_key = '_start_time' AND events_end_time.meta_value >= '".strtotime($start_time)."' ) AND";
    }
    if($_POST['end_time']!=''){
        $start_SQL = 'INNER JOIN wp_postmeta AS events_start_time ON ( wp_posts.ID = events_start_time.post_id )';
        $time_SQL .= "( events_start_time.meta_key = '_start_time' AND events_start_time.meta_value <= '".strtotime($end_time)."' ) AND";
    }
    if(isset($_POST['city'])&&$city !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _city ON ( wp_posts.ID = _city.post_id )';
        $customSQL .= "( _city.meta_key = '_city' AND _city.meta_value = '".$city."' ) AND";
    }
    if(isset($_POST['area'])&&$area !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _area ON ( wp_posts.ID = _area.post_id )';
        $customSQL .= "( _area.meta_key = '_area' AND _area.meta_value = '".$area."' ) AND";
    }
    if(isset($_POST['class'])&&$class !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _class ON ( wp_posts.ID = _class.post_id )';
        $customSQL .= "( _class.meta_key = '_class' AND _class.meta_value = '".$class."' ) AND";
    }
    if(isset($_POST['nature'])&&$nature !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _nature ON ( wp_posts.ID = _nature.post_id )';
        $customSQL .= "( _nature.meta_key = '_nature' AND _nature.meta_value = '".$nature."' ) AND";
    }
    if(isset($_POST['person'])&&$person !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _person ON ( wp_posts.ID = _person.post_id )';
        $customSQL .= "( _person.meta_key = '_person' AND _person.meta_value = '".$person."' ) AND";
    }
    if(isset($_POST['cost'])&&$cost !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _cost ON ( wp_posts.ID = _cost.post_id )';
        $customSQL .= "( _cost.meta_key = '_cost' AND _cost.meta_value = '".$cost."' ) AND";
    }
    if($keyword != ''){
        $key_array = array('mfn-page-items','_city','_area','_class','_nature','_person','_cost','_actor','_host','_address' );
        foreach ($key_array as $key){
            $keywordSQL .="( wp_postmeta.meta_key = '".$key."' AND wp_postmeta.meta_value LIKE '%".$keyword."%' )OR ";
        }
        $keywordSQL .= "(wp_posts.post_title LIKE '%".$keyword."%') OR (wp_posts.post_excerpt LIKE '%".$keyword."%') OR (wp_posts.post_content LIKE '%".$keyword."%')";
        $e_cat_array = array();
        $terms_e_cat_d = get_terms( array('taxonomy' => 'events_category','hide_empty' => false,'description__like' => $keyword));//cat 簡介 LIKE 關鍵字
        $terms_e_cat_n = get_terms( array('taxonomy' => 'events_category','hide_empty' => false,'name__like' => $keyword));//cat name LIKE 關鍵字
        foreach ($terms_e_cat_d as $e_cat){//符合關鍵字之簡介的作者ID存入陣列
            array_push($e_cat_array,$e_cat->term_id);
        }
        foreach ($terms_e_cat_n as $e_cat){//符合關鍵字之name的作者ID存入陣列
            array_push($e_cat_array,$e_cat->term_id);
        }
        $e_cat_array = array_unique($e_cat_array);//清除重複ID
        $search_term = implode(',',$e_cat_array);// 作者分類array 轉 字串
    }
    if(isset($_POST['type'])&&$type !=''){
        $type_tax_array= get_term_by('name', $type, 'events_category');
        $term_SQL .=  'LEFT JOIN wp_term_relationships AS events_term ON (wp_posts.ID = events_term.object_id)';//活動要的term 資料庫加入SQL
        $termSQL = "AND events_term.term_taxonomy_id IN (".$type_tax_array->term_id.")" ;
    }else{
        if($search_term !=''){
            $term_SQL .=  'LEFT JOIN wp_term_relationships AS events_term ON (wp_posts.ID = events_term.object_id)';//活動要的term 資料庫加入SQL
            $termSQL = "AND ((  events_term.term_taxonomy_id IN (".$search_term.")" ;
        }
    }

    if($time_SQL  == ''){
        $time_SQL = "1=1";
    }else{
        $time_SQL = rtrim($time_SQL,'AND');
    }
    if($keywordSQL  == ''){
        $keywordSQL = "1=1";
    }else{
        $keywordSQL = rtrim($keywordSQL,'OR');
    }
    if($customSQL  == ''){
        $customSQL = "1=1";
    }else{
        $customSQL = rtrim($customSQL,'AND');
    }
    if($search_term !=''){
        $keywordSQL .= ')';
        $customSQL .= ') )OR';
    }else{
        $customSQL .= ') AND';
    }
    $order_SQL = 'INNER JOIN wp_postmeta AS orderby ON ( wp_posts.ID = orderby.post_id AND orderby.meta_key = "_start_time")
     INNER JOIN wp_postmeta AS ordershow ON ( wp_posts.ID = ordershow.post_id AND ordershow.meta_key = "_end_time"  AND ordershow.meta_value >='.time().' )';
    $strSQL = "SELECT * FROM wp_posts
                INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id )
                ".$order_SQL.$term_SQL.$start_SQL.$end_SQL.$custom_SQL."
                WHERE 1=1 AND wp_posts.post_type = 'events' AND
                ((wp_posts.post_status = 'publish')) 
                ".$termSQL."AND
                (".$time_SQL.")AND
                (".$customSQL."
                (".$keywordSQL.") GROUP BY wp_posts.ID ORDER BY orderby.meta_value  ASC LIMIT ".(($paged-1)*10).", 10";
    global $wpdb;
//    print_r($strSQL);
    $found_posts = $wpdb->get_results( $strSQL ,ARRAY_N );
    $output = '';
    ?>
    <div class="events-search-section">
        <form method="post" id="events-searchform" name="events-searchform" action="<?php echo esc_url( home_url( '/events-search/' ) ); ?>">
            <div>
                <div class="searchform-title-btn-div">
                    <h1 class="search-box-title">搜活動</h1>
                    <div class="clean-btn-div">
                        <button type="button" class="clean-btn">清除</button>
                    </div>
                </div>
                <div class="keyword-div">
                    <input class="keyword" type="text" name="keyword" placeholder="關鍵字" value="<?=$keyword?>" >
                </div>
            </div>
            <div>
                <div class="datepicker-div">
                    <input class="datepicker-input datepicker-input-start" type="text" name="start_time" placeholder="起訖日期設定" value="<?=$start_time?>">
                    <input class="datepicker-input datepicker-input-end" type="text" name="end_time" placeholder="起訖日期設定" value="<?=$end_time?>">
                </div>
            </div>
            <div class="all-select-div">
                <div class="select-div">
                    <select class="search-box-select" name="city">
                        <option value="" disabled selected hidden>活動縣市</option>
                        <?php foreach($states as $state){ ?>
                            <option value="<?=$state?>" <?=($state==$city)?'selected':''?>><?=$state?></option>
                        <?php }?>
                    </select>
                    <select class="search-box-select" name="person">
                        <option value="" >活動對象</option>
                        <?php foreach($post_persons as $post_person){ ?>
                            <option value="<?=$post_person->name?>" <?=($post_person->name==$person)?'selected':''?>><?=$post_person->name?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="select-div">
                    <select class="search-box-select" name="type">
                        <option value="" >音樂類型</option>
                        <?php foreach($post_types as $post_type){ ?>
                            <option value="<?=$post_type->name?>" <?=($post_type->name==$type)?'selected':''?>><?=$post_type->name?></option>
                        <?php }?>
                    </select>
                    <select class="search-box-select" name="class">
                        <option value="" >活動類型</option>
                        <?php foreach($post_classs as $post_class){ ?>
                            <option value="<?=$post_class->name?>" <?=($post_class->name==$class)?'selected':''?>><?=$post_class->name?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="select-div">
                    <select class="search-box-select" name="nature">
                        <option value="" >活動性質</option>
                        <?php foreach($post_natures as $post_nature){ ?>
                            <option value="<?=$post_nature->name?>" <?=($post_nature->name==$nature)?'selected':''?>><?=$post_nature->name?></option>
                        <?php }?>
                    </select>
                    <select class="search-box-select" name="cost">
                        <option value="" >活動門票</option>
                        <option value="付費" <?=($cost=='付費')?'selected':''?>>售票</option>
                        <option value="免費" <?=($cost=='免費')?'selected':''?>>免費</option>
                    </select>
                </div>
                <div class="select-div-one">
                    <select class="search-box-select" name="area">
                        <option value="" >活動場館</option>
                        <?php foreach($post_areas as $post_area){ ?>
                            <option value="<?=$post_area->name?>" <?=($post_area->name==$area)?'selected':''?>><?=$post_area->name?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <div>
                <button type="submit" class="search-btn">搜活動</button>
            </div>
        </form>
    </div>
    <?php
    $output .= '<div class="post-list-section-custom-area">';
    if(count($found_posts) ==0){
        $output .= '<div class="column one search-not-found">';
        $output .= '<div class="snf-pic">';
        $output .= '<i class="themecolor icon-traffic-cone"></i>';
        $output .= '</div>';
        $output .= '<div class="snf-desc">';
        $output .= '<h2>Ooops...</h2>';
        $output .= '<h4>目前沒有該類別資料</h4>';
        $output .= '</div>';
        $output .= '</div>';
    }else{
        foreach($found_posts as $found_post){
            $posts = get_post($found_post[0]);
            $post_meta = get_post_meta($posts->ID);
            $category_name = '音樂活動';
            $post_category = get_the_terms( $posts->ID, 'events_category' );
            $wpseo_primary_term = new WPSEO_Primary_Term( 'events_category', $posts->ID );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $Primary_term = get_term( $wpseo_primary_term );
            if($Primary_term->name){
                if($category_name !=''){
                    $category_name .= '/'.$Primary_term->name;
                }else{
                    $category_name .= $Primary_term->name;
                }
            }else{
                if($category_name !=''){
                    $category_name .= '/'.$post_category[0]->name;
                }else{
                    $category_name .= $post_category[0]->name;
                }
            }
            $output .= '<div class="post-list-wrap">';
            $output .= '<div class="post-events-img">';
            $output .= '<div class="image_wrapper">';
            $output .= '<a href="'.$posts->guid.'">';
            $output .= get_the_post_thumbnail($posts->ID,'thumbnail');
            $output .= '</a>';
            $output .= '</div>';
            $output .= '</div>';
            // desc ---------------------------------------------------------------------------
            $output .= '<div class="post-event-desc-wrapper">';
            $output .= '<div class="post-event-desc">';
            // title -------------------------------------
            $output .= '<div class="post-title">';
            // default ----------------------------
            $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $posts->guid .'">'. $posts->post_title .'</a></h2>';
            $output .= '<div class="button-icon">';
            $output .= '<div class="button-favorites">';//收藏按鈕
            $output .= get_favorites_button($posts->ID);
            $output .= '</div>';
            $output .= '<div class="button-love">';//按讚按鈕
            $output .= '<span class="icons-wrapper">';
            $output .= '<img class="thumb-up-icon" src="'.wp_get_attachment_url('566').'">';
            $output .= '</span>';
            $output .= '<span class="label">'.$post_meta['mfn-post-love'][0].'</span>';
            $output .= '</div>';
            $output .= '<div class="button-view-count">';//觀看次數
            $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($posts->ID));
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '<h5 class="second-title" >'.date('Y.m.d',$post_meta['_start_time'][0]).'/'.$category_name.'</h5>';
            $output .= '<div class="post-content">';
            $output .= '<p>活動縣市：'.$post_meta['_city'][0].'</p>';
            $output .= '<p>活動場館：'.$post_meta['_area'][0].'</p>';
            $output .= '<p>活動類型：'.$post_meta['_class'][0].'| 活動對象：'.$post_meta['_person'][0].'| 活動門票：'.(($post_meta['_cost'][0]=='付費')?"付費":"免費").'</p>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
        }
    }
    $output .= '</div>';
    echo $output;
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_events_search_paginate_shortcode_function($atts){
    ob_start();
    if(isset($_POST)){
        foreach($_POST as $kk => $vv){
            $$kk = $vv;
        }
    }

    $keywordSQL = '';
    $termSQL = '';
    $time_SQL = '';
    $term_SQL = '';
    $custom_SQL = '';
    $customSQL = '';
    if($_POST['start_time']!=''){
        $end_SQL = 'INNER JOIN wp_postmeta AS events_end_time ON ( wp_posts.ID = events_end_time.post_id )';
        $time_SQL .= "( events_end_time.meta_key = '_start_time' AND events_end_time.meta_value >= '".strtotime($start_time)."' ) AND";
    }
    if($_POST['end_time']!=''){
        $start_SQL = 'INNER JOIN wp_postmeta AS events_start_time ON ( wp_posts.ID = events_start_time.post_id )';
        $time_SQL .= "( events_start_time.meta_key = '_start_time' AND events_start_time.meta_value <= '".strtotime($end_time)."' ) AND";
    }
    if(isset($_POST['city'])&&$city !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _city ON ( wp_posts.ID = _city.post_id )';
        $customSQL .= "( _city.meta_key = '_city' AND _city.meta_value = '".$city."' ) AND";
    }
    if(isset($_POST['area'])&&$area !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _area ON ( wp_posts.ID = _area.post_id )';
        $customSQL .= "( _area.meta_key = '_area' AND _area.meta_value = '".$area."' ) AND";
    }
    if(isset($_POST['class'])&&$class !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _class ON ( wp_posts.ID = _class.post_id )';
        $customSQL .= "( _class.meta_key = '_class' AND _class.meta_value = '".$class."' ) AND";
    }
    if(isset($_POST['nature'])&&$nature !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _nature ON ( wp_posts.ID = _nature.post_id )';
        $customSQL .= "( _nature.meta_key = '_nature' AND _nature.meta_value = '".$nature."' ) AND";
    }
    if(isset($_POST['person'])&&$person !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _person ON ( wp_posts.ID = _person.post_id )';
        $customSQL .= "( _person.meta_key = '_person' AND _person.meta_value = '".$person."' ) AND";
    }
    if(isset($_POST['cost'])&&$cost !=''){
        $custom_SQL .= 'INNER JOIN wp_postmeta AS _cost ON ( wp_posts.ID = _cost.post_id )';
        $customSQL .= "( _cost.meta_key = '_cost' AND _cost.meta_value = '".$cost."' ) AND";
    }
    if($keyword != ''){
        $key_array = array('mfn-page-items','_city','_area','_class','_nature','_person','_cost','_actor','_host','_address' );
        foreach ($key_array as $key){
            $keywordSQL .="( wp_postmeta.meta_key = '".$key."' AND wp_postmeta.meta_value LIKE '%".$keyword."%' )OR ";
        }
        $keywordSQL .= "(wp_posts.post_title LIKE '%".$keyword."%') OR (wp_posts.post_excerpt LIKE '%".$keyword."%') OR (wp_posts.post_content LIKE '%".$keyword."%')";
        $e_cat_array = array();
        $terms_e_cat_d = get_terms( array('taxonomy' => 'events_category','hide_empty' => false,'description__like' => $keyword));//cat 簡介 LIKE 關鍵字
        $terms_e_cat_n = get_terms( array('taxonomy' => 'events_category','hide_empty' => false,'name__like' => $keyword));//cat name LIKE 關鍵字
        foreach ($terms_e_cat_d as $e_cat){//符合關鍵字之簡介的作者ID存入陣列
            array_push($e_cat_array,$e_cat->term_id);
        }
        foreach ($terms_e_cat_n as $e_cat){//符合關鍵字之name的作者ID存入陣列
            array_push($e_cat_array,$e_cat->term_id);
        }
        $e_cat_array = array_unique($e_cat_array);//清除重複ID
        $search_term = implode(',',$e_cat_array);// 作者分類array 轉 字串
    }
    if(isset($_POST['type'])&&$type !=''){
        $type_tax_array= get_term_by('name', $type, 'events_category');
        $term_SQL .=  'LEFT JOIN wp_term_relationships AS events_term ON (wp_posts.ID = events_term.object_id)';//活動要的term 資料庫加入SQL
        $termSQL = "AND events_term.term_taxonomy_id IN (".$type_tax_array->term_id.")" ;
    }else{
        if($search_term !=''){
            $term_SQL .=  'LEFT JOIN wp_term_relationships AS events_term ON (wp_posts.ID = events_term.object_id)';//活動要的term 資料庫加入SQL
            $termSQL = "AND ((  events_term.term_taxonomy_id IN (".$search_term.")" ;
        }
    }

    if($time_SQL  == ''){
        $time_SQL = "1=1";
    }else{
        $time_SQL = rtrim($time_SQL,'AND');
    }
    if($keywordSQL  == ''){
        $keywordSQL = "1=1";
    }else{
        $keywordSQL = rtrim($keywordSQL,'OR');
    }
    if($customSQL  == ''){
        $customSQL = "1=1";
    }else{
        $customSQL = rtrim($customSQL,'AND');
    }
    if($search_term !=''){
        $keywordSQL .= ')';
        $customSQL .= ') )OR';
    }else{
        $customSQL .= ') AND';
    }
    $order_SQL = 'INNER JOIN wp_postmeta AS orderby ON ( wp_posts.ID = orderby.post_id AND orderby.meta_key = "_start_time")
     INNER JOIN wp_postmeta AS ordershow ON ( wp_posts.ID = ordershow.post_id AND ordershow.meta_key = "_end_time"  AND ordershow.meta_value >='.time().' )';
    $strSQL = "SELECT * FROM wp_posts
                INNER JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id )
                ".$order_SQL.$term_SQL.$start_SQL.$end_SQL.$custom_SQL."
                WHERE 1=1 AND wp_posts.post_type = 'events' AND
                ((wp_posts.post_status = 'publish')) 
                ".$termSQL."AND
                (".$time_SQL.")AND
                (".$customSQL."
                (".$keywordSQL.") GROUP BY wp_posts.ID ORDER BY orderby.meta_value  ASC ";
    global $wpdb;
    $found_posts = $wpdb->get_results( $strSQL ,ARRAY_N );
    // Get posts
    ?>
    <div class="post-pagination">
        <div class="pagination">
            <?php
            echo paginate_links( array(
                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                'total'        => ceil(count($found_posts)/10),
                'current'      => max( 1, get_query_var( 'paged' ) ),
                'format'       => '?paged=%#%',
                'show_all'     => false,
                'type'         => 'plain',
                'end_size'     => 2,
                'mid_size'     => 1,
                'prev_next'    => true,
                'prev_text'    => '<i class="icon-left-open"></i>',
                'next_text'    => '<i class="icon-right-open"></i>',
                'add_args'     => false,
                'add_fragment' => '',
            ) );
            ?>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}






?>