<?php
if(!defined('LIT_DIR')) define('LIT_DIR', get_stylesheet_directory());
if(!defined('LIT_URL')) define('LIT_URL', get_stylesheet_directory_uri());

require_once __DIR__ . "/lohasit-shortcode.php";
require_once __DIR__ . "/lohasit-search.php";
require_once __DIR__ . "/lohas-usershort.php";
class LohasItCustom
{
    private static $instance;

    private $menu_names = [];

    public function __construct()
    {
        load_child_theme_textdomain('lohasit', plugin_dir_path(__FILE__) . 'languages');
        $this->menu_names = [
            'ARForms' => __('Manage Forms', 'lohasit'),
            'woocommerce' => __('Cart System' ,'lohasit')
        ];
        $this->register_hooks();
    }

    private function register_hooks()
    {
        /**
         * 更改woocommerce地址顯示姓名的格式 added by kroutony 2017-05-03
         */
        add_filter('woocommerce_formatted_address_replacements', array($this, 'woocommerce_address_name_reverse'));
        /**
         * 取消運送到不同地址的勾選 added by kroutony 2017-12-05
         */
        add_action('woocommerce_ship_to_different_address_checked', '__return_false', 999);
        /**
         * 讓WooCommerce Customer/Order CSV Export匯出的csv檔加上BOM檔頭 2018-02-02
         */
        add_filter('wc_customer_order_csv_export_enable_bom', '__return_true');
        /**
         * 移除admin bar的wordpress logo
         */
        add_action('admin_bar_menu', array($this, 'remove_admin_bar_wordpress_logo'), 999);
        /**
         * 針對非管理員移除admin bar上的項目
         */
        add_action('wp_before_admin_bar_render' , array($this, 'remove_admin_bar_none_admin_nodes'), 1000);
        /**
         * 購物車自動更新
         */
        add_action('wp_footer', array($this, 'woocommerce_auto_update_cart'));
        /**
         * 取消 Wordpress 自動更新
         */
        add_filter('pre_site_transient_update_core', array($this, 'remove_core_updates'));
        add_filter('pre_site_transient_update_plugins', array($this, 'remove_core_updates'));
        add_filter('pre_site_transient_update_themes', array($this, 'remove_core_updates'));
        /**
         * 加入版權資訊至admin頁面的footer
         */
        add_filter('admin_footer_text', array($this, 'replace_admin_footer'));
        /**
         * 調整Woocommerce Zoom Magnifier Premium產生的圖片藝廊Wrapper高度
         */
        add_action('woocommerce_after_single_product', array($this, 'resize_caroufredsel_wrapper_height'));
        /**
         * 新增Mailpoet選項，讓MailPoet使用WP Mail
         */
        add_action('init', array($this, 'enable_mailpoet_wp_mail_support'));
        /**
         * 重新選取超商運送方式時，重設超商資訊(選擇從統一->全家 或 全家->統一時)
         */
        add_action('woocommerce_review_order_after_order_total', array($this, 'reselect_cvs_store_info'));
        /**
         * 紀錄超商資訊到訂單備註
         */
        add_action('woocommerce_add_shipping_order_item', array($this, 'log_cvs_info_to_order_note'));
        /**
         * 更改密碼強度限制
         */
        add_filter('woocommerce_min_password_strength', array($this, 'change_password_length'));
        /**
         * 當紅利點數可用折扣金額為0時，隱藏購物車及結帳頁折抵訊息
         */
        add_filter('wc_points_rewards_redeem_points_message', array($this, 'hide_applying_points_message'), 10, 2);
        /**
         * 載入後台CSS檔案
         */
        add_action('admin_head', array($this, 'load_admin_css_files'));
        /**
         * 預設前台頁尾Copyright區塊文字
         */
        add_filter('lohasit_default_footer_copyright_text', array($this, 'default_footer_copyright_text'));
        /**
         * 修改綠界超商及宅配物流訂單送出的取件人姓名
         */
        add_filter('ecpay_c2c_logistics_create_order_data', array($this, 'override_receiver_name_from_ecpay_c2c_post_data'), 20, 2);
        add_filter('ecpay_home_logistics_create_order_data', array($this, 'override_receiver_name_from_ecpay_c2c_post_data'), 20, 2);
        /**
         * 增加角色的body-class至後台
         */
        add_filter('admin_body_class', array($this, 'add_user_role_to_admin_body_classes'));
        /**
         * 增加角色的body-class至前台
         */
        add_filter('body_class', array($this, 'add_user_role_to_body_classes'));
        /**
         * 修改WordPress從文章內容產生的摘要內的點點點樣式
         */
        add_filter('excerpt_more', array($this, 'override_excerpt_more'), 999);
		
		/**
         * 調整YITH三個外掛的後台選單位置
		 */
        add_filter('yit_plugin_fw_panel_option_args', array($this, 'adjust_yith_submenu'));
        add_filter('yit_plugin_fw_wc_panel_option_args', array($this, 'adjust_yith_submenu'));

        /**
         *  修正YITH動態定價無法正常使用
         */
        add_filter('ywdpd_pricing_rules' , array($this, 'modify_multiple_value_to_array'));
        /**
         * 從HTML的資源連結中移除網站網址資訊
         */
        add_action('get_header', array($this, 'remove_domain_from_html'));
        /**
         * 修改選單翻譯
         */
        add_action('admin_init', array($this, 'override_menu_names'));
        /**
         * 針對特定分類、靜態頁面、單一文章頁自動載入指定的CSS與JS檔案
         */
        add_action('wp_enqueue_scripts', array($this, 'load_type_css_js_files'), 102);
        /**
         * 自動載入全域CSS檔案
         */
        add_action('wp_enqueue_scripts', array($this, 'load_global_css_files'), 101);
        /**
         * 自動載入全域JS檔案
         */
        add_action('wp_enqueue_scripts', array($this, 'load_global_js_files'), 100);
        /**
         * 後台功能hook
         */
        add_filter( 'wp_terms_checklist_args', array($this,'set_category_checked_ontop'), 10, 2 );//設定選中的分類不會排在最前方
        add_action( 'add_meta_boxes', [$this, 'add_meta_boxes' ] ,20); //在後台編輯頁面增加設定的欄位
        add_action( 'save_post', [$this, 'save_data_for_product_and_shop'],1,2); //儲存post增加欄位
        add_action('admin_enqueue_scripts',  [$this,'add_datepicker_js'],1);//後台引入datepicker js
        add_action('wp_enqueue_scripts',  [$this,'add_datepicker_js'],1);//前台引入datepicker js
        add_action( 'init', [$this,'customer_post_taxonomy'], 0 );//自定義分類
        /**
         * 新增shortcode
         */
        add_shortcode("musico_index_events_shortcode","musico_index_events_shortcode_function");//首頁最新活動
        add_shortcode("musico_index_portcolio_shortcode","musico_index_portcolio_shortcode_function");//首頁最新活動
        add_shortcode("musico_events_up_shortcode", "musico_events_up_shortcode_function");//音樂活動-上層內容
        add_shortcode("musico_events_middle_shortcode", "musico_events_middle_shortcode_function");//音樂活動-中層層內容
        add_shortcode("musico_events_down_shortcode", "musico_events_down_shortcode_function");//音樂活動-下層內容
        add_shortcode("musico_index_search_box_shortcode", "musico_index_search_box_shortcode_function");//首頁活動搜尋框
        add_shortcode("musico_index_top10_shortcode", "musico_index_top10_shortcode_function");//首頁Top10
        add_shortcode("musico_portfolio_up_shortcode", "musico_portfolio_up_shortcode_function");//音樂專欄-觀點隨筆-上層內容
        add_shortcode("musico_portfolio_down_shortcode", "musico_portfolio_down_shortcode_function");//音樂專欄-觀點隨筆-下層內容
        add_shortcode("musico_portfolio_author_shortcode", "musico_portfolio_author_shortcode_function");//音樂專欄-觀點隨筆-作者分類
        add_shortcode("musico_special_portfolio_up_shortcode", "musico_special_portfolio_up_shortcode_function");//音樂專欄-特別企劃/樂享生活-上層內容
        add_shortcode("musico_special_portfolio_down_shortcode", "musico_special_portfolio_down_shortcode_function");//音樂專欄-特別企劃/樂享生活-下層內容
        add_shortcode("musico_newset_protfoilo_shortcode", "musico_newset_protfoilo_shortcode_function");//音樂專欄-推薦閱讀
        add_shortcode("musico_post_list_shortcode", "musico_post_list_shortcode_function");//文章分類內容
        add_shortcode("musico_post_paginate_shortcode", "musico_post_paginate_shortcode_function");//文章分類內容分頁
        add_shortcode("musico_blog_left_shortcode","musico_blog_left_shortcode_function");//blog列表頁-大圖
        add_shortcode("musico_blog_right_shortcode","musico_blog_right_shortcode_function");//blog列表頁-列表

        add_shortcode("musico_search_list_shortcode", "musico_search_list_shortcode_function");//全站搜尋內容
        add_shortcode("musico_search_paginate_shortcode", "musico_search_paginate_shortcode_function");//全站搜尋分頁
        add_shortcode("musico_events_search_list_shortcode", "musico_events_search_list_shortcode_function");//全站搜尋內容
        add_shortcode("musico_events_search_paginate_shortcode", "musico_events_search_paginate_shortcode_function");//全站搜尋分頁

        add_shortcode("musico_user_new_and_editor_blog_shortcode", "musico_user_new_and_editor_blog_shortcode_function");//網誌修改/新增
        add_shortcode("musico_user_favorite_shortcode", "musico_user_favorite_shortcode_function");//會員中心-文章收藏列表
        add_shortcode("musico_user_blog_list_shortcode", "musico_user_blog_list_shortcode_function");//會員中心-網誌列表
        add_shortcode("musico_user_privacy_shortcode", "musico_user_privacy_shortcode_function");//會員中心-隱私設定
        add_shortcode("musico_user_bbs_list_shortcode", "musico_user_bbs_list_shortcode_function");//會員中心-交流收藏
        add_shortcode("musico_user_topic", "musico_user_topic_function");//會員中心-主題
        add_shortcode("musico_user_reply", "musico_user_reply_function");//會員中心-回覆
        add_shortcode("musico_resend_register_mail", "musico_resend_register_mail_function");//會員中心-重發認證信

        add_shortcode("musico_other_user_page", "musico_other_user_page_function");//其他會員中心-文章收藏列表
        add_shortcode("musico_other_user_favorite", "musico_other_user_favorite_function");//其他會員中心-文章收藏列表
        add_shortcode("musico_other_user_blog", "musico_other_user_blog_function");//其他會員中心-部落格
        add_shortcode("musico_other_user_topic", "musico_other_user_topic_function");//其他會員中心-主題
        add_shortcode("musico_other_user_reply", "musico_other_user_reply_function");//其他會員中心-回覆


        add_filter('manage_events_posts_columns', [$this,'events_columns'], 1); //後台活動列表分類欄位
        add_action('manage_events_posts_custom_column', [$this,'output_events_columns'], 1, 2); // 後台活動列表分類顯示

        add_action( 'wp_ajax_favorited_cancel_action', [$this,'favorited_cancel'] ); // 針對已登入的使用者_ajax-取消收藏文章
        add_action( 'wp_ajax_nopriv_favorited_cancel_action', [$this,'favorited_cancel'] ); // 針對未登入的使用者_ajax-取消收藏文章
        add_action( 'wp_ajax_change_user_image_action', [$this,'change_user_image'] ); // 針對已登入的使用者_ajax-更換大頭貼
        add_action( 'wp_ajax_user_blog_update_action', [$this,'user_blog_update'] ); // 針對已登入的使用者_ajax-網誌修改
        add_action( 'wp_ajax_user_privacy_action', [$this,'user_privacy'] ); // 針對已登入的使用者_ajax-隱私設定


        add_action( 'woocommerce_save_account_details', [$this,'save_customer_data_in_fronted']); // 會員中心 修改資料儲存
        add_filter('bbp_pre_get_user_profile_url', [$this,'new_forums_user_url'], 1); // bbp 其他使用者連結頁面

        add_action('template_redirect', [$this,'check_login_in_account'],1);//會員中心頁面確定是否有登入
        add_action( 'user_register', [$this,'new_member_save_token'], 10, 1 );//創建會員存入token(驗證用)
        add_filter( 'woocommerce_registration_redirect', [$this,'dont_login_after_register'], 10, 1 );//創建完會員不自動登入
        add_action( 'init',[$this,'register_token_check'],1);//確認驗證信並自動登入
        add_action( 'woocommerce_register_form_end',[$this,'resend_register_mail_btn'],1);//在註冊位置下方加上重發註冊信連結
        add_action( 'woocommerce_process_login_errors' , [$this,'check_token_before_login'],1,3 );//登入驗證認證信

        add_action('portfolio_author_add_form_fields',[$this,'author_belong_class'],1);//新增頁作者所屬分類顯示
        add_action('portfolio_author_edit_form_fields',[$this,'author_belong_class_inside'],1);//修改頁作者所屬分類顯示
        add_action( 'edit_portfolio_author', [$this, 'save_data_for_portfolio_author'],1,2); //修改時儲存作者所屬分類
        add_action( 'create_portfolio_author', [$this, 'save_data_for_portfolio_author'],1,2); //新增時儲存作者所屬分類

        add_action( 'wp_head', [$this,'fb_login_redirect'],1);
    }
    public function fb_login_redirect(){
        echo "<script>
            window.onload = function() {
                if (window.location.hash && window.location.hash == '#_=_')
                {
                    window.location.hash = '';
                    history.pushState('', document.title, window.location.pathname);
                    origin = location.origin;
                    pathname = location.pathname;
                    window.location.href = origin + pathname;
                }
            };
        </script>";
    }
    public function save_data_for_portfolio_author($term_id){
        if(isset($_POST['author_belong_class'])){
            $author_belong_class = $_POST['author_belong_class'];
            update_term_meta($term_id, 'author_belong_class', $author_belong_class);
        }
    }
    public function author_belong_class(){
        ?>
        <tr class="form-field term-state-wrap">
            <th scope="row"><label for="state">分類顯示</label></th>
            <td>
                <input type="radio" name="author_belong_class" id="belong_class1" checked value="57">觀點隨筆
                <input type="radio" name="author_belong_class" id="belong_class2" value="58">品樂生活
            </td>
        </tr>
        <?php
    }
    function author_belong_class_inside($term){
        $author_belong_class = get_term_meta( $term->term_id, 'author_belong_class', true );
        ?>
        <tr class="form-field term-state-wrap">
            <th scope="row"><label for="state">分類顯示</label></th>
            <td>
                <input type="radio" name="author_belong_class" id="belong_class1" <?=($author_belong_class=='57')?'checked':''?> value="57">觀點隨筆
                <input type="radio" name="author_belong_class" id="belong_class2" <?=($author_belong_class)=='58'?'checked':''?> value="58">品樂生活
            </td>
        </tr>
        <?php
    }
    public function check_token_before_login($validation_error, $post_username, $post_password){
        $user_login = get_user_by('login',$post_username);
        $token = get_user_meta($user_login->ID,'_token','single');
        if ($token !='pass' && $token !='') //<--recommend option
        {
            throw new Exception( '<strong>' . __( 'Error', 'woocommerce' ) . ':</strong> 請先完成信箱驗證後再登入!' );
        }
        return $validation_error;
    }
    public function resend_register_mail_btn(){
        echo '<p class="woocommerce-LostPassword lost_password">
				<a href="'.home_url('/my-account/resend-register/').'">重發認證信?</a>
			</p>';
    }
    public function register_token_check(){
        if(!is_user_logged_in()){
            if($_GET['source']=='register'&&$_GET['login']!=''&&$_GET['token']!=''){
                $user = get_user_by('login',$_GET['login']);
                $token = get_user_meta($user->ID,'_token','single');
                if($token !='pass' ||$token ==''){
                    if($token == $_GET['token']){
                        update_user_meta($user->ID,'_token','pass');
                        wp_set_auth_cookie($user->ID);
                        wp_set_current_user($user->ID);
                        wp_safe_redirect(home_url('/my-account/edit-account/'));
                        exit;
                    }else{
                        wp_safe_redirect(home_url('/my-account/?approved=token_error'));
                        exit;
                    }
                }else{
                    wp_safe_redirect(home_url('/my-account/?approved=token_pass'));
                    exit;
                }
            }
        }
    }
    public function dont_login_after_register(){
        if ( is_user_logged_in() ) {
            $current_user = wp_get_current_user();
            $user_id = $current_user->ID;
            $approved_status = get_user_meta($user_id, '_token', true);
            //if the user hasn't been approved destroy the cookie to kill the session and log them out
            if ( $approved_status == 'pass' ){
                return get_permalink(wc_get_page_id('myaccount'));
            }
            else{
                wc_add_notice('已成功註冊!請至信箱收取驗證信並通過驗證後才可登入。','success');
                wp_logout();
                return get_permalink(wc_get_page_id('myaccount')) . "?approved=register";
            }
        }
    }
    public function new_member_save_token( $user_id ) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 20; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        if(get_current_user_id() == '0'||get_current_user_id()==''){
            update_user_meta($user_id, '_token', $randomString);
        }else{
            update_user_meta($user_id, '_token', 'pass');
        }
    }
    public function check_login_in_account(){
        global $post;
        if(($post->post_parent =='9' ||$post->ID == '1019' || $post->post_parent == '1019')&&($post->ID != '1157')){
            if(get_current_user_id() == '0'||get_current_user_id()==''){
                wp_redirect(home_url('/my-account/'));
                exit;
            }
        }
    }
    public function user_privacy(){
        $user_id = $_POST['user_id'];
        $privacy_type = $_POST['privacy_type'];
        $privacy_value = $_POST['privacy_value'];
        $user_privacy = get_user_meta( $user_id ,'user_privacy')[0];
        if($user_privacy){
            $privacy = $user_privacy;
        }else{
            $privacy = array(
                'favorite'=> 'n',
                'blog'    => 'n',
                'topic'   => 'n',
                'reply'   => 'n',
            );
        }
        $privacy[$privacy_type] = $privacy_value;
        update_user_meta($user_id,'user_privacy',$privacy);
        die();
    }
    public function user_blog_update(){
        $flag = 0;
        $blog_action = $_POST['blog_action'];
        $post_id = $_POST['post_id'];
        $user_id = $_POST['user_id'];
        $feature_img_id = $_POST['feature_img_id'];
        $title = $_POST['title'];
        $content = $_POST['content'];
        if($blog_action == 'insert'){
            $post = array(
                'post_author'   => $user_id,
                'post_title'    => $title,
                'post_status'   => 'publish',
                'post_type'     => 'custom_blog',
                'post_content'  => $content,
                'comment_status'=> 'open',
            );
            //新增新文章
            $last_id = wp_insert_post($post);
            set_post_thumbnail( $last_id, $feature_img_id);
            $flag = 1;
        }else{
            $post = array(
                'ID'            => $post_id,
                'post_author'   => $user_id,
                'post_title'    => $title,
                'post_status'   => 'publish',
                'post_type'     => 'custom_blog',
                'post_content'  => $content,
                'comment_status'=> 'open',
            );
            //新增新文章
            wp_update_post($post);
            set_post_thumbnail( $post_id, $feature_img_id);
            $flag = 2;
        }
        echo $flag;
        die();
    }
    public function change_user_image(){
        $user_id = $_POST['user_id'];
        $img_id = $_POST['img_id'];
        update_user_meta($user_id,'pcg_custom_gravatar',$img_id);
        die();
    }
    public function favorited_cancel(){
        $user_id = $_POST['user_id'];
        $post_id = $_POST['post_id'];
        $db_type = $_POST['db_type'];
        if($db_type == 'posts'){
            $favorites_list = get_user_meta( $user_id, 'simplefavorites', true );
            foreach($favorites_list as $key => $site_favorites){
                foreach($site_favorites['posts'] as $k => $fav){
                    if ( $fav == $post_id ) unset($favorites_list[$key]['posts'][$k]);
                }
                foreach( $site_favorites['groups'] as $group_key => $group){
                    foreach ( $group['posts'] as $k => $g_post_id ){
                        if ( $g_post_id == $post_id ) unset($favorites_list[$key]['groups'][$group_key]['posts'][$k]);
                    }
                }
            }
            update_user_meta($user_id,'simplefavorites',$favorites_list);
        }else if($db_type == 'bbs'){
            bbp_remove_user_favorite($user_id,$post_id);
        }
        die();

    }
    public function events_columns( $columns ){
        $columns['events_category'] = __('活動分類');
        return $columns;
    }
    public function new_forums_user_url( $user_id ){
        $now_id = get_current_user_id();
        if($now_id == $user_id){
            return home_url('/my-account/');
        }else{
            return home_url('/musician/?user_id='.$user_id);
        }
    }


    public function output_events_columns( $column_name, $post_id ) {
        switch( $column_name ) {
            case 'events_category' :
                $output = '';
                $terms = get_the_terms( $post_id, 'events_category' );

                foreach($terms as $term){
                    $output .=  ''.$term->name . '、';
                }
                echo rtrim($output,'、');
                break;
        }
    }

    public function save_customer_data_in_fronted($user_id) {
        update_user_meta($user_id, 'nickname', $_POST['nickname']);
        update_user_meta($user_id, 'description', $_POST['description']);
    }
    public function set_category_checked_ontop( $args,$post_id){
        $args['checked_ontop'] = false;
        return $args;
    }
    public function customer_post_taxonomy(){
        $labels = array(
            'name'               => '音樂活動',
            'all_items'          => '所有活動',
            'add_new'            => '新增活動',
            'add_new_item'       => '增加活動資料',
            'edit_item'          => '編輯活動',
            'view_item'          => '檢視活動',
            'search_items'       => '搜尋活動',
            'not_found'          => '沒有找到',
            'not_found_in_trash' => '沒有於回收桶之中找到',
            'parent_item_colon' 	=> '',
        );
        $args = array(
            'labels' 				=> $labels,
            'menu_icon'				=> 'dashicons-calendar-alt',
            'public' 				=> true,
            'publicly_queryable' 	=> true,
            'show_ui' 				=> true,
            'show_admin_column'     => true,
            'query_var' 			=> true,
            'capability_type' 		=> 'post',
            'hierarchical' 			=> false,
            'menu_position' 		=> null,
            'supports' 				=> array( 'title', 'editor', 'thumbnail', 'comments', 'page-attributes','excerpt' ),
            'has_archive' => true,
            'taxonomies' => array('events_category'),
        );
        register_post_type( 'events', $args );

        register_taxonomy( 'events_category', 'events', array(
            'label' => '分類', 'hierarchical' => true,
        ));
        register_taxonomy( 'post_area', 'events', array(
            'label' => '場館', 'hierarchical' => false, 'meta_box_cb' => false,
        ));
        register_taxonomy( 'post_class', 'events', array(
            'label' => '類型', 'hierarchical' => false, 'meta_box_cb' => false,
        ));
        register_taxonomy( 'post_person', 'events', array(
            'label' => '對象', 'hierarchical' => false, 'meta_box_cb' => false,
        ));
        register_taxonomy( 'post_nature', 'events', array(
            'label' => '性質', 'hierarchical' => false, 'meta_box_cb' => false,
        ));
        register_taxonomy( 'portfolio_author', 'portfolio', array(
            'label' => '專欄作者', 'hierarchical' => true
        ));

        $labels = array(
            'name'               => '網誌',
            'all_items'          => '所有網誌',
            'add_new'            => '新增網誌',
            'add_new_item'       => '增加網誌資料',
            'edit_item'          => '編輯網誌',
            'view_item'          => '檢視網誌',
            'search_items'       => '搜尋網誌',
            'not_found'          => '沒有找到',
            'not_found_in_trash' => '沒有於回收桶之中找到',
            'parent_item_colon' 	=> '',
        );
        $args = array(
            'labels' 				=> $labels,
            'menu_icon'				=> 'dashicons-analytics',
            'public' 				=> true,
            'publicly_queryable' 	=> true,
            'show_ui' 				=> true,
            'query_var' 			=> true,
            'capability_type' 		=> 'post',
            'hierarchical' 			=> false,
            'menu_position' 		=> null,
            'supports' 				=> array( 'title', 'editor', 'thumbnail', 'comments', 'page-attributes','excerpt' ),
            'has_archive' => true,
            'taxonomies' => array('blog_category'),
        );
        register_post_type( 'custom_blog', $args );


        register_taxonomy_for_object_type('events_category','events');
    }
    public function add_meta_boxes(){
        //加在文章
        add_meta_box( 'post_events_detail_setting', '音樂活動內容設定',  [$this,'events_detail'], 'events', 'normal' );
        //加在作品
//        add_meta_box( 'portfolio_author_setting', '專欄作者設定',  [$this,'portfolio_author_detail'], 'portfolio', 'side' );
    }
    public function portfolio_author_detail($post){
        $_author = get_post_meta($post->ID,'_portfolio_author',1);
        $portfolio_author = get_terms( array('taxonomy' => 'portfolio_author','hide_empty' => false,) );
        ?>
        專欄作者：<select name="portfolio_author" id="portfolio_author">
            <?php foreach($portfolio_author as $portfolio_authors){ ?>
                <option value="<?=$portfolio_authors->name?>" <?=($portfolio_authors->name==$_author)?'selected':''?>><?=$portfolio_authors->name?></option>
            <?php }?>
        </select></br><?php
    }
    public function events_detail($post){
        $data = array('start_time','end_time','city','area','class','person','nature','cost','buy_link','host','actor','address');
        foreach($data as $vv){
            $$vv=get_post_meta($post->ID,'_'.$vv,1);
        }
        $states = array('基隆市' => '基隆市','台北市' => '台北市','新北市' => '新北市','宜蘭縣' => '宜蘭縣','桃園市' => '桃園市','新竹市' => '新竹市','新竹縣' => '新竹縣',
            '苗栗縣' => '苗栗縣','台中市' => '台中市','彰化縣' => '彰化縣','南投縣' => '南投縣','雲林縣' => '雲林縣','嘉義市' => '嘉義市','嘉義縣' => '嘉義縣','台南市' => '台南市',
            '高雄市' => '高雄市','屏東縣' => '屏東縣','花蓮縣' => '花蓮縣','台東縣' => '台東縣','澎湖縣' => '澎湖縣','金門縣' => '金門縣','連江縣' => '連江縣',
        );
        $post_areas = get_terms( array('taxonomy' => 'post_area','hide_empty' => false,) );
        $post_classs = get_terms( array('taxonomy' => 'post_class','hide_empty' => false,) );
        $post_persons = get_terms( array('taxonomy' => 'post_person','hide_empty' => false,) );
        $post_natures = get_terms( array('taxonomy' => 'post_nature','hide_empty' => false,) );
        ?>
        活動時間：<input class="air-datetimepicker-start"  data-timepicker="true" data-language='en' type="text" name="start_time" id="start_time" value="<?=date('Y-m-d h:i a',$start_time)?>"> ~ <input class="air-datetimepicker-end" data-timepicker="true" data-language='en' type="text" name="end_time" id="end_time" value="<?=date('Y-m-d h:i a',$end_time)?>"><br/>
        地點：<select name="city" id="city">
                <?php foreach($states as $state){ ?>
                    <option value="<?=$state?>" <?=($state==$city)?'selected':''?>><?=$state?></option>
                <?php }?>
        </select></br>
        場館：<select name="area" id="area">
            <?php foreach($post_areas as $post_area){ ?>
                <option value="<?=$post_area->name?>" <?=($post_area->name==$area)?'selected':''?>><?=$post_area->name?></option>
            <?php }?>
        </select></br>
        地址：<input type="text" name="address" id="address" value="<?=$address?>"></br>
        類型：<select name="class" id="class">
            <?php foreach($post_classs as $post_class){ ?>
                <option value="<?=$post_class->name?>" <?=($post_class->name==$class)?'selected':''?>><?=$post_class->name?></option>
            <?php }?>
        </select></br>
        對象：<select name="person" id="person">
            <?php foreach($post_persons as $post_person){ ?>
                <option value="<?=$post_person->name?>" <?=($post_person->name==$person)?'selected':''?>><?=$post_person->name?></option>
            <?php }?>
        </select></br>
        性質：<select name="nature" id="nature">
            <?php foreach($post_natures as $post_nature){ ?>
                <option value="<?=$post_nature->name?>" <?=($post_nature->name==$nature)?'selected':''?>><?=$post_nature->name?></option>
            <?php }?>
        </select></br>
        費用：<input type="radio" name="cost" id="cost1" value="付費" <?=($cost =='付費')?'checked':''?>>付費<input type="radio" name="cost" id="cost2" value="免費" <?=($cost =='免費')?'checked':''?>>免費<br/>
        購票連結：<input type="text" name="buy_link" id="buy_link" value="<?=$buy_link?>"></br>
        主辦：<input type="text" name="host" id="host" value="<?=$host?>"></br>
        演出人員：<textarea name="actor" id="actor" cols="30" rows="5"><?=$actor?></textarea></br>
        <script type="text/javascript">
            jQuery(document).ready(function(){
                var start = new Date();
                start.setHours(6);
                start.setMinutes(0);
                jQuery('.air-datetimepicker-start').datepicker({
                    timepicker: true,
                    language: 'en',
                    startDate:start,
                    startHour:'6',
                    // onSelect: function (select_date) {
                    //     jQuery('.air-datetimepicker-end').datepicker({
                    //         timepicker: true,
                    //         language: 'en',
                    //         minDate: new Date(select_date),
                    //         startDate:start,
                    //         startHour:'6',
                    //     });
                    // }
                });
                jQuery('.air-datetimepicker-end').datepicker({
                    timepicker: true,
                    language: 'en',
                    startDate:start,
                    startHour:'6',
                    // onSelect: function (select_date) {
                    //     jQuery('.air-datetimepicker-start').datepicker({
                    //         timepicker: true,
                    //         language: 'en',
                    //         maxDate: new Date(select_date),
                    //         startDate:start,
                    //         startHour:'6',
                    //     });
                    // }
                });
            });
        </script>
        <?php
    }
    public function save_data_for_product_and_shop($post_id){
        //儲存自訂內容
        if($_POST['post_type']=='events'){
            foreach ($_POST as $kk => $vv){
                $$kk = (trim($vv));
            }
            $data = array(
                'start_time'=>strtotime($start_time),
                'end_time'=>strtotime($end_time),
                'city'=>$city,
                'area'=>$area,
                'class'=>$class,
                'person'=>$person,
                'nature'=>$nature,
                'cost'=>$cost,
                'buy_link'=>$buy_link,
                'host'=>$host,
                'actor'=>$actor,
                'address'=>$address
            );
            foreach ($data as $key=>$value){
                update_post_meta($post_id, '_'.$key, $value);
            }
        }
        if($_POST['post_type']=='portfolio'){
            update_post_meta($post_id, '_portfolio_author', $_POST['portfolio_author']);
        }
    }
    public function add_datepicker_js(){
//        wp_enqueue_script( 'jquery-ui-datepicker',home_url().'/wp-includes/js/jquery/ui/datepicker.min.js');
//        wp_enqueue_style('jquery-style', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
        wp_enqueue_script( 'jquery-air-datepicker',home_url().'/wp-content/themes/betheme-child/assets/air-datepicker/js/datepicker.min.js');
        wp_enqueue_script( 'jquery-air-en-datepicker',home_url().'/wp-content/themes/betheme-child/assets/air-datepicker/js/i18n/datepicker.en.js');
        wp_enqueue_style('jquery-air-style', home_url().'/wp-content/themes/betheme-child/assets/air-datepicker/css/datepicker.min.css');

    }

    public static function get_instance()
    {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function get_current_user_role_body_class()
    {
        return 'role-' . lohasit_get_current_user_role();
    }

    public function woocommerce_address_name_reverse($args) {
        //名字內有中文·更改順序
        if( lohasit_is_string_contains_chinese($args["{last_name}"]) || lohasit_is_string_contains_chinese($args["{first_name}"]) )	{
            $args["{name}"] = $args["{last_name}"] . $args["{first_name}"];
        }
        return $args;
    }

    public function remove_admin_bar_wordpress_logo( $wp_admin_bar ) {
        /** @var WP_Admin_Bar $wp_admin_bar */
        $wp_admin_bar->remove_node('wp-logo');

    }

    public function remove_admin_bar_none_admin_nodes() {
        /** @var WP_Admin_Bar $wp_admin_bar */
        global $wp_admin_bar;
        if(!current_user_can('administrator')) {
            //W3 Total Cache
            $wp_admin_bar->remove_node('w3tc');
            //語系切換
            $wp_admin_bar->remove_node('WPML_ALS');
            //Simply Show Hooks
            $wp_admin_bar->remove_node('cxssh-main-menu');
        }
    }

    public function woocommerce_auto_update_cart() {
        if (is_cart()) :
            ?>
            <script>
                (function($){
                    $('div.woocommerce').on('change', '.qty', function(){
                        var $updateCartButton = $("[name='update_cart']");
                        $updateCartButton.removeAttr('disabled');
                        $updateCartButton.click();
                    });
                })(jQuery);
            </script>
        <?php
        endif;
    }

    public function remove_core_updates(){
        global $wp_version;
        return(object) array('last_checked'=> time(),'version_checked'=> $wp_version);
    }

    public function replace_admin_footer ($text) {
        $text = 'Designed by ' . self::get_official_site_link();
        return $text;
    }

    public function default_footer_copyright_text()
    {
        return '&copy; '. date( 'Y' ) .' '. get_bloginfo( 'name' ) .'. All Rights Reserved. Designed by ' . self::get_official_site_link();
    }

    public function resize_caroufredsel_wrapper_height() {
        if(function_exists('yith_ywzm_premium_init')) {
        ?>
            <script>
                (function ($) {
                    $(window).resize(function () {
                        let $caroufredselWrapper = $('.caroufredsel_wrapper'),
                            imgHeight = $caroufredselWrapper.find('li').first().height();
                        $caroufredselWrapper.css('max-height', imgHeight);
                    });
                })(jQuery);
            </script>
        <?php
        }
    }

    public function enable_mailpoet_wp_mail_support() {
        if(class_exists('WYSIJA')){
            $model_config = WYSIJA::get('config','model');
            $model_config->save( array( 'allow_wpmail' => true ));
        }
    }

    public function reselect_cvs_store_info() {
        $cvs_methods = ['cvs_unimart_shipping', 'cvs_fami_shipping'];
        $chosens = WC()->session->get('chosen_shipping_methods');
        $old_chosens = WC()->session->get('old_chosen_shipping_methods');
        if($chosens[0] !== $old_chosens[0]) {
            $alert = false;
            if(WC()->session->get('ecpay_map_result'))
                $alert = true;
            WC()->session->set('ecpay_map_result', null);
            ?>
            <script>
                (function($){
                    $('.store-info').hide();
                })(jQuery);
            </script>

            <?php if($alert && in_array($old_chosens[0], $cvs_methods) && in_array($chosens[0], $cvs_methods)): ?>
            <?php endif;
        }
        WC()->session->set('old_chosen_shipping_methods', $chosens);
    }

    public function log_cvs_info_to_order_note($order_id){
        $order = wc_get_order($order_id);
        $method_id = array_pop(array_reverse($order->get_shipping_methods()))['item_meta']['method_id'][0];
        $cvs_methods = ['cvs_unimart_shipping', 'cvs_fami_shipping'];
        if(in_array($method_id, $cvs_methods)) {
            $note = '';
            $cvs_result = WC()->session->get('ecpay_map_result');
            foreach($cvs_result as $name => $result) {
                $note .= $name.':'.$result."<br>";
            }
            $order->add_order_note($note);
        }
    }

    public function change_password_length() {
        /**
         * 0: Disable
         * 1: Very weak
         * 2: Weak
         * 3: Strong (Default)
         */
        return 1;
    }

    public function hide_applying_points_message($message, $points)
    {
        if($points<=0)
            return '';

        return $message;
    }

    public function load_admin_css_files()
    {
        $style_uri = get_stylesheet_directory_uri();
        $css_file = '/assets/css/admin-css.css';
        $min_css_file = '/assets/css/admin-css.css';
        if(file_exists(get_stylesheet_directory() . $min_css_file))
            wp_enqueue_style("lohasit_admin_css", $style_uri . $min_css_file);
        elseif(file_exists(get_stylesheet_directory() . $css_file))
            wp_enqueue_style("lohasit_admin_css", $style_uri . $css_file);
    }

    public static function get_official_site_link()
    {
        return '<a target="_blank" rel="nofollow" href="https://www.lohaslife.cc">LOHAS IT</a>';
    }

    /**
     * @param array $data
     * @param WC_Order $order
     * @return mixed
     */
    public function override_receiver_name_from_ecpay_c2c_post_data($data, $order)
    {
        $receiver_name = $data['ReceiverName'];
        if(lohasit_is_string_contains_chinese($receiver_name))
            $data['ReceiverName'] = $order->shipping_last_name . $order->shipping_first_name;
        return $data;
    }

    public function add_user_role_to_admin_body_classes($classes)
    {
        $classes .= ' ' . $this->get_current_user_role_body_class();
        return $classes;
    }

    public function add_user_role_to_body_classes($classes)
    {
        $classes[] = $this->get_current_user_role_body_class();
        return $classes;
    }

    public function override_excerpt_more($excerpt_more)
    {
        return '...';
    }
	
	 /**
     * YITH的"紅利點數"&"動態定價"位置 加到 "優惠設定"位置、YITH的"願望清單"位置 加到 "購物車系統"位置
     * @param $args
     * @return mixed
     */
    public function adjust_yith_submenu($args){
        if($args['page'] == 'yith_woocommerce_points_and_rewards' || $args['page'] == 'yith_woocommerce_dynamic_pricing_and_discounts'){
            $args['parent_page'] = 'member_customer_woocommerce_page_option';
        }
        elseif($args['page'] == 'yith_wcwl_panel'){
            $args['parent_page'] = 'woocommerce';
        }
        return $args;
    }


    public function modify_multiple_value_to_array($pricing_rules){
        $rules_name = array(
            'apply_to_categories_list'          , 'apply_to_categories_list_excluded' ,
            'apply_adjustment_categories_list'  , 'apply_adjustment_categories_list_excluded' ,
            'user_rules_customers_list'         , 'user_rules_customers_list_excluded' ,
            'apply_to_products_list'            , 'apply_adjustment_products_list_excluded' ,
            'apply_adjustment_products_list'    , 'apply_adjustment_products_list_excluded' ,
            'apply_to_tags_list'                , 'apply_to_tags_list_excluded' ,
            'apply_adjustment_tags_list'        , 'apply_adjustment_tags_list_excluded' ,
        );

        foreach($pricing_rules as $key => $rule)
        {
            foreach($rule as $name => $value){
                if($value && in_array($name,$rules_name)){
                    $pricing_rules[$key][$name] = explode(',',$value);
                }
            }
        }

        return $pricing_rules;
    }

    public function remove_domain_from_html()
    {
        global $CONFIG;
        if(isset($CONFIG['domains_to_be_removed'])) {
            ob_start(function($html) use($CONFIG){
                foreach($CONFIG['domains_to_be_removed'] as $url) {
                    $url = str_replace('.', '\.', $url);
                    $patterns = [
                        "/https?:\/\/$url\/*/",
                        "/\/\/$url\/*/",
                    ];
                    $html = preg_replace($patterns, '/', $html);
                }
                return $html;
            });
        }
    }

    public function override_menu_names()
    {
        global $menu;
        foreach ($menu as $i => $_menu) {
            $menu_slug = $_menu[2];
            if(!empty($this->menu_names[$menu_slug])) {
                $menu[$i][0] = $menu[$i][1] = $this->menu_names[$menu_slug];
            }
        }
        return;
    }

    /**
     * 針對特定分類、靜態頁面、單一文章頁自動載入指定的CSS與JS檔案
     * @hooked wp_enqueue_scripts
     */
    public function load_type_css_js_files()
    {
        global $post;
        $css_dir = '/assets/css';
        $js_dir = '/assets/js';
        $file_names = [];
        if(is_page() || is_404()) {
            $type = 'page';
            $css_dir = $css_dir . '/page/';
            $js_dir = $js_dir . '/page/';
            if(is_front_page()) {
                $file_names[] = 'home';
            } elseif(is_404()) {
                $file_names[] = '404';
            } else {
                $file_names[] = $post->post_name;
                $temp_page = $post;
                while($temp_page->post_parent) {
                    $temp_page = get_post($temp_page->post_parent);
                    $file_names[] = $temp_page->post_name;
                }
            }
        } elseif(is_archive() || (!is_front_page() && is_home())) {
            $type = 'archive';
            $css_dir = $css_dir . '/archive/';
            $js_dir = $js_dir . '/archive/';
            $file_names[] = get_post_type();
            if(!$file_names[0]) {
                global $wp_taxonomies;
                $term = get_queried_object();
                $post_types = (isset($wp_taxonomies[$term->taxonomy])) ? $wp_taxonomies[$term->taxonomy]->object_type : [];
                $file_names[0] = $post_types[0];
            }
        } elseif(is_single()) {
            $type = 'single';
            $css_dir = $css_dir . '/single/';
            $js_dir = $js_dir . '/single/';
            $file_names[] = get_post_type();
        } else
            return;

        foreach ($file_names as $file_name) {
            $min_css = $css_dir . "$file_name.min.css";
            $css = $css_dir . "$file_name.css";
            $min_js = $js_dir . "$file_name.min.js";
            $js = $js_dir . "$file_name.js";

            if(file_exists(LIT_DIR . $min_css)) {
                wp_enqueue_style("lohasit-{$type}-css-{$file_name}", LIT_URL . $min_css);
            } elseif(file_exists(LIT_DIR . $css)) {
                wp_enqueue_style("lohasit-{$type}-css-{$file_name}", LIT_URL . $css);
            }

            if(file_exists(LIT_DIR . $min_js)) {
                wp_enqueue_script("lohasit-{$type}-js-{$file_name}", LIT_URL . $min_js, ['jquery'], false, true);
            } elseif(file_exists(LIT_DIR . $js)) {
                wp_enqueue_script("lohasit-{$type}-js-{$file_name}", LIT_URL . $js, ['jquery'], false, true);
            }
        }
    }

    /**
     * 載入全域CSS檔案
     * @hooked wp_enqueue_scripts
     */
    public function load_global_css_files()
    {
        $directory = '/assets/css/global/';
        $files = glob(LIT_DIR . $directory . '*.css');
        foreach ($files as $key => $file) {
            $file_name = pathinfo($file)['filename'];
            $file_base_name = str_replace('.min', '', $file_name);
            $files[$key] = $file_base_name;
        }
        foreach (array_unique($files) as $css_filename) {
            $min_css = "$css_filename.min.css";
            $css = "$css_filename.css";
            if(file_exists(LIT_DIR . $directory . $min_css))
                wp_enqueue_style("lohasit-global-css-{$min_css}", LIT_URL . $directory . $min_css);
            elseif(file_exists(LIT_DIR . $directory . $css))
                wp_enqueue_style("lohasit-global-css-{$css}", LIT_URL . $directory . $css);
        }
    }

    /**
     * 載入全域CSS檔案
     * @hooked wp_enqueue_scripts
     */
    public function load_global_js_files()
    {
        $directory = '/assets/js/global/';
        $files = glob(LIT_DIR . $directory . '*.js');
        foreach ($files as $key => $file) {
            $file_name = pathinfo($file)['filename'];
            $file_base_name = str_replace('.min', '', $file_name);
            $files[$key] = $file_base_name;
        }
        foreach (array_unique($files) as $js_filename) {
            $min_js = "$js_filename.min.js";
            $js = "$js_filename.js";
            if(file_exists(LIT_DIR . $directory . $min_js))
                wp_enqueue_script("lohasit-global-js-{$min_js}", LIT_URL . $directory . $min_js, ['jquery'], false, true);
            elseif(file_exists(LIT_DIR . $directory . $js))
                wp_enqueue_script("lohasit-global-js-{$js}", LIT_URL . $directory . $js, ['jquery'], false, true);
        }
    }
}
$GLOBALS['LohasItCustom'] = LohasItCustom::get_instance();