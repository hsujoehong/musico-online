<?php
/**
 * The main template file.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */

$translate['search-placeholder'] = mfn_opts_get('translate') ? mfn_opts_get('translate-search-placeholder','Enter your search') : __('Enter your search','betheme');
?>

<form method="post" id="site-searchform" action="<?php echo esc_url( home_url( '/all-search/' ) ); ?>">

	<i class="icon_search icon-search-fine"></i>
	<a href="#" class="icon_close"><i class="icon-cancel-fine"></i></a>
	
	<input type="text" class="field" name="keyword" id="keyword" placeholder="<?php echo $translate['search-placeholder']; ?>" />
	<input type="submit" class="submit" value="" style="display:none;" />
	
</form>