<?php
/**
 * The template for displaying content in the single-portfolio.php template
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */

// prev & next post -------------------
mfn_post_navigation_sort();

$single_post_nav = array(
	'hide-header'	=> false,
	'hide-sticky'	=> false,
	'in-same-term'	=> false,
);

$opts_single_post_nav = mfn_opts_get( 'prev-next-nav' );
if( is_array( $opts_single_post_nav ) ){

	if( isset( $opts_single_post_nav['hide-header'] ) ){
		$single_post_nav['hide-header'] = true;
	}
	if( isset( $opts_single_post_nav['hide-sticky'] ) ){
		$single_post_nav['hide-sticky'] = true;
	}
	if( isset( $opts_single_post_nav['in-same-term'] ) ){
		$single_post_nav['in-same-term'] = true;
	}

}

$post_prev = get_adjacent_post( $single_post_nav['in-same-term'], '', true, 'portfolio-types' );
$post_next = get_adjacent_post( $single_post_nav['in-same-term'], '', false, 'portfolio-types' );
$portfolio_page_id = mfn_opts_get( 'portfolio-page' );


// categories -------------------------
$categories 	= '';
$aCategories 	= array();

$terms = get_the_terms( get_the_ID(), 'portfolio-types' );
if( is_array( $terms ) ){
	foreach( $terms as $term ){
		$categories		.= '<li><a href="'. get_term_link($term) .'">'. $term->name .'</a></li>';
		$aCategories[]	= $term->term_id;  
	}
}


// post classes -----------------------
$classes = array();
if( get_post_meta(get_the_ID(), 'mfn-post-slider-header', true) ) $classes[] = 'no-img';
if( ! mfn_opts_get( 'portfolio-single-title' ) ) $classes[] = 'no-title';

if( mfn_opts_get( 'share' ) == 'hide-mobile' ){
	$classes[] = 'no-share-mobile';
} elseif( ! mfn_opts_get( 'share' ) ) {
	$classes[] = 'no-share';
}
setPostViews(get_the_ID());

$translate['published'] 	= mfn_opts_get('translate') ? mfn_opts_get('translate-published','Published by') : __('Published by','betheme');
$translate['at'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-at','at') : __('at','betheme');
$translate['categories'] 	= mfn_opts_get('translate') ? mfn_opts_get('translate-categories','Categories') : __('Categories','betheme');
$translate['all'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-all','Show all') : __('Show all','betheme');
$translate['related'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-related','Related posts') : __('Related posts','betheme');
$translate['readmore'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-readmore','Read more') : __('Read more','betheme');
$translate['client'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-client','Client') : __('Client','betheme');
$translate['date'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-date','Date') : __('Date','betheme');
$translate['website'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-website','Website') : __('Website','betheme');
$translate['view'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-view','View website') : __('View website','betheme');
$translate['task'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-task','Task') : __('Task','betheme');

$portfoliio = get_post(get_the_ID());
$portfoliio_meta = get_post_meta(get_the_ID());
$portfoliio_author = get_the_terms(get_the_ID(),'portfolio_author')[0];

$wpseo_primary_term = new WPSEO_Primary_Term( 'portfolio-types', get_the_ID() );
$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
if(!empty($wpseo_primary_term)){
    $Primary_term_menu = get_term( $wpseo_primary_term );
}else{
    $Primary_term_menu = get_the_terms( get_the_ID(), 'portfolio-types' )[0];
}
$menu = wp_get_nav_menu_items('main');//menu data
$post_type = get_post_type();
if($post_type =='portfolio'){
    foreach($menu as $menus){
        if($menus->title ==$Primary_term_menu->name){
            ?>
            <script>
                jQuery('#menu-item-<?=$menus->menu_item_parent?>').addClass('current-menu-parent');
                jQuery('#menu-item-<?=$menus->ID?>').addClass('submenu-lohas-active');
            </script>
            <?php
        }
    }
}
if($Primary_term_menu->term_id =='57'||$Primary_term_menu->term_id =='58'){
    $classes[] = 'portfolio-have-author';
}else{
    $classes[] = 'portfolio-no-author';
}

?>


<div id="portfolio-item-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>

	<?php 
		// single post navigation | sticky
		if( ! $single_post_nav['hide-sticky'] ){
			echo mfn_post_navigation_sticky( $post_prev, 'prev', 'icon-left-open-big' ); 
			echo mfn_post_navigation_sticky( $post_next, 'next', 'icon-right-open-big' );
		} 
	?>
	
	<?php if( get_post_meta( get_the_ID(), 'mfn-post-template', true ) != 'intro' ): ?>
        <?php if(!empty($portfoliio_author)):?>
		<div class="section section-post-header">
            <div class="portfolio-side-wrapper">
                <div class="portfolio-author">
                    <div class="portfolio-author-img">
                        <?=ttw_thumbnail_image($portfoliio_author->term_id,'thumbnail');?>
                    </div>
                    <div class="portfolio-author-detail">
                        <div class="portfolio-author-title">
                            <?=$portfoliio_author->name?>
                        </div>
                        <div class="portfolio-author-description">
                            <?=$portfoliio_author->description?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif;?>

        <div class="post-wrapper-content">
            <div class="section_wrapper clearfix">

                <?php
                    // single post navigation | header
                    if( ! $single_post_nav['hide-header'] ){
                        echo mfn_post_navigation_header( $post_prev, $post_next, mfn_wpml_ID( $portfolio_page_id ), $translate );
                    }
                ?>

                <div class="column one post-header">
                    <div class="top-icon">
                        <div class="button-icon-single-area">
                            <div class="button-icon">
                                <div class="button-favorites"><!--收藏按鈕-->
                                    <?=get_favorites_button();?>
                                </div>
                                <div class="button-love"><!--按讚按鈕-->
                                    <?=mfn_love();?>
                                </div>
                                <div class="button-view-count"><!--觀看次數-->
                                    <img class="heart-empty-icon" src="<?=wp_get_attachment_url('565')?>">
                                    <span class="label"><?=number_format(getPostViews(get_the_ID()));?></span>
                                </div>
                            </div>
                        </div>
                        <?php echo do_shortcode("[addtoany]"); ?>
                    </div>
                    <div class="title_wrapper">

                        <?php
                        if( mfn_opts_get( 'portfolio-single-title' ) ){
                            $h = mfn_opts_get( 'title-heading', 1 );
                            echo '<h'. $h .' class="entry-title" itemprop="headline">'. get_the_title() .'</h'. $h .'>';
                        }
                        ?>

                    </div>
                </div>
            </div>

	<?php endif; ?>
	
            <div class="entry-content" itemprop="mainContentOfPage">
                <?php
                    // Content Builder & WordPress Editor Content
                    mfn_builder_print( get_the_ID() );
                ?>
            </div>
	
	<div class="section section-post-footer">
		<div class="section_wrapper clearfix">
		
			<div class="column one post-pager">
				<?php
					// List of pages
					wp_link_pages(array(
						'before'			=> '<div class="pager-single">',
						'after'				=> '</div>',
						'link_before'		=> '<span>',
						'link_after'		=> '</span>',
						'next_or_number'	=> 'number'
					));
				?>
			</div>
			
		</div>
	</div>
    </div>
</div>

<?php
    if($Primary_term_menu->term_id =='57'||$Primary_term_menu->term_id =='58'){
        $portfolio_list = array($Primary_term_menu->term_id);
        $args = array(
            'post_type' => 'portfolio',
            'orderby' => 'date',
            'order' => 'DESC',
            'post_status '=>'publish',
            'post__not_in'=> array(get_the_ID()),
            'posts_per_page' => 3,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'portfolio_author',
                    'field' => 'term_id',
                    'terms' => array($portfoliio_author->term_id),
                ),
                array(
                    'taxonomy' => 'portfolio-types',
                    'field' => 'term_id',
                    'terms' => $portfolio_list,
                ),
            ));
        $portfoliios = new WP_Query($args);

        if($portfoliios->found_posts != '0'){
            $output = '<div class="newest-three-portfolio-in-single">';
                $output .= '<div class="newest-three-post-area">';
                    $output .= '<div class="title-bar"><h1>推薦閱讀</h1></div>';
                    $output .= '<div class="newest-three-post small-list">';
                    foreach($portfoliios->get_posts() as $portfoliio){
                        $post_author = get_the_terms( $portfoliio->ID, 'portfolio_author' )[0];
                        $author_class = get_term_by( 'id',$post_author->parent, 'portfolio_author' );
                        $output .= '<div class="post-list-wrap">';
                            $output .= '<div class="post-events-img">';
                                $output .= '<div class="image_wrapper">';
                                    $output .= '<a href="'.$portfoliio->guid.'">';
                                        $output .= get_the_post_thumbnail($portfoliio->ID,'thumbnail');
                                    $output .= '</a>';
                                $output .= '</div>';
                            $output .= '</div>';
                            // desc ---------------------------------------------------------------------------
                            $output .= '<div class="post-event-desc-wrapper">';
                                $output .= '<div class="post-event-desc">';
                                    // title -------------------------------------
                                    $output .= '<div class="post-title">';
                                    // default ----------------------------
                                        $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $portfoliio->guid .'">'. $portfoliio->post_title .'</a></h2>';
                                        $output .= '<div class="button-icon">';
                                            $output .= '<div class="button-favorites">';//收藏按鈕
                                                $output .= get_favorites_button($portfoliio->ID);
                                            $output .= '</div>';
                                            $output .= '<div class="button-love">';//按讚按鈕
                                                $output .= mfn_love($portfoliio->ID);
                                            $output .= '</div>';
                                            $output .= '<div class="button-view-count">';//觀看次數
                                                $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($portfoliio->ID));
                                            $output .= '</div>';
                                        $output .= '</div>';
                                    $output .= '</div>';
                                    $output .= '<h5 class="second-title" >'.date('Y.m.d',strtotime($portfoliio->post_date)).' / '. $author_class->name .'/'.$post_author->name .'</h5>';
                                    if(has_excerpt($portfoliio->ID)) {
                                        $post_excerpt = mb_substr( $portfoliio->post_excerpt, 0, 40 ) . '...<a class="read-all" href="'.$portfoliio->guid.'">詳全文</a>';
                                        $output .= '<div class="post-excerpt">' .$post_excerpt . '</div>';
                                    }
                                $output .= '</div>';
                            $output .= '</div>';
                        $output .= '</div>';
                    }
                    $output .= '</div>';
                $output .= '</div>';
            $output .= '</div>';
            echo $output;
        }
    }
?>