<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */

// prev & next post -------------------
$single_post_nav = array(
	'hide-header'	=> false,	
	'hide-sticky'	=> false,	
	'in-same-term'	=> false,	
);

$opts_single_post_nav = mfn_opts_get( 'prev-next-nav' );
if( is_array( $opts_single_post_nav ) ){
	
	if( isset( $opts_single_post_nav['hide-header'] ) ){
		$single_post_nav['hide-header'] = true;
	}
	if( isset( $opts_single_post_nav['hide-sticky'] ) ){
		$single_post_nav['hide-sticky'] = true;
	}
	if( isset( $opts_single_post_nav['in-same-term'] ) ){
		$single_post_nav['in-same-term'] = true;
	}
	
}

$post_prev = get_adjacent_post( $single_post_nav['in-same-term'], '', true );
$post_next = get_adjacent_post( $single_post_nav['in-same-term'], '', false );
$blog_page_id = get_option('page_for_posts');


// post classes -----------------------
$classes = array();
if( ! mfn_post_thumbnail( get_the_ID() ) ) $classes[] = 'no-img';
if( get_post_meta(get_the_ID(), 'mfn-post-hide-image', true) ) $classes[] = 'no-img';
if( post_password_required() ) $classes[] = 'no-img';
if( ! mfn_opts_get( 'blog-title' ) ) $classes[] = 'no-title';

if( mfn_opts_get( 'share' ) == 'hide-mobile' ){
	$classes[] = 'no-share-mobile';
} elseif( ! mfn_opts_get( 'share' ) ) {
	$classes[] = 'no-share';
}
setPostViews(get_the_ID());

$translate['published'] 	= mfn_opts_get('translate') ? mfn_opts_get('translate-published','Published by') : __('Published by','betheme');
$translate['at'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-at','at') : __('at','betheme');
$translate['tags'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-tags','Tags') : __('Tags','betheme');
$translate['categories'] 	= mfn_opts_get('translate') ? mfn_opts_get('translate-categories','Categories') : __('Categories','betheme');
$translate['all'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-all','Show all') : __('Show all','betheme');
$translate['related'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-related','Related posts') : __('Related posts','betheme');
$translate['readmore'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-readmore','Read more') : __('Read more','betheme');

$post = get_post(get_the_ID());
$post_meta = get_post_meta(get_the_ID());
$wpseo_primary_term = new WPSEO_Primary_Term( 'events_category', get_the_ID() );
$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
if(!empty($wpseo_primary_term)){
    $Primary_term_menu = get_term( $wpseo_primary_term );
}else{
    $Primary_term_menu = get_the_terms( get_the_ID(), 'events_category' )[0];
}
$menu = wp_get_nav_menu_items('main');//menu data
$post_type = get_post_type();
if($post_type =='events'){
    $classes[]='post-events-section-group';
    foreach($menu as $menus){
        if($menus->title ==$Primary_term_menu->name){
?>
            <script>
                jQuery('#menu-item-<?=$menus->menu_item_parent?>').addClass('current-menu-parent');
                jQuery('#menu-item-<?=$menus->ID?>').addClass('submenu-lohas-active');
            </script>
<?php
        }
    }
}else{
    $classes[]='post-section-group-in-single';
}

?>

<?php
if(get_post_type() == 'post'){
    wp_enqueue_style("lohas-single-page-custom-css", get_stylesheet_directory_uri().'/assets/css/custom-menu.css');
    $cats = get_the_terms(get_the_ID(),'category')[0];
    if($cats->term_id != '38'){
        if($cats->parent != '0'){
            $parents = get_term_by('term_id',$cats->parent,'category');
            $children = get_term_children($cats->parent,'category');
            $link_name = $parents->name;
        }else{
            $children = get_term_children($cats->term_id,'category');
            $link_name = $cats->name;
        }
    ?>
    <div class="post-cat-list-area">
        <ul>
            <?php
            foreach($children as $child){
                $active=($child ==$cats->term_id)?'cat-list-active':'';
                $cat = get_term($child);
                ?>
                <a href="/<?=$link_name?>/?cat_id=<?=$cat->term_id?>" class="<?=$active?>"><li><?=$cat->name?></li></a>
            <?php } ?>
        </ul>
    </div>
<?php
    }
}
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>

	<?php 
		// single post navigation | sticky
		if( ! $single_post_nav['hide-sticky'] ){
			echo mfn_post_navigation_sticky( $post_prev, 'prev', 'icon-left-open-big' ); 
			echo mfn_post_navigation_sticky( $post_next, 'next', 'icon-right-open-big' );
		}
	?>



	<?php if( get_post_meta( get_the_ID(), 'mfn-post-template', true ) != 'intro' ): ?>

    <?php if($post->post_type == 'events'){?>
		<div class="section section-post-header">
            <div class="post-side-wrapper">
                <div class="post-events-section">
                    <?php if($post_meta['_start_time'][0]!='' || $post_meta['_end_time'][0]!=''){?>
                    <div class="post-events-each">
                        <span class="events-each-title">活動時間</span>
                        <span class="events-each-content"><?=date('Y-m-d D g:i:s A',$post_meta['_start_time'][0])?> ~<br/><?=date('Y-m-d D g:i:s A',$post_meta['_end_time'][0])?></span>
                    </div>
                    <?php }if($post_meta['_city'][0]!=''){?>
                    <div class="post-events-each">
                        <span class="events-each-title">活動縣市</span>
                        <span class="events-each-content"><?=$post_meta['_city'][0]?></span>
                    </div>
                    <?php }if($post_meta['_area'][0]!=''){?>
                        <div class="post-events-each">
                            <span class="events-each-title">活動場館</span>
                            <span class="events-each-content"><?=$post_meta['_area'][0]?></span>
                        </div>
                    <?php }if($post_meta['_address'][0]!=''){?>
                    <div class="post-events-each">
                        <span class="events-each-title">場館地址</span>
                        <span class="events-each-content"><?=$post_meta['_address'][0]?></span>
                    </div>
                    <?php }if($post_meta['_class'][0]!=''){?>
                    <div class="post-events-each">
                        <span class="events-each-title">活動類型</span>
                        <span class="events-each-content"><?=$post_meta['_class'][0]?></span>
                    </div>
                    <?php }if($post_meta['_host'][0]!=''){?>
                    <div class="post-events-each">
                        <span class="events-each-title">主辦單位</span>
                        <span class="events-each-content"><?=$post_meta['_host'][0]?></span>
                    </div>
                    <?php }if($post_meta['_actor'][0]!=''){?>
                    <div class="post-events-each">
                        <span class="events-each-title">演出團隊</span>
                        <span class="events-each-content"><?=nl2br($post_meta['_actor'][0])?></span>
                    </div>
                    <?php }?>
                </div>
            </div>
            <?php if($post_meta['_cost'][0]=='付費'){?>
            <div class="events-btn">
                <a href="<?=$post_meta['_buy_link'][0]?>" target="_blank"><button type="button" > 購票去</button></a>
            </div>
            <?php }?>
		</div>
        <?php }?>

	<?php endif; ?>

	<div class="post-wrapper-content">
        <div class="section_wrapper clearfix">

            <?php
            // single post navigation | header
            if( ! $single_post_nav['hide-header'] ){
                echo mfn_post_navigation_header( $post_prev, $post_next, $blog_page_id, $translate );
            }
            ?>

            <div class="column one post-header">
                <div class="top-icon">
                    <div class="button-icon-single-area">
                        <div class="button-icon">
                            <div class="button-favorites"><!--收藏按鈕-->
                                <?=get_favorites_button(get_the_ID());?>
                            </div>
                            <div class="button-love"><!--按讚按鈕-->
                                <?=mfn_love(get_the_ID());?>
                            </div>
                            <div class="button-view-count"><!--觀看次數-->
                                <img class="heart-empty-icon" src="<?=wp_get_attachment_url('565')?>">
                                <span class="label"><?=number_format(getPostViews(get_the_ID()));?></span>
                            </div>
                        </div>
                    </div>
                    <?php echo do_shortcode("[addtoany]"); ?>
                </div>

                <div class="title_wrapper">

                    <?php
                    if( mfn_opts_get( 'blog-title' ) ){
                        if( get_post_format() == 'quote'){
                            echo '<blockquote>'. get_the_title() .'</blockquote>';
                        } else {
                            $h = mfn_opts_get( 'title-heading', 1 );
                            echo '<h'. $h .' class="entry-title" itemprop="headline">'. get_the_title() .'</h'. $h .'>';
                        }
                    }
                    ?>

                    <?php
                    if( get_post_format() == 'link'){
                        $link = get_post_meta(get_the_ID(), 'mfn-post-link', true);
                        echo '<a href="'. $link .'" target="_blank">'. $link .'</a>';
                    }
                    ?>

                    <?php if( mfn_opts_get( 'blog-meta' ) ): ?>
                        <div class="post-meta clearfix">
                            <div class="post-main-title">
                                <?=get_the_title()?>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>

            </div>

        </div>


		<?php
			// Content Builder & WordPress Editor Content
			mfn_builder_print( $post->ID );	
		?>

		<div class="section section-post-footer">
			<div class="section_wrapper clearfix">
			
				<div class="column one post-pager">
					<?php
						// List of pages
						wp_link_pages(array(
							'before'			=> '<div class="pager-single">',
							'after'				=> '</div>',
							'link_before'		=> '<span>',
							'link_after'		=> '</span>',
							'next_or_number'	=> 'number'
						));
					?>
				</div>
				
			</div>
		</div>
		

		
		<div class="section section-post-about">
			<div class="section_wrapper clearfix">
			
				<?php if( mfn_opts_get( 'blog-author' ) ): ?>
				<div class="column one author-box">
					<div class="author-box-wrapper">
						<div class="avatar-wrapper">
							<?php 
								global $user;
								echo get_avatar( get_the_author_meta('email'), '64', false, get_the_author_meta('display_name', $user['ID']) );
							?>
						</div>
						<div class="desc-wrapper">
							<h5><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author_meta( 'display_name' ); ?></a></h5>
							<div class="desc"><?php the_author_meta('description'); ?></div>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>	
		</div>
		
	</div>

	<?php if( $post_type == 'custom_blog' ): ?>
		<div class="section section-post-comments">
			<div class="section_wrapper clearfix">
			
				<div class="column one comments">
					<?php comments_template( '', true ); ?>
				</div>
				
			</div>
		</div>
	<?php endif; ?>

</div>
<?php
if($post->post_type == 'events') {
    $event_list = array();
    $events_category = get_terms( array('taxonomy' => 'events_category','hide_empty' => false) );
    foreach ($events_category as $events_categorys){
        array_push($event_list,$events_categorys->term_id);
    }
    $args = array(
        'post_type' => 'events',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => 3,
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'events_category',
                'field' => 'term_id',
                'terms' => $event_list,
            ),
        ));
    $events = new WP_Query($args);
    if($events ->found_posts !='0'){
    $output = '<div class="newest-three-events-in-single">';
    $output .= '<div class="newest-three-post-area">';
    $output .= '<div class="title-bar"><h1>最新活動</h1></div>';
    $output .= '<div class="newest-three-post small-list">';
    foreach($events->get_posts() as $event){
        $post_meta = get_post_meta($event->ID);
        $post_category = get_the_terms( $event->ID, 'events_category' );
        $wpseo_primary_term = new WPSEO_Primary_Term( 'events_category', $event->ID );
        $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
        $Primary_term = get_term( $wpseo_primary_term );
        $category_name = '音樂活動 / ';
        if($Primary_term->name){
            $category_name .= $Primary_term->name;
        }else{
            $category_name .= $post_category[0]->name;
        }
        $output .= '<div  class="post-list-wrap">';
        $output .= '<div class="post-events-img">';
        $output .= '<div class="image_wrapper">';
        $output .= '<a href="'.$event->guid.'">';
        $output .= get_the_post_thumbnail($event->ID,'thumbnail');
        $output .= '</a>';
        $output .= '</div>';
        $output .= '</div>';
        // desc ---------------------------------------------------------------------------
        $output .= '<div class="post-event-desc-wrapper">';
        $output .= '<div class="post-event-desc">';
        // title -------------------------------------
        $output .= '<div class="post-title">';
        // default ----------------------------
        $output .= '<h2 class="entry-title" itemprop="headline"><a href="'. $event->guid .'">'. $event->post_title .'</a></h2>';
        $output .= '<div class="button-icon">';
        $output .= '<div class="button-favorites">';//收藏按鈕
        $output .= get_favorites_button($event->ID);
        $output .= '</div>';
        $output .= '<div class="button-love">';//按讚按鈕
        $output .= mfn_love($event->ID);
        $output .= '</div>';
        $output .= '<div class="button-view-count">';//觀看次數
        $output .= '<img class="heart-empty-icon" src="'.wp_get_attachment_url('565').'">'. number_format(getPostViews($event->ID));
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '<h5 class="second-title" >'.date('Y.m.d',$post_meta['_start_time'][0]).' / '. $category_name .'</h5>';
        $output .= '<div class="post-content">';
        $output .= '<p>活動縣市：'.$post_meta['_city'][0].'</p>';
        $output .= '<p>活動場館：'.$post_meta['_area'][0].'</p>';
        $output .= '<p>活動類型：'.$post_meta['_class'][0].'| 活動對象：'.$post_meta['_person'][0].'| 活動門票：'.(($post_meta['_cost'][0]=='付費')?"付費":"免費").'</p>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
    }
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';

    }
    echo $output;
}else if($post->post_type == 'post' ){
        if($parents->term_id == '36') {
            $title_name = '最新新聞';
            $args = array(
                'post_type' => 'post',
                'orderby' => 'date',
                'order' => 'DESC',
                'post_status ' => 'publish',
                'post__not_in' => array(get_the_ID()),
                'posts_per_page' => 3,
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'category',
                        'field' => 'term_id',
                        'terms' => $cats->term_id,
                    ),
                ));
            $newest_posts = new WP_Query($args);
            if($newest_posts->found_posts != '0'){
            $output = '<div class="newest-three-post-in-single">';
            $output .= '<div class="newest-three-post-area">';
            $output .= '<div class="title-bar"><h1>' . $title_name . '</h1></div>';
            $output .= '<div class="newest-three-post small-list">';
            foreach ($newest_posts->get_posts() as $newest_post) {
                $output .= '<div class="post-list-wrap">';
                $output .= '<div class="post-events-img">';
                $output .= '<div class="image_wrapper">';
                $output .= '<a href="' . $newest_post->guid . '">';
                $output .= get_the_post_thumbnail($newest_post->ID, 'thumbnail');
                $output .= '</a>';
                $output .= '</div>';
                $output .= '</div>';
                // desc ---------------------------------------------------------------------------
                $output .= '<div class="post-event-desc-wrapper">';
                $output .= '<div class="post-event-desc">';
                // title -------------------------------------
                $output .= '<div class="post-title">';
                // default ----------------------------
                $output .= '<h2 class="entry-title" itemprop="headline"><a href="' . $newest_post->guid . '">' . $newest_post->post_title . '</a></h2>';
                $output .= '<div class="button-icon">';
                $output .= '<div class="button-favorites">';//收藏按鈕
                $output .= get_favorites_button($newest_post->ID);
                $output .= '</div>';
                $output .= '<div class="button-love">';//按讚按鈕
                $output .= mfn_love($newest_post->ID);
                $output .= '</div>';
                $output .= '<div class="button-view-count">';//觀看次數
                $output .= '<img class="heart-empty-icon" src="' . wp_get_attachment_url('565') . '">' . number_format(getPostViews($newest_post->ID));
                $output .= '</div>';
                $output .= '</div>';
                $output .= '</div>';
                $output .= '<h5 class="second-title" >' . date('Y.m.d', strtotime($newest_post->post_date)) . ' / 音樂新聞 / ' . $cats->name . '</h5>';
                if (has_excerpt($newest_post->ID)) {
                    $post_excerpt = mb_substr($newest_post->post_excerpt, 0, 40) . '...<a class="read-all" href="' . $newest_post->guid . '">詳全文</a>';
                    $output .= '<div class="post-excerpt">' . $post_excerpt . '</div>';
                }
                $output .= '</div>';
                $output .= '</div>';
                $output .= '</div>';
            }
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            echo $output;
        }
        }
}
?>

