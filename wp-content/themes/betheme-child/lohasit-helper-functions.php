<?php
function lohasit_is_string_contains_chinese($str)
{
    return preg_match("/\p{Han}+/u", $str);
}
function lohasit_get_current_user_role()
{
    $user = wp_get_current_user();
    return array_shift($user->roles);
}