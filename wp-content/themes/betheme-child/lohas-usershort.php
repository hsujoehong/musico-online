<?php

function musico_user_privacy_shortcode_function($atts){ 
    ob_start();
    $id = get_current_user_id();
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $user_privacy = get_user_meta( $id ,'user_privacy')[0];
    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/my-account/edit-account/"><li>會員資料</li></a>
                        <a href="/my-account/edit-privacy/"><li>隱私設定</li></a>
                        <a href="/my-account/favorite/"><li>收藏管理</li></a>
<!--                        <a href="/my-account/user-blog/"><li>網誌管理</li></a>-->
<!--                        <a href="/my-account/bbs-favorite/"><li>交流管理</li></a>-->
                        <a href="/my-account/customer-logout/"><li>登出</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-privacy-section">
            <h3>我的隱私設定</h3>
            <div class="user-privacy-area">
                <div class="user-privacy-list-div">
                    <ul class="favorite-item-list">
                        <li class="favorite-item">
                            <div class="item-title">我的收藏</div>
                            <div class="item-select">
                                <select class="item-display-select" data-type="favorite">
                                    <option <?=($user_privacy['favorite']=='y')?'selected':''?> value="y">公開</option>
                                    <option <?=($user_privacy==''||$user_privacy['favorite']=='n')?'selected':''?> value="n">不公開</option>
                                </select>
                            </div>
                        </li>
<!--                        <li class="favorite-item">-->
<!--                            <div class="item-title">我的網誌</div>-->
<!--                            <div class="item-select">-->
<!--                                <select class="item-display-select" data-type="blog">-->
<!--                                    <option --><?//=($user_privacy['blog']=='y')?'selected':''?><!-- value="y">公開</option>-->
<!--                                    <option --><?//=($user_privacy==''||$user_privacy['blog']=='n')?'selected':''?><!-- value="n">不公開</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                        </li>-->
<!--                        <li class="favorite-item">-->
<!--                            <div class="item-title">我的主題</div>-->
<!--                            <div class="item-select">-->
<!--                                <select class="item-display-select" data-type="topic">-->
<!--                                    <option --><?//=($user_privacy['topic']=='y')?'selected':''?><!-- value="y">公開</option>-->
<!--                                    <option --><?//=($user_privacy==''||$user_privacy['topic']=='n')?'selected':''?><!-- value="n">不公開</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                        </li>-->
<!--                        <li class="favorite-item">-->
<!--                            <div class="item-title">我的回覆</div>-->
<!--                            <div class="item-select">-->
<!--                                <select class="item-display-select" data-type="reply">-->
<!--                                    <option --><?//=($user_privacy['reply']=='y')?'selected':''?><!-- value="y">公開</option>-->
<!--                                    <option --><?//=($user_privacy==''||$user_privacy['reply']=='n')?'selected':''?><!-- value="n">不公開</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_user_favorite_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $id = get_current_user_id();
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $favorites = get_user_meta( $id, 'simplefavorites', true )[0]['posts'];
    $term_id = '';
    $cats = explode('-',$_GET['cats'])[0];
    if($cats == 'post'){
        $term_id = explode('-',$_GET['cats'])[1];
    }

    if($cats != ''&& $cats !='all'){
        $post_type = array($cats);
    }else{
        $post_type = array('events','portfolio','post');
    }
    if($term_id !=''){
        $term_array =array('taxonomy' => 'category','field' => 'term_id','terms' => $term_id);
    }
    $args = array(
        'post_type' => $post_type,
        'post__in'=>$favorites,
        'posts_per_page' => 10,
        'offset'=>(($paged-1)*10),
        'orderby' => 'post__in',
        'tax_query' => array(
            'relation' => 'OR',
            $term_array,
        )
       );
    $favorites_list= new WP_Query( $args );

    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/my-account/edit-account/"><li>會員資料</li></a>
                        <a href="/my-account/edit-privacy/"><li>隱私設定</li></a>
                        <a href="/my-account/favorite/"><li>收藏管理</li></a>
<!--                        <a href="/my-account/user-blog/"><li>網誌管理</li></a>-->
<!--                        <a href="/my-account/bbs-favorite/"><li>交流管理</li></a>-->
                        <a href="/my-account/customer-logout/"><li>登出</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-favorite-section">
            <h3>收藏管理</h3>
            <div class="user-favorite-area">
                <select name="cats" id="favorite-select">
                    <option value="all" <?=($cats=='all'||$cats=='')?'selected':''?>>分類</option>
                    <option value="events" <?=($cats=='events')?'selected':''?>>音樂活動</option>
                    <option value="portfolio" <?=($cats=='portfolio')?'selected':''?>>音樂專欄</option>
                    <option value="post-36" <?=($cats=='post'&&$term_id=='36')?'selected':''?>>音樂新聞</option>
                    <option value="post-37" <?=($cats=='post'&&$term_id=='37')?'selected':''?>>資源媒合</option>
                    <option value="post-38" <?=($cats=='post'&&$term_id=='38')?'selected':''?>>好康分享</option>
                </select>
                <div class="user-favorite-list-div">
                    <table class="user-favorite-list-table">
                        <tr class="user-favorite-list-table-title">
                            <th>標題</th>
                            <th>類別</th>
                            <th>文章日期</th>
                            <th></th>
                        </tr>
                        <?php
                         if($favorites_list->found_posts != '0'){
                        foreach($favorites_list->get_posts() as $posts){
                                    if($posts->post_type == 'events'){
                                        $taxonmy = 'events_category';
                                        $category_name = '音樂活動';
                                    }else if($posts->post_type == 'portfolio'){
                                        $taxonmy = 'portfolio-types';
                                        $category_name = '音樂專欄';
                                    }else if($posts->post_type == 'post'){
                                        $taxonmy = 'category';
                                    }
                                    $post_category = get_the_terms( $posts->ID, $taxonmy );
                                    $wpseo_primary_term = new WPSEO_Primary_Term( $taxonmy, $posts->ID );
                                    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                    if($wpseo_primary_term){//有primary
                                        $Primary_term = get_term($wpseo_primary_term);
                                        if($posts->post_type == 'post'){//type是post(有分上下層)
                                            if($Primary_term->parent == '0'){
                                                $category_name = $Primary_term->name;
                                            }else{
                                                $category_name = get_term($Primary_term->parent)->name.'/'.$Primary_term->name;
                                            }
                                        }else {
                                            $category_name .= '/' . $Primary_term->name;
                                        }
                                    }else{//沒有primary(只有一個分類)
                                        if($post_category[0]->parent == '0'){
                                            $category_name .= '/'. $post_category[0]->name;
                                        }else{
                                            $category_name = get_term($post_category[0]->parent)->name.'/'.$post_category[0]->name;
                                        }
                                    }
                                ?>
                                <tr class="user-favorite-list-table-content">
                                    <td class="favorite-title"><a href="<?=$posts->guid?>"><?=$posts->post_title?></a></td>
                                    <td><?=$category_name?></td>
                                    <td><?=$posts->post_date?></td>
                                    <td><img class="favorited-cancel" data-user="<?=$id?>" data-id="<?=$posts->ID?>" data-type="posts" src="<?=wp_get_attachment_url(918)?>"></td>
                                </tr>
                                <?php
                                }
                            }else{
                                ?>
                                <tr class="user-favorite-list-table-content">
                                    <td colspan="4"> 目前還沒有收藏哦!</td>
                                </tr>
                                <?php
                            }
                            ?>
                    </table>
                </div>
            </div>
            <div class="account-pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => ceil($favorites_list->found_posts/10),
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => '<i class="icon-left-open"></i>',
                    'next_text'    => '<i class="icon-right-open"></i>',
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        </div>
    </div>
<?php
    $myvariable = ob_get_clean();
    return $myvariable;
}


function musico_user_blog_list_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $id = get_current_user_id();
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $args = array(
        'post_type' => 'custom_blog',
        'order' => 'DESC',
        'author' => $id,
        'orderby' => 'date',
        'posts_per_page' => 10,
        'offset'=>(($paged-1)*10),
    );
    $blog_list= new WP_Query( $args );

    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/my-account/edit-account/"><li>會員資料</li></a>
                        <a href="/my-account/edit-privacy/"><li>隱私設定</li></a>
                        <a href="/my-account/favorite/"><li>收藏管理</li></a>
<!--                        <a href="/my-account/user-blog/"><li>網誌管理</li></a>-->
<!--                        <a href="/my-account/bbs-favorite/"><li>交流管理</li></a>-->
                        <a href="/my-account/customer-logout/"><li>登出</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-blog-section">
            <h3>網誌管理</h3>
            <div class="user-blog-area">
                <div id="insert-blog-btn">
                    <a href="/my-account/blog-editor/?action=insert"><button>新增網誌</button></a>
                </div>
                <div class="user-blog-list-div">
                    <table class="user-blog-list-table">
                        <tr class="user-blog-list-table-title">
                            <th>標題</th>
                            <th>文章日期</th>
                            <th>動作</th>
                        </tr>
                        <?php
                        if($blog_list->found_posts != '0'){
                            foreach($blog_list->get_posts() as $posts){
                                ?>
                                <tr class="user-blog-list-table-content">
                                    <td class="blog-title"><a href="<?=$posts->guid?>"><?=$posts->post_title?></a></td>
                                    <td><?=$posts->post_date?></td>
                                    <td>
                                        <div class="user-blog-action">
                                            <a href="/my-account/blog-editor/?action=update&blog_id=<?=$posts->ID?>"><img class="blog-editor" data-user="<?=$id?>" data-id="<?=$posts->ID?>" src="<?=wp_get_attachment_url(1055)?>"></a>
                                            <a href="/my-account/blog-editor/?action=delete&blog_id=<?=$posts->ID?>"><img class="blog-cancel" data-user="<?=$id?>" data-id="<?=$posts->ID?>" src="<?=wp_get_attachment_url(918)?>"></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        }else{
                            ?>
                            <tr class="user-favorite-list-table-content">
                                <td colspan="4"> 目前還沒有網誌哦!</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div class="account-pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => ceil($blog_list->found_posts/10),
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => '<i class="icon-left-open"></i>',
                    'next_text'    => '<i class="icon-right-open"></i>',
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}


function musico_user_bbs_list_shortcode_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $id = get_current_user_id();
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $bbs = bbp_get_user_favorites_topic_ids($id);
    $args = array(
        'post_type' => 'topic',
        'post__in'=>$bbs,
        'orderby' => 'post__in',
        'posts_per_page' => 10,
        'offset'=>(($paged-1)*10),
    );
    $bbs_list= new WP_Query( $args );
    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/my-account/edit-account/"><li>會員資料</li></a>
                        <a href="/my-account/edit-privacy/"><li>隱私設定</li></a>
                        <a href="/my-account/favorite/"><li>收藏管理</li></a>
<!--                        <a href="/my-account/user-blog/"><li>網誌管理</li></a>-->
<!--                        <a href="/my-account/bbs-favorite/"><li>交流管理</li></a>-->
<!--                        <ul id="user-navigation-bbs-list">-->
<!--                            <a href="/my-account/bbs-favorite/"><li class="account-bbs-active">交流收藏</li></a>-->
<!--                            <a href="/my-account/topic/"><li>我的主題</li></a>-->
<!--                            <a href="/my-account/reply/"><li>我的回覆</li></a>-->
<!--                        </ul>-->
                        <a href="/my-account/customer-logout/"><li>登出</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-blog-section">
            <h3>交流收藏</h3>
            <div class="user-blog-area">
                <div class="user-blog-list-div">
                    <table class="user-blog-list-table">
                        <tr class="user-blog-list-table-title">
                            <th>主題</th>
                            <th>討論版</th>
                            <th>文章日期</th>
                            <th>動作</th>
                        </tr>
                        <?php
                        if($bbs_list->found_posts != '0'){
                            foreach($bbs_list->get_posts() as $posts){
                                $forum = get_post($posts->post_parent);
                                ?>
                                <tr class="user-blog-list-table-content">
                                    <td class="blog-title"><a href="<?=$posts->guid?>"><?=$posts->post_title?></a></td>
                                    <td class="blog-title" ><a href="<?=$forum->guid?>"><?=$forum->post_title?></a></td>
                                    <td><?=$posts->post_date?></td>
                                    <td>
                                        <div class="user-blog-action">
                                            <img class="favorited-cancel" data-user="<?=$id?>" data-id="<?=$posts->ID?>" data-type="bbs" src="<?=wp_get_attachment_url(918)?>">
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        }else{
                            ?>
                            <tr class="user-favorite-list-table-content">
                                <td colspan="4"> 目前還沒有收藏哦!</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div class="account-pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => ceil($bbs_list->found_posts/10),
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => '<i class="icon-left-open"></i>',
                    'next_text'    => '<i class="icon-right-open"></i>',
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}


function musico_user_topic_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $id = get_current_user_id();
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $user_privacy = get_user_meta( $id ,'user_privacy')[0];
    $args = array(
        'post_type' => 'topic',
        'author'    => $id,
        'orderby'   => 'date',
        'order'     => 'DESC',
        'posts_per_page' => 10,
        'offset'=>(($paged-1)*10),
    );
    $topic_list= new WP_Query( $args );

    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/my-account/edit-account/"><li>會員資料</li></a>
                        <a href="/my-account/edit-privacy/"><li>隱私設定</li></a>
                        <a href="/my-account/favorite/"><li>收藏管理</li></a>
<!--                        <a href="/my-account/user-blog/"><li>網誌管理</li></a>-->
<!--                        <a href="/my-account/bbs-favorite/"><li>交流管理</li></a>-->
<!--                        <ul id="user-navigation-bbs-list">-->
<!--                            <a href="/my-account/bbs-favorite/"><li>交流收藏</li></a>-->
<!--                            <a href="/my-account/topic/"><li class="account-bbs-active">我的主題</li></a>-->
<!--                            <a href="/my-account/reply/"><li>我的回覆</li></a>-->
<!--                        </ul>-->
                        <a href="/my-account/customer-logout/"><li>登出</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-favorite-section">
            <h3>我的主題</h3>
            <div class="user-favorite-area">
                <div class="user-favorite-list-div">
                    <table class="user-favorite-list-table">
                        <tr class="user-favorite-list-table-title">
                            <th width="45%">標題</th>
                            <th width="25%">討論版</th>
                            <th width="30%" style="text-align: center;">文章日期</th>
                        </tr>
                        <?php
                        if($user_privacy['topic']=='y') {
                            if ($topic_list->found_posts != '0') {
                                foreach ($topic_list->get_posts() as $posts) {
                                    $forum = get_post($posts->post_parent);
                                    ?>
                                    <tr class="user-favorite-list-table-content">
                                        <td width="45%" class="favorite-title"><a
                                                    href="<?= $posts->guid ?>"><?= $posts->post_title ?></a></td>
                                        <td width="25%"><a href="<?=$forum->guid?>"><?= $forum->post_title ?></a></td>
                                        <td width="30%"><?= $posts->post_date ?></td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr class="user-favorite-list-table-content">
                                    <td colspan="3"> 目前還沒有發表主題哦!</td>
                                </tr>
                                <?php
                            }
                        }else{
                            ?>
                            <tr class="user-favorite-list-table-content">
                                <td colspan="3"> 此頁面被設為隱藏哦!</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div class="account-pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => ceil($topic_list->found_posts/10),
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => '<i class="icon-left-open"></i>',
                    'next_text'    => '<i class="icon-right-open"></i>',
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}


function musico_user_reply_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $id = get_current_user_id();
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $user_privacy = get_user_meta( $id ,'user_privacy')[0];
    $args = array(
        'post_type' => 'reply',
        'author'    => $id,
        'orderby'   => 'date',
        'order'     => 'DESC',
        'posts_per_page' => 10,
        'offset'=>(($paged-1)*10),
    );
    $reply_list= new WP_Query( $args );

    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/my-account/edit-account/"><li>會員資料</li></a>
                        <a href="/my-account/edit-privacy/"><li>隱私設定</li></a>
                        <a href="/my-account/favorite/"><li>收藏管理</li></a>
<!--                        <a href="/my-account/user-blog/"><li>網誌管理</li></a>-->
<!--                        <a href="/my-account/bbs-favorite/"><li>交流管理</li></a>-->
<!--                        <ul id="user-navigation-bbs-list">-->
<!--                            <a href="/my-account/bbs-favorite/"><li>交流收藏</li></a>-->
<!--                            <a href="/my-account/topic/"><li>我的主題</li></a>-->
<!--                            <a href="/my-account/reply/"><li class="account-bbs-active">我的回覆</li></a>-->
<!--                        </ul>-->
                        <a href="/my-account/customer-logout/"><li>登出</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-favorite-section">
            <h3>我的回覆</h3>
            <div class="user-favorite-area">
                <div class="user-favorite-list-div">
                    <table class="user-favorite-list-table">
                        <tr class="user-favorite-list-table-title">
                            <th width="45%">主題/回覆</th>
                            <th width="25%">討論版</th>
                            <th width="30%" style="text-align: center;">文章日期</th>
                        </tr>
                        <?php
                        if($user_privacy['topic']=='y') {
                            if ($reply_list->found_posts != '0') {
                                foreach ($reply_list->get_posts() as $posts) {
                                    $topic = get_post($posts->post_parent);
                                    $forum = get_post($topic->post_parent);
                                    ?>
                                    <tr class="user-favorite-list-table-content">
                                        <td width="45%" class="favorite-title">
                                            <p class="topic-title"><span class="topic-title-span">主題</span><a
                                                        href="<?= $topic->guid ?>"><?= $topic->post_title ?></a></p>
                                            <p class="reply-content"><span
                                                        class="reply-content-span">回覆</span><?= ($posts->post_content) ? mb_substr(strip_tags($posts->post_content), 0, 30) : '' ?>
                                            </p>
                                        </td>
                                        <td width="25%"><a href="<?=$forum->guid?>"><?= $forum->post_title ?></a></td>
                                        <td width="30%"><?= $posts->post_date ?></td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr class="user-favorite-list-table-content">
                                    <td colspan="3"> 目前還沒有任何回覆哦!</td>
                                </tr>
                                <?php
                            }
                        }else{
                            ?>
                            <tr class="user-favorite-list-table-content">
                                <td colspan="3"> 此頁面被設為隱藏哦!</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div class="account-pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => ceil($reply_list->found_posts/10),
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => '<i class="icon-left-open"></i>',
                    'next_text'    => '<i class="icon-right-open"></i>',
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_user_new_and_editor_blog_shortcode_function($atts){
    ob_start();

    $id = get_current_user_id();
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $action = $_GET['action'];
    if($action == 'insert'){
    }else if($action == 'update') {
        $blog_id = $_GET['blog_id'];
        $post = get_post($blog_id);
    }else if($action == 'delete'){
        $blog_id = $_GET['blog_id'];
        wp_delete_post($blog_id,true);
        echo '<script>alert("網誌已刪除");window.location="/my-account/user-blog/";</script>';
        die();
    }else{
        echo '<script> window.location="/my-account/user-blog/";</script>';
        die();
    }
    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/my-account/edit-account/"><li>會員資料</li></a>
                        <a href="/my-account/edit-privacy/"><li>隱私設定</li></a>
                        <a href="/my-account/favorite/"><li>收藏管理</li></a>
<!--                        <a href="/my-account/user-blog/"><li>網誌管理</li></a>-->
<!--                        <a href="/my-account/bbs-favorite/"><li>交流管理</li></a>-->
                        <a href="/my-account/customer-logout/"><li>登出</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-blog-section">
            <h3>新增/修改網誌</h3>
            <div class="user-blog-area">
                <input type="hidden" name="blog_action" id="blog_action" value="<?=$action?>" data-post="<?=$blog_id?>">
                <div class="user-blog-div">
                        <div class="user-blog-Feature-img">
                            <input type="hidden" name="feature_img" id="feature_img_input" value="<?=$blog_id?>">
                            <?php if(get_the_post_thumbnail_url($blog_id)!=''){?>
                                <img id="feature_img_src" src="<?=get_the_post_thumbnail_url($blog_id);?>">
                                <p>點擊圖片更換</p>
                            <?php }else{?>
                                <img id="feature_img_src" src="">
                                <button type="button">上傳封面圖</button>
                            <?php }?>
                        </div>
                        <div class="user-blog-title">
                        <input type="text" name="title" id="title" value="<?=$post->post_title?>" placeholder="網誌標題">
                        </div>
                        <?=wp_editor( $post->post_content, 'blog_content', array( 'media_buttons' => true,'default_editor'=>'tinymce', 'tinymce' => true,'quicktags'=>false) );?>
                    </div>
                    <button type="button" name="submit-blog" id="submit-blog">送出網誌</button>
            </div>
        </div>
    </div>

    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}


function musico_other_user_page_function($atts){
    ob_start();
    $id = $_GET['user_id'];
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $user_description = get_user_meta( $id ,'description')[0];
    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/musician/favorites/?user_id=<?=$id?>"><li><?=$user_nickname?>的收藏</li></a>
                        <a href="/musician/blog/?user_id=<?=$id?>"><li><?=$user_nickname?>的網誌</li></a>
                        <a href="/musician/topic/?user_id=<?=$id?>"><li><?=$user_nickname?>的主題</li></a>
                        <a href="/musician/reply/?user_id=<?=$id?>"><li><?=$user_nickname?>的回覆</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-favorite-section">
            <h3>自我介紹</h3>
            <div class="user-favorite-area">
                <div class="user-favorite-list-div">
                    <?=($user_description)?nl2br($user_description):'這個用戶沒有填寫自我介紹哦！'?>
                </div>
            </div>
        </div>
    </div>

    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}

function musico_other_user_favorite_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $id = $_GET['user_id'];
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $favorites = get_user_meta( $id, 'simplefavorites', true )[0]['posts'];
    $term_id = '';
    $cats = explode('-',$_GET['cats'])[0];
    if($cats == 'post'){
        $term_id = explode('-',$_GET['cats'])[1];
    }
    $user_privacy = get_user_meta( $id ,'user_privacy')[0];
    if($cats != ''&& $cats !='all'){
        $post_type = array($cats);
    }else{
        $post_type = array('events','portfolio','post');
    }
    if($term_id !=''){
        $term_array =array('taxonomy' => 'category','field' => 'term_id','terms' => $term_id);
    }
    $args = array(
        'post_type' => $post_type,
        'post__in'=>$favorites,
        'orderby' => 'post__in',
        'posts_per_page' => 10,
        'offset'=>(($paged-1)*10),
        'tax_query' => array(
            'relation' => 'OR',
            $term_array,
        )
    );
    $favorites_list= new WP_Query( $args );

    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/musician/favorites/?user_id=<?=$id?>"><li><?=$user_nickname?>的收藏</li></a>
                        <a href="/musician/blog/?user_id=<?=$id?>"><li><?=$user_nickname?>的網誌</li></a>
                        <a href="/musician/topic/?user_id=<?=$id?>"><li><?=$user_nickname?>的主題</li></a>
                        <a href="/musician/reply/?user_id=<?=$id?>"><li><?=$user_nickname?>的回覆</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-favorite-section">
            <h3><?=$user_nickname?>的收藏</h3>
            <div class="user-favorite-area">
                <select name="cats" id="other-favorite-select">
                    <option value="all" <?=($cats=='all'||$cats=='')?'selected':''?>>分類</option>
                    <option value="events" <?=($cats=='events')?'selected':''?>>音樂活動</option>
                    <option value="portfolio" <?=($cats=='portfolio')?'selected':''?>>音樂專欄</option>
                    <option value="post-36" <?=($cats=='post'&&$term_id=='36')?'selected':''?>>音樂新聞</option>
                    <option value="post-37" <?=($cats=='post'&&$term_id=='37')?'selected':''?>>資源媒合</option>
                    <option value="post-38" <?=($cats=='post'&&$term_id=='38')?'selected':''?>>好康分享</option>
                </select>
                <div class="user-favorite-list-div">
                    <table class="user-favorite-list-table">
                        <tr class="user-favorite-list-table-title">
                            <th width="45%">標題</th>
                            <th width="25%">類別</th>
                            <th width="30%" style="text-align: center;">文章日期</th>
                        </tr>
                        <?php
                        if($user_privacy['favorite']=='y'){
                            if($favorites_list->found_posts != '0'){
                                foreach($favorites_list->get_posts() as $posts){
                                    if($posts->post_type == 'events'){
                                        $taxonmy = 'events_category';
                                        $category_name = '音樂活動';
                                    }else if($posts->post_type == 'portfolio'){
                                        $taxonmy = 'portfolio-types';
                                        $category_name = '音樂專欄';
                                    }else if($posts->post_type == 'post'){
                                        $taxonmy = 'category';
                                    }
                                    $post_category = get_the_terms( $posts->ID, $taxonmy );
                                    $wpseo_primary_term = new WPSEO_Primary_Term( $taxonmy, $posts->ID );
                                    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                    if($wpseo_primary_term){//有primary
                                        $Primary_term = get_term($wpseo_primary_term);
                                        if($posts->post_type == 'post'){//type是post(有分上下層)
                                            if($Primary_term->parent == '0'){
                                                $category_name = $Primary_term->name;
                                            }else{
                                                $category_name = get_term($Primary_term->parent)->name.'/'.$Primary_term->name;
                                            }
                                        }else {
                                            $category_name .= '/' . $Primary_term->name;
                                        }
                                    }else{//沒有primary(只有一個分類)
                                        if($post_category[0]->parent == '0'){
                                            $category_name .= '/'. $post_category[0]->name;
                                        }else{
                                            $category_name = get_term($post_category[0]->parent)->name.'/'.$post_category[0]->name;
                                        }
                                    }
                                    ?>
                                    <tr class="user-favorite-list-table-content">
                                        <td width="45%" class="favorite-title"><a href="<?=$posts->guid?>"><?=$posts->post_title?></a></td>
                                        <td width="25%"><?=$category_name?></td>
                                        <td width="30%" ><?=$posts->post_date?></td>
                                    </tr>
                                    <?php
                                }
                            }else{
                                ?>
                                <tr class="user-favorite-list-table-content">
                                    <td colspan="3"> 目前還沒有收藏哦!</td>
                                </tr>
                                <?php
                            }
                        }else{
                            ?>
                            <tr class="user-favorite-list-table-content">
                                <td colspan="3"> 此頁面被設為隱藏哦!</td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
            <div class="account-pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => ceil($favorites_list->found_posts/10),
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => '<i class="icon-left-open"></i>',
                    'next_text'    => '<i class="icon-right-open"></i>',
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}


function musico_other_user_blog_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $id = $_GET['user_id'];
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $user_privacy = get_user_meta( $id ,'user_privacy')[0];
    $args = array(
        'post_type' => 'custom_blog',
        'author'    => $id,
        'orderby'   => 'date',
        'order'     => 'DESC',
        'posts_per_page' => 10,
        'offset'=>(($paged-1)*10),
    );
    $blog_list= new WP_Query( $args );

    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/musician/favorites/?user_id=<?=$id?>"><li><?=$user_nickname?>的收藏</li></a>
                        <a href="/musician/blog/?user_id=<?=$id?>"><li><?=$user_nickname?>的網誌</li></a>
                        <a href="/musician/topic/?user_id=<?=$id?>"><li><?=$user_nickname?>的主題</li></a>
                        <a href="/musician/reply/?user_id=<?=$id?>"><li><?=$user_nickname?>的回覆</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-favorite-section">
            <h3><?=$user_nickname?>的網誌</h3>
            <div class="user-favorite-area">
                <div class="user-favorite-list-div">
                    <table class="user-favorite-list-table">
                        <tr class="user-favorite-list-table-title">
                            <th width="45%">標題</th>
                            <th width="25%">類別</th>
                            <th width="30%" style="text-align: center;">文章日期</th>
                        </tr>
                        <?php
                            if($user_privacy['blog']=='y') {
                                if ($blog_list->found_posts != '0') {
                                    foreach ($blog_list->get_posts() as $posts) {
                                        ?>
                                        <tr class="user-favorite-list-table-content">
                                            <td width="45%" class="favorite-title"><a href="<?= $posts->guid ?>"><?= $posts->post_title ?></a>
                                            </td>
                                            <td width="25%"> 網誌</td>
                                            <td width="30%"><?= $posts->post_date ?></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr class="user-favorite-list-table-content">
                                        <td colspan="3"> 目前還沒有網誌哦!</td>
                                    </tr>
                                    <?php
                                }
                            }else{
                                ?>
                                <tr class="user-favorite-list-table-content">
                                    <td colspan="3"> 此頁面被設為隱藏哦!</td>
                                </tr>
                                <?php
                            }
                        ?>
                    </table>
                </div>
            </div>
            <div class="account-pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => ceil($blog_list->found_posts/10),
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => '<i class="icon-left-open"></i>',
                    'next_text'    => '<i class="icon-right-open"></i>',
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}


function musico_other_user_topic_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $id = $_GET['user_id'];
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $user_privacy = get_user_meta( $id ,'user_privacy')[0];
    $args = array(
        'post_type' => 'topic',
        'author'    => $id,
        'orderby'   => 'date',
        'order'     => 'DESC',
        'posts_per_page' => 10,
        'offset'=>(($paged-1)*10),
    );
    $topic_list= new WP_Query( $args );

    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/musician/favorites/?user_id=<?=$id?>"><li><?=$user_nickname?>的收藏</li></a>
                        <a href="/musician/blog/?user_id=<?=$id?>"><li><?=$user_nickname?>的網誌</li></a>
                        <a href="/musician/topic/?user_id=<?=$id?>"><li><?=$user_nickname?>的主題</li></a>
                        <a href="/musician/reply/?user_id=<?=$id?>"><li><?=$user_nickname?>的回覆</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-favorite-section">
            <h3><?=$user_nickname?>的主題</h3>
            <div class="user-favorite-area">
                <div class="user-favorite-list-div">
                    <table class="user-favorite-list-table">
                        <tr class="user-favorite-list-table-title">
                            <th width="45%">標題</th>
                            <th width="25%">討論版</th>
                            <th width="30%" style="text-align: center;">文章日期</th>
                        </tr>
                        <?php
                        if($user_privacy['topic']=='y') {
                            if ($topic_list->found_posts != '0') {
                                foreach ($topic_list->get_posts() as $posts) {
                                    $forum = get_post($posts->post_parent);
                                    ?>
                                    <tr class="user-favorite-list-table-content">
                                        <td width="45%" class="favorite-title"><a
                                                    href="<?= $posts->guid ?>"><?= $posts->post_title ?></a></td>
                                        <td width="25%"><a href="<?=$forum->guid?>"><?= $forum->post_title ?></a></td>
                                        <td width="30%"><?= $posts->post_date ?></td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr class="user-favorite-list-table-content">
                                    <td colspan="3"> 目前還沒有發表主題哦!</td>
                                </tr>
                                <?php
                            }
                        }else{
                            ?>
                            <tr class="user-favorite-list-table-content">
                                <td colspan="3"> 此頁面被設為隱藏哦!</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div class="account-pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => ceil($topic_list->found_posts/10),
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => '<i class="icon-left-open"></i>',
                    'next_text'    => '<i class="icon-right-open"></i>',
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}


function musico_other_user_reply_function($atts){
    ob_start();
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $id = $_GET['user_id'];
    $user_img = get_user_meta( $id ,'pcg_custom_gravatar')[0];
    $user_nickname = get_user_meta( $id ,'nickname')[0];
    $user_privacy = get_user_meta( $id ,'user_privacy')[0];
    $args = array(
        'post_type' => 'reply',
        'author'    => $id,
        'orderby'   => 'date',
        'order'     => 'DESC',
        'posts_per_page' => 10,
        'offset'=>(($paged-1)*10),
    );
    $reply_list= new WP_Query( $args );

    ?>
    <div class="woocommerce lohasit-customer-woocommerce">
        <div class="user-navigation-section">
            <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>" src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
            <div class="user-name"><?=$user_nickname?></div>
            <div class="user-code">會員編號：<?=$id?></div>
            <div>
                <nav class="user-navigation-list">
                    <ul>
                        <a href="/musician/favorites/?user_id=<?=$id?>"><li><?=$user_nickname?>的收藏</li></a>
                        <a href="/musician/blog/?user_id=<?=$id?>"><li><?=$user_nickname?>的網誌</li></a>
                        <a href="/musician/topic/?user_id=<?=$id?>"><li><?=$user_nickname?>的主題</li></a>
                        <a href="/musician/reply/?user_id=<?=$id?>"><li><?=$user_nickname?>的回覆</li></a>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="user-favorite-section">
            <h3><?=$user_nickname?>的主題</h3>
            <div class="user-favorite-area">
                <div class="user-favorite-list-div">
                    <table class="user-favorite-list-table">
                        <tr class="user-favorite-list-table-title">
                            <th width="45%">主題/回覆</th>
                            <th width="25%">討論版</th>
                            <th width="30%" style="text-align: center;">文章日期</th>
                        </tr>
                        <?php
                        if($user_privacy['topic']=='y') {
                            if ($reply_list->found_posts != '0') {
                                foreach ($reply_list->get_posts() as $posts) {
                                    $topic = get_post($posts->post_parent);
                                    $forum = get_post($topic->post_parent);
                                    ?>
                                    <tr class="user-favorite-list-table-content">
                                        <td width="45%" class="favorite-title">
                                            <p class="topic-title"><span class="topic-title-span">主題</span><a
                                                        href="<?= $topic->guid ?>"><?= $topic->post_title ?></a></p>
                                            <p class="reply-content"><span
                                                        class="reply-content-span">回覆</span><?= ($posts->post_content) ? mb_substr(strip_tags($posts->post_content), 0, 30) : '' ?>
                                            </p>
                                        </td>
                                        <td width="25%"><a href="<?=$forum->guid?>"><?= $forum->post_title ?></a></td>
                                        <td width="30%"><?= $posts->post_date ?></td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr class="user-favorite-list-table-content">
                                    <td colspan="3"> 目前還沒有任何回覆哦!</td>
                                </tr>
                                <?php
                            }
                        }else{
                            ?>
                            <tr class="user-favorite-list-table-content">
                                <td colspan="3"> 此頁面被設為隱藏哦!</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div class="account-pagination">
                <?php
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => ceil($reply_list->found_posts/10),
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => '<i class="icon-left-open"></i>',
                    'next_text'    => '<i class="icon-right-open"></i>',
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
                ?>
            </div>
        </div>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}


function musico_resend_register_mail_function($atts){
    ob_start();
    if($_POST['flag']=='true'){
        $login = $_POST['user_login'];
        $email = $_POST['user_email'];
        $user_login = get_user_by('login',$login);
        $user_email = get_user_by('email',$email);
        if($user_login->ID != $user_email->ID){
            wc_add_notice('帳號或E-MAIL錯誤','error');
        }else{
            $token = get_user_meta($user_login->ID,'_token','single');
            if($token =='pass'&&$token!=''){
                wc_add_notice('該帳號已通過註冊!請由<a href="/my-account/">登入頁面</a>進行登入!','error');
            }else{
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < 20; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                update_user_meta($user_login->ID, '_token', $randomString);
                $mailer = WC()->mailer();
                $mails = $mailer->get_emails();
                $message = $mails['WC_Email_Customer_New_Account'];
                $message->trigger( $user_login->ID );
                wc_add_notice('已重發認證信!請至信箱收取','success');
            }
        }
        wc_print_notices();
    }
    ?>
    <div class="resend-register-section">
        <form method="post" class="resend_register">
            <input type="hidden" name="flag" value="true" />
            <p> 重發認證信，請輸入註冊時的帳號與email，來重發認證信</p>

            <p class="resend-register-FormInput">
                <label for="user_login">帳號</label>
                <input class="resend-register-Input" type="text" name="user_login" id="user_login" />
            </p>
            <p class="resend-register-FormInput">
                <label for="user_email">E-Mail</label>
                <input class="resend-register-Input" type="text" name="user_email" id="user_email" />
            </p>

            <p class="woocommerce-FormRow form-row">
                <input type="submit" class="resend-register-Button" value="發送認證信" />
            </p>

        </form>
    </div>
    <?php
    $myvariable = ob_get_clean();
    return $myvariable;
}