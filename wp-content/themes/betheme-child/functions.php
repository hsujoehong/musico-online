<?php

/**
 * 引入壓板自訂功能的php
 */
require_once __DIR__ . "/lohasit-helper-functions.php";
require_once __DIR__ . "/lohasit-custom.php";

/* ---------------------------------------------------------------------------
 * Child Theme URI | DO NOT CHANGE
 * --------------------------------------------------------------------------- */
define( 'CHILD_THEME_URI', get_stylesheet_directory_uri() );


/* ---------------------------------------------------------------------------
 * Define | YOU CAN CHANGE THESE
 * --------------------------------------------------------------------------- */

// White Label --------------------------------------------
define( 'WHITE_LABEL', false );

// Static CSS is placed in Child Theme directory ----------
define( 'STATIC_IN_CHILD', true );


/* ---------------------------------------------------------------------------
 * Enqueue Style
 * --------------------------------------------------------------------------- */
add_action( 'wp_enqueue_scripts', 'mfnch_enqueue_styles', 101 );
function mfnch_enqueue_styles() {
	
	// Enqueue the parent stylesheet
// 	wp_enqueue_style( 'parent-style', get_template_directory_uri() .'/style.css' );		//we don't need this if it's empty
	
	// Enqueue the parent rtl stylesheet
	if ( is_rtl() ) {
		wp_enqueue_style( 'mfn-rtl', get_template_directory_uri() . '/rtl.css' );
	}
	
	// Enqueue the child stylesheet
	wp_dequeue_style( 'style' );
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() .'/style.css' );
	
}


/* ---------------------------------------------------------------------------
 * Load Textdomain
 * --------------------------------------------------------------------------- */
add_action( 'after_setup_theme', 'mfnch_textdomain' );
function mfnch_textdomain() {
    load_child_theme_textdomain( 'betheme',  get_stylesheet_directory() . '/languages' );
    load_child_theme_textdomain( 'mfn-opts', get_stylesheet_directory() . '/languages' );
}
/**
 * WordPress 媒體庫只顯示用戶自己上傳的文件
 * https://www.wpdaxue.com/view-user-own-media-only.html
 */
//在文章编辑頁面的[新增媒體]只顯示用戶自己上傳的文件
function my_upload_media( $wp_query_obj ) {
    global $current_user, $pagenow;
    if( !is_a( $current_user, 'WP_User') )
        return;
    if( 'admin-ajax.php' != $pagenow || $_REQUEST['action'] != 'query-attachments' )
        return;
    if( !current_user_can( 'manage_options' ) && !current_user_can('manage_media_library') )
        $wp_query_obj->set('author', $current_user->ID );
    return;
}
add_action('pre_get_posts','my_upload_media');

//在[媒體庫]只顯示用戶自己上傳的文件
function my_media_library( $wp_query ) {
    if ( strpos( $_SERVER[ 'REQUEST_URI' ], '/wp-admin/upload.php' ) !== false ) {
        if ( !current_user_can( 'manage_options' ) && !current_user_can( 'manage_media_library' ) ) {
            global $current_user;
            $wp_query->set( 'author', $current_user->id );
        }
    }
}
add_filter('parse_query', 'my_media_library' );


/**
 * 文章觀看次數取得 & 修改
 */
//取得文章觀看數
function getPostViews($postID){
    $count = get_post_meta($postID,'views', true);
    if($count==''){
        delete_post_meta($postID,'views');
        add_post_meta($postID,'views', '0');
        return '0';
    }
    return $count.'';
}
//修改文章觀看數(進頁面後才更新)
function setPostViews($postID) {
    $count = get_post_meta($postID,'views', true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID,'views');
        add_post_meta($postID,'views', '0');
    }else{
        $count++;
        update_post_meta($postID,'views', $count);
    }
}


?>