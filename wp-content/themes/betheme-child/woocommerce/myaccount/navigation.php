<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

do_action( 'woocommerce_before_account_navigation' );
$id = get_current_user_id();
$img = get_user_meta( $id , 'pcg_custom_gravatar' ,true);
$user_img = get_user_meta( $id ,'pcg_custom_gravatar',true);
$user_nickname = get_user_meta( $id ,'nickname')[0];
$favorites = get_user_meta( $id, 'simplefavorites', true )[0]['posts'];

?>
<div class="user-navigation-section">
    <div class="user-image"><img id="user-gravatar-image" data-user="<?=$id?>"  src="<?=($user_img)? wp_get_attachment_url($user_img) : wp_get_attachment_url(950);?>"></div>
    <div class="user-name"><?=$user_nickname?></div>
    <div class="user-code">會員編號：<?=$id?></div>
    <div>
        <nav class="user-navigation-list">
            <ul>
                <a href="/my-account/edit-account/"><li>會員資料</li></a>
                <a href="/my-account/edit-privacy/"><li>隱私設定</li></a>
                <a href="/my-account/favorite/"><li>收藏管理</li></a>
<!--                <a href="/my-account/user-blog/"><li>網誌管理</li></a>-->
<!--                <a href="/my-account/bbs-favorite/"><li>交流管理</li></a>-->
                <a href="/my-account/customer-logout/"><li>登出</li></a>
            </ul>
        </nav>
    </div>
</div>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
