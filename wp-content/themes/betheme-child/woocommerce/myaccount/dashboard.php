<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="user-dashboard-div">

    <p>
        您好 <strong><?=$current_user->display_name?></strong> (如果您不是 <strong><?=$current_user->display_name?></strong> <a href="<?= esc_url( wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ) )?>">請登出</a>)
    </p>
    <p>
        在您帳號中，您可以查看近期的<a href="/my-account/favorite/">收藏</a>以及<a href="/my-account/edit-account/">修改帳戶資料及密碼</a>。
<!--        在您帳號中，您可以查看近期的<a href="/my-account/favorite/">收藏</a>、 管理<a href="/my-account/user-blog/">網誌</a>以及<a href="/my-account/edit-account/">修改帳戶資料及密碼</a>。-->
    </p> 

    <p class="notice-to-customer">
        身為一位良好的網路公民，請注意禮貌，勿惡意攻擊他人，上傳或傳播具色情、暴力、血腥內容之圖片與文字。本站為維持美好的網站閱聽氛圍及公序良俗，一經發現以上惡意內容，將主動刪除，不另行通知。
    </p>
    <p> <a href="/privacy-policy/">隱私權政策</a> | <a href="/terms-of-service/">服務條款</a> </p>
</div>
<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );
?>
