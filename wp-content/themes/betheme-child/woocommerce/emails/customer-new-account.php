<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


$user = get_user_by('login',$user_login);
$token = get_user_meta($user->ID,'_token','single');
?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

	<p><?php printf( __( 'Thanks for creating an account on %s. Your username is <strong>%s</strong>', 'woocommerce' ), esc_html( $blogname ), esc_html( $user_login ) ); ?></p>

    <p>請點擊連結來通過驗證 : <a href="<?=home_url('/my-account/').'?source=register&login='.$user_login.'&token='.$token?>"> <?=home_url('/my-account/').'?source=register&login='.$user_login.'&token='.$token?> </a> </p>
    <p>(如果無法點擊連結，請將連結複製至網誌列)</p>


<?php do_action( 'woocommerce_email_footer', $email );
