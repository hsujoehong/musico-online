<?php
// 超商地圖
require_once ( '../../../wp-load.php' );

$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
$chosen_shipping = $chosen_methods[0]; // cvs_fami_shipping or cvs_unimart_shipping

list($prefix, $sub_type, $suffix) = explode('_', $chosen_shipping);

$sub_type = strtoupper($sub_type).'C2C';

if (version_compare(get_woo_version(), '2.6.0') == 1) {
	$settings = get_shipping_option( $chosen_shipping );
} else {
	$settings = get_option( 'woocommerce_'.$chosen_shipping.'_settings' );
}

if($settings['ecpay_merchant_id'] == '2000933') {
    $map_api = 'https://logistics-stage.ecpay.com.tw/Express/map';
} else {
    $map_api = 'https://logistics.ecpay.com.tw/Express/map';
}

$Device = (wp_is_mobile())?1:0;

$args = array(
	'MerchantID' => $settings['ecpay_merchant_id'],
	'MerchantTradeNo' => uniqid(),
	'LogisticsType' => 'CVS',
	'LogisticsSubType' => $sub_type,
	// 'IsCollection' => 'N',
	'ServerReplyURL' => plugins_url('cvs-result.php', __FILE__),
	'ExtraData' => $chosen_shipping,
    'Device' => $Device,
);
?>
<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript">
        function submitForm() {
            document.forms['allpay_form'].submit();
        }
    </script>
</head>
<body onload="submitForm();">
	<form name="allpay_form" action="<?php echo $map_api?>" method="POST">
		<?php foreach($args as $name => $val):?>
		<input type="hidden" name="<?php echo $name?>" value="<?php echo $val?>">
		<?php endforeach?>
	</form>
</body>
</html>
