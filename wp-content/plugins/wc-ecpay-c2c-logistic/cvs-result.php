<?php

require_once ( '../../../wp-load.php' );

WC()->session->set('ecpay_map_result', $_POST);

$result = WC()->session->get('ecpay_map_result');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Result</title>
    <style type="text/css">
        h2 {
            margin: 200px auto 20px;
            text-align: center;
        }
        div.notice {
            border: 5px solid #ccc;
            background-color: #efefef;
            padding: 20px;
            margin: 0 auto;
            width: 630px;
            text-align: center;
        }
    </style>	
	<script src="https://code.jquery.com/jquery-1.7.2.min.js"></script>
</head>
<body>
	
	<input type='hidden' id='_checkout_url' value='<?php echo WC_Cart::get_checkout_url();?>'></div>
<div class="notice">
	請稍候!正在處理中...
	<?php foreach($result as $name => $value): ?>
	<input type="hidden" id="<?php echo $name; ?>" name="<?php echo $name; ?>" value="<?php echo $value; ?>">
	<?php endforeach; ?>
</div>

<script>
	
window.opener.jQuery('.store-info').show();
window.opener.jQuery('#CVSStoreName').html(jQuery('#CVSStoreName').val());
window.opener.jQuery('#CVSAddress').html(jQuery('#CVSAddress').val());
window.opener.jQuery('#CVSTelephone').html(jQuery('#CVSTelephone').val());
var storeInfo = jQuery('#CVSAddress').val()+jQuery('#CVSStoreName').val()+jQuery('#CVSStoreID').val();
window.opener.jQuery('#order_comments').val(storeInfo);
<?php if ($result['ExtraData'] === 'cvs_unimart_shipping'):?>
window.opener.jQuery('#telephone').hide();
window.opener.jQuery('#CVSTelephone').hide();
<?php endif?>
	
	setTimeout(function() {

		if(isFacebookApp() || isLineApp()){
			window.location=jQuery('#_checkout_url').val()+'?mr=1';
		}else{
			window.close();
		}

	}, 2000);
	function isFacebookApp() {
		var ua = navigator.userAgent || navigator.vendor || window.opera;
		return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1);
	}
	function isLineApp() {
		var ua = navigator.userAgent || navigator.vendor || window.opera;
		return (ua.indexOf("Line") > -1);
	}
</script>
</body>
</html>