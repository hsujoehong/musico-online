<?php

class WC_Shipping_Cvs_Unimart extends Shipping_Ecpay_Cvs
{
    public $min_amount;

    public $requires;

    public function __construct($instance_id = 0)
    {
        $this->id                 = 'cvs_unimart_shipping';
        $this->method_title       = '超商取貨(統一)';
        $this->method_description = '7-ELEVEN(交貨便)';
        $this->cost               = $this->get_option('cost');
        $this->availability       = $this->get_option('availability');
        $this->countries          = $this->get_option('countries');
        $this->requires           = $this->get_option('requires');
        $this->free_cost          = $this->get_option('free_cost');
        // $this->free_shipping_items = $this->get_option('free_shipping_items');


            $this->instance_id = absint($instance_id);

            $this->enabled = $this->get_option('enabled');


        parent::__construct($instance_id);
        $this->title = $this->get_option('title', $this->method_title);
    }

    public function calculate_shipping($package = array())
    {
        global $woocommerce;

        $free_cost = $this->get_option('free_cost');

        $cost = ($woocommerce->cart->cart_contents_total >= $free_cost && $free_cost != '')
            ? 0
            : $this->get_option('cost');

        $rate = array(
            'id'       => $this->id,
            'label'    => $this->title,
            'cost'     => $cost,
            'calc_tax' => 'per_item',
        );

        // Register the rate
        $this->add_rate($rate);
    }
}
