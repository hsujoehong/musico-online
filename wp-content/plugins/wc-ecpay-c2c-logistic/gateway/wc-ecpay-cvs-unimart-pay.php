<?php

class Wc_Ecpay_Cvs_Unimart_Pay extends Gateway_Ecpay_Cvs 
{
	public function __construct() 
	{
		parent::__construct();			

		$this->id = 'ecpay_cvs_unimart_pay';
		$this->icon = apply_filters( 'wc_ecpay_cvs_fami_icon', '' );		
		$this->method_title = __('超商取貨付款(統一)', 'woocommerce');
		$this->max_amount = '19999';


		$this->init_form_fields();
		$this->init_settings();			

		$this->title = $this->settings['title'];
		$this->description = $this->settings['description'];	

        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
        add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page') );  //需與id名稱大小寫相同
        add_action( 'woocommerce_receipt_' . $this->id, array( $this, 'receipt_page') );	
	}
}