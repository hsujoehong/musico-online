<?php

class Shipping_Ecpay_Cvs extends WC_Shipping_Method
{
    public function __construct($instance_id = 0)
    {
        $this->init_form_fields();
        $this->init_settings();

        add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
    }

    protected function name($id)
    {
        $default_name = array(
            'cvs_unimart_shipping' => '超商取貨(統一)',
            'cvs_fami_shipping'    => '超商取貨(全家)',
        );
        return $default_name[$id];
    }

    public function init_form_fields()
    {
        $form_fields = array(
            'enabled'                => array(
                'title'   => __('Enable', 'woocommerce'),
                'type'    => 'checkbox',
                'label'   => __('Enable', 'woocommerce') . $this->name($this->id),
                'default' => 'no',
            ),
            'title'                  => array(
                'title'    => __('Title', 'woocommerce'),
                'type'     => 'text',
                // 'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
                'default'  => $this->title,
                'desc_tip' => true,
            ),
            'availability'           => array(
                'title'   => __('Method availability', 'woocommerce'),
                'type'    => 'select',
                'default' => 'all',
                'class'   => 'availability wc-enhanced-select',
                'options' => array(
                    'all'      => __('All allowed countries', 'woocommerce'),
                    'specific' => __('Specific Countries', 'woocommerce'),
                ),
            ),
            'countries'              => array(
                'title'             => __('Specific Countries', 'woocommerce'),
                'type'              => 'multiselect',
                'class'             => 'wc-enhanced-select',
                'css'               => 'width: 450px;',
                'default'           => '',
                'options'           => WC()->countries->get_shipping_countries(),
                'custom_attributes' => array(
                    'data-placeholder' => __('Select some countries', 'woocommerce'),
                ),
            ),
            'cost'                   => array(
                'title'   => __('費用', 'woocommerce'),
                'type'    => 'text',
                // 'description' => __('運送費用', 'woocommerce' ),
                'default' => '60',
            ),
            'free_cost'              => array(
                'title'   => __('訂單金額多少以上免運費'),
                'type'    => 'text',
                'default' => '1000',
            ),
            // 'free_shipping_items' =>array(
            //     'title' =>__('訂購數量多少以上免運費'),
            //     'type' => 'number',
            //     'default' => '10'
            // ),
            'ecpay_merchant_id'      => array(
                'title'       => __('商店代號', 'woocommerce'),
                'type'        => 'text',
                'default'     => '2000933',
                'description' => __('特店編號(MerchantID)', 'woocommerce'),
            ),
            'ecpay_hash_key'         => array(
                'title'       => __('HashKey', 'woocommerce'),
                'type'        => 'text',
                'default'     => 'XBERn1YOvpM9nfZc',
                'description' => __('All in one 介接的 HashKey', 'woocommerce'),
            ),
            'ecpay_hash_iv'          => array(
                'title'       => __('HashIV', 'woocommerce'),
                'type'        => 'text',
                'default'     => 'h1ONHk4P4yqbl5LK',
                'description' => __('All in one 介接的 HashIV', 'woocommerce'),
            ),
            'ecpay_sender_name'      => array(
                'title'       => __('寄件人', 'woocommerce'),
                'type'        => 'text',
                'default'     => '購物網',
                'description' => '必填',
            ),
            'ecpay_sender_cellphone' => array(
                'title'       => __('寄件人手機', 'woocommerce'),
                'type'        => 'text',
                'default'     => '0988888888',
                'description' => '必填',
            ),
        );
            $this->form_fields = $form_fields;

    }
}
