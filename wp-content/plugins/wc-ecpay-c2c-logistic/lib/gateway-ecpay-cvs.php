<?php

class Gateway_Ecpay_Cvs extends WC_Payment_Gateway 
{
	public function __construct() 
	{
			
	}	

	// 後台設置
	public function init_form_fields() 
	{  
		$this->form_fields = array(
			'enabled' => array(
				'title' => __('啟用/關閉', 'woocommerce'),
				'type' => 'checkbox',
				'label' => __('啟用 ', 'woocommerce') . $this->method_title,
				'default' => 'no'
			),
			'title' => array(
			    'title' => __('標題', 'woocommerce'),
			    'type' => 'text',
			    'description' => __('客戶在結帳時所看到的標題', 'woocommerce'),
			    'default' => $this->method_title
			),
			'description' => array(
			    'title' => __('客戶訊息', 'woocommerce'),
			    'type' => 'textarea',
			    'description' => __('', 'woocommerce'),
			    'default' => $this->method_title
			), 
			'ecpay_min_amount' => array(
			    'title' => __('最低商品金額', 'woocommerce'),
			    'type' => 'text',
			    'description' => __('最低商品金額 1', 'woocommerce'),
			    'default' => '1'
			),
			'ecpay_max_amount' => array(
			    'title' => __('最高商品金額', 'woocommerce'),
			    'type' => 'text',
			    'description' => __('最高商品金額 '. $this->max_amount, 'woocommerce'),
			    'default' => $this->max_amount,
			)	         
    	);
	}		

	public function process_payment( $order_id )
	{
		$order = new WC_Order($order_id);		
		
		// Mark as on-hold (we're awaiting the cheque)
		// $order->update_status( 'processing', '訂單處理中' );

	    // unset( $_SESSION['order_awaiting_payment'] );		

		    $method_title = get_post_meta( $order_id, '_payment_method_title', true );
		 $order->add_order_note( $this->method_title, true );
		
		return array(
			'result' => 'success',
			// 'redirect' => $order->get_checkout_payment_url(true)
			'redirect' => $this->get_return_url( $order )
		);
	}

	public function thankyou_page( $order_id )
	{
		global $woocommerce;
		wc_get_order($order_id)->update_status('processing');
		$woocommerce->cart->empty_cart();	
	}
}