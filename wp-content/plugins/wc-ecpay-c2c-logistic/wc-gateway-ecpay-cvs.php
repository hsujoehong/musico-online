<?php

class Wc_Gateway_Ecpay_Cvs
{
	private static $_instance = null;
	private $methods = array();

	public static function forge()
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new self;
		}

		return self::$_instance;
	}

	private function __construct() 
	{	
		// register_activation_hook( __FILE__, array( $this, 'plugin_activated' ) );
		// register_deactivation_hook( __FILE__, array( $this, 'plugin_deactivated' ) );

		add_action ( 'plugins_loaded', array( $this, 'init_gateway_ecpay_cvs' ) );	
		add_filter( 'woocommerce_available_payment_gateways', array( $this, 'ecpay_cvs_available_payment_gateways' ) );		
	}	

	// public function plugin_activated()
	// {

	// }

	// public function plugin_deactivated()
	// {

	// }

	public function init_gateway_ecpay_cvs()
	{
		if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;

		if ( ! class_exists( 'Gateway_Ecpay_Cvs') ) {
			require_once( dirname(__FILE__) . '/lib/gateway-ecpay-cvs.php' );
		}

		$gateway_path = plugin_dir_path( __FILE__ ) . 'gateway/';

		$classes = glob( $gateway_path . 'wc-ecpay-cvs-*.php' );

		if ( ! $classes ) return;

		foreach ( $classes as $class ) {
			$class_name =  basename( str_replace( '-', '_', $class), '.php');
			if ( ! class_exists( $class_name ) ) {
				require_once( $class );
				$this->methods[] = $class_name;
			}			
		}
		add_filter( 'woocommerce_payment_gateways', array( $this, 'add_ecpay_cvs_gateway' ) );		
	}

	public function add_ecpay_cvs_gateway( $methods )
	{
	    $methods = array_merge( $methods, $this->methods );
	    return $methods;
	}

	public function ecpay_cvs_available_payment_gateways( $gateways )
	{
		global $woocommerce;
		$items = $woocommerce->cart->cart_contents;
		$items = array_pop( $items );

		$subtotal = $items['line_subtotal'];

		foreach ( $gateways as $gateway ) {
			$gateway->chosen = 0;
			
			if ( strstr( $gateway->id, 'ecpay_cvs_' ) ) {
				$settings = get_option( 'woocommerce_'.$gateway->id.'_settings' );
				if ( $subtotal > $settings['ecpay_max_amount'] ) {
					unset( $gateways[$gateway->id] );
				}
			}
		}

		$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
 		$chosen_shipping = $chosen_methods[0]; 	

		// 配送方式為超商取貨(統一)
 		if ($chosen_shipping == 'cvs_unimart_shipping') { 
 			unset( $gateways['ecpay_cvs_fami_pay'] );
 		}
 		// 配送方式為超商取貨(全家)
 		if ($chosen_shipping == 'cvs_fami_shipping') { 
 			unset( $gateways['ecpay_cvs_unimart_pay']);
 		}
 		// 配送方式為其它配送方式
 		if ($chosen_shipping != 'cvs_unimart_shipping' 
 			AND $chosen_shipping != 'cvs_fami_shipping'
 		) { 
 			unset( $gateways['ecpay_cvs_fami_pay'] ); 			
 			unset( $gateways['ecpay_cvs_unimart_pay']);			
 		}

		return $gateways;
	}
}

Wc_Gateway_Ecpay_Cvs::forge();
