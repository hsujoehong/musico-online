<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Google OAuth</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>

<?php
    if (session_status() == PHP_SESSION_NONE) session_start();
    
    require( $_SERVER["DOCUMENT_ROOT"] . '/wp-load.php' );

    $clientID = get_option( 'google_oauth_client_id');
    $clientSecret = get_option( 'google_oauth_client_secret');

    if($clientID==='Oauth_ClientId' || $clientSecret==='Oauth_ClientSecret'){
        ?>
        <div style="padding-top: 40vh; color:#666; text-align:center;">
            <h3 style="color:#d03226;">OAuth Config Not Found</h3>
        </div>
        <?php

        exit();
    }

    require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';
    $client = new Google_Client;
    $client->setClientId($clientID);
    $client->setClientSecret($clientSecret);
    $client->setRedirectUri("https://".$_SERVER['HTTP_HOST']."/?lohas-sso=1");
    $client->addScope(['https://www.googleapis.com/auth/userinfo.email','https://www.googleapis.com/auth/userinfo.profile']);
?>

<?php

if (isset($GLOBALS['google_auth_code'])){
    $result = $client->authenticate($GLOBALS['google_auth_code']);

    if(!isset($result['error'])){
		$oauth2 = new \Google_Service_Oauth2($client);
        $userInfo = $oauth2->userinfo->get();

        // Only account with domain 'lohaslife.cc' is autherized
        $email_domain = explode("@",$userInfo->email)[1];
        if($email_domain!=="lohaslife.cc"){
            header('HTTP/1.0 403 Forbidden');
            ?>
            <div style="padding-top: 40vh;">
                <h3 style="text-align:center; color:#666;">Your Email: "<?= $userInfo->email ?>" is unauthorized</h3>
            </div>
            <?php
            exit();
        }

        $wp_user = get_user_by( 'email', $userInfo->email );

        //  Create User if not exist
        if(!$wp_user){
            $username = explode("@",$userInfo->email)[0];
            $user_id = wp_create_user( 
                'lohaslife.'.$username, 
                wp_generate_password( 16, false ), 
                $userInfo->email 
            );

            if($user_id){
                $wp_user = get_user_by( 'id', $user_id );
                $wp_user->remove_role( 'subscriber' );
                $wp_user->set_role( 'administrator' );
                wp_update_user([
                    'ID'        =>  $wp_user->ID,
                    'first_name'=>  $userInfo->familyName,
                    'last_name' =>  $userInfo->givenName,
                    'display_name'  => "LohasIT.".$username
                ]);
            }else{
                echo "User create failed";
            }
        }

        if($wp_user){
            wp_set_current_user($wp_user->ID);
            wp_set_auth_cookie($wp_user->ID);
            do_action( 'wp_login', $wp_user->user_login );

            add_user_meta( $wp_user->ID, '_lohasit_admin', true);

            $user_meta=get_userdata($wp_user->ID);
            $current_role = ( array ) $user_meta->roles;
            $current_role = reset($current_role);
?>
        <div class="container p-5 text-center">
            <div class="d-inline-block jumbotron text-left w-75">
                <h4><span class="font-weight-light">Welcome!</span><br><?= $wp_user->display_name ?></h4>
                <form action="/?lohas-sso=1&set_role=1" method="post">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="roleSelect">選擇角色</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="role" id="role">
                                <?php
                                    foreach ($wp_roles->roles as $key => $value) {
                                        $name = translate_user_role($value['name'] );
                                        $selected = ($current_role==$key) ? "selected" : "";
                                        echo '<option value="'.$key.'" '.$selected.'>'.$name.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <input class="btn btn-primary float-right" type="submit" value="設定">
                </form>
            </div>
        </div>
<?php
        }
	}
}elseif($_GET['set_role']==1){
    $user = wp_get_current_user();

    $is_lohasit_admin = get_user_meta($user->ID, '_lohasit_admin', true);

    if($is_lohasit_admin){
        $user_meta=get_userdata($user->ID);
        
        $role = $_POST['role'];
        if($user instanceof WP_User && isset($wp_roles->roles[$role])){
            foreach ($user->roles as $key => $role_key) {
                $user->remove_role($role_key);
            }
            $user->set_role($role);
            wp_redirect("/wp-admin");
        }
    }
}else{
    header("Location: ".str_replace('&approval_prompt=auto','',$client->createAuthUrl()).'&prompt=consent');
}

?>
</body>
</html>