<?php
/**
 * Plugin Name: Lohasit Google OAuth
 * Description: 樂活壓板系統 - SSO登入外掛
 * Version: 0.0.1
 * Author: Lohas IT
 * Author URI: https://www.lohaslife.cc/
 */

 class LohasItGoogleOauth{
    private static $instance;
    
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) session_start();

        add_action('init', array($this, 'set_sso_url'));
        add_action('template_redirect', array($this, 'lohas_sso_template') );
        add_action('admin_bar_menu', array($this, 'admin_role_switcher'), 99999);
        add_action('admin_menu', array($this, 'set_side_menu'));

        if(get_option( 'google_oauth_client_id')===false){
            add_option( 'google_oauth_client_id','');
        }
        if(get_option( 'google_oauth_client_secret')===false){
            add_option( 'google_oauth_client_secret','');
        }
    }

    public function set_sso_url()
    {
        GLOBAL $google_auth_code, $google_auth_scope;

        if(isset($_GET['code']) && isset($_GET['scope'])){
            $google_auth_code = $_GET['code'];
            $google_auth_scope = $_GET['scope'];
        }
        add_rewrite_endpoint('lohas-sso', EP_ROOT);
        flush_rewrite_rules();
    }

    public function lohas_sso_template()
    {
        global $wp_query, $wp_rewrite, $wp_roles;

        if(!isset( $wp_query->query['lohas-sso'] )){
            return;
        }

        include dirname( __FILE__ ) . '/wp-sso.php';

        exit();
    }

    public static function get_instance()
    {
        if(is_null(self::$instance))
            self::$instance = new self;
        return self::$instance;
    }

    public function set_side_menu()
    {
        $user = wp_get_current_user();
        $is_lohasit_admin = get_user_meta($user->ID, '_lohasit_admin', true);

        $client_id = get_option( 'google_oauth_client_id');
        $client_secret = get_option( 'google_oauth_client_secret');

        if(current_user_can('administrator') || $_SESSION['admin_switched']==$user->ID){
            if($is_lohasit_admin || (empty($client_id) || empty($client_secret))){
                add_menu_page('Google OAuth', 'Google OAuth', 'administrator', 'google_oauth', array($this, 'setting_page'),'dashicons-admin-network');
            }
            
            add_submenu_page(null, 'Switch Role', 'Switch Role', 'read', 'switch_role', array($this, 'switch_role'));
        }else{
            return;
        }

    }

    public function switch_role()
    {
        GLOBAL $wp_roles;

        if (session_status() == PHP_SESSION_NONE) session_start();

        $user = wp_get_current_user();
        $user_role = reset($user->roles);
        $is_admin = current_user_can('administrator');

        if ( $user_role=='administrator' || $_SESSION['admin_switched']==$user->ID) {
            $_SESSION['admin_switched']=$user->ID;
            $role = $_GET['role'];
            if($user instanceof WP_User && isset($wp_roles->roles[$role])){
                foreach ($user->roles as $key => $role_key) {
                    $user->remove_role($role_key);
                }
                $user->set_role($role);
            }
        }
        exit();
    }

    public function setting_page()
    {
        if (session_status() == PHP_SESSION_NONE) session_start();
        
        $user = wp_get_current_user();

        $client_id = get_option( 'google_oauth_client_id');
        $client_secret = get_option( 'google_oauth_client_secret');

        $has_oauth_config = ((!empty($client_id)|| !empty($client_secret))) ? true : false;

        if(!get_user_meta($user->ID, '_lohasit_admin', true)){
            if(!current_user_can('administrator') && !$has_oauth_config){
                
                header('HTTP/1.0 403 Forbidden');
                exit();
            }
        }

        if($_SERVER['REQUEST_METHOD']==='POST') {
            $nonce = $_REQUEST['_wpnonce'];
            if (wp_verify_nonce( $nonce, 'google_oauth_api' ) ) {
                update_option( 'google_oauth_client_id', $_POST['client_id'] );
                update_option( 'google_oauth_client_secret', $_POST['client_secret'] );
            }
        }
        
        $google_oauth_client_id = get_option( 'google_oauth_client_id');
        $google_oauth_client_secret = get_option( 'google_oauth_client_secret');
        
        include_once('setting_page.php');
    }

    public function admin_role_switcher()
    {
        GLOBAL $wp_admin_bar;

        $user = wp_get_current_user();
        $user_role = reset($user->roles);
        $is_admin = current_user_can('administrator');

        if ( (current_user_can('administrator') || $_SESSION['admin_switched']==$user->ID)) {
            $_SESSION['admin_switched']=$user->ID;
            $args = array(
                'id'    => 'admin-role-switcher',
                'title' => self::get_role_switcher($user_role),
                'meta'  => [
                    'class' => 'admin-role-switcher'
                ]
            );
            $wp_admin_bar->add_node( $args );
            wp_enqueue_script( 'admin_role_switcher', plugins_url('lohasit-google-oauth/assets/admin_role_switcher.js'), array ( 'jquery' ), null, true);
        }
    }

    private function get_role_switcher($selected)
    {
        if ( ! function_exists( 'get_editable_roles' ) ) {
            require_once ABSPATH . 'wp-admin/includes/user.php';
        }

        $role_options = "";        
        $editable_roles = array_reverse( get_editable_roles() );
        foreach ( $editable_roles as $role => $details ) {
            $name = translate_user_role($details['name'] );
            if ( $selected == $role ) {
                $role_options .= "<option selected='selected' value='" . esc_attr( $role ) . "'>$name</option>";
            } else {
                $role_options .= "<option value='" . esc_attr( $role ) . "'>$name</option>";
            }
        }

        $switcher = '<div style="padding: 0 10px;display: inline-block;">'.
                        '切換角色：'.
                        '<select name="admin_role_change" id="adminSwitchRole" style="display: inline;">'.
                            $role_options.
                        '</select>'.
                    '</div>';
        return $switcher;
    }
 }

$GOAuth = new LohasItGoogleOauth();