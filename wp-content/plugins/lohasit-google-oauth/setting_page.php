<div class="wrap">
    <h1>Lohasit Google OAuth</h1>
    <div class="welcome-panel" style="display: block; width: 700px;">
        <div class="main">
            <form action="" method="post">
                <table>
                    <tbody>
                        <tr class="input-text-wrap">
                            <td>Client ID:</td>
                            <td><input type="text" name="client_id" autocomplete="off" size="80" value="<?= $google_oauth_client_id ?>"></td>
                        </tr>
                        <tr class="input-text-wrap">
                            <td>Client Secret:</td>
                            <td><input type="text" name="client_secret" autocomplete="off" size="80" value="<?= $google_oauth_client_secret ?>"></td>
                        </tr>
                    </tbody>
                </table>
                
                <p class="submit">	
                    <?php wp_nonce_field('google_oauth_api'); ?>
                    <input type="submit" name="save" id="save-post" class="button button-primary" value="儲存">			
                    <br class="clear">
                </p>
            </form>
        </div>
    </div>
</div>