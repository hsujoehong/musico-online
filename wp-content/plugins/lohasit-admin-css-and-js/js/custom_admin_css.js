
var editor = ace.edit("customCss");
editor.setTheme("ace/theme/monokai");
editor.getSession().setMode("ace/mode/css");

var editor_js = ace.edit("customJs");
editor_js.setTheme("ace/theme/monokai");
editor_js.getSession().setMode("ace/mode/javascript");




jQuery(document).ready(function($){
	
	var code = editor.getValue();
	
	var updateCss = function(){
		$('#admin_css').val( editor.getSession().getValue() );
	}

	$('#save-custom-admin-css-form').submit(updateCss);



	var code_js = editor_js.getValue();
	
	var updateJs = function(){
		$('#admin_js').val( editor_js.getSession().getValue() );
	}

	$('#save-custom-admin-js-form').submit(updateJs);

});