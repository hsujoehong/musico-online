<?php
/*
 * Plugin Name: Lohasit Admin CSS And JS
 * Description: 樂活壓板系統 - 可加入CSS與JS至後台
 * Version: 1.0.0
 * Author: Lohas IT
 * Author URI: https://www.lohaslife.cc/
*/

function load_ace($hook){
	if ( 'settings_page_admin_css' == $hook ) {
		wp_enqueue_style( 'ace', plugins_url() . '/custom-admin-css/css/custom_admin_css.ace.css', array(), '1.0.0', 'all' );
		wp_enqueue_script('ace', plugins_url() . '/custom-admin-css/js/ace/ace.js', array('jquery'), '1.2.1', true );
		wp_enqueue_script('custom-admin-css-script', plugins_url() . '/custom-admin-css/js/custom_admin_css.js', array('jquery'), '1.0.0', true );

	}
}
add_action( 'admin_enqueue_scripts', 'load_ace' );



add_action('admin_menu', 'wpdocs_my_custom_admin_css_submenu_page');
  
function wpdocs_my_custom_admin_css_submenu_page() {
    add_submenu_page(
        'options-general.php',
        '自訂後台CSS/JS',
        '自訂後台CSS/JS',
        'manage_options',
        'admin_css',
        'custom_admin_css' );

    add_action( 'admin_init', 'custom_admin_css_settings' );
}

function custom_admin_css_settings(){
	register_setting( 'custom-admin-css-options', 'admin_css', 'sanitize_custom_css' );
	register_setting( 'custom-admin-js-options', 'admin_js', 'sanitize_custom_js' );

	add_settings_section( 'custom-admin-css-section', '', 'custom_admin_css_section_callback', 'admin_css' );
	add_settings_section( 'custom-admin-js-section', '', 'custom_admin_js_section_callback', 'admin_js' );

	add_settings_field( 'custom-admin-css', '後台CSS', 'custom_css_callback', 'admin_css', 'custom-admin-css-section' );
	add_settings_field( 'custom-admin-js', '後台Javascript', 'custom_js_callback', 'admin_js', 'custom-admin-js-section' );
}

function sanitize_custom_css( $input ){
	$output = esc_textarea( $input );
	return $output;
}
function sanitize_custom_js( $input ){
	$output = esc_textarea( $input );
	return $output;
}


function custom_admin_css_section_callback(){
	//echo 'Customize Admin CSS';
	echo '後台的 body 中加上了「role-角色」的 class，可在 css 前加上「.role-角色」的 class 做篩選'; 
}
function custom_admin_js_section_callback(){
	echo '請以「jQuery」取代一般的「$」<br>'.
			'或可使用「jQuery(document).ready(function($){	//coding 	});」，裡面可用一般的「$」';
}


function custom_css_callback(){
	$custom_admin_CSS = get_option( 'admin_css' );
	$custom_admin_CSS = ( empty($custom_admin_CSS) ? '/* Custom Admin CSS */' : $custom_admin_CSS);
	echo '<div id="customCss">'.$custom_admin_CSS.'</div><textarea id="admin_css" style="display:none;visibility:hidden;" name="admin_css">'.$custom_admin_CSS.'</textarea>';
}
function custom_js_callback(){
	$custom_admin_JS = get_option( 'admin_js' );
	$custom_admin_JS = ( empty($custom_admin_JS) ? '/* Custom Admin JS */' : $custom_admin_JS);

	echo '<div id="customJs">'.$custom_admin_JS.'</div><textarea id="admin_js" style="display:none;visibility:hidden;" name="admin_js">'.$custom_admin_JS.'</textarea>';
}




function custom_admin_css() { ?>
	<h1>自定後台CSS與JavaScript</h1>
	<?php //settings_errors(); ?>

	<form method="post" action="options.php" id="save-custom-admin-css-form">
		<?php settings_fields( 'custom-admin-css-options' ); ?>
		<?php do_settings_sections( 'admin_css' ); ?>
		<?php submit_button(); ?>
	</form>
	<form method="post" action="options.php" id="save-custom-admin-js-form">
		<?php settings_fields( 'custom-admin-js-options' ); ?>
		<?php do_settings_sections( 'admin_js' ); ?>
		<?php submit_button(); ?>
	</form>
<?php } 


//admin css /js
function admin_style() {
	$custom_admin_CSS = get_option( 'admin_css' );
	
	echo '<style>'.htmlspecialchars_decode($custom_admin_CSS, ENT_QUOTES).'</style>';
	
}
add_action('admin_enqueue_scripts', 'admin_style');

add_action('in_admin_footer', 'foot_admin_js');
function foot_admin_js () {
	$custom_admin_JS = get_option( 'admin_js' );
	echo '<script>'.htmlspecialchars_decode($custom_admin_JS, ENT_QUOTES).'</script>';
}



?>