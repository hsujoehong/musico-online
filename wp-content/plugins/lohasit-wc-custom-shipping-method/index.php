<?php
/*
 * Plugin Name: Lohasit WC Custom Shipping Method
 * Description: 樂活壓板系統 - Woocommerce - 自定義物流
 * Version: 1.0.0
 * Author URI: https://www.lohaslife.cc/
 * Author: Lohas IT
*/

 

$active_plugins2 = apply_filters('active_plugins', get_option('active_plugins'));

if (in_array('woocommerce/woocommerce.php', $active_plugins2)) {
    require_once __DIR__ . '/wc-my-home.php';
	require_once __DIR__ . '/wc-lohas2017-home.php';
	require_once __DIR__ . '/wc-lohas2018-home.php';
	require_once __DIR__ . '/wc-lohas2019-home.php';
	 require_once __DIR__ . '/wc-your-home.php';
	  require_once __DIR__ . '/wc-they-home.php';
    

}
