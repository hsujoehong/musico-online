<?php

// ini_set('display_errors', 1);
// error_reporting(-1);

require_once '../../../wp-load.php';

global $ecpay_home;

if (isset($_GET['id']) && !empty($_GET['id'])) {
    $data      = $ecpay_home->run_package_request($_GET['id'],$_GET['size']);
    $ecpay_url = $data['ecpay_url'];
    unset($data['ecpay_url']);
    include __DIR__ . '/template/process.php';
    //echo '<script>window.opener.location.reload();setTimeout(function() {window.close();}, 2000);</script>';
} else {
    ksort($_POST);

    if ($_POST['RtnCode'] == '300' and $ecpay_home->check_value($_POST)) {
        
        $order_id = $_POST['MerchantTradeNo'];

        $order = new WC_Order($order_id);

        if (get_post_meta($order_id, 'ecpay_logistics', true) == '') {
            $note = sprintf('物流編號:%s ', $_POST['AllPayLogisticsID']);
            $order->add_order_note($note, true);
            add_post_meta($order_id, 'ecpay_logistics', json_encode($_POST));
        } else {
            $order->add_order_note($_POST['RtnMsg']);
        }
        echo '<script>window.opener.location.reload();setTimeout(function() {window.close();}, 2000);</script>';
    }
}
