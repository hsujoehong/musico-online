<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * WC_Report_Stock.
 *
 * @author      WooThemes
 * @category    Admin
 * @package     WooCommerce/Admin/Reports
 * @version     2.1.0
 */
class WC_Report_Stock extends WP_List_Table {

    /**
     * Max items.
     *
     * @var int
     */
    protected $max_items;

    /**
     * Constructor.
     */
    public function __construct() {

        parent::__construct( array(
            'singular'  => __( 'Stock', 'woocommerce' ),
            'plural'    => __( 'Stock', 'woocommerce' ),
            'ajax'      => false
        ) );
    }

    /**
     * No items found text.
     */
    public function no_items() {
        _e( 'No products found.', 'woocommerce' );
    }

    /**
     * Don't need this.
     *
     * @param string $position
     */
    public function display_tablenav( $position ) {

        if ( $position != 'top' ) {
            parent::display_tablenav( $position );
        }
    }

    public function set_large_record()
    {
        return 2147483647;
    }

    /**
     * Output the report.
     */
    public function output_report() {
        if(isset($_POST['action']) && $_POST['action'] === "export") {
            add_filter('woocommerce_admin_stock_report_products_per_page', array($this, 'set_large_record'));
            $this->prepare_items();
            $data_header = $this->get_columns();
            unset($data_header['wc_actions']);
            $data_set = [];
            foreach($this->items as $item) {
                ob_start();
                $this->column_default($item, 'product');
                $title = wp_strip_all_tags(str_replace('<br>', "\n", ob_get_clean()));

                ob_start();
                $this->column_default($item, 'parent');
                $parent = wp_strip_all_tags(ob_get_clean());

                ob_start();
                $this->column_default($item, 'stock_status');
                $stock_status = wp_strip_all_tags(ob_get_clean());

                ob_start();
                $this->column_default($item, 'stock_level');
                $stock_level = ob_get_clean();

                $data = [$title, $parent, $stock_level, $stock_status];
                array_push($data_set, $data);
            }
            ob_start();
            $stream = fopen('php://output' ,'w');
            fputs($stream, ( chr(0xEF) . chr(0xBB) . chr(0xBF) ) );
            fputcsv($stream, array_values($data_header));
            foreach ($data_set as $data ) {
                fputcsv($stream, array_values($data));
            }
            $csv = ob_get_clean();
            fclose($stream);
            ob_end_clean();

            $time = current_time('mysql');
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=庫存{$time}.csv");
            header("Pragma: no-cache");
            header("Expires: 0");

            echo $csv;
            exit;

        }
        $this->prepare_items();
        ?>

        <form action="" method="post">
            <input type="hidden" name="action" value="export">
            <input type="submit" value="匯出">
        </form>
        <?php
        echo '<div id="poststuff" class="woocommerce-reports-wide">';
        $this->display();
        echo '</div>';
    }

    /**
     * Get column value.
     *
     * @param mixed $item
     * @param string $column_name
     */
    public function column_default( $item, $column_name) {
        global $product;

        if ( ! $product || $product->id !== $item->id ) {
            $product = wc_get_product($item->id);
        }

        $output = '';
        switch( $column_name ) {
            case 'product' :
                if($product->parent != 0)
                    $output .= __('Name', 'woocommerce') . '：' . $product->get_title() . "<br>";
                else
                    $output .= __('Name', 'woocommerce') . '：' . '<a href="' . get_edit_post_link($product->get_id()) . '" target="_blank">' . $product->get_title() . '</a>' ."<br>";

                if ( $sku = $product->get_sku() ) {
                    $output .= __('SKU', 'woocommerce') . '：' . $sku . '<br>';
                }
                // Get variation data
                if ( $product->is_type( 'variation' ) ) {
                    $list_attributes = array();
                    $attributes = $product->get_variation_attributes();
                    foreach ( $attributes as $name => $attribute ) {
                        $name = str_replace( 'attribute_', '', $name );
                        $term = get_term_by('slug', $attribute, urldecode($name));
                        $name = str_replace( 'pa_', '', $name );
                        $attribute_name= $term-> name;
                        if(!$term) {
                            $attribute_name= get_post_meta($product->variation_id, 'attribute_'. $name, true);
                        }

                        $list_attributes[] = urldecode(wc_attribute_label( $name )) . '：<strong>' . $attribute_name . '</strong>';
                    }

                    $output .= implode( ', ', $list_attributes );
                }
                break;

            case 'parent' :
                if ( $item->parent ) {
                    $output .= '<a href="'. get_edit_post_link($item->parent) .'" target="_blank">' . get_the_title( $item->parent ) . '</a>';
                } else {
                    $output .= '-';
                }
                break;

            case 'stock_status' :
                if ( $product->is_in_stock() ) {
                    $output .= '<mark class="instock">' . __( 'In stock', 'woocommerce' ) . '</mark>';
                } else {
                    $output .= '<mark class="outofstock">' . __( 'Out of stock', 'woocommerce' ) . '</mark>';
                }
                $output = apply_filters( 'woocommerce_admin_stock_html', $output, $product );
                break;

            case 'stock_level' :
                $output .= $product->get_stock_quantity();
                break;

            case 'wc_actions' :
                ?><p>
                <?php
                $actions = array();
                $action_id = $product->is_type( 'variation' ) ? $item->parent : $item->id;

                $actions['edit'] = array(
                    'url'       => admin_url( 'post.php?post=' . $action_id . '&action=edit' ),
                    'name'      => __( 'Edit', 'woocommerce' ),
                    'action'    => "edit"
                );

                if ( $product->is_visible() ) {
                    $actions['view'] = array(
                        'url'       => get_permalink( $action_id ),
                        'name'      => __( 'View', 'woocommerce' ),
                        'action'    => "view"
                    );
                }

                $actions = apply_filters( 'woocommerce_admin_stock_report_product_actions', $actions, $product );

                foreach ( $actions as $action ) {
                    $output .= sprintf( '<a class="button tips %s" href="%s" data-tip="%s ' . __( 'product', 'woocommerce' ) . '">%s</a>', $action['action'], esc_url( $action['url'] ), esc_attr( $action['name'] ), esc_attr( $action['name'] ) );
                }
                ?>
                </p><?php
                break;
        }
        echo $output;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    public function get_columns() {

        $columns = array(
            'product'      => __( 'Product', 'woocommerce' ),
            'parent'       => __( 'Parent', 'woocommerce' ),
            'stock_level'  => __( 'Units in stock', 'woocommerce' ),
            'stock_status' => __( 'Stock status', 'woocommerce' ),
            'wc_actions'   => __( 'Actions', 'woocommerce' ),
        );

        return $columns;
    }

    /**
     * Prepare customer list items.
     */
    public function prepare_items() {

        $this->_column_headers = array( $this->get_columns(), array(), $this->get_sortable_columns() );
        $current_page          = absint( $this->get_pagenum() );
        $per_page              = apply_filters( 'woocommerce_admin_stock_report_products_per_page', 20 );

        $this->get_items( $current_page, $per_page );

        /**
         * Pagination.
         */
        $this->set_pagination_args( array(
            'total_items' => $this->max_items,
            'per_page'    => $per_page,
            'total_pages' => ceil( $this->max_items / $per_page )
        ) );

    }
}
