<?php
/*
Plugin Name: Wp Show Payment
Plugin URI: http://wordpress.org/plugins/
Description:  Wp Show Payment 訂單列表在總計顯示付款方式
Author: ymlin
Version: 1.0
Author URI: http://ma.tt/
Text Domain:  show-payment
*/
class Show_Payment {
    private static $instance;
    public function __construct() {
        load_plugin_textdomain('show-payment',false,basename(dirname( __FILE__ )).'/languages');
        $this->includes();
        $this->hooks();
    }
    private function hooks() {


        add_action('manage_shop_order_posts_custom_column', array($this, 'wc_shop_custom_column'),10,2);
    }
    private function includes() {

    }
    public static function get_instance() {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    public function wc_shop_custom_column($column) {
        global  $post;
        $order = new WC_Order($post->ID);

        if($column == 'order_total'){
            $order_id = $order->id;
            $payment=get_post_meta($order_id,'ecpay_payment',true);

            if($payment == 'Credit'){

                _e( 'Credit', 'show-payment' );
            }
            else if($payment == 'WebATM'){
                _e( 'WebATM', 'show-payment' );

            }
            else if($payment == 'ATM'){
                _e( 'ATM', 'show-payment' );

            }
            else if($payment == 'CVS'){
                _e( 'CVS', 'show-payment' );

            }
            else if($payment == 'BARCODE'){
                _e( 'BARCODE', 'show-payment' );

            }
        }
    }
}
add_action('plugins_loaded' ,array('Show_Payment', 'get_instance'));
?>