<?php

/**
* Share Locker
*/
class Share_Locker
{   
    public function __construct()
    {
        load_plugin_textdomain('share-locker', false, basename(dirname(__FILE__)) . '/languages');
        
        add_action('init', array($this, 'init'));    
        add_action('admin_init', array($this, 'adminCss'));
        add_action('admin_menu', array($this, 'createAdminMenu'));
        add_action('wp_ajax_share_locker', array($this, "ajax"));
		add_action('wp_ajax_nopriv_share_locker', array($this, "ajax"));

        add_shortcode("share-locker", array($this, 'shortcodeFunc'));
    }
    
    public function adminCss()
    {
        wp_register_style('share_locker_admin_css', SHARE_LOCKER_URL . '/css/admin.css');
    }
    
    public function adminCssLoad()
    {
        wp_enqueue_style('share_locker_admin_css');
    }
    
    public function createAdminMenu()
    {
    	$settings = add_menu_page("Share Locker", "Share Locker", 'administrator', 'share-locker-settings', array($this, 'settingsPage'), SHARE_LOCKER_URL . '/images/icon16.png');
        $documentation = add_submenu_page("share-locker-settings", __("Documentation", 'share_locker'), __("Documentation", 'share_locker'), 'administrator', 'share-locker-documentation', array($this, 'documentationPage'));
        
    	add_action('admin_init', array($this, 'registerSettings'));
    	add_action('admin_print_styles-' . $settings, array($this, 'adminCssLoad'));
    	add_action('admin_print_styles-' . $documentation, array($this, 'adminCssLoad'));
    }
    
    public function registerSettings()
    {
    	register_setting('share-locker-settings-group', 'share-locker');
    	
    	add_option('share-locker', array(
    	    'default-message' => 'Share to unlock content!',
    	    'facebook-enabled' => '1',
    	    'facebook-colorscheme' => 'light',
    	    'twitter-enabled' => '1',
    	    'twitter-default-tweet' => 'Check out this post',
    	    'google-enabled' => '1',
    		"default-theme" => "gray",
    		"message" => 'Share to unlock content!',
    		'facebook-share-enabled' => '1',
    		"facebook-app-id" => '',
    		"load-twitter" => '1',
    		"load-google" => '1',
            "load-facebook" => '1',
            "init-facebook" => '1',
            "vk-share-enabled" => '0',
            "vk-app-id" => '',
            
    	));
    }
    
    public function settingsPage()
    {
        require(SHARE_LOCKER_PATH . '/settings_template.php');
    }
    public function documentationPage()
    {
        require(SHARE_LOCKER_PATH . '/documentation_template.php');
    }
    
    public function init()
    {
        $options = get_option('share-locker');
        
        $locale = get_locale();
        if (!$locale) $locale = 'en_US';
        
        if (!is_admin()) {
            
            wp_enqueue_script('jquery');
            
            if (($options['facebook-enabled'] || $options['facebook-share-enabled']) && $options['load-facebook']) {
    			add_action("wp_footer", array($this, "loadFacebookSDK"));
    		}
    		
    		if ($options['twitter-enabled'] && $options['load-twitter']) {
    			add_action("wp_head", array($this, "loadTwitterJS"));
    		}

    		if ($options['google-enabled'] && $options['load-google']) {
    			add_action("wp_head", array($this, "loadGooglePlus"));
    		}
    		
            if ($options['init-facebook']) {
                add_action("wp_head", array($this, "header"));
                add_action("wp_footer", array($this, "footer"));
            }
            
            if ($options['vk-share-enabled'] && $options['vk-app-id']) {
                add_action("wp_footer", array($this, "loadVK"));
            }
            
            wp_register_style('share_locker_css', SHARE_LOCKER_URL . '/css/style.css');
            wp_enqueue_style('share_locker_css');
        }
         
    	if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
    	    return;
    	}
    	
    	if (get_user_option('rich_editing') == 'true') {
            add_filter('mce_external_plugins', array($this, 'mcePlugin'));
            add_filter('mce_buttons', array($this, 'mceButton'), 0);
    	}
    }
    
    public function loadFacebookSDK()
    {
        $options = get_option('share-locker');
        ?>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/<?php echo get_locale() ?>/all.js#xfbml=1<?php echo ($options['facebook-app-id']) ? '&appId=' . $options['facebook-app-id'] : '' ?>";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>
        <?php
    }
    
    public function loadTwitterJS()
    {
        ?>
        <script>window.twttr = (function (d,s,id) {
    var t, js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
    js.src="//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
    return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
  }(document, "script", "twitter-wjs"));
        </script>
        <?php
    }
    
    public function loadGooglePlus()
    {
        $locale = get_locale();
        if (!$locale) $locale = 'en_US';
        $tmp = explode("_", $locale);
    	$lang = $tmp[0];
        ?>
        <script type="text/javascript">
          window.___gcfg = { lang: '<?php echo $lang ?>' };
          (function() 
          {var po = document.createElement("script");
          po.type = "text/javascript"; po.async = true;po.src = "https://apis.google.com/js/plusone.js";
          var s = document.getElementsByTagName("script")[0];
          s.parentNode.insertBefore(po, s);
          })();</script>
        <?php
    }
    
    public function loadVK()
    {
        ?>
        <div id="vk_api_transport"></div>
        <script type="text/javascript">
          setTimeout(function() {
            var el = document.createElement("script");
            el.type = "text/javascript";
            el.src = "http://vk.com/js/api/openapi.js";
            el.async = true;
            document.getElementById("vk_api_transport").appendChild(el);
          }, 0);
        </script>
        <?php
    }
    
    public function header()
    {
        $options = get_option('share-locker');
        if ($options['facebook-share-enabled'] && $options['facebook-app-id']) {
		    echo '
		        <script>
		          window.fbAsyncInit = function() {
			        FB.init(
			            { appId: "' . $options['facebook-app-id'] . '", status: true, cookie: true }
			        );
			      };
	            </script>
		    ';
	    } elseif ($options['facebook-share-enabled'] || $options['facebook-enabled']) {
	        echo '
		        <script>
		          window.fbAsyncInit = function() {
			        FB.init();
			      };
	            </script>
		    ';
	    }
	    
	    if ($options['vk-share-enabled'] && $options['vk-app-id']) {
	        echo '
		        <script>
		          window.vkAsyncInit = function() {
                      VK.init({
                        apiId: ' . intval($options['vk-app-id']) . ',
                        onlyWidgets: true
                      });
                  };
	            </script>
		    ';
	    }
    }
	  
    public function footer()
	{
        $options = get_option('share-locker');
        if ($options['facebook-share-enabled']) {
            echo '<div id="fb-root"></div>';
        }
	}
    
    public function mcePlugin($plugin_array)
    {
       $plugin_array['sharelockerplugin'] = SHARE_LOCKER_URL . "/js/mce_plugin.js";
       return $plugin_array;
    }
    
    public function mceButton($buttons)
    {
       array_push($buttons, "separator", "sharelockerplugin");
       return $buttons;
    }
    
    public function ajax()
    {
		$id = $_POST['id'];	
		setcookie("share_locker_" . $id, 'share_locker_ok', time() + 3600 * 24 * 90, "/");
    }
    
    public function shortcodeFunc($atts, $content)
    {
        $options = get_option('share-locker');
        
        extract(shortcode_atts(array(
    		"id" => get_the_ID(),
    		"theme" => "blue",
    		"message" => $options['default-message'],
    		"main_url" => '',
    		"facebook_share" => $options['facebook-share-enabled'],
    		"facebook_share_title" => get_the_title(),
    		"facebook_share_message" => '',
    		"facebook_share_url" => '',
    		"facebook_like" => $options['facebook-enabled'],
    		"facebook_like_url" => '',
    		"facebook_colorscheme" => $options['facebook_colorscheme'],
    		"twitter" => $options['twitter-enabled'],
    		"twitter_url" => '',
    		"twitter_tweet" => $options['twitter-default-tweet'],
    		"google" => $options['google-enabled'],
    		"google_url" => '',
    		"vk_share" => $options['vk-share-enabled'],
    		"vk_share_url" => '',
    	), $atts));
    	
    	//resolve options
    	$facebook_app_id = $options['facebook-app-id'];
    	
    	if (!$facebook_share_title) $facebook_share_title = get_the_title();
    	
    	$locale = get_locale();
        if (!$locale) $locale = 'en_US';
    	
    	if (!$locale) $locale = 'en_US';

    	$id = str_replace(' ', '', $id);
    	$id = str_replace('"', '', $id);
        if (!$id) $id = get_the_ID();
        if (!$theme) $theme = 'blue';
    	
    	//lang for twitter
    	$tmp = explode("_", $locale);
    	$lang = $tmp[0];
    	
		if (!$main_url) $main_url = get_permalink();
		if (!$facebook_share_url) $facebook_share_url = $main_url;
		if (!$facebook_like_url) $facebook_like_url = $main_url;
		if (!$twitter_url) $twitter_url = $main_url;
		if (!$google_url) $google_url = $main_url;
		if (!$vk_share_url) $vk_share_url = $main_url;
		
		if (!$options['facebook-enabled']) $facebook_like = false;
		if (!$options['facebook-share-enabled'] || !$facebook_app_id) $facebook_share_enabled = false;
        if (!$options['twitter-enabled']) $twitter = false;
        if (!$options['google-enabled']) $google = false;
    
    $facebook_share_title = esc_js( $facebook_share_title );
    $facebook_share_message = esc_js( $facebook_share_message );
    
    
    	//if cookie exists show content
    	if ($_COOKIE['share_locker_' . $id] == 'share_locker_ok') {
    	   return do_shortcode($content);
    	//else show lock
    	} else {
    	    $ajax_url = admin_url('admin-ajax.php');
    	    
    	    $buttons = '';
    	        	    
    	    if ($facebook_share && $facebook_app_id) {
    	        $path = SHARE_LOCKER_URL;
    	        $buttons .= <<< EOB
    	        <div class="share-locker-share"><a id="fb_share_$id" href="#"><img alt="share" src="$path/images/fb-share-button.png"></a>
    	        <script>
    	           jQuery(document).ready(function() {
    	               jQuery('#fb_share_$id').click(function(e) {
               	       e.preventDefault();
               		   FB.ui(
                        {
               				method: 'feed',
               				link: '$facebook_share_url',
               				name: '$facebook_share_title',
               				description: '$facebook_share_message',
               		    }, 
               		    function(response) {
               		    	if (response) {
               		    		var data = { id: "$id", action: "share_locker", network: "facebook-share" };
                                jQuery.post("$ajax_url", data, function(response) {
            						window.location.reload(true);
            					});
               		    	}
               		    }
               		    );
               	  });
               	});
    	        </script>
    	        </div>
EOB;
    	    }

    	    if ($facebook_like) {
    	        $buttons .= <<< EOB
    	        <div id="fb_like_$id" class="fb-like share-locker-like" data-href="$facebook_like_url" data-layout="button_count" data-width="20" data-show-faces="false" data-send="false" data-colorscheme="$facebook_colorscheme"></div>
                <script>
                    var oldCB_$id = window.fbAsyncInit;
                    window.fbAsyncInit = function(){
                      if (typeof oldCB_$id === 'function'){
                        oldCB_$id();
                      }

                      FB.Event.subscribe("edge.create", function(href, widget){
                        //if (href == "$facebook_like_url" && widget.dom.id == "fb_like_$id") {
                        if (href == "$facebook_like_url") {
                            var data = { id: "$id", action: "share_locker", network: "facebook" };
                            jQuery.post("$ajax_url", data, function(response) {
        						          window.location.reload(true);
        					          });
        				        }
            			    });
            		    };
                </script>
EOB;
    	    }
    	    
    	    if ($twitter) {
    	        $buttons .= <<< EOB
    	        <div class="share-locker-twitter">
    	        <div id="twitter_$id" class="twitter-button"><a href="https://twitter.com/share" class="twitter-share-button" data-text="$twitter_tweet" data-url="$twitter_url" data-counturl="$twitter_url" data-count="horizontal" data-lang="$lang">Tweet</a></div>
    	        <script>
        		twttr.ready(function (twttr) {
        	        	twttr.events.bind("tweet", function(event) {
        					    if (event.target.parentNode.id == 'twitter_$id') {
                                    var data = { id: "$id", action: "share_locker", network: "twitter" };
                                    jQuery.post("$ajax_url", data, function(response) {
            							window.location.reload(true);
            						});
                                }
        					});
        				});
        		</script>
        		</div>
EOB;
    	    }
    	    
    	    if ($google) {
    	        $buttons .= <<<EOB
    	        <div class="share-locker-google">
    	        <div class="g-plusone" data-size="medium" data-callback="share_locker_google_$id" data-href="$google_url"></div>
    	        <script>
    	            function share_locker_google_$id(plus) {
    	                if (plus.state == "on") {
        					var data = { id: "$id", action: "share_locker", network: "google" };
        					jQuery.post("$ajax_url", data, function(response) {
                                                        setTimeout(function() {
                                                           window.location.reload(true);					 
							}, 10000);
        					});
        				}
    	            }
    	        </script>
    	        </div>
EOB;
    	    }
    	    
    	    if ($vk_share) {
    	       $buttons .= <<<EOB
    	           <div class="share-locker-vk-share">
    	           <div id="vk-share-button_$id"></div>
       	           <script>
       	                var oldVK_$id = window.vkAsyncInit;
                        window.vkAsyncInit = function() {
                          if (typeof oldVK_$id === 'function'){
                            oldVK_$id();
                          }
       	                
       	                  VK.Widgets.Like("vk-share-button_$id", { type: "button", pageTitle: "$facebook_share_title", pageUrl:"$vk_share_url" });
       	           
       	                  VK.Observer.subscribe('widgets.like.liked', function(e) {
                            var data = { id: "$id", action: "share_locker", network: "vk" };
           					jQuery.post("$ajax_url", data, function(response) {
           						window.location.reload(true);
           					});
           					
                          });
                        };
       	            </script>
       	        </div>
EOB;
    	    }
    	    
    	    
    	    // Create Locker
        	$content = <<<EOM

        		<div class="share-locker $theme">
        			<p class="sl-message">$message</p>
        			<div class="share-locker-buttons-row">$buttons</div>
        			<div style="clear:both"></div>
        		</div>

EOM;
    	    
    	}
    	return $content;
    }
}
