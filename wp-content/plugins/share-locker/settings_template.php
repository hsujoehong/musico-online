<div class="wrap">

	<?php screen_icon() ?>
	<h2><?php _e('Share Locker', 'share_locker') ?></h2>
    
    <?php if ($_GET['settings-updated'] == 'true'): ?>
        
        <div class="updated settings-error" id="setting-error-settings_updated"> 
        <p><strong><?php _e('Settings saved', 'share_locker') ?></strong></p></div>

    <?php endif ?>     
    
    <form id="share-locker-options" action="options.php" method="post">
        
        <?php $options = get_option('share-locker') ?>
        
        <h3><?php _e('Basic settings', 'share-locker') ?></h3>
        
        <table class="form-table">
            <tbody>
                
                <tr valign="top">
                    <th scope="row"><?php _e('Facebook Share', 'share_locker') ?></th>
                    <td>
                        <fieldset><legend class="screen-reader-text"><span><?php _e('Enable Facebook Share', 'share_locker') ?></span></legend>
                            <label for="share-locker-facebook-share-enabled">
                            <input type="checkbox" value="1" id="share-locker-facebook-share-enabled" name="share-locker[facebook-share-enabled]" <?php checked('1', $options['facebook-share-enabled']) ?>>
                            <?php _e('enable', 'share_locker') ?></label>
                        </fieldset>
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><?php _e('Facebook Like', 'share_locker') ?></th>
                    <td>
                        <fieldset><legend class="screen-reader-text"><span><?php _e('Enable Facebook Like', 'share_locker') ?></span></legend>
                            <label for="share-locker-facebook-enabled">
                            <input type="checkbox" value="1" id="share-locker-facebook-enabled" name="share-locker[facebook-enabled]" <?php checked('1', $options['facebook-enabled']) ?>>
                            <?php _e('enable', 'share_locker') ?></label>
                        </fieldset>
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><?php _e('Twitter Share', 'share_locker') ?></th>
                    <td>
                        <fieldset><legend class="screen-reader-text"><span><?php _e('Enable Twitter', 'share_locker') ?></span></legend>
                            <label for="share-locker-twitter-enabled">
                            <input type="checkbox" value="1" id="share-locker-twitter-enabled" name="share-locker[twitter-enabled]" <?php checked('1', $options['twitter-enabled']) ?>>
                            <?php _e('enable', 'share_locker') ?></label>
                        </fieldset>
                    </td>
                </tr>
                                
                <tr valign="top">
                    <th scope="row"><?php _e('Google+', 'share_locker') ?></th>
                    <td>
                        <fieldset><legend class="screen-reader-text"><span><?php _e('Enable Google+', 'share_locker') ?></span></legend>
                            <label for="share-locker-google-enabled">
                            <input type="checkbox" value="1" id="share-locker-google-enabled" name="share-locker[google-enabled]" <?php checked('1', $options['google-enabled']) ?>>
                            <?php _e('enable', 'share_locker') ?></label>
                        </fieldset>
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><?php _e('VK share', 'share_locker') ?></th>
                    <td>
                        <fieldset><legend class="screen-reader-text"><span><?php _e('Enable VK', 'share_locker') ?></span></legend>
                            <label for="share-locker-vk-share-enabled">
                            <input type="checkbox" value="1" id="share-locker-vk-share-enabled" name="share-locker[vk-share-enabled]" <?php checked('1', $options['vk-share-enabled']) ?>>
                            <?php _e('enable', 'share_locker') ?></label>
                        </fieldset>
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><label for="share-locker-default-message"><?php _e('Default teaser message', 'share_locker') ?></label></th>
                    <td>
                        <input type="text" class="regular-text ltr" value="<?php echo $options['default-message'] ?>" id="share-locker-default-message" name="share-locker[default-message]">
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><label for="share-locker-twitter-default-tweet"><?php _e('Default tweet text', 'share_locker') ?></label></th>
                    <td>
                        <input type="text" class="regular-text ltr" value="<?php echo $options['twitter-default-tweet'] ?>" id="share-locker-twitter-default-tweet" name="share-locker[twitter-default-tweet]">
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><label for="share-locker-facebook-app-id"><?php _e('Facebook app id', 'share_locker') ?></label></th>
                    <td>
                        <input type="text" class="regular-text ltr" value="<?php echo $options['facebook-app-id'] ?>" id="share-locker-facebook-app-id" name="share-locker[facebook-app-id]">
                        <p class="description"><?php _e('Facebook app id. Get it at: <a href="https://developers.facebook.com/apps/" target="_blank">developers.facebook.com/apps/</a>', 'share_locker') ?></p>
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><label for="share-locker-vk-app-id"><?php _e('VK.com app id', 'share_locker') ?></label></th>
                    <td>
                        <input type="text" class="regular-text ltr" value="<?php echo $options['vk-app-id'] ?>" id="share-locker-vk-app-id" name="share-locker[vk-app-id]">
                        <p class="description"><?php _e('Vk.com app id. Get it at: <a href="http://vk.com/editapp?act=create" target="_blank">http://vk.com/editapp?act=create</a>', 'share_locker') ?></p>
                    </td>
                </tr>
                
            </tbody>
        </table>
                
        <h3><?php _e('Advanced options', 'share_locker') ?></h3>
        
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row"><?php _e('Load Facebook SDK', 'share_locker') ?></th>
                    <td>
                        <fieldset><legend class="screen-reader-text"><span><?php _e('Load Facebook JS', 'share_locker') ?></span></legend>
                            <label for="share-locker-load-facebook">
                            <input type="checkbox" value="1" id="share-locker-load-facebook" name="share-locker[load-facebook]" <?php checked('1', $options['load-facebook']) ?>>
                            <?php echo _e('uncheck it another plugins already loads Facebook SDK', 'share_locker') ?></label>
                        </fieldset>
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><?php _e('Init Facebook app', 'share_locker') ?></th>
                    <td>
                        <fieldset><legend class="screen-reader-text"><span><?php _e('Load Facebook JS', 'share_locker') ?></span></legend>
                            <label for="share-locker-init-facebook">
                            <input type="checkbox" value="1" id="share-locker-init-facebook" name="share-locker[init-facebook]" <?php checked('1', $options['init-facebook']) ?>>
                            <?php echo _e('uncheck it if another plugins initializes Facebook app', 'share_locker') ?></label>
                        </fieldset>
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><?php _e('Load Twitter JS', 'share_locker') ?></th>
                    <td>
                        <fieldset><legend class="screen-reader-text"><span><?php _e('Load Twitter JS', 'share_locker') ?></span></legend>
                            <label for="share-locker-load-twitter">
                            <input type="checkbox" value="1" id="share-locker-load-twitter" name="share-locker[load-twitter]" <?php checked('1', $options['load-twitter']) ?>>
                            <?php echo _e('uncheck it if another plugins already loads Twitter javascript', 'share_locker') ?></label>
                        </fieldset>
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><?php _e('Load Google+ JS', 'share_locker') ?></th>
                    <td>
                        <fieldset><legend class="screen-reader-text"><span><?php _e('Load Google+ JS', 'share_locker') ?></span></legend>
                            <label for="share-locker-load-google">
                            <input type="checkbox" value="1" id="share-locker-load-google" name="share-locker[load-google]" <?php checked('1', $options['load-google']) ?>>
                            <?php echo _e('uncheck it if another plugins already loads Google javascript', 'share_locker') ?></label>
                        </fieldset>
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><label for="share-locker-facebook-colorscheme"><?php _e('Facebook color scheme', 'share_locker') ?></label></th>
                    <td>
                        <select id="share-locker-facebook-colorscheme" name="share-locker[facebook-colorscheme]">
                	        <option value="light" <?php selected($options['facebook-colorscheme'], 'light') ?>>light</option>
                	        <option value="dark" <?php selected($options['facebook-colorscheme'], 'dark') ?>>dark</option>
                	    </select>
                    </td>
                </tr>
                
                <?php settings_fields('share-locker-settings-group') ?>
                
            </tbody>
        </table>
        	      
        <?php submit_button(); ?>
    </form>
</div>
