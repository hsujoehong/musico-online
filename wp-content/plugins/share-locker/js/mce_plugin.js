function tinyplugin() {
    return "[share-locker-plugin]";
}

(function() {

    tinymce.create('tinymce.plugins.sharelockerplugin', {
        init : function(ed, url) {
            
            ed.addCommand('sharelocker-dialog', function() {
				ed.windowManager.open({
				  file : url + '/mce_plugin_dialog.php',
				  width : 600,
				  height : 450,
				  inline : 1
				}, {
				  plugin_url : url
				});
			});
                        
            ed.addButton('sharelockerplugin', {
                title : 'Protect content with Share Locker.',
                cmd : 'sharelocker-dialog',
                image: url + "/../images/icon20.png"
            });
        },
        
        
        getInfo : function() {
            return {
                longname : 'Share Locker',
                author : 'iJanki',
                authorurl : 'http://www.danielecesarini.com/',
                infourl : 'http://www.danielecesarini.com/',
                version : '1.0'
            };
        }
    });
    
    
    tinymce.PluginManager.add('sharelockerplugin', tinymce.plugins.sharelockerplugin);
    
})();
