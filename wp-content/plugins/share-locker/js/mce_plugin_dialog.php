<?php

$wp_include = "../wp-load.php";
$i = 0;
while (!file_exists($wp_include) && $i++ < 12) {
  $wp_include = "../$wp_include";
}

// let's load WordPress
require($wp_include);

load_plugin_textdomain('share-locker', false, basename(dirname(__FILE__)) . '/../languages');

$options = get_option('share-locker');

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="<?php echo get_option('blog_charset') ?>">
<title>Share Locker</title>

<link href="<?php echo get_option('siteurl') ?>/wp-includes/js/tinymce/themes/advanced/skins/wp_theme/dialog.css" rel="stylesheet">
<style>

    #flipper {
        background-color: #FFFFFF;
        border-bottom: 1px solid #DFDFDF;
        border-left: 1px solid #DFDFDF;
        margin: 0;
        padding: 5px 20px 10px;
    }

    #shortcodeform label{
    	display: inline-block;
    	width: 150px;
    	margin:0 20px 0 0;
    	vertical-align: top;
    }

    #shortcodeform input[type=text] {
        display: inline-block;
    	width: 200px;
    }

    #shortcodeform select {
        display: inline-block;
        width: 100px;
    }

    #shortcodeform textarea {
        width: 300px;
    }
    
    .row {
        margin-bottom: 10px;
    }
    
    .row:before, .row:after {
        content: "";
        display: table;
        line-height: 0;
    }
    
    .row:after {
        clear: both;
    }
</style>

<script src="<?php echo get_option('siteurl') ?>/wp-includes/js/jquery/jquery.js"></script>
<script src="<?php echo get_option('siteurl') ?>/wp-includes/js/tinymce/tiny_mce_popup.js"></script>
<script src="<?php echo get_option('siteurl') ?>/wp-includes/js/tinymce/utils/mctabs.js"></script>
<script src="<?php echo get_option('siteurl') ?>/wp-includes/js/tinymce/utils/form_utils.js"></script>

<script>

	function insertCode() {		
		var id = "<?php echo substr(md5(uniqid()), 0, 8) ?>";
		var theme = document.getElementById('theme').value;
		var message = document.getElementById('message').value;
		var main_url = document.getElementById('main_url').value;
		var facebook_share = document.getElementById('facebook_share').value;
		var facebook_share_title = document.getElementById('facebook_share_title').value;
		var facebook_share_message = document.getElementById('facebook_share_message').value;
		var facebook_share_url = document.getElementById('facebook_share_url').value;
		var facebook_like = document.getElementById('facebook_like').value;
		var facebook_like_url = document.getElementById('facebook_like_url').value;
		var facebook_colorscheme = document.getElementById('facebook_colorscheme').value;
		var twitter = document.getElementById('twitter').value;
		var twitter_url = document.getElementById('twitter_url').value;
		var twitter_tweet = document.getElementById('twitter_tweet').value;
		var google = document.getElementById('google').value;
		var google_url = document.getElementById('google_url').value;
		var vk_share = document.getElementById('vk_share').value;
		var vk_share_url = document.getElementById('vk_share_url').value;
		
		if (window.tinyMCE) {
			
			selection = tinyMCE.activeEditor.selection.getContent();
			
			attributes = new Array(); 
			
			attributes.push('id="' + id + '" theme="' + theme + '" message="' + message + '" main_url="' + main_url + '"');
			
			// Facebook Share
			if (facebook_share) {
				attributes.push('facebook_share="1" facebook_share_title="' + facebook_share_title + '" facebook_share_url="' + facebook_share_url + '" facebook_share_message="' + facebook_share_message + '"');
			} else {
				attributes.push('facebook_share="0"');
			}
            
            // Facebook Like
			if (facebook_like) {
				attributes.push('facebook_like="1" facebook_like_url="' + facebook_like_url + '" facebook_colorscheme="' + facebook_colorscheme + '"');
			} else {
				attributes.push('facebook_like="0"');
			}
			
			// Twitter 
			if (twitter) {
				attributes.push('twitter="1" twitter_url="' + twitter_url + '" twitter_tweet="' + twitter_tweet + '"');
			} else {
				attributes.push('twitter="0"');
			}
			
			// Google+
			if (google) {
				attributes.push('google="1" google_url="' + google_url + '"');
			} else {
				attributes.push('google="0"');
			}
			
			// VK.com
			if (vk_share) {
				attributes.push('vk_share="1" vk_share_url="' + vk_share_url + '"');
			} else {
				attributes.push('vk_share="0"');
			}
									
//			window.tinyMCE.execInstanceCommand('content', 'mceInsertContent', false, '[share-locker ' + attributes.join(' ') + ']' + selection + '[/share-locker]');
			
			tinyMCE.activeEditor.selection.setContent('[share-locker ' + attributes.join(' ') + ']' + selection + '[/share-locker]');

			tinyMCEPopup.editor.execCommand('mceRepaint');
			
			tinyMCEPopup.close();
			
		}
		return;
	}
</script>
</head>

<body class="wp-core-ui forceColors">
    
    <div id="flipper" class="wrapper">

        <form method="post" id="shortcodeform" action="">
        
            <div class="row">
                <label for="main_url"><?php _e('URL to share*', 'share_locker') ?></label>
                <input name="main_url" id="main_url" type="text" placeholder="http://" value="">
            </div>
        
            <div class="row">
                <label><?php _e('Theme', 'share_locker') ?></label>
                <select id="theme" name="theme">
                    <option value="blue" selected="selected">Blue</option>
                    <option value="green">Green</option>
                    <option value="red">Red</option>
                    <option value="yellow">Yellow</option>
                    <option value="custom">Custom</option>
                </select>
            </div>
        
            <div class="row">
                <label for="message"><?php _e('Teaser text', 'share_locker') ?></label>
                <textarea name="message" id="message" cols="20" rows="3"><?php echo $options['default-message'] ?></textarea>
            </div>
        
            <p><small>
                <?php _e('* (leave empty to share current post/page)', 'share_locker') ?>
            </small>
        
            <p>
                <?php _e('Use the advanced options below if you want to customize every single social button.', 'share_locker') ?>
            </p>
      
            <div class="tabs">
                <ul>
                    <li id="facebook_share_tab" class="current"><span><a href="javascript:mcTabs.displayTab('facebook_share_tab','facebook_share_panel');" onmousedown="return false;"><?php _e('FB Share', 'share_locker') ?></a></span></li>
                    <li id="facebook_like_tab" class=""><span><a href="javascript:mcTabs.displayTab('facebook_like_tab','facebook_like_panel');" onmousedown="return false;"><?php _e('FB Like', 'share_locker') ?></a></span></li>
                    <li id="twitter_tab" class=""><span><a href="javascript:mcTabs.displayTab('twitter_tab','twitter_panel');" onmousedown="return false;"><?php _e('Twitter', 'share_locker') ?></a></span></li>
                    <li id="google_tab" class=""><span><a href="javascript:mcTabs.displayTab('google_tab','google_panel');" onmousedown="return false;"><?php _e('Google+', 'share_locker') ?></a></span></li>
                    <li id="vk_tab" class=""><span><a href="javascript:mcTabs.displayTab('vk_tab','vk_panel');" onmousedown="return false;"><?php _e('VK.com', 'share_locker') ?></a></span></li>
                </ul>
            </div>

            <div class="panel_wrapper">
 
                <div id="facebook_share_panel" class="panel current" style="height:auto">
    
                    <div class="row">
                        <label><?php _e('Enable Share Button?', 'share_locker') ?></label>
                        <select id="facebook_share" name="facebook_share">
                            <option value="1" selected="selected"><?php _e('Yes', 'share_locker') ?></option>
                            <option value="0"><?php _e('No', 'share_locker') ?></option>
                        </select>
                    </div>
                
                    <div class="row">
                        <label for="facebook_share_title"><?php _e('Share title', 'share_locker') ?></label>
                        <input name="facebook_share_title" id="facebook_share_title"></input>
                    </div>
                    
                    <div class="row">
                        <label for="facebook_share_message"><?php _e('Share message', 'share_locker') ?></label>
                        <input name="facebook_share_message" id="facebook_share_message"></input>
                    </div>
                
                    <div class="row">
                        <label for="facebook_share_url"><?php _e('Share URL', 'share_locker') ?></label>
                        <input name="facebook_share_url" id="facebook_share_url" type="text" placeholder="http://" value="">
                        <small><?php _e('Leave empty to use main URL', 'share_locker') ?></small>
                    </div>
    
                </div>
    
                <div id="facebook_like_panel" class="panel" style="height:auto">
                
                    <div class="row">
                        <label><?php _e('Enable Like Button?', 'share_locker') ?></label>
                        <select id="facebook_like" name="facebook_like">
                            <option value="1" selected="selected"><?php _e('Yes', 'share_locker') ?></option>
                            <option value="0"><?php _e('No', 'share_locker') ?></option>
                        </select>
                    </div>

                    <div class="row">
                        <label><?php _e('Color scheme', 'share_locker') ?></label>
                        <select id="facebook_colorscheme" name="facebook_colorscheme">
                            <option value="light" selected="selected"><?php _e('Light', 'share_locker') ?></option>
                            <option value="dark"><?php _e('Dark', 'share_locker') ?></option>
                        </select>
                    </div>
                
                    <div class="row">
                        <label for="facebook_like_url"><?php _e('URL to like', 'share_locker') ?></label>
                        <input name="facebook_like_url" id="facebook_like_url" type="text" placeholder="http://" value="">
                        <small><?php _e('Leave empty to use main URL', 'share_locker') ?></small>
                    </div>
                                  
                </div>

                <div id="twitter_panel" class="panel" style="height:auto">
                
                    <div class="row">
                        <label><?php _e('Enable Tweet Button?', 'share_locker') ?></label>
                        <select id="twitter" name="twitter">
                            <option value="1" selected="selected"><?php _e('Yes', 'share_locker') ?></option>
                            <option value="0"><?php _e('No', 'share_locker') ?></option>
                        </select>
                    </div>
        
                    <div class="row">
                        <label for="twitter_tweet"><?php _e('Tweet Text', 'share_locker') ?></label>
                        <input name="twitter_tweet" id="twitter_tweet" value="<?php echo $options['twitter-default-tweet'] ?>">
                    </div>
                
                    <div class="row">
                        <label for="twitter_url"><?php _e('URL to Tweet about', 'share_locker') ?></label>
                        <input name="twitter_url" id="twitter_url" type="text" placeholder="http://" value="">
                        <small><?php _e('Leave empty to use main URL', 'share_locker') ?></small>
                    </div>
                                  
                </div>
    
                <div id="google_panel" class="panel" style="height:auto">
    
                    <div class="row">
                        <label><?php _e('Enable Google+ Button?', 'share_locker') ?></label>
                        <select id="google" name="google">
                            <option value="1" selected="selected"><?php _e('Yes', 'share_locker') ?></option>
                            <option value="0"><?php _e('No', 'share_locker') ?></option>
                        </select>
                    </div>

                    <div class="row">
                        <label for="google_url"><?php _e('Google+ URL to like', 'share_locker') ?></label>
                        <input name="google_url" id="google_url" type="text" placeholder="http://" value="">
                        <small><?php _e('Leave empty to use main URL', 'share_locker') ?></small>
                    </div>
                                  
                </div>
                
                <div id="vk_panel" class="panel" style="height:auto">
    
                    <div class="row">
                        <label><?php _e('Enable VK share Button?', 'share_locker') ?></label>
                        <select id="vk_share" name="vk_share">
                            <option value="1" selected="selected"><?php _e('Yes', 'share_locker') ?></option>
                            <option value="0"><?php _e('No', 'share_locker') ?></option>
                        </select>
                    </div>

                    <div class="row">
                        <label for="vk_share_url"><?php _e('VK.com URL to like', 'share_locker') ?></label>
                        <input name="vk_share_url" id="vk_share_url" type="text" placeholder="http://" value="">
                        <small><?php _e('Leave empty to use main URL', 'share_locker') ?></small>
                    </div>
                                  
                </div>

            </div>
        
            <div id="mceActionPanel">
            
                <div style="margin: 8px auto; text-align: center;padding-bottom: 10px;">
                	<input type="button" class="updateButton" value="<?php _e('Insert', 'share_locker') ?>" onclick="insertCode();">
            	</div>
            
            </div>
    
        </form>
    
    </div>

</body>
</html>