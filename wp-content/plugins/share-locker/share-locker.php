<?php
/*
Plugin Name: Share Locker
Plugin URI: http://www.danielecesarini.com/
Description: Share Locker lets you protect content until users share the page on Facebook, Twitter or Google+.
Version: 1.9
Author: iJanki
Author URI: http://www.danielecesarini.com/
License: Proprietary
*/

define('SHARE_LOCKER_URL',  plugin_dir_url(__FILE__));
define('SHARE_LOCKER_PATH', plugin_dir_path(__FILE__));

require(SHARE_LOCKER_PATH . '/lib/ShareLocker.php');

$share_locker = new Share_Locker();
