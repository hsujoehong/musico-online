<?php
/**
 * Class WC_Report_Product_Sales
 * 年度商品銷售報表
 */

class WC_Report_Product_Sales extends WC_Admin_Report {

	use WsttsDownloadHelper;

	public function get_chart_legend() {
		$legend = [];

		return $legend;
	}
	public function get_main_chart() {

	}
	public function output_report() {

		Wstts_Api::form_html();

		if(isset($_POST['action']) && $_POST['action'] === 'export') {
			$year  = $_POST['year'];
			$orders  = (new WsttsOrder())
				->start($year.'-01-01')
				->end($year.'-12-31')
				->inclusive()
				->orderby('post_date', 'ASC')
				->all()
				->get();

			$month_orders = [
				1 => [],
				2 => [],
				3 => [],
				4 => [],
				5 => [],
				6 => [],
				7 => [],
				8 => [],
				9 => [],
				10 => [],
				11 => [],
				12 => [],
			];

			$all_products = (new WsttsProduct())->all()->get();

			$product_statistics = [];
			foreach ($all_products as $product ) {
				for ( $i = 1; $i <= 12; $i ++ ) {
					$product_statistics[$i][(int)$product->id]['count'] = 0;
					$product_statistics[$i][(int)$product->id]['amount'] = 0;
				}
			}

			foreach ($orders as $order ) {
				/** @var WC_Order $order */
				$order_month = date('m', strtotime($order->order_date));
				array_push($month_orders[(int)$order_month], $order);
			}

			foreach ($month_orders as $month => $orders_in_month ) {
				foreach ($orders_in_month as $order ) {
					/** @var WC_Order $order**/
					$order_items = $order->get_items();
					foreach ($order_items as $order_item ) {
//						/** @var WC_Product $item_product */
						$item_product = wc_get_product($order_item['product_id']);
						if($item_product) {
							$product_statistics[$month][(int)$item_product->id]['count'] += $order_item['qty'];
							$product_statistics[$month][(int)$item_product->id]['amount'] += $order_item['line_total'];
						}
					}
				}
			}

			$product_data = array_values(
				array_map(function($product){
					/** @var WC_Product $product */
					$sku = $product->get_sku();
					if(!$sku) $sku = '';
					return [
						'id' => $product->id,
						'sku' => $sku,
						'title' => $product->get_title()
					];
				}, $all_products)
			);

			$data_headers = [];

			$data_set = [];

			foreach ($product_data as $data ) {
				$data_headers[0][] = $data['id'];
				$data_headers[1][] = $data['sku'];
				$data_headers[2][] = $data['title'];
			}

			$data_set[] = array_merge((array)__('Product id', 'wc-statistics'), $data_headers[0], ['']);
			$data_set[] = array_merge((array)__('Product sku', 'wc-statistics'), $data_headers[1], ['']);
			$data_set[] = array_merge((array)__('Product name', 'wc-statistics'), $data_headers[2], (array)__('Subtotal of month', 'wc-statistics'));

			$product_prices = [];

			$product_prices[] = __('Product price', 'wc-statistics');

			$product_count_total = [];
			$product_amount_total = [];

			$product_count_total[] = __('Total count of product', 'wc-statistics');
			$product_amount_total[] = __('Total amount of product', 'wc-statistics');

			$year_count_total = [];
			$year_amount_total = [];

			$year_count_total[] = __('Total count', 'wc-statistics');
			$year_amount_total[] = __('Total amount of product', 'wc-statistics');

			foreach ($all_products as $product ) {
				/** @var WC_Product $product */
				$price = $product->get_price();
				$product_prices[] = $price;
			}

			$data_set[] = $product_prices;

			$year_count = 0;
			$year_amount = 0;
			foreach ($product_statistics as $month => $products_statistic ) {
				$amount_line = [Wstts_Api::get_month_name($month) . '(' .__('amount', 'wc-statistics') . ')'];
				$count_line = [Wstts_Api::get_month_name($month) . '(' .__('quantity', 'wc-statistics') . ')'];
				$amount_subtotal = 0;
				$count_subtotal = 0;
				$index = 1;
				foreach ($products_statistic as $product_id => $product_statistic ) {
					$amount = round($product_statistic['amount']);
					$amount_line[] = $amount;
					$amount_subtotal += $amount;
					$count_line[] = $product_statistic['count'];
					$count_subtotal += $product_statistic['count'];

					if(!isset($product_count_total[$index]))
						$product_count_total[$index] = $product_statistic['count'];
					else
						$product_count_total[$index] += $product_statistic['count'];

					if(!isset($product_amount_total[$index]))
						$product_amount_total[$index] = $amount;
					else
						$product_amount_total[$index] += $amount;

					$year_count += $product_statistic['count'];
					$year_amount += $amount;

					$index++;
				}
				$amount_line[] = $amount_subtotal;
				$count_line[] = $count_subtotal;
				$data_set[] = $amount_line;
				$data_set[] = $count_line;
			}
			$data_set[] = $product_amount_total;
			$data_set[] = $product_count_total;

			$year_count_total[] = $year_count;
			$year_amount_total[] = $year_amount;

			$data_set[] = $year_amount_total;
			$data_set[] = $year_count_total;

			$file_name = __('Product Sales Report of Year', 'wc-statistics') . $year;
			$this->download_csv($file_name, $data_set);
		}
	}
}