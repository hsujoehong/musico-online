<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/1/18
 * Time: 下午 03:03
 */

class WsttsProduct {

	private $query_args = [];

	public function __construct()
	{
		$this->query_args['post_type'] = 'product';
	}

	public function start($time_start)
	{
		if($time_start) {
			$this->query_args['date_query'][] = [
				'column' => 'post_date_gmt',
				'after' => $time_start,
			];
		}
		return $this;
	}

	public function end($time_end)
	{
		if($time_end) {
			$this->query_args['date_query'][] = [
				'column' => 'post_date_gmt',
				'before' => $time_end,
			];
		}
		return $this;
	}

	public function orderby($name, $order)
	{
		$this->query_args['orderby'] = $name;
		$this->query_args['order'] = $order;
		return $this;
	}

	public function all()
	{
		$this->query_args['posts_per_page'] = -1;
		$this->query_args['paged'] = 1;
		return $this;
	}

	public function limit($limit, $page = 0)
	{
		$this->query_args['posts_per_page'] = $limit;
		$this->query_args['paged'] = $page;
		return $this;
	}

	public function get()
	{
		$product_posts = (new WP_Query($this->query_args))->get_posts();
		$products = array_map(function($post){
			return wc_get_product($post->ID);
		}, $product_posts);
		return $products;
	}
}