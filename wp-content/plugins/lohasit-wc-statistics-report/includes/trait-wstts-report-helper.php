<?php
trait WsttsReportHelper
{
	private $chart_colors = [];

	private $data_helper;

	private $data;

	private $ticks;

	private $current_range;

	private function get_data_helper()
    {
        return new WsttsStatisticsData();
    }

	private function get_ranges()
	{
		return [
			'year'         => __( 'Year', 'wc-statistics' ),
			'last_month'   => __( 'Last Month', 'wc-statistics' ),
			'month'        => __( 'This Month', 'wc-statistics' ),
			'7day'         => __( 'Last 7 Days', 'wc-statistics' )
		];
	}

	private function get_chart_colors()
	{
		return [
			'#3498db', '#33cc33' , '#cc33ff', '#ff9933' , '#800000'
		];
	}

	private function get_current_range(){
		$current_range = !empty($_GET['range']) ? sanitize_text_field($_GET['range']) : '7day';

		if ( ! in_array( $current_range, array( 'custom', 'year', 'last_month', 'month', '7day' ) ) ) {
			$current_range = '7day';
		}
		return $current_range;
	}

	private function process_data($raw_data)
	{
		$this->data = [];
		$this->ticks = [];
		foreach ($raw_data as $i => $data) {
			array_push($this->ticks, [$i, $data[0]]);
			array_push($this->data, [$i, $data[1]]);
		}
	}

	private function chart_html()
	{
		ob_start();
		?>
		<div class="chart-container">
			<div class="chart-placeholder main"></div>
		</div>
		<?php
		echo ob_get_clean();
	}

	private function chart_script()
	{
		ob_start();
		?>
		<script>
            (function($){
                var main_chart;
                var drawGraph = function(highlight) {

                    var data = $.parseJSON( '<?php echo json_encode($this->data); ?>' ),
                        ticks = $.parseJSON('<?=json_encode($this->ticks)?>');

                    var colors = $.parseJSON('<?=json_encode($this->chart_colors)?>');

                    var series = [];

                    for(var i in data) {
                        series.push(
                            {
                                label: ticks[i][1],
                                data: [data[i]],
                                color: colors[i],
                                bars: {
                                    show: true,
                                    barWidth: 1,
                                    align: "center",
                                    lineWidth: 1,
                                    fill: 0.75
                                }
                            }
                        );
                    }

                    var options = {
                        xaxis: {
                            ticks: ticks
                        }
                    };

                    if ( highlight !== 'undefined' && series[ highlight ]) {
                        highlight_series = series[ highlight ];

                        if ( highlight_series.bars )
                            highlight_series.bars.fill = 1;

                    }

                    main_chart = $.plot(
                        $('.chart-placeholder.main'),
                        series,
                        options
                    );

                    $('.chart-placeholder').resize();
                };
                drawGraph();

                $('.highlight_series').hover(
                    function() {
                        drawGraph( $(this).data('series') );
                    },
                    function() {
                        drawGraph();
                    }
                );
            })(jQuery);

		</script>
		<?php
		echo ob_get_clean();
	}


	private function main_chart_html($data)
    {
	    $this->process_data($data);
	    $this->chart_html();
	    $this->chart_script();
    }

    private function calculate_range()
    {
	    $this->current_range = $this->get_current_range();

	    $this->calculate_current_range($this->current_range);
    }

    private function report_html()
    {
	    $this->chart_colors = $this->get_chart_colors();

	    $current_range = $this->current_range;

	    $ranges = $this->get_ranges();

	    include( WC()->plugin_path() . '/includes/admin/views/html-report-by-date.php' );
    }



}