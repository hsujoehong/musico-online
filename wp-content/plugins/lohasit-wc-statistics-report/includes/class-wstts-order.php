<?php

class WsttsOrder {

	private $query_args = [];

	public function __construct()
	{
		$this->query_args['post_type'] = 'shop_order';
		$this->query_args['post_status'] = 'wc-completed';
	}

	public function start($time_start)
	{
		if($time_start) {
			$this->query_args['date_query'][0]['after'] = $time_start;
		}
		return $this;
	}

	public function end($time_end)
	{
		if($time_end) {
			$this->query_args['date_query'][0]['before'] = $time_end;
		}
		return $this;
	}
	public function inclusive()
	{
		$this->query_args['date_query'][0]['inclusive'] = true;
		return $this;
	}

	public function orderby($name, $order)
	{
		$this->query_args['orderby'] = $name;
		$this->query_args['order'] = $order;
		return $this;
	}

	public function all()
	{
		$this->query_args['posts_per_page'] = -1;
		$this->query_args['paged'] = 1;
		return $this;
	}

	public function limit($limit, $page = 0)
	{
		$this->query_args['posts_per_page'] = $limit;
		$this->query_args['paged'] = $page;
		return $this;
	}

	public function get()
	{
		$order_posts = (new WP_Query($this->query_args))->get_posts();
		$orders = array_map(function($post){
			return new WC_Order($post->ID);
		}, $order_posts);
		return $orders;
	}
}