<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/1/18
 * Time: 下午 02:44
 */

class Wstts_Installer {

	public static function install()
	{
		self::create_tables();
	}

	private static function create_tables()
	{
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		global $wpdb;

		$tables = self::get_schema();
		$charset_collate = $wpdb->get_charset_collate();

		foreach($tables as $table) {
			$table_name = $wpdb->prefix . WCPD_TABLE_PREFIX . $table['table_name'];
			$sql = sprintf($table['schema'], $table_name, $charset_collate);
			dbDelta($sql);
		}
	}

	private static function get_schema()
	{
		$tables = [

		];
		return $tables;
	}
}