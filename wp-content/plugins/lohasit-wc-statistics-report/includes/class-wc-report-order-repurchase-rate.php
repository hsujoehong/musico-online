<?php
/**
 * 回購率
 */

class WC_Report_Order_Repurchase_Rate extends WC_Admin_Report
{
	use WsttsReportHelper;

	private $raw_data;

	public function __construct()
	{
		$this->data_helper = $this->get_data_helper();

		$this->calculate_range();

		$this->raw_data = $this->data_helper
			->set_range(date('Y-m-d', $this->start_date), date('Y-m-d', $this->end_date))
			->get_repurchase_rate();
	}

	public function get_chart_legend()
	{
		$legend[] = array(
			'title' => sprintf(__('Repurchase Rate', 'wc-statistics').': %s', '<strong>' . $this->raw_data['repurchase_rate']*100 .'</strong>%' ),
			'color' => $this->chart_colors[0],
			'highlight_series' => 0
		);

		return $legend;
	}

	public function get_main_chart()
	{
		$data = [
			[__('Repurchase Rate', 'wc-statistics'), $this->raw_data['repurchase_rate']],
		];
		$this->main_chart_html($data);
	}

	public function output_report()
	{
		$this->report_html();
	}
}