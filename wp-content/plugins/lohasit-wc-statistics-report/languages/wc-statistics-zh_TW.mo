��    I      d  a   �      0     1     N     j     �     �     �     �     �     �     �  	   �     �                '     E     c     l     s     |  
   �  
   �     �     �     �     �  
   �     �     �     �     �     �     �     �               &     2  
   O     Z     g     u     �     �     �  	   �     �     �     �     �     	     '	     ?	     S	     k	     �	     �	  
   �	     �	     �	     �	     �	      
     
     .
     F
     R
     i
     n
     �
     �
     �
  �  �
     9     S     m     }     �     �     �     �     �  	   �  	   �  	   �     �     �     	     "  	   5     ?     F     M  
   T     _     r     y     �     �  	   �     �  
   �     �     �     �  	   �     �     �     �     �          %     .     ;     H     U     e  	   u          �     �     �     �     �     �     �               5     H     _     f     m     }     �     �  	   �     �     �     �     �     �                                 /   &   :              !   $       >   =       +         	       F   %          '   D   8             <   G                  *      ;                     ,   1   ?   (   3   H       @                 A   6          9      )      -      I   0   "             7      5   4   
               B                2         .   E   C   #               %s cancelled orders in total %s refunded orders in total 15 - 25 years old: %s 25 - 35 years old: %s 35 - 45 years old: %s 45 - 55 years old: %s April August Average Customer Value By Age By Gender By State Cancelled Orders Cancelled Rate: %s %% Category Sales Report of Year Category Total Report of Year December Export February Female Female: %s Item Count January July June Last 7 Days Last Month Male Male: %s March May Month November Number of cancelled orders Number of refunded orders October Order Count Product Sales Report of Year Product id Product name Product price Product sku Refunded Orders Refunded Rate: %s %% Repurchase Rate September Subtotal of category Subtotal of month Taiwan Central Area Taiwan Central Area: %s Taiwan Eastern Area Taiwan Eastern Area: %s Taiwan Islands Area Taiwan Islands Area: %s Taiwan Northern Area: %s Taiwan Southern Area Taiwan Southern Area: %s This Month Total Total Discount Amount Total Refund Amount Total Sales Amount Total Sales Report of Year Total Shipping Fee Total amount of product Total count Total count of product Year Year Report Export above 55 years old: %s amount quantity Project-Id-Version: WC Statistics
POT-Creation-Date: 2018-02-09 15:55+0800
PO-Revision-Date: 2018-02-09 15:55+0800
Last-Translator: 
Language-Team: 
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ..
X-Poedit-WPHeader: wc-statistics.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 共 %s 筆已取消訂單 共 %s 筆已退費訂單 15 - 25 歲: %s 25 - 35 歲: %s 35 - 45 歲: %s 45 - 55 歲: %s 四月 八月 平均客單價 依年齡 依性別 依地區 已取消訂單 棄單率 %s %% 年度分類銷售報表 年度銷售報表 十二月 匯出 二月 女性 女性: %s 售出項目數量 一月 七月 六月 過去七日 上個月 男性 男性: %s 三月 五月 月份 十一月 已取消訂單數量 退費訂單數量 十月 訂單數量 年度商品銷售報表 商品ID 商品名稱 商品價格 商品貨號 已退費訂單 退貨率 %s %% 回購率 九月 分類小計 月份小計 台灣中部地區 台灣中部地區: %s 台灣東部地區 台灣東部地區: %s 台灣離島地區 台灣離島地區: %s 台灣北部地區: %s 台灣南部地區 台灣南部地區: %s 本月 總計 總折扣金額 總退費金額 總銷售金額 年度銷售報表 總運費 商品價格總計 數量總計 商品數量總計 年 年度報表匯出 55 歲以上: %s 價格 數量 