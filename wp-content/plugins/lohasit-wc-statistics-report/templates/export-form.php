<form action="" method="POST">
    <select name="year" id="">
        <?php for($y=2017; $y<=date("Y"); $y++):?>
            <option value="<?=$y?>" <?=($y==date("Y"))?'selected="selected"':""?>><?=$y?></option>
        <?php endfor?>
    </select>
    <input type="hidden" name="action" value="export">
    <button type="submit" class="button"><?=__('Export' ,'wc-statistics')?></button>
</form>