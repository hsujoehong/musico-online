jQuery( function($) {
    var dateFormat = "yy-mm-dd",
        from = $( "#from" )
            .datepicker({
                defaultDate: "+1w",
                maxDate:0,
                dateFormat : "yy-mm-dd",
                changeYear: true,
                changeMonth: true
            })
            .on( "change", function() {
                to.datepicker( "option", "minDate", getDate( this ) );
            }),
        to = $( "#to" ).datepicker({
                dateFormat : "yy-mm-dd",
                defaultDate: "+1w",
                maxDate:0,
                changeYear: true,
                changeMonth: true
            })
            .on( "change", function() {
                from.datepicker( "option", "maxDate", getDate( this ) );
            });

    function getDate( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
            date = null;
        }

        return date;
    }
} );