<?php
$page_url = menu_page_url( 'buying-history', false );
if ( ! isset( $_GET['orderby'] ) ) {
	wp_redirect( $page_url . '&orderby=u_id&order=asc&page_num=1&limit=50&n=1' );
}
if ( isset( $_GET['n'] ) && $_GET['n'] == '1' ):
	$page_num    = 1;
	$limit       = 50;
	$order_by    = ( isset( $_GET['orderby'] ) ) ? $_GET['orderby'] : '';
	$order       = ( isset( $_GET['order'] ) ) ? $_GET['order'] : '';
	$page_num    = ( isset( $_GET['page_num'] ) ) ? $_GET['page_num'] : $page_num;
	$limit       = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : $limit;
	$s           = ( isset( $_GET['s'] ) ) ? $_GET['s'] : '';
	$search_type = ( isset( $_GET['search_type'] ) ) ? $_GET['search_type'] : '';

    $date = [
        'year' => date('Y'),
        'month' => date('m')
    ];
	$results      = Chir_Statistics::every_mem_buy_record( $search_type, $s, $order_by, $order, $page_num, $limit, $date);
	$data         = $results['results'];
	$result_count = $results['count'];
	$max_page_num = ceil( $result_count / $limit );
	?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?= CHIR_STATISTICS_PLUGIN_DIR ?>includes/admin/asset/css/table-style.css">
    <div class="content-body container-fluid" style="visibility: hidden;">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                <h3 class="page-title">會員購物歷史紀錄</h3>
            </div>
        </div>
        <div class="row search-row">
            <div class="col-sm-12 col-xs-12 col-md-9 col-lg-9">
                <div class="type-picking inline">
                    <select id="search-type-picking">
                        <option value="0">請選擇篩選項目</option>
                        <option value="1">帳號</option>
                        <option value="2">姓名</option>
                        <option value="3">信箱</option>
                    </select>
                </div>
                <div class="search-field search-input col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<?php include_once( 'components/input-s.php' ); ?>
                </div>
                <div class="inline search-button-field">
                    <button type="button" id="search-button" class="btn btn-success">搜尋</button>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
				<?php include_once( 'components/select-limit.php' ); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 table-responsive">
                <table class="list-table table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="cell-center">
                            <a class="table-header <?php echo $this->get_orderby_icon_css( 'u_id' ); ?>" href="<?php echo $this->get_orderby_url( 'u_id' ); ?>">
                                會員編號
                            </a>
                        </th>
                        <th>
                            <a class="table-header <?php echo $this->get_orderby_icon_css( 'account' ); ?>" href="<?php echo $this->get_orderby_url( 'account' ); ?>">
                                帳號
                            </a>
                        </th>
                        <th>
                            <a class="table-header <?php echo $this->get_orderby_icon_css( 'name' ); ?>" href="<?php echo $this->get_orderby_url( 'name' ); ?>">
                                姓名
                            </a>
                        </th>
                        <th>
                            <a class="table-header <?php echo $this->get_orderby_icon_css( 'email' ); ?>" href="<?php echo $this->get_orderby_url( 'email' ); ?>">
                                信箱
                            </a>
                        </th>
                        <th>消費金額</th>
                        <th>
                            本月活耀度(訂單量/1)
                        </th>
                        <th>
                            本季活耀度(訂單量/3)
                        </th>
                        <th>
                            本年活耀度(訂單量/12)
                        </th>
                        <th>購物歷史紀錄</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php if ( sizeof( $data ) <= 0 ): ?>
                        <tr>
                            <td colspan="9">查無結果</td>
                        </tr>
					<?php endif; ?>
					<?php foreach ( $data as $r ):
                        $query = new WP_Query([
                            'numberposts' => - 1,
                            'meta_key'    => '_customer_user',
                            'meta_value'  => $r->u_id,
                            'post_type'   => array( 'shop_order' ),
                            'post_status' => array( 'wc-completed' ),
                        ]);
					$user_orders = $query->get_posts();
					$total = 0;
					foreach($user_orders as $user_order) {
					    $order = wc_get_order($user_order);
                        foreach ($order->get_items() as $item) {
                            $total += $item['line_total'];
					    }
//					    $total += $order->get_total() - $order->get_total_shipping();
                    }
//                    $total = round($total);
                        ?>
                        <tr>
                            <td class="cell-center">
                                <a href=" <?php echo get_edit_user_link( $r->u_id ); ?>" target="_blank">
									<?php echo $r->u_id; ?>
                                </a>
                            </td>
                            <td><?php echo $r->account; ?></td>
                            <td><?php echo $r->name; ?></td>
                            <td><?php echo $r->email; ?></td>
                            <td><?=$total?></td>
                            <td><?=round($r->month_buy,1)?></td>
                            <td><?=round($r->quarter_buy, 1)?></td>
                            <td><?=round($r->year_buy, 1)?></td>
                            <td>
                                <a class="button success"
                                   href="<?= $page_url . '&u_id=' . $r->u_id . '&page_num=1&limit=50&orderby=rank&order=asc&n=2'; ?>"
                                >
                                    查看
                                </a>
                            </td>
                        </tr>
					<?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
				<?php include( 'components/pagination.php' ); ?>
            </div>
        </div>
    </div>
    <script src="<?=CHIR_STATISTICS_PLUGIN_DIR?>includes/admin/asset/js/jq-bs-extend.js"></script>
    <script src="<?=CHIR_STATISTICS_PLUGIN_DIR?>includes/admin/asset/js/cy-admin-custom.js"></script>
    <script>
        (function ($) {
            $('.search-field').hide();

			<?php if($search_type != ''):?>
            $('#search-type-picking').val(<?php echo $search_type;?>);
			<?php endif; ?>

            var searchType = $('#search-type-picking').val();
            var target = 0;
            switch (searchType) {
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                    $('input[name="s"]').val('<?php echo $s;?>');
                    target = 'search-input';
                    break;
            }
            if (target) {
                $('.search-field').each(function () {
                    if ($(this).hasClass(target))
                        $(this).show();
                    else
                        $(this).hide();
                });
            }
            if (searchType != 0) {
                $('.search-button-field').addCols(12, 12, 4, 3);
                $('.type-picking').addCols(12, 12, 4, 3);
                $('#search-type-picking').width('100%');
            } else {
                $('.search-button-field').removeCols(12, 12, 4, 3);
                $('.type-picking').removeCols(12, 12, 4, 3);
                $('#search-type-picking').width('auto');
            }

            $('#search-type-picking').change(function () {
                var target = 0;
                var val = $(this).val();
                if (val != 0) {
                    $('.search-button-field').addCols(12, 12, 4, 3);
                    $('.type-picking').addCols(12, 12, 4, 3);
                    $(this).width('100%');
                } else {
                    $('.search-button-field').removeCols(12, 12, 4, 3);
                    $('.type-picking').removeCols(12, 12, 4, 3);
                    $(this).width('auto');
                }
                switch (val) {
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                        target = 'search-input';
                        break;
                }
                $('.search-field').each(function () {
                    if ($(this).hasClass(target)) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });
            });
            $('#search-button').click(function () {
                var searchType = $('#search-type-picking').val();
                var limit = $('#limit').val();
                var url = "<?php echo $page_url;?>&page_num=1&orderby=u_id&order=asc&n=1";
                var s = '';
                if (searchType == '6') {
                    s = $('input[name="from"]').val() + '+' + $('input[name="to"]').val();
                } else if (searchType != '0') {
                    s = $('input[name="s"]').val();
                }
                if (searchType != '0') {
                    url += '&search_type=' + searchType + '&s=' + s;
                }

                url += '&limit=' + limit;
                window.location = url;
            });

            $('.content-body').css('visibility', 'visible');
        })(jQuery);
    </script>
<?php
elseif ( isset( $_GET['n'] ) && $_GET['n'] == '2' ):
	include_once( 'single-buying-history.php' );
endif;
?>