<?php

function bsp_new() {
 ?>
			
						<table class="form-table">
					
					<tr valign="top">
						<th colspan="2">
						
						<h3>
						<?php _e ("What's New?" , 'bbp-style-pack' ) ; ?>
	

	</h3> 
	
	<h4><span style="color:blue"><?php _e('Version 4.3.1', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've added the ability to change the wordpress toolbar edit profile link to go to the bbpress profile edit page - see Login tab item 6" , 'bbp-style-pack') ; ?>
</p>
	
	<h4><span style="color:blue"><?php _e('Version 4.2.9/4.3.0', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("Just an update of the translation POT file to allow an update of the french version." , 'bbp-style-pack') ; ?>
</p>
	<h4><span style="color:blue"><?php _e('Version 4.2.7 and 4.2.8', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've further improved template loading. This should now work in most cases with both 2.5 and 2.6 versions, but also let admins amend the template priority to suit cases where themes or other plugins load templates.  See Forum Templates tab for details." , 'bbp-style-pack') ; ?>
</p>
	<h4><span style="color:blue"><?php _e('Version 4.2.6', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've amended some code to prevent topic and reply form templates loading unless needed" , 'bbp-style-pack') ; ?>
</p>
	<h4><span style="color:blue"><?php _e('Version 4.2.5', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've amended some code to allow for templates to work under 2.6rc7" , 'bbp-style-pack') ; ?>
</p>
	<h4><span style="color:blue"><?php _e('Version 4.2.4', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've amended the wording on the alternate template to make it clearer how to use" , 'bbp-style-pack') ; ?>
</p>
	<h4><span style="color:blue"><?php _e('Version 4.2.3', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've corrected a minor error in the import plugin settings" , 'bbp-style-pack') ; ?>
</p>
	<h4><span style="color:blue"><?php _e('Version 4.2.2', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've corrected a minor error in the css generation which ensures the forum information font works" , 'bbp-style-pack') ; ?>
</p>
	<h4><span style="color:blue"><?php _e('Version 4.2.1', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've added the option to recalculate the last active topic for sub forums as this can be wrong if topics are marked as spam or deleted" , 'bbp-style-pack') ; ?>
</p>
<h4><span style="color:blue"><?php _e('Version 4.2.0', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've corrected an issue where the style pack css was generated on each load, instead of only when it doesn't exist" , 'bbp-style-pack') ; ?>
</p>
<h4><span style="color:blue"><?php _e('Version 4.1.9/4.1.10', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've added the ability to have different topic and reply rules - see Topic/Reply Form tab" , 'bbp-style-pack') ; ?>
</p>	
	<h4><span style="color:blue"><?php _e('Version 4.1.8', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've added the ability for a confirmation when particpaints trash their own topics/replies - see Topic/Reply display tab item 18 & 19" , 'bbp-style-pack') ; ?>
</p>
<h4><span style="color:blue"><?php _e('Version 4.1.7', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've added the ability for particpaints to trash their own replies - see Topic/Reply display tab item 19" , 'bbp-style-pack') ; ?>
</p>
<h4><span style="color:blue"><?php _e('Version 4.1.6', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've corrected the subscription email reply to show the reply not the topic." , 'bbp-style-pack') ; ?>
</p>
<h4><span style="color:blue"><?php _e('Version 4.1.5', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've added the ability for particpaints to trash their own topics - see Topic/Reply display tab item 18" , 'bbp-style-pack') ; ?>
</p>	
<h4><span style="color:blue"><?php _e('Version 4.1.4', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("I've added \"template ='list'\" to the shortcode [bsp-display-topic-index] to just display a list of topics with links" , 'bbp-style-pack') ; ?>
</p>	
<h4><span style="color:blue"><?php _e('Version 4.1.1/4.1.2/4.1.3', 'bbp-style-pack' ) ; ?></span></h4>
<p>
<?php _e("Debug releases to fix issues with subscription emails in html" , 'bbp-style-pack') ; ?>
</p>		
	




 <?php
}
