
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
            crossorigin="anonymous"></script>
    <title><?=bloginfo()?></title>
    <style>
        body {
            margin: 0;
            padding: 0;
        }
        .title-wrapper {
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 5px 0;
        }
        .site-title {
            margin: 0 5px;
        }
        .logo-wrapper {
            margin: 0 5px;
            width: 80px;
        }
        .logo-wrapper img {
            width: 100%;
        }
        .iframe-wrapper {
            width:100%;
        }
        .iframe-wrapper iframe {
            width: 100%;
            height: 100%;
        }
    </style>
    <?php if($css = $this->lockedup_page_custom_css): ?>
    <style>
        <?=$css?>
    </style>
    <?php endif;?>
</head>
<body>
<?php

if(current_user_can('create_pages')) {
    $alert_button = ($this->alert_button_url)?$this->alert_button_url:plugin_dir_url(__FILE__).'assets/img/alert-button.png';
    ?>
    <div class="alert-links">
        <div id="wp-admin-bar-limsc-show-alert-message-button">
            <a href="#">
                <img id="" src="<?=$alert_button?>" alt="">
            </a>
        </div>
        <div class="goto-admin">
            <a href="/wp-admin">前往後臺</a>
        </div>
    </div>
    <?php
}
?>
    <div class="main">
        <?php if($this->lockedup_page_logo_turn_on == '1'):?>
        <div class="title-wrapper">
            <span class="logo-wrapper">
            <?php if(function_exists('mfn_opts_get')):?>

                <img src="<?=mfn_opts_get('logo-img')?>" alt="logo">

            <?php elseif(wp_get_theme()->get('Name') == 'The7' || wp_get_theme()->parent()->get('Name') == 'The7'): ?>

                <?php if($img_src = get_option('the7')['header-logo_regular'][0] != ''):?>
                    <img src="<?=$img_src?>" alt="logo">
                <?php endif;?>

            <?php else:?>
            <?php endif;?>
            </span>
            <span class="site-title"><?=bloginfo()?></span>
        </div>

        <?php endif;?>

        <?php if($this->lockedup_page_iframe_url != ''):?>
        <div class="iframe-wrapper">
            <iframe src="<?=$this->lockedup_page_iframe_url?>" frameborder="0"></iframe>
        </div>
        <?php endif;?>
    </div>
    <?php
    do_action('limsc_expiration_message');
    ?>
</body>
</html>