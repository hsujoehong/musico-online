<?php
/**
 * Plugin Name: Lohasit Maintaining Service Client
 * Description: 樂活壓板系統 - 維護管理客戶端
 * Version: 1.1.0
 * Author URI: https://www.lohaslife.cc/
 * Author: Lohas IT
 */

class LohasitMaintainingServiceClient{

    private static $instance;

    public function __construct()
    {
        $this->include_files();
        $this->register_hooks();
        LimscSetting::boot();
    }

    public static function install_options()
    {
        add_option('limsc_turn_on', '');
        add_option('limsc_due_date', current_time('Y-m-d'));
        add_option('limsc_alert_date', current_time('Y-m-d'));
        add_option('limsc_alert_message', '');
        add_option('limsc_alert_button_url', '');
        add_option('limsc_lockedup_page_logo_turn_on', '');
        add_option('limsc_lockedup_page_iframe_url', '');
        add_option('limsc_lockedup_page_custom_css', '');
        add_option('limsc_client_name', '');
    }

    /**
     * @property string $alert_message
     * @property string $due_date
     * @property string $alert_date
     * @property string $alert_message
     * @property string $lockedup_page_logo_turn_on
     * @property string $lockedup_page_iframe_url
     * @param $key
     *
     * @return mixed|void
     */
    public function __get($key)
    {
        return get_option('limsc_' . $key);
    }

    private function set_option($key, $value) {
        $allowed = [
            'client_name',
            'turn_on',
            'due_date',
            'alert_date',
            'alert_message',
            'alert_button_url',
            'lockedup_page_logo_turn_on',
            'lockedup_page_iframe_url',
            'lockedup_page_custom_css'
        ];
        if(in_array($key, $allowed))
            return update_option('limsc_'. $key, $value);
        else
            return false;
    }

    private function include_files()
    {
        require_once (plugin_dir_path(__FILE__). 'admin/includes/class-limsc-setting-page.php');
    }

    private function register_hooks()
    {

        add_action('admin_head', array($this, 'admin_head_style'));
        add_action('admin_footer', array($this, 'expiration_message'));
        add_action('wp_footer', array($this, 'expiration_message'));
        add_action('limsc_expiration_message', array($this, 'expiration_message'));
        add_action('init', array($this, 'disable_site'), 30);
        add_action('admin_bar_menu', array($this, 'add_show_expiration_message_button'), 999999);
        add_action('init', array($this, 'rewrite'));
        add_filter('template_include', array($this, 'echo_404_page'));

        add_action('wp_ajax_nopriv_limsc_ack', array($this, 'ajax_ack'));
        add_action('wp_ajax_limsc_ack', array($this, 'ajax_ack'));

        add_action('wp_ajax_nopriv_limsc_get_all_setting', array($this, 'ajax_get_all_setting'));
        add_action('wp_ajax_limsc_get_all_setting', array($this, 'ajax_get_all_setting'));

        add_action('wp_ajax_nopriv_limsc_set_turn_on', array($this, 'ajax_set_turn_on'));
        add_action('wp_ajax_limsc_set_turn_on', array($this, 'ajax_set_turn_on'));


        add_action('wp_ajax_nopriv_limsc_set_expiration_date', array($this, 'ajax_set_expiration_date'));
        add_action('wp_ajax_limsc_set_expiration_date', array($this, 'ajax_set_expiration_date'));


        add_action('wp_ajax_nopriv_limsc_set_alert_date', array($this, 'ajax_set_alert_date'));
        add_action('wp_ajax_limsc_set_alert_date', array($this, 'ajax_set_alert_date'));


        add_action('wp_ajax_nopriv_limsc_set_alert_message', array($this, 'ajax_set_alert_message'));
        add_action('wp_ajax_limsc_set_alert_message', array($this, 'ajax_set_alert_message'));

        add_action('wp_ajax_nopriv_limsc_set_alert_button_url', array($this, 'ajax_set_alert_button_url'));
        add_action('wp_ajax_limsc_set_alert_button_url', array($this, 'ajax_set_alert_button_url'));

        add_action('wp_ajax_nopriv_limsc_set_client_name', array($this, 'ajax_set_client_name'));
        add_action('wp_ajax_limsc_set_client_name', array($this, 'ajax_set_client_name'));

        add_action('wp_ajax_nopriv_limsc_set_lockedup_page_iframe_url', array($this, 'ajax_set_lockedup_page_iframe_url'));
        add_action('wp_ajax_limsc_set_lockedup_page_iframe_url', array($this, 'ajax_set_lockedup_page_iframe_url'));

        add_action('wp_ajax_nopriv_limsc_set_lockedup_page_logo_turn_on', array($this, 'ajax_set_lockedup_page_logo_turn_on'));
        add_action('wp_ajax_limsc_set_lockedup_page_logo_turn_on', array($this, 'ajax_set_lockedup_page_logo_turn_on'));

        add_action('wp_ajax_nopriv_limsc_set_lockedup_page_custom_css', array($this, 'ajax_set_lockedup_page_custom_css'));
        add_action('wp_ajax_limsc_set_lockedup_page_custom_css', array($this, 'ajax_set_lockedup_page_custom_css'));

    }

    public static function get_instance()
    {
        if(is_null(self::$instance))
            self::$instance = new self;
        return self::$instance;
    }

    public function rewrite()
    {
        flush_rewrite_rules();
        add_rewrite_endpoint('limsc_404', EP_PERMALINK | EP_PAGES );
    }

    public function echo_404_page($template)
    {
        $rewriteValue = get_query_var('limsc_404', false);
        if($rewriteValue) {
            include_once(plugin_dir_path(__FILE__) . '/templates/404.php');
            $template = '';
        }
        return $template;
    }

    public function admin_head_style()
    {
        ?>
        <style>
            #wp-admin-bar-limsc-show-alert-message-button {
                display: block !important;
            }
        </style>
        <?php
    }

    private function is_showing_alert_message()
    {
        $date_now = new DateTime();
        $date_alert = new DateTime($this->alert_date);
        if($date_now < $date_alert)
            return false;
        return true;
    }

    public function add_show_expiration_message_button($wp_admin_bar)
    {
        if($this->turn_on != '1') return;
        if($this->is_showing_alert_message()) {
            $alert_button = ($this->alert_button_url)?$this->alert_button_url:plugin_dir_url(__FILE__).'assets/img/alert-button.png';
            $args = array(
                'id'    => 'limsc-show-alert-message-button',
                'title' => '<span class="ab-icon"><img src="'.$alert_button.'" alt=""></span>'.'',
                'href'  => '#',
                'meta'  => [
                    'class' => 'limsc-show-aliert-message-button'
                ]
            );
            $wp_admin_bar->add_node( $args );
        }
    }

    private function replace_message($message)
    {
        $replacements = [
            '{due_date}' => $this->due_date,
            '{client_name}' => $this->client_name
        ];
        foreach($replacements as $key => $replacement) {
            $message = str_replace($key, $replacement, $message);
        }
        return $message;
    }

    public function expiration_message()
    {
        if($this->turn_on != '1') return;
        if($this->is_showing_alert_message()) {
            $message = $this->alert_message;
            $message = $this->replace_message($message);
            $message = nl2br($message);
            ob_start();
            include_once(plugin_dir_path(__FILE__) . '/templates/expiration-message.php');
            echo ob_get_clean();
            ?>
            <script>
                jQuery(function($){
                    $('#wp-admin-bar-limsc-show-alert-message-button').on('click', function(e){
                        e.preventDefault();
                        $('#lohasit-maintaining-service-expiration-message').show();
                    })
                });
                // jQuery(function($){
                //     $('#wp-admin-bar-limsc-show-aliert-message-button').on('click', function(e){
                //         e.preventDefault();
                //         $.blockUI({
                //             message: $('#lohasit-maintaining-service-expiration-message'),
                //             onOverlayClick: $.unblockUI,
                //             css: {
                //                 border: 'none',
                //                 padding: 10,
                //                 width: '80%',
                //                 left: '50%',
                //                 top: '50%',
                //                 transform: 'translate(-50%,-50%)',
                //                 borderRadius: 10,
                //                 '-webkit-border-radius': '10px',
                //                 '-moz-border-radius': '10px'
                //             }
                //         });
                //     });
                // });
            </script>
            <?php
        }
    }

    public function disable_site()
    {
        $due_date = new DateTime($this->due_date);
        $date_now = new DateTime();
        if($this->turn_on == '1' && $date_now > $due_date) {
            if( !(is_user_logged_in() && current_user_can('administrator'))
                && !is_admin()
                && basename( $_SERVER['PHP_SELF']) != 'wp-login.php'
                && basename( $_SERVER['PHP_SELF']) != 'wp-cron.php'
                && basename( $_SERVER['PHP_SELF']) != 'xmlrpc.php' ){
                include_once(plugin_dir_path(__FILE__) . '/templates/404.php');
                exit();
            }
        }
    }

    private function disable_cors()
    {
        header('Access-Control-Allow-Origin: *');
    }
    public function ajax_get_expiration_date()
    {
        $this->disable_cors();
        wp_send_json([
            'result' => $this->due_date
        ]);
        wp_die();
    }

    public function ajax_ack()
    {
        $this->disable_cors();
        wp_send_json([
            'result' => true
        ]);
        wp_die();
    }

    public function ajax_set_expiration_date()
    {
        $this->disable_cors();
        if(isset($_POST['value'])) {
            $value = sanitize_text_field($_POST['value']);
            if($this->set_option('due_date', $value)) {
                wp_send_json([
                    'success' => true,
                    'date' => $value
                ]);
            } else {
                wp_send_json([
                    'success' => false,
                    'msg' => '未更新'
                ]);
            }

        } else {
            wp_send_json([
                'success' => false,
                'msg' => '值有誤'
            ]);
        }
        wp_die();
    }

    public function ajax_set_turn_on()
    {
        $this->disable_cors();
        if(isset($_POST['value'])) {
            $value = sanitize_text_field($_POST['value']);

            if($this->set_option('turn_on', $value)) {
                wp_send_json([
                    'success' => true
                ]);
            } else {
                wp_send_json([
                    'success' => false,
                    'msg' => '未更新'
                ]);
            }

        } else {
            wp_send_json([
                'success' => false,
                'msg' => '值有誤'
            ]);
        }
    }

    public function ajax_set_alert_date()
    {
        $this->disable_cors();
        if(isset($_POST['value'])) {
            $value = sanitize_text_field($_POST['value']);
            if($this->set_option('alert_date', $value)) {
                wp_send_json([
                    'success' => true,
                    'date' => $value
                ]);
            } else {
                wp_send_json([
                    'success' => false,
                    'msg' => '未更新'
                ]);
            }
        } else {
            wp_send_json([
                'success' => false,
                'msg' => '值有誤'
            ]);
        }
        wp_die();
    }

    public function ajax_set_alert_message()
    {
        $this->disable_cors();
        if(isset($_POST['value'])) {
            $alert_message = json_decode(json_encode($_POST['value']));
            $alert_message = str_replace('\n', "\r\n", $alert_message);
            if($this->set_option('alert_message', $alert_message)) {
                wp_send_json([
                    'success' => true,
                    'alert_message' => $alert_message
                ]);
            } else {
                wp_send_json([
                    'success' => false,
                    'msg' => '未更新'
                ]);
            }

        } else {
            wp_send_json([
                'success' => false,
                'msg' => '值有誤'
            ]);
        }
        wp_die();
    }

    public function ajax_set_alert_button_url()
    {
        $this->disable_cors();
        if(isset($_POST['value'])) {
            $value = sanitize_text_field($_POST['value']);
            if($this->set_option('alert_button_url', $value)) {
                wp_send_json([
                    'success' => true,
                    'date' => $value
                ]);
            } else {
                wp_send_json([
                    'success' => false,
                    'msg' => '未更新'
                ]);
            }
        } else {
            wp_send_json([
                'success' => false,
                'msg' => '值有誤'
            ]);
        }
        wp_die();
    }
    public function ajax_set_client_name()
    {
        $this->disable_cors();
        if(isset($_POST['value'])) {
            $value = sanitize_text_field($_POST['value']);
            if($this->set_option('client_name', $value)) {
                wp_send_json([
                    'success' => true,
                    'name' => $value
                ]);
            } else {
                wp_send_json([
                    'success' => false,
                    'msg' => '未更新'
                ]);
            }
        } else {
            wp_send_json([
                'success' => false,
                'msg' => '值有誤'
            ]);
        }
        wp_die();
    }

    public function ajax_set_lockedup_page_logo_turn_on()
    {
        $this->disable_cors();
        if(isset($_POST['value'])) {
            $value = sanitize_text_field($_POST['value']);

            if($this->set_option('lockedup_page_logo_turn_on', $value)) {
                wp_send_json([
                    'success' => true
                ]);
            } else {
                wp_send_json([
                    'success' => false,
                    'msg' => '未更新'
                ]);
            }

        } else {
            wp_send_json([
                'success' => false,
                'msg' => '值有誤'
            ]);
        }
        wp_die();
    }
    
    public function ajax_set_lockedup_page_iframe_url()
    {
        $this->disable_cors();
        if(isset($_POST['value'])) {
            $value = sanitize_text_field($_POST['value']);
            if($this->set_option('lockedup_page_iframe_url', $value)) {
                wp_send_json([
                    'success' => true,
                    'url' => $value
                ]);
            } else {
                wp_send_json([
                    'success' => false,
                    'msg' => '未更新'
                ]);
            }
        } else {
            wp_send_json([
                'success' => false,
                'msg' => '值有誤'
            ]);
        }
        wp_die();
    }

    public function ajax_set_lockedup_page_custom_css()
    {
        $this->disable_cors();
        if(isset($_POST['value'])) {
            $css = json_decode(json_encode($_POST['value']));
            $css = str_replace('\n', "\r\n", $css);
            if($this->set_option('lockedup_page_custom_css', $css)) {
                wp_send_json([
                    'success' => true,
                    'css' => $css
                ]);
            } else {
                wp_send_json([
                    'success' => false,
                    'msg' => '未更新'
                ]);
            }

        } else {
            wp_send_json([
                'success' => false,
                'msg' => '值有誤'
            ]);
        }
        wp_die();
    }

    public function ajax_get_all_setting()
    {
        $this->disable_cors();
        if($this->turn_on == '1') $turn_on = true;
        else $turn_on = false;

        if($this->lockedup_page_logo_turn_on == '1') $lockedup_page_logo_turn_on = true;
        else $lockedup_page_logo_turn_on = false;
        wp_send_json([
            'turn_on' => $turn_on,
            'client_name' => $this->client_name,
            'alert_date' => $this->alert_date,
            'alert_message' => $this->alert_message,
            'alert_button_url' => $this->alert_button_url,
            'expiration_date' => $this->due_date,
            'lockedup_page_logo_turn_on' => $lockedup_page_logo_turn_on,
            'lockedup_page_iframe_url' => $this->lockedup_page_iframe_url,
            'lockedup_page_custom_css' => $this->lockedup_page_custom_css,
        ]);
        wp_die();
    }
}

$GLOBALS['LohasitMaintainingServiceClient'] = new LohasitMaintainingServiceClient();
register_activation_hook(__FILE__, array('LohasitMaintainingServiceClient', 'install_options'));