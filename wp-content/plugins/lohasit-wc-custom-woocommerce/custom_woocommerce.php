<?php 
/*
 * Plugin Name: Lohasit WC Custom Woocommerce
 * Description: 樂活壓板系統 - Woocommerce - 自訂Woocommerce 金物流設定的頁面，方便客戶操作
 * Version: 1.0.0
 * Author URI: https://www.lohaslife.cc/
 * Author: Lohas IT
*/

add_action('admin_menu', 'custom_woocommerce_option_menu_page');
function custom_woocommerce_option_menu_page(){

	add_menu_page( '金物流設定', '金物流設定', 'manage_options', 'custom_customer_woocommerce_page_option', 'custom_customer_creat_page', 'dashicons-cart', 50 );
	add_submenu_page( 'custom_customer_woocommerce_page_option', '綠界物流運費設定', '綠界物流運費設定', 'manage_options', 'custom_customer_woocommerce_page_option', 'custom_customer_creat_page' );
	add_submenu_page( 'custom_customer_woocommerce_page_option', '金流設定', '金流設定', 'manage_options', 'custom_customer_woocommerce_shipping_page_option', 'custom_customer_shipping_page' );
}


function custom_customer_creat_page(){ 
	
	include('custom_woocommerce_backend.php'); 
	include('custom_woocommerce_info.php'); 
	include('custom_woocommerce_css.php'); 
	?>
	<div class="wrap">
		<h1>綠界物流運費設定</h1>
		<form method="post">
			<table>
				<thead>
					<tr>
						<th>運送方式</th>
						<th>啟用</th>
						<th>標題</th>
						<th>運費</th>
						<th>免運金額</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ( $shipping_method_data as $i ) { ?>
						<tr id="<?php echo $i['shipping-id'] ?>">
							<th><?php echo $i['shipping-method'] ?></th>
							<th>
								<input type="hidden" name="<?php echo $i['shipping-id'] ?>-ckeck" value="<?php echo $i['shipping-check'] ?>">
								<input type="checkbox" class="checkout">
							</th>
							<th>
								<input type="text" name="<?php echo $i['shipping-id'] ?>-title" value="<?php echo $i['shipping-title'] ?>">
							</th>
							<th>
								<input type="number" name="<?php echo $i['shipping-id'] ?>-shipment" placeholder="運費金額" value="<?php echo $i['shipping-shipment'] ?>">
							</th>
							<th>
								<input type="number" name="<?php echo $i['shipping-id'] ?>-shipment-free_cost" placeholder="訂單金額多少以上免運費" value="<?php echo $i['shipping-free_cost'] ?>">
							</th>
							<th class="setting info-fields">
								<span class=" setting dashicons dashicons-admin-generic"></span>

								<input type="hidden" name="<?php echo $i['shipping-id'] ?>-merchant_id" value="<?php echo $i['shipping-merchant_id'] ?>">
								<input type="hidden" name="<?php echo $i['shipping-id'] ?>-shipping-hash_key" value="<?php echo $i['shipping-hash_key'] ?>">
								<input type="hidden" name="<?php echo $i['shipping-id'] ?>-hash_iv" value="<?php echo $i['shipping-hash_iv'] ?>">
								<input type="hidden" name="<?php echo $i['shipping-id'] ?>-sender_state" value="<?php echo $i['shipping-sender_state'] ?>">
								<input type="hidden" name="<?php echo $i['shipping-id'] ?>-sender_name" value="<?php echo $i['shipping-sender_name'] ?>">

								<input type="hidden" name="<?php echo $i['shipping-id'] ?>-sender_cellphone" value="<?php echo $i['shipping-sender_cellphone'] ?>">
								<input type="hidden" name="<?php echo $i['shipping-id'] ?>-sender_zip_code" value="<?php echo $i['shipping-sender_zip_code'] ?>">
								<input type="hidden" name="<?php echo $i['shipping-id'] ?>-sender_address" value="<?php echo $i['shipping-sender_address'] ?>">
							</th>
						</tr>
					<?php } ?>
				</tbody>
           		
			</table>
			<div class="more-info">
				<div class="info tcat_shipping">
					<h3>黑貓宅急便</h3>
					<div class="info-field">
						<h4>商店代號</h4>
						<input type="text" name="tcat_shipping_ecpay_merchant_id" placeholder="特店編號(MerchantID)">
					</div>
					<div class="info-field">
						<h4>HashKey</h4>
						<input type="text" name="tcat_shipping_ecpay_hash_key" placeholder="All in one 介接的 HashKey">
					</div>
					<div class="info-field">
						<h4>HashIV</h4>
						<input type="text" name="tcat_shipping_ecpay_hash_iv" placeholder="All in one 介接的 HashKey">
					</div>
					<div class="info-field">
						<h4>商店所在縣市</h4>
						<input type="text" name="tcat_shipping_ecpay_sender_state" placeholder="商店所在縣市">
					</div>
					<div class="info-field">
						<h4>寄件人*</h4>
						<input type="text" name="tcat_shipping_ecpay_sender_name" placeholder="請填寫寄件人（必填）">
					</div>
					<div class="info-field">
						<h4>寄件人手機*</h4>
						<input type="text" name="tcat_shipping_ecpay_sender_cellphone" placeholder="請填寫寄件人手機（必填）">
					</div>
					<div class="info-field">
						<h4>寄件人郵遞區號*</h4>
						<input type="text" name="tcat_shipping_ecpay_sender_zip_code" placeholder="請填寫寄件人郵遞區號（必填）">
					</div>
					<div class="info-field">
						<h4>寄件人地址*</h4>
						<input type="text" name="tcat_shipping_ecpay_sender_address" placeholder="請填寫寄件人地址（必填）">
					</div>
					<div class="btn_group">
						<div class="btn update button-primary">更新</div>
						<div class="btn cancle button">取消</div>
					</div>
				</div>
				<div class="info cvs_unimart">
					<h3>超商取貨（統一）</h3>
					<div class="info-field">
						<h4>商店代號</h4>
						<input type="text" name="cvs_unimart_ecpay_merchant_id" placeholder="特店編號(MerchantID)">
					</div>
					<div class="info-field">
						<h4>HashKey</h4>
						<input type="text" name="cvs_unimart_ecpay_hash_key" placeholder="All in one 介接的 HashKey">
					</div>
					<div class="info-field">
						<h4>HashIV</h4>
						<input type="text" name="cvs_unimart_ecpay_hash_iv" placeholder="All in one 介接的 HashKey">
					</div>
					<div class="info-field none">
						<h4>商店所在縣市</h4>
						<input type="text" name="cvs_unimart_ecpay_sender_state" placeholder="商店所在縣市">
					</div>
					<div class="info-field">
						<h4>寄件人*</h4>
						<input type="text" name="cvs_unimart_ecpay_sender_name" placeholder="請填寫寄件人（必填）">
					</div>
					<div class="info-field">
						<h4>寄件人手機*</h4>
						<input type="text" name="cvs_unimart_ecpay_sender_cellphone" placeholder="請填寫寄件人手機（必填）">
					</div>
					<div class="info-field none">
						<h4>寄件人郵遞區號*</h4>
						<input type="text" name="cvs_unimart_ecpay_sender_zip_code" placeholder="請填寫寄件人郵遞區號（必填）">
					</div>
					<div class="info-field none">
						<h4>寄件人地址*</h4>
						<input type="text" name="cvs_unimart_ecpay_sender_address" placeholder="請填寫寄件人地址（必填）">
					</div>
					<div class="btn_group">
						<div class="btn update button-primary">更新</div>
						<div class="btn cancle button">取消</div>
					</div>
				</div>
				<div class="info cvs_fami">
					<h3>超商取貨(全家)</h3>
					<div class="info-field">
						<h4>商店代號</h4>
						<input type="text" name="cvs_fami_ecpay_merchant_id" placeholder="特店編號(MerchantID)">
					</div>
					<div class="info-field">
						<h4>HashKey</h4>
						<input type="text" name="cvs_fami_ecpay_hash_key" placeholder="All in one 介接的 HashKey">
					</div>
					<div class="info-field">
						<h4>HashIV</h4>
						<input type="text" name="cvs_fami_ecpay_hash_iv" placeholder="All in one 介接的 HashKey">
					</div>
					<div class="info-field none">
						<h4>商店所在縣市</h4>
						<input type="text" name="cvs_fami_ecpay_sender_state" placeholder="商店所在縣市">
					</div>
					<div class="info-field">
						<h4>寄件人*</h4>
						<input type="text" name="cvs_fami_ecpay_sender_name" placeholder="請填寫寄件人（必填）">
					</div>
					<div class="info-field">
						<h4>寄件人手機*</h4>
						<input type="text" name="cvs_fami_ecpay_sender_cellphone" placeholder="請填寫寄件人手機（必填）">
					</div>
					<div class="info-field none">
						<h4>寄件人郵遞區號*</h4>
						<input type="text" name="cvs_fami_ecpay_sender_zip_code" placeholder="請填寫寄件人郵遞區號（必填）">
					</div>
					<div class="info-field none">
						<h4>寄件人地址*</h4>
						<input type="text" name="cvs_fami_ecpay_sender_address" placeholder="請填寫寄件人地址（必填）">
					</div>
					<div class="btn_group">
						<div class="btn update button-primary">更新</div>
						<div class="btn cancle button">取消</div>
					</div>
				</div>
				<div class="info home_ecan">
					<h3>宅配通</h3>
					<div class="info-field">
						<h4>商店代號</h4>
						<input type="text" name="home_ecan_ecpay_merchant_id" placeholder="特店編號(MerchantID)">
					</div>
					<div class="info-field">
						<h4>HashKey</h4>
						<input type="text" name="home_ecan_ecpay_hash_key" placeholder="All in one 介接的 HashKey">
					</div>
					<div class="info-field">
						<h4>HashIV</h4>
						<input type="text" name="home_ecan_ecpay_hash_iv" placeholder="All in one 介接的 HashKey">
					</div>
					<div class="info-field">
						<h4>商店所在縣市</h4>
						<input type="text" name="home_ecan_ecpay_sender_state" placeholder="商店所在縣市">
					</div>
					<div class="info-field">
						<h4>寄件人*</h4>
						<input type="text" name="home_ecan_ecpay_sender_name" placeholder="請填寫寄件人（必填）">
					</div>
					<div class="info-field">
						<h4>寄件人手機*</h4>
						<input type="text" name="home_ecan_ecpay_sender_cellphone" placeholder="請填寫寄件人手機（必填）">
					</div>
					<div class="info-field">
						<h4>寄件人郵遞區號*</h4>
						<input type="text" name="home_ecan_ecpay_sender_zip_code" placeholder="請填寫寄件人郵遞區號（必填）">
					</div>
					<div class="info-field">
						<h4>寄件人地址*</h4>
						<input type="text" name="home_ecan_ecpay_sender_address" placeholder="請填寫寄件人地址（必填）">
					</div>
					<div class="btn_group">
						<div class="btn update button-primary">更新</div>
						<div class="btn cancle button">取消</div>
					</div>
				</div>
				<div class="info-bg">
				
				</div>
			</div>
			<button type="submit" class="button-primary" >儲存變更</button>
		</form>
		
	</div>
	<script type="text/javascript">
		jQuery(document).ready(function($){

			$('input[type="checkbox"]').change(function(){
				if(this.checked) {
					$(this).siblings('input[type="hidden"]').val('yes');
				} else {
					$(this).siblings('input[type="hidden"]').val('no');
				}
			});
			$('tr th:nth-of-type(2) input[type="hidden"]').each(function(){
				if( $(this).val() == 'yes' ){
					$(this).siblings('input[type="checkbox"]').attr('checked' ,true);
				}
			});


			$('span.setting').on('click', function(){
				var id = $(this).parents('tr').attr('id');
				openinfo(id);
				$(this).addClass('active');

				var fields_array = [];
				var input_length = $(this).siblings('input[type="hidden"]').length;
				for (var i = 0; i<input_length; i++) {
					var input_val = $(this).parent().find('input[type="hidden"]:nth-of-type('+(i+1)+')').val();
					fields_array.push(input_val);
				}
				for (var i = 0; i<fields_array.length; i++) {
					$('.info.'+id+' .info-field:nth-of-type('+(i+1)+')').find('input').val(fields_array[i]);
					console.log(fields_array[i]);
				}
			});
			$('.more-info .info-bg, .cancle.btn').on('click', function(){
				closeinfo();
			});

			$('.update.btn').on('click', function(){
				var id = $(this).parents('.info').attr('class');
					id = id.replace('info ', '');
					id = id.replace('active', '');
					console.log('id: '+id);
				var info_array = [],
					field_length = $(this).parent().siblings('.info-field').length;
				for( var i = 0; i<field_length; i++) {
					var field_val = $(this).parents('.info').find('.info-field:nth-of-type('+(i+1)+') input').val();
					info_array.push(field_val);
				}
				for (var i = 0; i< info_array.length; i++) {
					$('tbody tr#'+id+' .info-fields input[type="hidden"]:nth-of-type('+(i+1)+')').val(info_array[i]);
				}
				closeinfo();
			});


			function openinfo(id){
				$('.info.'+id).parent().addClass('active');
				$('.info.'+id).addClass('active');
			}
			function closeinfo(){
				$('.info').removeClass('active');
				$('.more-info').removeClass('active');
				$('span.setting').removeClass('active');
			}
		});
	</script>
<?php }
function custom_customer_shipping_page(){
	include('custom_woocommerce_billing_page.php');
}
?>