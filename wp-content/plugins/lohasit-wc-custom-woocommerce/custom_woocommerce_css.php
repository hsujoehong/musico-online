<style type="text/css">
		.none {
			display: none !important;
		}
		table {
		    width: 80%;
		    text-align: left;
		}
		input {
			width: 95%;
		}
		thead th {
			font-weight: 700;
		}
		tbody th {
			font-weight: 300;
		}
		span.setting {
		    border: 1px solid #aaa;
		    border-radius: 6px;
		    padding: 4px;
		    transition: all 0.3s; 
			-moz-transition: all 0.3s; /* Firefox 4 */ 
			-webkit-transition: all 0.3s; /* Safari 和 Chrome */ 
			-o-transition: all 0.3s; /* Opera */
			cursor: pointer;
		}
		span.setting:hover, span.setting.active {
		    background-color: rgba(150,150,150,0.8);
		    color: rgba(255,255,255,0.8);
		}
		.hidden {
			display: none;
			opacity: 0;
		}
		.more-info {
			display: none;
		}
		.more-info.active {
			display: block;
		}
		.more-info .info {
			display: none;
		}
		.more-info .info.active {
			display: block;
			background-color: rgba(255,255,255,0);
			width: 70%;
			transition: all 0.3s; 
			position: fixed;
		    top: 50%;
		    left: 50%;
		    transform: translateX(-50%) translateY(-50%);
			-webkit-transform: translateX(-50%) translateY(-50%);
			-moz-transform: translateX(-50%) translateY(-50%);
			-ms-transform: translateX(-50%) translateY(-50%);
			-o-transform: translateX(-50%) translateY(-50%);
			-moz-transition: all 0.3s; /* Firefox 4 */ 
			-webkit-transition: all 0.3s; /* Safari 和 Chrome */ 
			-o-transition: all 0.3s; /* Opera */
			padding: 10px;
		    border-radius: 5px;
		    box-shadow: 0 0 15px 5px rgba(0,0,0,0.3);
		}
		.more-info.active .info.active {
		    width: 65%;
		    height: auto;
		    max-width: 1000px;
   			min-height: 200px;
		    background-color: #fff;
		    position: fixed;
		    top: 50%;
		    left: 50%;
		    z-index: 5;
		    transform: translateX(-50%) translateY(-50%);
		}
		.more-info.active .info-field {
		    width: 49%;
		    margin: 0 0.5%;
		    display: block;
    		float: left;
		}
		.more-info.active h3 {
		    text-align: center;
		}
		.more-info.active h4, .more-info.active input {
		    display: inline-block;
		}
		.more-info.active h4 {
		    width: 30%;
		    text-align: right;
		    padding-right: 10px;
		    box-sizing: border-box;
	        margin: 10px 0;
		}
		.more-info.active input {
		    width: 65%;
		    margin: 0;
		}
		.more-info.active .btn_group {
		    width: 100%;
		    float: left;
		    text-align: center;
		    margin: 30px 0 10px;
		}
		.more-info .info-bg {
			background-color: rgba(0,0,0,0);
			transition: all 0.3s; 
			-moz-transition: all 0.3s; /* Firefox 4 */ 
			-webkit-transition: all 0.3s; /* Safari 和 Chrome */ 
			-o-transition: all 0.3s; /* Opera */
		}
		.more-info.active .info-bg {
		    position: fixed;
		    width: 100%;
		    height: 100%;
		    top: 0;
		    left: 0;
		    background-color: rgba(0,0,0,0.5);
		}
		@media only screen and (max-width: 960px) {
			table {
				width: 100%;
			}
		}
		@media screen and (max-width: 782px) {
			.more-info.active .info-field {
			    width: 100%;
    			margin: 5px 0;
			}
			.more-info.active .info.active {
				width: 80%;
			}
		}

	</style>