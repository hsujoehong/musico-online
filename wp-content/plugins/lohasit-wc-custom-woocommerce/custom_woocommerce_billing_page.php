﻿<?php include('custom_woocommerce_billing_page.css'); ?>
<?php include('custom_woocommerce_billing_backend.php'); ?>
<div class="wrap">
	<h1>金流設定(點擊文字即可看到設定內容)</h1>
	<form method="post">
		<div class="bacs group">
			<div class="header">
				<input type="checkbox" name="" class="check"
				<?php 
					if($bacs_check=="yes")echo "checked";
				?>><input type="hidden" class="" name="bacs_check" value="<?php echo $bacs_check; ?>">
				<h3 class="title-click">銀行轉帳</h3>
				<span class="dashicons dashicons-arrow-up"></span>
				<span class="dashicons dashicons-arrow-down"></span>
			</div>
			<div class="body">
				<p>允許使用銀行轉帳，就是一般人常說的 ATM 轉帳</p>
				<div class="field bacs title">
					<h4>標題</h4>
					<div class="content">
						<input type="text" name="bacs_title" value="<?php echo $bacs_title; ?>">
					</div>
				</div>
				<div class="field bacs description">
					<h4>商品說明</h4>
					<div class="content">
						<textarea name="bacs_description"><?php echo $bacs_description; ?></textarea>
					</div>
				</div>
				<div class="field bacs instructions">
					<h4>指示</h4>
					<div class="content">
						<textarea name="bacs_instructions"><?php echo $bacs_instructions; ?></textarea>
					</div>
				</div>
				<div class="field bacs accounts">
					<h4>帳號詳細資料</h4>
					<div class="content">
						<div class="accounts_input_table">
							<table>
								<thead>
									<td>戶名</td>
									<td>帳號</td>
									<td>銀行別(代號)</td>
									<td>排序編號</td>
									<td>IBAN 國際銀行帳戶碼</td>
									<td>BIC / Swift 國際銀行編號</td>
									<td>刪除</td>
								</thead>
								<tbody>
									<?php
										$bacs_account_length = count( $bacs_account );
										//$bacs_account_length = 3;
										for ( $i = 0; $i < $bacs_account_length; $i ++ ) {
									?>
										<tr class="bacs_account_<?php echo $i ?>">
											<td>
												<input type="text" name="bacs_account_name[<?php echo $i ?>]" value="<?php echo $bacs_account[$i]['bacs_account_name']; ?>">
											</td>
											<td>
												<input type="number" name="bacs_account_number[<?php echo $i ?>]" value="<?php echo $bacs_account[$i]['bacs_account_number']; ?>">
											</td>
											<td>
												<input type="text" name="bacs_bank_name[<?php echo $i ?>]" value="<?php echo $bacs_account[$i]['bacs_bank_name']; ?>">
											</td>
											<td>
												<input type="number" name="bacs_sort_code[<?php echo $i ?>]" value="<?php echo $bacs_account[$i]['bacs_sort_code']; ?>">
											</td>
											<td>
												<input type="number" name="bacs_iban[<?php echo $i ?>]" value="<?php echo $bacs_account[$i]['bacs_iban']; ?>">
											</td>
											<td>
												<input type="number" name="bacs_bic[<?php echo $i ?>]" value="<?php echo $bacs_account[$i]['bacs_bic'] ?>">
											</td>
											<td><span class="dashicons dashicons-trash delete_bacs_account"></span></td>
										</tr>
									<?php
										}
									?>
								</tbody>
							</table>
							<input id="bacs_account_num" type="hidden" name="tr_num" value="<?php echo $bacs_account_length ?>">
							<div class="btn add_btn button">新增帳號</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<div class="paypal group">
			<div class="header">
				<input type="checkbox" name="" disabled="disabled" <?php
					if($paypal_check=="yes")echo "checked";
				?>><input type="hidden" class="" name="paypal_check" value="<?php echo $paypal_check; ?>">
				<h3 class="title-click">PayPal</h3>
				<span class="dashicons dashicons-arrow-up"></span>
				<span class="dashicons dashicons-arrow-down"></span>
			</div>
			<div class="body">
				<p>PayPal 標準付款會將顧客轉到 PayPal 以輸入他們的付款資訊\，PayPal IPN 需要支援 fsockopen/cURL 功能以便在付款後更新訂單狀態</p>
				<div class="field paypal title">
					<h4>標題</h4>
					<div class="content">
						<input type="text" name="paypal_title" value="<?php echo $paypal_title; ?>">
						<p>控制使用者在結帳時所看到的標題.</p>
					</div>
				</div>
				<div class="field paypal description">
					<h4>商品說明</h4>
					<div class="content">
						<textarea name="paypal_description"><?php echo $paypal_description; ?></textarea>
						<p>此項目控制使用者在結帳時會看到的說明</p>
					</div>
				</div>
				<div class="field paypal email">
					<h4>PayPal 電子郵件</h4>
					<div class="content">
						<input type="email" name="paypal_email" value="<?php echo $paypal_email; ?>">
						<p>請輸入您的 PayPal 電子郵件信箱，繼續進行結帳手續</p>
					</div>
				</div>
				<div class="field paypal receiver_email">
					<h4>收件人電子郵件</h4>
					<div class="content">
						<input type="email" name="paypal_receiver_email" value="<?php echo $paypal_receiver_email; ?>">
						<p>如果您主要的 PayPal 電子郵件與上面輸入的 PayPal 電子郵件不同時，請在此輸入您 PayPal 的主要收款電子郵件帳號。這是用於確認 IPN 請求</p>
					</div>
				</div>
				<div class="field paypal api_username">
					<h4>API 使用者名稱</h4>
					<div class="content">
						<input type="text" name="paypal_api_username" value="<?php echo $paypal_api_username; ?>">
						<p>從 PayPal 取得您的 API 憑證</p>
					</div>
				</div>
				<div class="field paypal api_password">
					<h4>API 使用者名稱</h4>
					<div class="content">
						<input type="password" name="paypal_api_password" value="<?php echo $paypal_api_password; ?>">
						<p>從 PayPal 取得您的 API 憑證</p>
					</div>
				</div>
			</div>
		</div>
		<div class="ecpay group">
			<div class="header">
				<input type="checkbox" name="" class="check" <?php 
					if($ecpay_check=="yes")echo "checked";
				?>><input type="hidden" class="" name="ecpay_check" value="<?php echo $ecpay_check; ?>">

				<h3 class="title-click">綠界科技</h3>
				<span class="dashicons dashicons-arrow-up"></span>
				<span class="dashicons dashicons-arrow-down"></span>
			</div>
			<div class="body">
				<p>綠界科技是台灣線上購物最熱門的整合金流</p>
				<div class="field ecpay title">
					<h4>標題</h4>
					<div class="content">
						<input type="text" name="ecpay_title" value="<?php echo $ecpay_title; ?>">
						<p>控制使用者在結帳時所看到的標題.</p>
					</div>
				</div>
				<div class="field ecpay description">
					<h4>說明</h4>
					<div class="content">
						<textarea name="ecpay_description"><?php echo $ecpay_description; ?></textarea>
						<p>此項目控制使用者在結帳時會看到的說明.</p>
					</div>
				</div>
				<div class="field ecpay merchant_id">
					<h4>特店編號(Merchant ID)</h4>
					<div class="content">
						<input type="text" name="ecpay_merchant_id" value="<?php echo $ecpay_merchant_id ?>">
					</div>
				</div>
				<div class="field ecpay hash_key">
					<h4>金鑰(Hash Key)</h4>
					<div class="content">
						<input type="text" name="ecpay_hash_key" value="<?php echo $ecpay_hash_key ?>">
					</div>
				</div>
				<div class="field ecpay hash_iv">
					<h4>向量(Hash IV)</h4>
					<div class="content">
						<input type="text" name="ecpay_hash_iv" value="<?php echo $ecpay_hash_iv; ?>">
					</div>
				</div>
				<div class="field ecpay payment_methods">
					<h4>付款方式</h4>
					<div class="content">
						<select class="multiselect" multiple="multiple" name="ecpay_payment_methods[]">
							<option value="Credit"
								<?php 
								foreach ($ecpay_payment_methods as $ecpay_selected)
	 								if($ecpay_selected=="Credit") echo ' selected="selected"';
	 							?>>信用卡(一次付清)</option>
							<option value="Credit_3"
								<?php 
								foreach ($ecpay_payment_methods as $ecpay_selected)
	 								if($ecpay_selected=="Credit_3") echo ' selected="selected"';
	 							?>>信用卡(3期)
 							</option>
							<option value="Credit_6"
								<?php 
								foreach ($ecpay_payment_methods as $ecpay_selected)
 									if($ecpay_selected=="Credit_6") echo ' selected="selected"';
 								?>>信用卡(6期)
 							</option>
							<option value="Credit_12"
								<?php 
								foreach ($ecpay_payment_methods as $ecpay_selected)
 									if($ecpay_selected=="Credit_12") echo ' selected="selected"';
 								?>>信用卡(12期)
 							</option>
							<option value="Credit_18"
								<?php 
								foreach ($ecpay_payment_methods as $ecpay_selected)
 									if($ecpay_selected=="Credit_18") echo ' selected="selected"';
 								?>>信用卡(18期)
 							</option>
							<option value="Credit_24"
								<?php 
								foreach ($ecpay_payment_methods as $ecpay_selected)
 									if($ecpay_selected=="Credit_24") echo ' selected="selected"';
 								?>>信用卡(24期)</option>
							<option value="WebATM"
								<?php 
								foreach ($ecpay_payment_methods as $ecpay_selected)
 								if($ecpay_selected=="WebATM") echo ' selected="selected"';
 								?>>網路ATM
 							</option>
							<option value="ATM"
								<?php 
								foreach ($ecpay_payment_methods as $ecpay_selected)
								if($ecpay_selected=="ATM") echo ' selected="selected"';
								?>>ATM
							</option>
							<option value="CVS"
								<?php 
								foreach ($ecpay_payment_methods as $ecpay_selected)
 								if($ecpay_selected=="CVS") echo ' selected="selected"';
 								?>>超商代碼
 							</option>
							<option value="BARCODE"
								<?php 
								foreach ($ecpay_payment_methods as $ecpay_selected)
 								if($ecpay_selected=="BARCODE") echo ' selected="selected"';
 								?>>超商條碼
 							</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="fami_pay group">
			<div class="header">
				<input type="checkbox" name="" class="check" <?php 
					if($fami_pay_check=="yes")echo "checked";
				?>><input type="hidden" class="" name="fami_pay_check" value="<?php echo $fami_pay_check; ?>">
				<h3 class="title-click">超商取貨付款(全家)</h3>
				<span class="dashicons dashicons-arrow-up"></span>
				<span class="dashicons dashicons-arrow-down"></span>
			</div>
			<div class="body">
				<p>全家便利超商僅允許台灣本島之店對店運送</p>
				<div class="field fami_pay title">
					<h4>標題</h4>
					<div class="content">
						<input type="text" name="fami_pay_title" value="<?php echo $fami_pay_title; ?>">
						<p>控制使用者在結帳時所看到的標題.</p>
					</div>
				</div>
				<div class="field fami_pay description">
					<h4>說明</h4>
					<div class="content">
						<textarea name="fami_pay_description"><?php echo $fami_pay_description; ?></textarea>
						<p>此項目控制使用者在結帳時會看到的說明.</p>
					</div>
				</div>
				<div class="field fami_pay fami_pay_min_amount">
					<h4>最低商品金額</h4>
					<div class="content">
						<input type="number" name="fami_pay_min_amount" min="1" max="20000" value="1" placeholder="">
						<p>最低商品金額為1元</p>
					</div>
				</div>
				<div class="field fami_pay fami_pay_max_amount">
					<h4>最高商品金額</h4>
					<div class="content">
						<input type="number" name="fami_pay_max_amount" min="1" max="20000" value="20000" placeholder="">
						<p>最高商品金額為20000元</p>
					</div>
				</div>
			</div>
		</div>
		<div class="unimart_pay group">
			<div class="header">
				<input type="checkbox" name="" class="check" <?php 
					if($unimart_paycheck=="yes")echo "checked";
				?>><input type="hidden" class="" name="unimart_paycheck" value="<?php echo $unimart_paycheck; ?>">
				<h3 class="title-click">超商取貨付款(統一7-11)</h3>
				<span class="dashicons dashicons-arrow-up"></span>
				<span class="dashicons dashicons-arrow-down"></span>
			</div>
			<div class="body">
				<p>統一超商在台灣擁有超過5000家門市，僅允許台灣本島之店對店運送</p>
				<div class="field unimart_pay title">
					<h4>標題</h4>
					<div class="content">
						<input type="text" name="unimart_pay_title" value="<?php echo $unimart_pay_title; ?>">
						<p>控制使用者在結帳時所看到的標題.</p>
					</div>
				</div>
				<div class="field unimart_pay description">
					<h4>說明</h4>
					<div class="content">
						<textarea name="unimart_pay_description"><?php echo $unimart_pay_description; ?></textarea>
						<p>此項目控制使用者在結帳時會看到的說明.</p>
					</div>
				</div>
				<div class="field unimart_pay fami_pay_min_amount">
					<h4>最低商品金額</h4>
					<div class="content">
						<input type="number" name="unimart_pay_min_amount" min="1" max="19999" value="1" placeholder="">
						<p>最低商品金額為1元</p>
					</div>
				</div>
				<div class="field unimart_pay unimart_pay_max_amount">
					<h4>最高商品金額</h4>
					<div class="content">
						<input type="number" name="unimart_pay_max_amount" min="1" max="19999" value="19999" placeholder="">
						<p>最高商品金額為19999元</p>
					</div>
				</div>
			</div>
		</div>
		<div class="cod_pay group">
			<div class="header">
				<input type="checkbox" name="" class="check" <?php 
					if($cod_pay_paycheck=="yes")echo "checked";
				?>><input type="hidden" class="" name="cod_pay_paycheck" value="<?php echo $cod_pay_paycheck; ?>">
				<h3 class="title-click">貨到付款</h3>
				<span class="dashicons dashicons-arrow-up"></span>
				<span class="dashicons dashicons-arrow-down"></span>
			</div>
			<div class="body">
				<p>讓你的顧客在收到貨物的時候以現金 (或其他方式) 付款.</p>
				<div class="field cod_pay title">
					<h4>標題</h4>
					<div class="content">
						<input type="text" name="cod_pay_title" value="<?php echo $cod_pay_title; ?>">
						<p>控制使用者在結帳時所看到的標題.</p>
					</div>
				</div>
				<div class="field cod_pay description">
					<h4>說明</h4>
					<div class="content">
						<textarea name="cod_pay_description"><?php echo $cod_pay_description; ?></textarea>
						<p>此項目控制使用者在結帳時會看到的說明.</p>
					</div>
					<h4>啟用運送方式</h4>
					<div class="content">
						<div class="content">
						    <select class="multiselect" multiple="multiple" name="cod_pay_selected[]" >
								<?php $shipping_methods = array();
								foreach ( WC()->shipping()->load_shipping_methods() as $method ) {
								$shipping_methods[ $method->id ] = $method->get_method_title();
								$shipping_methods_danny[ $method->id ] = $method->get_title();
                                    $key_danny_array=array("my_shipping_method","lohas2017_shipping_method","lohas2018_shipping_method","lohas2019_shipping_method","your_shipping_method","they_shipping_method");
                                    $key_danny_hiden_array=array("shipping_by_rules","cvs_fami_shipping","home_ecan_shipping","home_tcat_shipping","cvs_unimart_shipping");
                                    //$key_danny_hiden_title_array=array("順豐快遞(店配)","黑貓宅急便(店配)","國際快遞(店配)","新竹貨運(店配)","宅配通(店配)","嘉里大榮物流(店配)");
                                    $key_danny_hiden_title_array=array();
                                    if(!in_array($method->id, $key_danny_hiden_array) && !in_array($shipping_methods_danny[$method->id], $key_danny_hiden_title_array)) {
                                        echo '<option value='.$method->id;
                                        foreach ($cod_pay_dbselected as $cod_selected)
                                            if($cod_selected==$method->id) echo ' selected="selected"';
                                        echo '>';
                                        if (!in_array($method->id, $key_danny_array))
                                            echo $shipping_methods[$method->id];
                                        else
                                            echo $shipping_methods_danny[$method->id];
                                        echo '</option>';
                                        echo "\n";
                                    }
	  							} ?>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
		<button type="submit" class="button-primary" >儲存變更</button>
	</form>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($){
		$('input.check, .title-click').on('click', function(){
			$(this).parents('.group').toggleClass('active');
		});

		$('input.check').on('click', function(){
			if( $(this).attr('checked') == 'checked' ) {
				$(this).parents('.group').addClass('check');
		     	$(this).next().val("true");
			} else {
				$(this).parents('.group').removeClass('check');
		     	$(this).next().val("false");
			}
		});

		var bacs_account_num = $('#bacs_account_num').val();
		$('.bacs.accounts .add_btn').on('click', function(){
			$(this).siblings('table').find('tbody').append('<tr>'+
								'<td><input type="text" name="bacs_account_name['+bacs_account_num+']"></td>'+
								'<td><input type="number" name="bacs_account_number['+bacs_account_num+']"></td>'+
								'<td><input type="number" name="bacs_bank_name['+bacs_account_num+']"></td>'+
								'<td><input type="number" name="bacs_sort_code['+bacs_account_num+']"></td>'+
								'<td><input type="number" name="bacs_iban['+bacs_account_num+']"></td>'+
								'<td><input type="number" name="bacs_bic['+bacs_account_num+']"></td>'+
								'<td><span class="dashicons dashicons-trash delete_bacs_account"></span></td>'+
							'</tr>');
			bacs_account_num ++;
			$(this).siblings('input[name="tr_num"]').val(bacs_account_num);
		});

		$('body').on('click','.delete_bacs_account' , function(){
			var delete_data = confirm("確認刪除該筆資料嗎?");
			if ( delete_data == true ) {
				$(this).parents('tr').remove();
				bacs_account_num = bacs_account_num -1;
				$('.accounts_input_table input[name="tr_num"]').val(bacs_account_num);
			}
		});

		$('.group .header span.dashicons-arrow-up').on('click', function(){
			$(this).parents('.group').removeClass('active');
		});
		$('.group .header span.dashicons-arrow-down').on('click', function(){
			$(this).parents('.group').addClass('active');
		});
	});
</script>

