<?php
$authors = [
        1 => 'HM',
        2 => 'ymlin',
        3 => 'danny',
        4 => '小汪',
        5 => '阿傑',
        6 => 'Manson'
];
$types = [
    'dev' => [
        1 => 'DEV-新增',
        2 => 'DEV-修正'
    ],
    'feature' => [
        1 => '優化調整',
        2 => '問題修正',
        3 => '更新外掛',
        4 => '新增外掛',
        5 => '移除外掛'
    ]
];
$data = [
    [
        'version' => '1.7.5',
        'date' => '2019/5/20',
        'list' => [
            ['feature', 5, '移除買A送B外掛(Lohasit WC Buy A Get B  for wcommerce)', 1],
            ['feature', 3, '更新FB live messenger外掛 1.4.1 => 1.4.8', 1],
        ]
    ],
    [
        'version' => '1.7.4',
        'date' => '2019/4/10',
        'list' => [
            ['feature', 2, '訂單匯出功能加入訂單備註欄位，修正css沒載入的問題，調整匯出欄位順序', 1],
            ['dev', 2, '整理壓板系統自行開發的外掛資訊及檔案名稱', 1],
        ]
    ],
    [
        'version' => '1.7.3',
        'date' => '2019/3/14',
        'list' => [
            ['feature', 2, '修正WooCommerce選單的翻譯方式', 1],
            ['feature', 3, '更新Adminimize外掛 1.11.2 => 1.11.4', 1],
            ['feature', 3, '更新Post SMTP外掛 1.9.4 => 1.9.8', 1],
            ['feature', 3, '更新 W3 Total Cache外掛 0.9.5.4 => 0.9.7.2', 1],
            ['dev', 2, '調整翻譯選單的程式碼', 1],
        ]
    ],
    [
        'version' => '1.7.2',
        'date' => '2019/2/25',
        'list' => [
            ['feature', 2, '修正購物紀錄無法查詢的問題', 1],
            ['feature', 2, '修正hide plugin updates notifications外掛中，不正常enqueue scripts的問題', 1],
            ['dev', 1, '加入自動載入css&js檔案的功能', 1],
        ]
    ],
    [
        'version' => '1.7.1',
        'date' => '2019/1/31',
        'list' => [
            ['feature', 2, '解決Postman SMTP無法正常取得Google OAuth認證的問題', 5],
            ['feature', 2, '年度統計報表改為只統計已完成訂單', 1],
        ]
    ],
    [
        'version' => '1.7.0',
        'date' => '2019/1/18',
        'list' => [
            ['feature', 1, '更新ARForms翻譯', 1],
            ['feature', 1, '修正ARForm匯出的中文CSV無法直接用Excel開啟的問題', 1],
            ['feature', 1, '未銷售商品列表加入匯出功能,修改為多國語系', 1],
            ['feature', 1, '針對非管理員隱藏後台Admin bar上的語系切換及Simply Show Hooks選單', 1],
            ['feature', 1, '加強訂單/顧客資料匯出外掛的可客制性, 全面改為多語架構', 1],
            ['feature', 3, '修改綠界物流自訂filter順序的位置,解決送出物流訂單驗證碼錯誤的問題', 5],
            ['feature', 4, '新增社群分享按鈕外掛 Add to Any (1.7.33)', 1],
            ['feature', 5, '移除外掛Wp Product Refund', 1],
            ['feature', 5, '移除外掛Wp Product Bundle', 1],
            ['dev', 2, '修正將網址從HTML移除掉的方法', 1],
        ]
    ],
    [
        'version' => '1.6.1',
        'date' => '2019/1/10',
        'list' => [
            ['feature', 1, '修改ARforms選單翻譯', 1],
            ['feature', 1, '修改CSV Import suite選單翻譯', 1],
        ]
    ],
    [
        'version' => '1.6.0',
        'date' => '2019/1/9',
        'list' => [
            ['feature', 4, '加入/會員購物紀錄活躍度列表功能外掛 WC Customer Shopping List & Activity Ratios 1.0.0', 1],
            ['feature', 4, '加入未銷售商品列表外掛 WC Unsold Product List 1.0.0', 1],
        ]
    ],
    [
        'version' => '1.5.0',
        'date' => '2019/1/9',
        'list' => [
            ['feature', 1, '新增會員資料匯出功能', 1],
            ['feature', 5, '刪除舊的匯出訂單外掛 WooCommerce Customer/Order CSV Export', 1],
            ['feature', 5, '刪除多餘的手機號碼欄位外掛 Wc Shipping Mobilephone', 1],
            ['feature', 5, '刪除撿貨清單匯出外掛 Wc Picking List Exportation', 1],
        ]
    ],
    [
        'version' => '1.4.0',
        'date' => '2019/1/8',
        'list' => [
            ['feature', 3, '更新ARForms外掛 2.7.8 => 3.6', 1],
            ['feature', 4, '新增訂單匯出的外掛 - WC Export Orders 2.0', 1],
            ['feature', 5, '刪除撿貨單外掛', 1],
            ['dev', 1, '從HTML的資源連結中移除網站網址資訊', 1],
            ['dev', 2, 'Betheme字型選項讀取新增的自訂字型檔', 1],
            ['dev', 2, 'plugins資料夾加入空白的index.php', 1],
        ]
    ],
    [
        'version' => '1.3.2',
        'date' => '2018/11/26',
        'list' => [
            ['feature', 1, '會員升級外掛，去掉“會員降級“此區塊', 6],
            ['feature', 1, '新增Google SSO登入外掛 - Lohasit Google OAuth 0.0.1，壓板加入SSO登入機制', 6],
            ['feature', 1, 'Betheme新增至五組自訂字型檔', 1],
            ['feature', 2, '修正YITH動態定價無法正常使用', 5],
            ['dev', 1, '壓板git新增mo檔', 1],
            ['dev', 1, '壓板git安裝composer套件 - google/apiclient', 1],
            ['dev', 1, '壓板git新增缺少的核心翻譯檔', 1],
        ]
    ],
    [
        'version' => '1.3.1',
        'date' => '2018/10/11',
        'list' => [
            ['feature', 1, '調整三個YITH外掛的選單位置，紅利點數及動態定價移至(優惠設定)內，願望清單移至(Woocommerce)內', 5],
            ['feature', 1, '翻譯:繁中-woocommerce-product-csv-import-suite', 5],
            ['feature', 3, '社群登入外掛Social Login WordPress Plugin - AccessPress Social Login Lite 3.2.7 => 3.3.8', 5],
        ]
    ],
    [
        'version' => '1.3.0',
        'date' => '2018/09/25',
        'list' => [
            ['dev', 1, '判斷字串內是否有中文的輔助函式', 1],
            ['dev', 1, '角色的body class(從原本custom-admin-css外掛中移除此功能)', 1],
            ['feature', 1, '綠界超商物流送出的訂單資料，收貨人姓名調整中英文順序', 1],
            ['feature', 1, '綠界宅配物流送出的訂單資料，收貨人姓名調整中英文順序', 1],
            ['feature', 1, 'WordPress從文章內容產生的摘要內的點點點樣式', 1],
            ['feature', 1, '綠界物流判斷使用設備類型，顯示相對應之電子地圖畫面', 4],
            ['feature', 2, 'MailPoet 3.6.2 - wordpress4.7之前版本的相容問題(缺少get_user_locale函式)', 5],
            ['feature', 3, 'advanced-floating-content，3.2.4 => 3.4.4', 1],
            ['feature', 3, 'FB Messenger Live Chat 外掛，1.3.3 =>1.4.1', 1],
            ['feature', 3, 'WP Real Media Library外掛，3.4.5 => 4.0.6', 1],
            ['feature', 3, 'MailPoet(3.6.2)、 YITH動態定價(1.4.5)、YITH紅利點數(1.5.3)', 5],
            ['feature', 4, 'WP REST API 2.0外掛，(real media library 4.0.6所需)', 1],
            ['feature', 5, 'updraftplus', 1],
        ]
    ],
    [
        'version' => '1.2.3',
        'date' => '2018/08/06',
        'list' => [
            ['feature', 2, '修正收取物流通知的方法', 1],
            ['feature', 2, '修正WordPress Editor的翻譯錯誤，列->行', 1],
            ['feature', 2, '修正綠界超商及宅配物流免運門檻計算方法', 1],
            ['feature', 2, '超商取貨付款金流，訂單狀態改為正在處理中', 1],
            ['feature', 3, '更新願望清單至2.2.1版', 2],
        ]
    ],
    [
        'version' => '1.2.2',
        'date' => '2018/07/06',
        'list' => [
            ['dev', 1, '新增後台CSS檔案並載入，位置betheme-child/assets/css/admin-css.css', 1],
            ['dev', 1, '前台copyright文字的方法，新增自訂hook', 1],
            ['feature', 2, '將Woocommerce預設的報表匯出CSV功能加入BOM檔頭，使其支援中文', 1],
            ['feature', 2, '修復WooCommerce Customer/Order CSV Export中，匯出結束時間的計算BUG', 1],
            ['feature', 2, '修正綠界金流的導回網址', 1],
            ['feature', 2, '修正紅利點數在可變商品無法設定百分比的問題', 1],
        ]
    ],
    [
        'version' => '1.2.1',
        'date' => '2018/06/12',
        'list' => [
            ['dev', 2, '修正更改姓名順序的程式碼', 1],
            ['dev', 2, '修正Betheme的mfn_get_authors方法，提高效能', 1],
            ['feature', 1, '紀錄物流狀態的所有通知到訂單備註', 1],
            ['feature', 1, '修改預設footer及logo為公司資訊', 1],
            ['feature', 1, '翻譯後台選單(MailPoet，Slider Revolution，WooCommerce)', 1],
            ['feature', 3, '更新 Manage WP worker至4.5.0', 1],
            ['feature', 4, '續約管理外掛 Lohasit Maintaining Service Client', 1]
        ]
    ],
    [
        'version' => '1.2.0',
        'date' => '2018/05/29',
        'list' => [
            ['dev', 2, '修改電子發票外掛名稱', 1],
            ['feature', 1, '使後臺訂單搜尋支援中文全姓名', 1],
            ['feature', 1, '更改woocommerce密碼強度限制至弱', 1],
            ['feature', 1, '當紅利點數可用折扣金額為0時，隱藏購物車及結帳頁折抵訊息', 1],
            ['feature', 1, '新增庫存匯出功能，庫存報表支援顯示中文', 1],
            ['feature', 3, '更新 Slider Revolution 至 5.4.7.3', 4],
            ['feature', 4, '新增WC Statistics Report外掛-新增自訂報表', 1],
        ]
    ],
    [
        'version' => '1.1.7',
        'date' => '2018/05/02',
        'list'  => [
            ['feature', 1, '結帳頁重新選取超商運送方式時，重設超商資訊(選擇從統一->全家 或 全家->統一時)', 1 ],
            ['feature', 1, '結帳完紀錄綠界超商資訊到訂單備註', 1 ],
            ['feature', 2, '修正ecpay物流外掛，回傳網址錯誤的問題', 1 ],
            ['feature', 2, '修正動態定價優惠券與其他外掛優惠卷使用時造成的衝突問題(filter沒回傳值)', 1 ],
            ['feature', 5, '移除 mail from外掛', 1 ],
        ]
    ],
    [
        'version' => '1.1.6',
        'date' => '2018/03/27',
        'list'  => [
            ['dev', 2, '將壓板自訂程式碼檔案 lohas-custom.php移到子主題', 1 ],
            ['feature', 1, '讓WooCommerce Customer/Order CSV Export匯出的csv檔加上BOM檔頭，可以直接用Excel開啟而不會出現亂碼', 1 ],
            ['feature', 1, '新增Mailpoet選項，讓MailPoet使用WP Mail', 1 ],
            ['feature', 1, 'woocommerce-points-and-rewards 可折抵紅利做無條件捨去小數點', 1 ],
            ['feature', 1, '移除Admin bar上非管理員看得到的項目，包含wp-logo與w3 total cache的項目', 1 ],
            ['feature', 2, '調整Woocommerce Zoom Magnifier Premium產生的圖片藝廊Wrapper高度', 1 ],
            ['feature', 2, '修正Mailpoet 使用WP Mail發送HTML信件的header沒設定的問題', 1 ],
            ['feature', 2, '修正wc-ecpay-c2c-logistic接收通知的問題，產生物流訂單及取貨後，會新增備註到訂單', 1 ],
            ['feature', 3, '更新Real Media Library至 3.4.5', 1 ],
        ]
    ],
    [
        'version' => '1.1.5',
        'date' => '2017/11/27',
        'list'  => [
            ['dev', 2, '將壓板功能在functions.php的程式碼移至lohasit-custom.php並優化部分程式碼', 1 ],
            ['dev', 2, '加入style-static.css到betheme-child', 1 ],
            ['feature', 3, '更新壓板版本號外掛', 1 ],
        ]
    ],
    [
        'version' => '1.1.4',
        'date' => '2017/10/20',
        'list'  => [
            ['feature', 2, '修正會員單筆消費無法升級 以及會員升級頁面顯示問題', 2 ],
            ['feature', 2, '1.批次管理刪除 2.修正無法產生物流單號問題', 2 ],
            ['feature', 2, '修正google+無法登入', 2 ],
            ['feature', 2, '修正warning :implode() invalid arguement', 2 ],
            ['feature', 3, '電子發票外掛allpay-ecpay-e-invoice 更新至版本1.4.1', 1 ],
            ['feature', 4, '加入WPML外掛', 1 ],
        ]
    ],
    [
        'version' => '1.1.3',
        'date' => '2017/10/20',
        'list'  => [
            ['feature', 2, '移除將price hash打亂的程式碼段', 1 ],
            ['feature', 5, '移除WP Admin UI & Visitor Map Who\'s Online外掛', 1 ],
        ]
    ],
    [
        'version' => '1.1.2',
        'date' => '2017/10/06',
        'list'  => [
            ['feature', 1, '修正購物車頁面 Apply coupon 沒被翻譯到的問題', 2 ],
            ['feature', 2, '解決登入時因顯示admin bar而使網頁下方出現一條空白區域的問題', 1 ],
            ['feature', 4, '新增yoast seo 外掛', 2 ],
        ]
    ],
    [
        'version' => '1.1.1',
        'date' => '2017/09/28',
        'list'  => [
            ['feature', 1, '修改自定義物流，可以下拉選擇國家', 2 ],
            ['feature', 1, '修改自定義綠界物流，可以跟著WC-設定-運送方式中的物流同步', 2 ],
            ['feature', 1, '綠界超商宅配改成固定顯示在上面運送方式列做設定', 2 ],
            ['feature', 1, '加入結帳手機號碼欄位外掛', 2 ],
            ['feature', 1, '讓後台訂單列表顯示金流付款方式 & 修改綠界金流結帳欄位顯示方式', 2 ],
            ['feature', 1, '移除在會員中心裡的會員階級內容', 1 ],
            ['feature', 1, '移除壓版不必要且有可能造成衝突的script', 1 ],
            ['feature', 2, '修正購物車頁面選單不正常掉落的問題', 1 ],
            ['feature', 2, '修正購物車頁的自定義物流出不來', 2 ],
            ['feature', 2, '修正送到綠界時間晚8hr / Nelly修改綠界物流外掛 產生物流單號 原本只能第一個id能產生 改成每筆訂單都ok', 2 ],
        ]
    ],
    [
        'version' => '1.1.0',
        'date' => '2017/09/11',
        'list'  => [
            ['dev', 2, '版本號外掛刪除多餘空行', 1 ],
            ['feature', 1, '自定義物流修正成標題可以自定義', 2 ],
            ['feature', 1, '新增全館商品優惠 商品頁都有折扣顯示(wp_product_bundle) 新增退貨機制', 2 ],
            ['feature', 1, '常溫-當成預設運送選項，拿掉無運送選項', 3 ],
            ['feature', 1, '買A送B-搜尋商品ID加上商品名稱', 3 ],
            ['feature', 1, '修改綠界金流設定-點擊文字即可看到設定內容(在title上加入', 3 ],
            ['feature', 1, '修改綠界金流設定-paypal選項不讓客戶勾選', 3 ],
            ['feature', 1, '買A送B禮物加上醒目標籤', 1 ],
            ['feature', 1, '超取建立出貨單的姓名改成抓取運送地址欄位的姓名，原本是帳單的姓名', 2 ],
            ['feature', 1, '修改全館商品可以在商品頁出現折扣', 3 ],
            ['feature', 1, '修改全館商品優惠，假如出現負值，就改成0', 2 ],
            ['feature', 1, '拿掉會員中心的圓圈', 1 ],
            ['feature', 1, '修正並移除優化內容內造成錯誤的無用js', 1 ],
            ['feature', 1, '修正並移除優化內容內造成錯誤的無用js(多餘的jquery.min.js', 1 ],
            ['feature', 2, '修正綠界宅配 異常出現在訂單問題', 2 ],
            ['feature', 2, '修正超取和電子發票 訂單欄位重複問題', 2 ],
            ['feature', 2, '修正"我的帳號"中螢幕小於1326px時，右邊的排版會亂掉的問題', 3 ],
            ['feature', 2, '解決WooCommerce Customer/Order CSV Export下載檔案時檔頭被加上BOM而無法正常開啟的問題', 1 ],
            ['feature', 4, '新增login-customizer外掛', 1 ],
            ['feature', 4, '新增撿貨單外掛Wc Picking List Exportation', 2 ],
        ]
    ]
]
?>
<!--
▐████████████████████████████▌
▐────────────────────────▄───▌
▐───────────────────────██───▌
▐─────────────────────▄████──▌
▐────────────────────▐█████──▌
▐───────────────────▐███████─▌
▐─────────────────▄█████████─▌
▐───────────────▄████████████▌
▐▄───────────▄███████████████▌
▐██████▄──▄██████████████████▌
▐████████████████████████████▌
▐─███████████████████████████▌
▐──██████████████████████████▌
▐───█████████████████████████▌
▐────████████████████████████▌
▐─────████████████████▀─█──▄█▌
▐──────███████████████▌─█─▐██▌
▐──────████████████████▄▄▄███▌
▐──────████▀▀─█─█████████████▌
▐──────████▌──█─█████████████▌
▐───────█████▄▄▄█████████████▌
▐────────████████████████████▌
▐─────────████████▄▀▀▌███████▌
▐──────────▀█████████▄███████▌
▐────────────▀███████████████▌
▐───────────────▀████████████▌
▐───────────────────────▀▀▀▀█▌
▐───────────────────────────█▌
▐────────────────────────────▌
▐────────────────────────────▌
▐████████████████████████████▌
-->
<div class="lohasit-versions">
    <?php foreach($data as $d):?>
        <div class="block">
            <div class="header">
                <div class="version"><?=$d['version']?></div>
                <div class="date"><?=$d['date']?></div>
            </div>
            <div class="data">
                <?php foreach($d['list'] as $l): ?>
                <div class="item">
                    <div class="content"><strong><?=$types[$l[0]][$l[1]]?>:</strong> <?=$l[2]?></div>
                    <div class="author"><?=$authors[$l[3]]?></div>
                </div>

                <?php endforeach;?>
            </div>
        </div>
    <?php endforeach;?>
</div>
<script>
    
    console.log(
        '%c'+
    '▐████████████████████████████▌\n'+
    '▐────────────────────────▄───▌\n'+
    '▐───────────────────────██───▌\n'+
    '▐─────────────────────▄████──▌\n'+
    '▐────────────────────▐█████──▌\n'+
    '▐───────────────────▐███████─▌\n'+
    '▐─────────────────▄█████████─▌\n'+
    '▐───────────────▄████████████▌\n'+
    '▐▄───────────▄███████████████▌\n'+
    '▐██████▄──▄██████████████████▌\n'+
    '▐████████████████████████████▌\n'+
    '▐─███████████████████████████▌\n'+
    '▐──██████████████████████████▌\n'+
    '▐───█████████████████████████▌\n'+
    '▐────████████████████████████▌\n'+
    '▐─────████████████████▀─█──▄█▌\n'+
    '▐──────███████████████▌─█─▐██▌\n'+
    '▐──────████████████████▄▄▄███▌\n'+
    '▐──────████▀▀─█─█████████████▌\n'+
    '▐──────████▌──█─█████████████▌\n'+
    '▐───────█████▄▄▄█████████████▌\n'+
    '▐────────████████████████████▌\n'+
    '▐─────────████████▄▀▀▌███████▌\n'+
    '▐──────────▀█████████▄███████▌\n'+
    '▐────────────▀███████████████▌\n'+
    '▐───────────────▀████████████▌\n'+
    '▐───────────────────────▀▀▀▀█▌\n'+
    '▐───────────────────────────█▌\n'+
    '▐────────────────────────────▌\n'+
    '▐──────────kroutony──────────▌\n'+
    '▐████████████████████████████▌\n',
        'background: #bfbfbf; color: black');



</script>
