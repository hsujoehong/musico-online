(function( $ ) {
	'use strict';


    jQuery("#remind_later").click(function(e){			
            e.preventDefault();        
            var data = {
                'action': 'update_remind_later',
                'remind_later': 1
            };            
            jQuery.ajax({
                type: "POST",               
                dataType:"json",
                data    : data,
                url: ajaxurl,
                //This fires when the ajax 'comes back' and it is valid json
                success: function (response) {                    
                    location.reload();

                }
                //This fires when the ajax 'comes back' and it isn't valid json
            }).fail(function (data) {               
                console.log(data);
                location.reload();
            });
        
        
		});
    jQuery("#no_thanks").click(function(e){			
            e.preventDefault();        
            var data = {
                'action': 'update_no_thanks',
                'remind_later': 0
            };            
            jQuery.ajax({
                type: "POST",               
                dataType:"json",
                data    : data,
                url: ajaxurl,
                //This fires when the ajax 'comes back' and it is valid json
                success: function (response) {                    
                    location.reload();

                }
                //This fires when the ajax 'comes back' and it isn't valid json
            }).fail(function (data) {               
                console.log(data);
                location.reload();
            });
        
        
		});
    
    jQuery("#review_posted").click(function(e){			
            e.preventDefault();       
       
            var data = {
                'action': 'update_reviewed',
                'reviewed': 1
            };            
            jQuery.ajax({
                type: "POST",               
                dataType:"json",
                data    : data,
                url: ajaxurl,
                //This fires when the ajax 'comes back' and it is valid json
                success: function (response) {    
                    console.log(response);
                    location.reload();

                }
                //This fires when the ajax 'comes back' and it isn't valid json
            }).fail(function (data) {               
                console.log(data);
                location.reload();
            });
        
        
		});
    
    
     jQuery("#remind_later_review").click(function(e){			
            e.preventDefault();       
       
            var data = {
                'action': 'update_remind_later_review',
                'remind_later': 1
            };            
            jQuery.ajax({
                type: "POST",               
                dataType:"json",
                data    : data,
                url: ajaxurl,
                //This fires when the ajax 'comes back' and it is valid json
                success: function (response) {                    
                    console.log(response);
                    location.reload();

                }
                //This fires when the ajax 'comes back' and it isn't valid json
            }).fail(function (data) {               
                console.log(data);
                location.reload();
            });
        
        
		});		
    
})( jQuery );
