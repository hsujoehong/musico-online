(function( $ ) {
	'use strict';

	if( jQuery().wpColorPicker ) {
		$(function() {
			$(".color-picker-afc").wpColorPicker({
                
                // you can declare a default color here,
            // or in the data-default-color attribute on the input
            //defaultColor: false,

            // a callback to fire whenever the color changes to a valid color
            change: function(event, ui){},
            // a callback to fire when the input is emptied or an invalid color
            clear: function() {},
            // hide the color picker controls on load
            hide: true,
            // set  total width
            width : 300,
            // show a group of common colors beneath the square
            // or, supply an array of colors to customize further
            palettes: ['','#000000','#ffffff','#dd3333','#dd9933','#dd9933','#81d742','#1e73be','#8224e3']
            });
		});
	}
	
	
	jQuery('input[type="radio"]').click(function(){
		var value = jQuery(this).attr("value");
		var name = jQuery(this).attr("name");
		var divid = name.substring(name.lastIndexOf("_") + 1, name.length);
		if(value==0)
		{
			jQuery("#selective_"+divid).show();
		}
		else
		{
			jQuery("#selective_"+divid).hide();
		}	  
	});
	jQuery('input[name="ct_afc_hide_on_certain_height"]').click(function(){
		var value = jQuery(this).attr("value");
		if(value==1)
		{
			jQuery("#certain_hide_height_area").fadeIn();
		}
        else
		{			
            jQuery("#certain_hide_height_area").fadeOut();
		}
	});
    jQuery('input[name="ct_afc_show_on_certain_height"]').click(function(){
		var value = jQuery(this).attr("value");
		if(value==1)
		{
			jQuery("#certain_height_area").fadeIn();
		}
        else
		{			
            jQuery("#certain_height_area").fadeOut();
		}
	});
	jQuery('input[name="ct_afc_show_on_certain_width"]').click(function(){
		var value = jQuery(this).attr("value");
		if(value==1)
		{
			jQuery("#certain_width_area").fadeIn();
		}
        else
		{			
            jQuery("#certain_width_area").fadeOut();
		}
	});
    jQuery('input[name="ct_afc_hide_on_certain_width"]').click(function(){
		var value = jQuery(this).attr("value");
		if(value==1)
		{
			jQuery("#certain_hide_width_area").fadeIn();
		}
        else
		{			
            jQuery("#certain_hide_width_area").fadeOut();
		}
	});
    
    jQuery('#ct_afc_border_radius').change(function(){
		var value = jQuery(this).attr("value");
		if(value==1)
		{
			jQuery("#border_radious_area").fadeIn();
		}
        else
		{			
            jQuery("#border_radious_area").fadeOut();
		}
	});
    
	jQuery('input[name="ct_afc_control_impressions"]').click(function(){
		var value = jQuery(this).attr("value");
		if(value==1)
		{
			jQuery("#control_impressions_limit").fadeIn();
		}
        else
		{			
            jQuery("#control_impressions_limit").fadeOut();
		}
	});
	
	jQuery('#ct_afc_close_button').change(function(){
		var value = jQuery(this).attr("value");
		if(value=='anchor')
		{
			jQuery("#ct_afc_close_button_string").fadeIn();
		}
        else
		{			
            jQuery("#ct_afc_close_button_string").fadeOut();
		}
	});
	
	jQuery("#afc-accordion").accordion({
			collapsible: true
		});
    
    jQuery("#remind_later").click(function(e){			
            e.preventDefault();        
            var data = {
                'action': 'update_remind_later',
                'remind_later': 1
            };            
            jQuery.ajax({
                type: "POST",               
                dataType:"json",
                data    : data,
                url: ajaxurl,
                //This fires when the ajax 'comes back' and it is valid json
                success: function (response) {                    
                    location.reload();

                }
                //This fires when the ajax 'comes back' and it isn't valid json
            }).fail(function (data) {               
                console.log(data);
                location.reload();
            });
        
        
		});
    jQuery("#no_thanks").click(function(e){			
            e.preventDefault();        
            var data = {
                'action': 'update_no_thanks',
                'remind_later': 0
            };            
            jQuery.ajax({
                type: "POST",               
                dataType:"json",
                data    : data,
                url: ajaxurl,
                //This fires when the ajax 'comes back' and it is valid json
                success: function (response) {                    
                    location.reload();

                }
                //This fires when the ajax 'comes back' and it isn't valid json
            }).fail(function (data) {               
                console.log(data);
                location.reload();
            });
        
        
		});
    
    jQuery("#review_posted").click(function(e){			
            e.preventDefault();       
       
            var data = {
                'action': 'update_reviewed',
                'reviewed': 1
            };            
            jQuery.ajax({
                type: "POST",               
                dataType:"json",
                data    : data,
                url: ajaxurl,
                //This fires when the ajax 'comes back' and it is valid json
                success: function (response) {    
                    console.log(response);
                    location.reload();

                }
                //This fires when the ajax 'comes back' and it isn't valid json
            }).fail(function (data) {               
                console.log(data);
                location.reload();
            });
        
        
		});
    
    
     jQuery("#remind_later_review").click(function(e){			
            e.preventDefault();       
       
            var data = {
                'action': 'update_remind_later_review',
                'remind_later': 1
            };            
            jQuery.ajax({
                type: "POST",               
                dataType:"json",
                data    : data,
                url: ajaxurl,
                //This fires when the ajax 'comes back' and it is valid json
                success: function (response) {                    
                    console.log(response);
                    location.reload();

                }
                //This fires when the ajax 'comes back' and it isn't valid json
            }).fail(function (data) {               
                console.log(data);
                location.reload();
            });
        
        
		});
		
		
		jQuery(".ip-hidden-content-button").click(function(e){			
            e.preventDefault();
			
			var id = jQuery(this).attr("id");
			var data_ip_elem = "data_ip_"+id;
			var data_ip = jQuery("#"+data_ip_elem).val();
			var data_msg_elem = "msg_data_"+id;
            var data = {
                'action': 'update_control_imp_ips',
                'data_ip': data_ip,
				'data_id': id
            };
            jQuery.ajax({
                type: "POST",               
                dataType:"json",
                data    : data,
                url: ajaxurl,
                //This fires when the ajax 'comes back' and it is valid json
                success: function (response) {                    
                    console.log(response);
					jQuery("#"+data_msg_elem).text('Data Updated!');
                   // location.reload();

                }
                //This fires when the ajax 'comes back' and it isn't valid json
            }).fail(function (data) {               
                console.log(data);
				jQuery("#"+data_msg_elem).text('Data Not Updated!');
               // location.reload();
            });
        
        
		});
		
		jQuery("#ct_afc_start_date").datepicker({
        dateFormat: 'yy-mm-dd',        
        onClose: function( selectedDate ) {
        jQuery( "#ct_afc_end_date" ).datepicker( "option", "minDate", selectedDate );
        }
    });
    jQuery("#ct_afc_end_date").datepicker({
        dateFormat: 'yy-mm-dd',        
        onClose: function( selectedDate ) {
        jQuery( "#ct_afc_start_date" ).datepicker( "option", "maxDate", selectedDate );
        }
    });
	
	/*
	
		jQuery('.afc_date').datepicker({
			dateFormat : 'yy-mm-dd'
		});

		
		jQuery("#publish").click(function(e){			
            e.preventDefault();
			var startDate = new Date(jQuery("#ct_afc_start_date").val());
			var endDate = new Date(jQuery("#ct_afc_start_date").val());
			if (startDate > endDate) {
                            alert("End date should be greater than Start date.");
                        } else {
                            alert("fromdate is greather then to date.");
                        }
			//if (startDate > endDate) {
			  //alert("End date should be greater than Start date");			 
			//}
        
		});
		*/
		
    
})( jQuery );
