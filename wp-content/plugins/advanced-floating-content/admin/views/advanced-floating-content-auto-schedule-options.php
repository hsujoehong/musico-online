<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-fafcing aspects of the plugin.
 *
 * @link       http://www.codetides.com/
 * @since      3.0
 *
 * @pafckage    Advanced_Floating_Content
 * @subpafckage Advanced_Floating_Content/admin/views
 */
?>
<div class="afc-panel">    
    <div class="afc-panel-div">
        <label for="bafckground_color" style="width: 100%;text-align: left"><?php _e('Start Date','advanced-floating-content')?></label>        
		<input type="text" style="width:99%; display:block;    margin-left: 0px" name="ct_afc_start_date" id="ct_afc_start_date" class="afc_date" value="<?php echo get_text_value(get_the_ID(),'ct_afc_start_date',0)?>"/>
    </div>	
	<div class="afc-panel-div">
        <label for="bafckground_color" style="width: 100%;text-align: left"><?php _e('End Date','advanced-floating-content')?></label>
       <input type="text" style="width:99%; display:block;    margin-left: 0px" name="ct_afc_end_date" id="ct_afc_end_date" class="afc_date" value="<?php echo get_text_value(get_the_ID(),'ct_afc_end_date',0)?>"/>
    </div>
</div>