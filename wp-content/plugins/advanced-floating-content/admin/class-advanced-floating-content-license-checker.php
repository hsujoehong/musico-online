<?php

/**
 * Envato API
 *
 * An PHP class to interact with the Envato Marketplace API
 *
 * @author		Philo Hermans
 * @copyright	Copyright (c) 2011 NETTUTS+
 * @link		http://net.tutsplus.com
 */
 
/**
 * envatoAPI
 *
 * Create our new class called "envatoAPI"
 */ 
class envatoAPI{
	
	private $api_url = 'http://marketplace.envato.com/api/edge/'; // Default URL
	private $api_set; // This will hold the chosen API set like "user-items-by-site"
	private $username; // The username of the author only needed to access the private sets
	private $api_key; // The api key of the author only needed to access the private sets
	public $options;
	/**
 	* request()
 	*
 	* Request data from the API
 	*
 	* @access	public
 	* @param	void
 	* @return	 	array		
 	*/
	public function request()
	{
		
		if(!empty($this->username) && !empty($this->api_key))
		{
			// Build the private url
			$this->api_url .= $this->username . '/'.$this->api_key.'/'.$this->api_set . '.json';
		}
		else
		{
			// Build the public url
			$this->api_url .=  $this->api_set . '.json';
		}
		//echo $this->api_url;
		$ch = curl_init($this->api_url); // Initialize a cURL session & set the API URL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); //The number of seconds to wait while trying to connect
		curl_setopt($ch, CURLOPT_USERAGENT, 'ENVATO-PURCHASE-VERIFY');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return the transfer as a string instead of outputting it out directly.
		$ch_data = curl_exec($ch); // Perform a cURL session
		curl_close($ch);  //Close a cURL session
		
		// Check if the variable contains data
		if(!empty($ch_data))
		{
			return json_decode($ch_data, true); // Decode the requested data into an array
		}
		else
		{
			return('We are unable to retrieve any information from the API.'); // Return error message
		}
		
	}
	
	/**
 	* set_api_set()
 	*
 	* Set the API set
 	*
 	* @access	public
 	* @param	string
 	* @return	 	void		
 	*/
	public function set_api_set($api_set)
	{
		$this->api_set = $api_set;
	}
	
	/**
 	* set_api_key()
 	*
 	* Set the API key
 	*
 	* @access	public
 	* @param	string
 	* @return	 	void		
 	*/
	public function set_api_key($api_key)
	{
		$this->api_key = $api_key;
	}
	
	/**
 	* set_username()
 	*
 	* Set the Username
 	*
 	* @access	public
 	* @param	string
 	* @return	 	void		
 	*/
	public function set_username($username)
	{
		$this->username = $username;
	}
		
	/**
 	* set_api_url()
 	*
 	* Set the API URL
 	*
 	* @access	public
 	* @param	string
 	* @return	 	void		
 	*/
	public function set_api_url($url)
	{
		$this->api_url = $url;
	}

	/**
 	* get_api_url()
 	*
 	* Return the API URL
 	*
 	* @access	public
 	* @return	 	string		
 	*/
	public function get_api_url()
	{
		return $this->api_url;
	}
	
	
	public function get_domain_validation($url){
         
		$whitelist = array('127.0.0.1', "::1","wpengine.com");
		if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
			return "localhost";
		}
		else{
			  $pieces = parse_url($url);			 
			  $domain = isset($pieces['host']) ? $pieces['host'] : '';
			  if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
				return $regs['domain'];
			  }
		}
	}
	
	public function validate_license_code($item_id,$product,$name,$email,$domain,$code, $purchase_date, $purchase_source){
		
		$api_url = 'http://codetides.com/api/rest_product_license_checker.php';
		$api_params = array(
			'i' => $item_id,
			'p' => $product,
			'n' => $name,
			'e' => $email,
			'd' => $domain,
			'c' => $code,
			'pd' => $purchase_date,
			'ps' => $purchase_source,
		);
		
		$response = wp_remote_get(add_query_arg($api_params, $api_url), array('timeout' => 20, 'sslverify' => false));
		if (is_wp_error($response)){
			
		    echo '<div id="setting-error-settings_updated" class="error settings-error notice is-dismissible below-h2"><p>Unexpected Error! The query returned with an error.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">'.__( 'Dismiss this notice.', 'advanced_floating_content' ).'</span></button></div>';
		}

		// License data.
		$license_data = wp_remote_retrieve_body($response);
		
		return $license_data;
	}
	
	
	public function initialize_license_checker($item_id, $email, $buyername, $purchase_code, $purchase_source)
	 {	
		
        $API = new envatoAPI();
		$API->set_username('codetides');
		$API->set_api_key('s3b30fcqhkei0hhebhzev41yjxr62vaz');
		
		$API->set_api_set('verify-purchase:' . $purchase_code);
		$data = $API->request();
		//	print_r($data);
        
			if(!empty($data['verify-purchase']))
			{
               
				// We got a valid API response let's match the item id and the username
				if($data['verify-purchase']['item_id'] == "9945856" && $data['verify-purchase']['buyer'] == $buyername)
				{
					// Everything seems to be correct! Purchase verified!
					// Show some info like purchase date and licence
					
					$valid_host = $this->get_domain_validation(home_url( '/' ));
					if($valid_host=="localhost")
					{
						update_option('ct_afc_verified_purchase', '0');
						$result = array('type'=>'error','message'=>'You are using Advanced Floating Content on Localhost, Development server does not required to validate license.');
						return $result;						
					}
					else if ($valid_host=="")
					{
						$result = array('type'=>'error','message'=>'Sorry, unfortunately we are unable to activate your license of Advanced Floating Content, Please contact support or try again.');
						return $result;
					}
					else
					{
						$buying_date = strtotime($data['verify-purchase']['created_at']);
						$response_data = $this->validate_license_code($item_id,"Advanced Floating Content",$buyername, $email, $valid_host,$purchase_code, date("Y-m-d",$buying_date),$purchase_source);
						$validation = json_decode($response_data, true);
						if($validation['type']!="error"){
							update_option('ct_afc_verified_purchase', '0');
							$buying_date = strtotime($data['verify-purchase']['created_at']);
							update_option('ct_afc_install_date', date("Y-m-d",$buying_date));
							$supported_until = strtotime($data['verify-purchase']['supported_until']);
							update_option('ct_afc_support_expire_date', date("Y-m-d",$supported_until));	
						}
						else{
							update_option('ct_afc_verified_purchase', '1');
						}
						
						$result = array('type'=>$validation['type'],'message'=>$validation['message']);
						
						return $result;		
						
					}
					              
						
				}
                else
                { 
                    update_option('ct_afc_verified_purchase', '1');                    
                    $result = array('type'=>'error','message'=>'Sorry, unfortunately we are unable to activate your license of Advanced Floating Content, Please try again.');
                    return $result;
                }
				
			}
			else
			{
				// Response from the API was empty, return error
                update_option('ct_afc_verified_purchase', '1');				
				$result = array('type'=>'error','message'=>'Sorry, unfortunately we are unable to activate your license of Advanced Floating Content, Please try again.');				
				return $result;				
			}
		
		
	 }
	
	
}
 
?>