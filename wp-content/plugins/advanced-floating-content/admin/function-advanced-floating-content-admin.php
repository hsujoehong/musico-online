<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.codetides.com/
 * @since      3.3.6
 *
 * @package    Advanced Floating Content
 * @subpackage /admin/functions
 */
 
 		
	function get_text_value($id, $input_name, $default_value='')
	{
		if(get_post_meta( $id, $input_name, true )!="")
			return get_post_meta( $id, $input_name, true );
		else
			return $default_value;
	}	
	function get_checkbox_value($id, $input_name)
	{
		if(get_post_meta( $id, $input_name, true )!="")
			return 'checked="checked"';
		else
			return "";
	}	
	function get_all_posts($id, $input_name)
	{
		$all_posts = get_posts( array('post_status'=>'publish','orderby'=>'post_title','posts_per_page'=> -1,'fields'=>'ids'));
		$selected_posts = get_post_meta( $id, $input_name, true );
        if(empty($selected_posts)) $select='selected="selected"';
		$output = '<select name="'.$input_name.'[]" id="'.$input_name.'" multiple="multiple" style="width:75%; height:150px;">';		
        $output .= '<option value="" '.$select.'>Please Select Post(s)</option>';
			foreach ( $all_posts as $post ) {
				if(@in_array($post, $selected_posts)) { $selected = 'selected="selected"'; }else {$selected = '';}
				$output .= '<option value="'.$post.'" '.$selected.'>'.get_the_title($post).'</option>';		
			}
		$output .= '</select>';
		return $output;
	}
	function get_all_pages($id, $input_name)
	{
			
		$all_pages = get_pages( array('post_status'=>'publish','post_type' => 'page','sort_column' => 'post_title','posts_per_page'=> -1));
		$selected_pages = get_post_meta( $id, $input_name, true );		        
		$output = '<select name="'.$input_name.'[]" id="'.$input_name.'" multiple="multiple" style="width:75%; height:150px;">';
        if(empty($selected_pages)) $select='selected="selected"';
        $output .= '<option value="" '.$select.'>Please Select Page(s)</option>';
			foreach ( $all_pages as $page ) {
				if(@in_array($page->ID, $selected_pages)) { $selected = 'selected="selected"'; } else {$selected = '';}
				$output .= '<option value="'.$page->ID.'" '.$selected.'>'.$page->post_title.'</option>';		
			}
		$output .= '</select>';
		return $output;
	}
	function get_all_categories($id, $input_name)
	{
		$all_categories = get_categories( array('orderby'=>'name','taxonomy'=>'category'));
		$selected_categories = get_post_meta( $id, $input_name, true );		
         if(empty($selected_categories)) $select='selected="selected"';
		$output = '<select name="'.$input_name.'[]" id="'.$input_name.'" multiple="multiple" style="width:75%; height:150px;">';	
        $output .= '<option value="" '.$select.'>Please Select Categories</option>';
			foreach ( $all_categories as $category ) {
				if(@in_array($category->cat_ID, $selected_categories)) { $selected = 'selected="selected"'; }else {$selected = '';}
				$output .= '<option value="'.$category->cat_ID.'" '.$selected.'>'.$category->cat_name.'</option>';		
			}
		$output .= '</select>';
		return $output;
	}
	function get_all_cpts($id, $input_name)
	{
		$args = array(
		   'public'   => true,
		   '_builtin' => false
		);
		$output = 'names'; // names or objects, note names is the default
		$operator = 'and'; // 'and' or 'or'
		$all_cpts = get_post_types($args, $output, $operator );		
		$selected_cpts = get_post_meta( $id, $input_name, true );
        if(empty($selected_cpts)) $select='selected="selected"';
		$output = '<select name="'.$input_name.'[]" id="'.$input_name.'" multiple="multiple" style="width:75%; height:150px;">';
        $output .= '<option value="" '.$select.'>Please Select Custom Post Types</option>';
			foreach ( $all_cpts as $cpts ) {
				if(@in_array($cpts, $selected_cpts)) { $selected = 'selected="selected"'; }else {$selected = '';}
				$output .= '<option value="'.$cpts.'" '.$selected.'>'.$cpts.'</option>';
			}
		$output .= '</select>';
		return $output;
		
	}
	function get_all_woocommerce($id, $input_name)
	{
		$all_woocommerce = get_posts( array('post_status'=>'publish','post_type' => 'product','sort_column' => 'post_title','posts_per_page'=> -1));
		$selected_woo = get_post_meta( $id, $input_name, true );
        if(empty($selected_woo)) $select='selected="selected"';
		$output = '<select name="'.$input_name.'[]" id="'.$input_name.'" multiple="multiple" style="width:75%; height:150px;">';
        $output .= '<option value="" '.$select.'>Please Select WooCommerce Products</option>';
			foreach ( $all_woocommerce as $woo ) {
				if(@in_array($woo->ID, $selected_woo)) { $selected = 'selected="selected"'; }else {$selected = '';}
				$output .= '<option value="'.$woo->ID.'" '.$selected.'>'.$id.$woo->post_title.'</option>';
			}
		$output .= '</select>';
		return $output;
		
	}
	
	function get_all_woocommerce_categories($id, $input_name)
	{
		$all_woocommerce_categories = get_terms( 'product_cat', array('orderby'=>'name','order'=> 'asc','hide_empty' => false) );		
		$selected_wooCat = get_post_meta( $id, $input_name, true );
        if(empty($selected_wooCat)) $select='selected="selected"';
		$output = '<select name="'.$input_name.'[]" id="'.$input_name.'" multiple="multiple" style="width:75%; height:150px;">';
        $output .= '<option value="" '.$select.'>Please Select WooCommerce Categories</option>';
			foreach ( $all_woocommerce_categories as $wooCat ) {
				if(@in_array($wooCat->term_id, $selected_wooCat)) { $selected = 'selected="selected"'; }else {$selected = '';}
				$output .= '<option value="'.$wooCat->term_id.'" '.$selected.'>'.$wooCat->name.'</option>';
			}
		$output .= '</select>';
		return $output;
		
	}
    
    function get_all_bbpress($id, $input_name)
	{
		$all_bbpress = get_posts( array('post_status'=>'publish','post_type' => 'forum','sort_column' => 'post_title','posts_per_page'=> -1));
		$selected_bbpress = get_post_meta( $id, $input_name, true );       
        if(empty($selected_bbpress)){
            $select='selected="selected"';
        } 
		$output = '<select name="'.$input_name.'[]" id="'.$input_name.'" multiple="multiple" style="width:75%; height:150px;">';
        $output .= '<option value="" '.$select.'>Please Select bbPress Pages</option>';
			foreach ( $all_bbpress as $bbpress ) {
				if(@in_array($bbpress->ID, $selected_bbpress)) { 
                    $selected = 'selected="selected"'; }
                else {
                $selected = '';}
                
				$output .= '<option value="'.$bbpress->ID.'" '.$selected.'>'.$bbpress->post_title.'</option>';
			}
		$output .= '</select>';
		return $output;
		
	}
	function get_all_countries($id, $input_name)
	{
		$all_countries = array(
	'AF' => 'Afghanistan','AX' => 'Aland Islands','AL' => 'Albania','DZ' => 'Algeria','AS' => 'American Samoa','AD' => 'Andorra','AO' => 'Angola',
	'AI' => 'Anguilla','AQ' => 'Antarctica','AG' => 'Antigua And Barbuda','AR' => 'Argentina','AM' => 'Armenia','AW' => 'Aruba','AU' => 'Australia',
	'AT' => 'Austria','AZ' => 'Azerbaijan','BS' => 'Bahamas','BH' => 'Bahrain','BD' => 'Bangladesh','BB' => 'Barbados','BY' => 'Belarus','BE' => 'Belgium',
	'BZ' => 'Belize','BJ' => 'Benin','BM' => 'Bermuda','BT' => 'Bhutan','BO' => 'Bolivia','BA' => 'Bosnia And Herzegovina','BW' => 'Botswana',
	'BV' => 'Bouvet Island','BR' => 'Brazil','IO' => 'British Indian Ocean Territory','BN' => 'Brunei Darussalam','BG' => 'Bulgaria','BF' => 'Burkina Faso',
	'BI' => 'Burundi','KH' => 'Cambodia','CM' => 'Cameroon','CA' => 'Canada','CV' => 'Cape Verde','KY' => 'Cayman Islands','CF' => 'Central African Republic',
	'TD' => 'Chad','CL' => 'Chile','CN' => 'China','CX' => 'Christmas Island','CC' => 'Cocos (Keeling) Islands','CO' => 'Colombia','KM' => 'Comoros',
	'CG' => 'Congo','CD' => 'Congo, Democratic Republic','CK' => 'Cook Islands','CR' => 'Costa Rica','CI' => 'Cote D\'Ivoire','HR' => 'Croatia','CU' => 'Cuba',
	'CY' => 'Cyprus','CZ' => 'Czech Republic','DK' => 'Denmark','DJ' => 'Djibouti','DM' => 'Dominica','DO' => 'Dominican Republic','EC' => 'Ecuador',
	'EG' => 'Egypt','SV' => 'El Salvador','GQ' => 'Equatorial Guinea','ER' => 'Eritrea','EE' => 'Estonia','ET' => 'Ethiopia','FK' => 'Falkland Islands (Malvinas)',
	'FO' => 'Faroe Islands','FJ' => 'Fiji','FI' => 'Finland','FR' => 'France','GF' => 'French Guiana','PF' => 'French Polynesia','TF' => 'French Southern Territories',
	'GA' => 'Gabon','GM' => 'Gambia','GE' => 'Georgia','DE' => 'Germany','GH' => 'Ghana','GI' => 'Gibraltar','GR' => 'Greece','GL' => 'Greenland','GD' => 'Grenada',
	'GP' => 'Guadeloupe','GU' => 'Guam','GT' => 'Guatemala','GG' => 'Guernsey','GN' => 'Guinea','GW' => 'Guinea-Bissau','GY' => 'Guyana','HT' => 'Haiti','HM' => 'Heard Island & Mcdonald Islands',
	'VA' => 'Holy See (Vatican City State)','HN' => 'Honduras','HK' => 'Hong Kong','HU' => 'Hungary','IS' => 'Iceland','IN' => 'India','ID' => 'Indonesia','IR' => 'Iran, Islamic Republic Of',
	'IQ' => 'Iraq','IE' => 'Ireland','IM' => 'Isle Of Man','IL' => 'Israel','IT' => 'Italy','JM' => 'Jamaica','JP' => 'Japan','JE' => 'Jersey','JO' => 'Jordan',
	'KZ' => 'Kazakhstan','KE' => 'Kenya','KI' => 'Kiribati','KR' => 'Korea','KW' => 'Kuwait','KG' => 'Kyrgyzstan','LA' => 'Lao People\'s Democratic Republic',
	'LV' => 'Latvia','LB' => 'Lebanon','LS' => 'Lesotho','LR' => 'Liberia','LY' => 'Libyan Arab Jamahiriya','LI' => 'Liechtenstein','LT' => 'Lithuania','LU' => 'Luxembourg',
	'MO' => 'Macao','MK' => 'Macedonia','MG' => 'Madagascar','MW' => 'Malawi','MY' => 'Malaysia','MV' => 'Maldives','ML' => 'Mali','MT' => 'Malta','MH' => 'Marshall Islands',
	'MQ' => 'Martinique','MR' => 'Mauritania','MU' => 'Mauritius','YT' => 'Mayotte','MX' => 'Mexico','FM' => 'Micronesia, Federated States Of','MD' => 'Moldova',
	'MC' => 'Monaco','MN' => 'Mongolia','ME' => 'Montenegro','MS' => 'Montserrat','MA' => 'Morocco','MZ' => 'Mozambique','MM' => 'Myanmar','NA' => 'Namibia',
	'NR' => 'Nauru','NP' => 'Nepal','NL' => 'Netherlands','AN' => 'Netherlands Antilles','NC' => 'New Caledonia','NZ' => 'New Zealand','NI' => 'Nicaragua',
	'NE' => 'Niger','NG' => 'Nigeria','NU' => 'Niue','NF' => 'Norfolk Island','MP' => 'Northern Mariana Islands','NO' => 'Norway','OM' => 'Oman','PK' => 'Pakistan',
	'PW' => 'Palau','PS' => 'Palestinian Territory, Occupied','PA' => 'Panama','PG' => 'Papua New Guinea','PY' => 'Paraguay','PE' => 'Peru','PH' => 'Philippines',
	'PN' => 'Pitcairn','PL' => 'Poland','PT' => 'Portugal','PR' => 'Puerto Rico','QA' => 'Qatar','RE' => 'Reunion','RO' => 'Romania','RU' => 'Russian Federation',
	'RW' => 'Rwanda','BL' => 'Saint Barthelemy','SH' => 'Saint Helena','KN' => 'Saint Kitts And Nevis','LC' => 'Saint Lucia','MF' => 'Saint Martin','PM' => 'Saint Pierre And Miquelon',
	'VC' => 'Saint Vincent And Grenadines','WS' => 'Samoa','SM' => 'San Marino','ST' => 'Sao Tome And Principe','SA' => 'Saudi Arabia','SN' => 'Senegal',
	'RS' => 'Serbia','SC' => 'Seychelles','SL' => 'Sierra Leone','SG' => 'Singapore','SK' => 'Slovakia','SI' => 'Slovenia','SB' => 'Solomon Islands','SO' => 'Somalia',
	'ZA' => 'South Africa','GS' => 'South Georgia And Sandwich Isl.','ES' => 'Spain','LK' => 'Sri Lanka','SD' => 'Sudan','SR' => 'Suriname','SJ' => 'Svalbard And Jan Mayen',
	'SZ' => 'Swaziland','SE' => 'Sweden','CH' => 'Switzerland','SY' => 'Syrian Arab Republic','TW' => 'Taiwan','TJ' => 'Tajikistan','TZ' => 'Tanzania','TH' => 'Thailand',
	'TL' => 'Timor-Leste','TG' => 'Togo','TK' => 'Tokelau','TO' => 'Tonga','TT' => 'Trinidad And Tobago','TN' => 'Tunisia','TR' => 'Turkey','TM' => 'Turkmenistan',
	'TC' => 'Turks And Caicos Islands','TV' => 'Tuvalu','UG' => 'Uganda','UA' => 'Ukraine','AE' => 'United Arab Emirates','GB' => 'United Kingdom','US' => 'United States',
	'UM' => 'United States Outlying Islands','UY' => 'Uruguay','UZ' => 'Uzbekistan','VU' => 'Vanuatu','VE' => 'Venezuela','VN' => 'Viet Nam','VG' => 'Virgin Islands, British',
	'VI' => 'Virgin Islands, U.S.','WF' => 'Wallis And Futuna','EH' => 'Western Sahara','YE' => 'Yemen','ZM' => 'Zambia','ZW' => 'Zimbabwe');
		$selected_countries = get_post_meta( $id, $input_name, true );       
        if(empty($selected_countries)){
            $select='selected="selected"';
        } 
		$output = '<select name="'.$input_name.'[]" id="'.$input_name.'" multiple="multiple" style="width:75%; height:150px;">';
        $output .= '<option value="" '.$select.'>Please Select Countries</option>';
			foreach ( $all_countries as $key => $countries_name ) {
				if(@in_array($key, $selected_countries)) { 
                    $selected = 'selected="selected"'; }
                else {
                $selected = '';}
                
				$output .= '<option value="'.$key.'" '.$selected.'>'.$countries_name.'</option>';
			}
		$output .= '</select>';
		return $output;
		
	}
?>