jQuery(function($){
    let $outputDataTable = $('#woce-modal-container .output-data-table');
    if($outputDataTable.length) {
        let initDataTable = function(height = 600){
            return $outputDataTable.DataTable({
                paging: false,
                ordering: false,
                responsive: false,
                scrollY: height,
                scrollX: true,
                scrollCollapse: true,
                language: Woce.string.datatable
            });
        };
        let dataTable;
        $('#woce-modal-container').fadeIn(100);
        $(window).on('resize', function(){
            let $modal = $('#woce-modal-container');
            let height = $modal.find('.content').height();
            $modal.find('.form-controls').each(function(){
                height -= $(this).outerHeight(true);
            });
            height -= 150;
            if(dataTable)
                dataTable.destroy();
            dataTable = initDataTable(height);
        }).trigger('resize');
    }

    let $colSelect = $('#filter-columns');
    let $statusSelect = $('#order-statuses');
    $.datepicker.setDefaults($.datepicker.regional["zh-TW"]);
    $('.init-select2').select2();
    $('.time-picker').datepicker({
        dateFormat: 'yy-mm-dd',
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        selectOtherMonths: true
    });
    $('[data-toggle="tooltip"]').tooltip({
        animation: false
    });
    $('.select-all-column').on('click', function(e){
        e.preventDefault();
        $colSelect.find('option').attr('selected', true).trigger('change');
    });
    $('.unselect-all-column').on('click', function(e){
        e.preventDefault();
        $colSelect.find('option').attr('selected', false).trigger('change');
    });
    $('.select-all-status').on('click', function(e){
        e.preventDefault();
        $statusSelect.find('option').attr('selected', true).trigger('change');
    });
    $('.unselect-all-status').on('click', function(e){
        e.preventDefault();
        $statusSelect.find('option').attr('selected', false).trigger('change');
    });

    $('.cell-id').change(function(){
        let orderId = $(this).data('order-id');
        $('.cell-id-'+orderId).attr('checked', $(this).is(':checked'));
    });

    $('#check-all-output-data').change(function(){
        $('.cell-id').attr('checked', $(this).is(':checked'))
    });
    $(document).on('click', '#woce-modal-container .close-modal, #woce-modal-container .mask', function(){
        $(this).closest('#woce-modal-container').hide();
    });
    $(document).on('click', '.show-data-result', function(e){
        e.preventDefault();
        $('#woce-modal-container').show();
    });

    $(window).on('resize', function(){
        let $target = $('.output-data-wrapper');
        let height = 0;
        $target.siblings().each(function(){
            height += $(this).outerHeight(true);
        });
    }).trigger('resize');
});