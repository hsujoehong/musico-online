<?php
class Woce_Data_Helper
{
    protected $data_headers = [];

    protected $query_args = [];

    protected $output_data = [];

    protected $filter_column = [];

    public function start($time_start)
    {
        if($time_start) {
            $this->query_args['date_query'][0]['after'] = $time_start;
        }
        return $this;
    }

    public function end($time_end)
    {
        if($time_end) {
            $this->query_args['date_query'][0]['before'] = $time_end;
        }
        return $this;
    }

    public function inclusive()
    {
        $this->query_args['date_query'][0]['inclusive'] = true;
        return $this;
    }

    public function set_filter_column(array $columns)
    {
        if(!empty($columns)) {
            $this->filter_column = $columns;
        }
        return $this;
    }

    public function get_filter_column()
    {
        return $this->filter_column;
    }

    public function get_data_headers()
    {
        return $this->data_headers;
    }

    public function get_query_args()
    {
        return $this->query_args;
    }

    public function set_query_arg($parameter, $value)
    {
        $this->query_args[$parameter] = $value;
        return $this;
    }

    public function set_query_args($query_args)
    {
        $this->query_args = $query_args;
        return $this;
    }
}