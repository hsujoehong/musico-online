<?php
class Woce_Customer_Data_Helper extends Woce_Data_Helper
{
    private $users_data = [];

    public function __construct()
    {
        $this->set_default_query_args();
        $this->reset_data_headers();
    }

    public function get_default_data_headers()
    {
        $data_headers = [
            'user_id'               => __('User ID', 'woce'),
            'user_login'            => __('Username', 'woce'),
            'user_email'            => __('Email', 'woce'),
            'user_registered'       => __('Registered Date', 'woce'),
            'billing_email'         => __('Billing Email', 'woce'),
            'billing_first_name'    => __('Billing First Name', 'woce'),
            'billing_last_name'     => __('Billing Last Name', 'woce'),
            'billing_phone'         => __('Billing Phone', 'woce'),
            'billing_address'       => __('Billing Address', 'woce'),
            'billing_postcode'      => __('Billing Postcode', 'woce'),
            'billing_country'       => __('Billing Country', 'woce'),
            'billing_state'         => __('Billing State', 'woce'),
            'billing_city'          => __('Billing City', 'woce'),
            'shipping_first_name'   => __('Shipping First Name', 'woce'),
            'shipping_last_name'    => __('Shipping Last Name', 'woce'),
            'shipping_postcode'     => __('Shipping Postcode', 'woce'),
            'shipping_address'      => __('Shipping Address', 'woce'),
            'shipping_country'      => __('Shipping Country', 'woce'),
            'shipping_state'        => __('Shipping State', 'woce'),
            'shipping_city'         => __('Shipping City', 'woce'),
        ];
        $data_headers = apply_filters('woce_customer_data_headers', $data_headers);
        return $data_headers;
    }

    public function reset_data_headers()
    {
        $this->data_headers = $this->get_default_data_headers();
    }

    private function set_default_query_args()
    {
        $this->query_args = [
            'number' => -1,
            'paged' => 1,
            'offset' => 0,
            'orderby' => 'ID',
            'order' => 'DESC'
        ];
    }

    public function limit($limit, $page = 1)
    {
        if($limit > 0) {
            $this->set_query_arg('number', $limit);
        }
        if($page > 1) {
            $this->set_query_arg('paged', $page);
        }
        return $this;
    }

    public function include($include)
    {
        return $this->set_query_arg('include', $include);
    }

    public function get_output_data()
    {
        $this->output_data = [];
        $this->prepare_users()->prepare_output_data();

        return $this->output_data;
    }

    public function prepare_users()
    {
        $query_args = apply_filters('woce_customer_post_query_args', $this->query_args);
        $this->users_data = (new WP_User_Query($query_args))->get_results();
        return $this;
    }

    public function prepare_output_data()
    {
        $data_headers = [];
        if(!empty($this->filter_column)) {
            foreach($this->filter_column as $filter_key) {
                $data_headers[$filter_key] = $this->data_headers[$filter_key];
            }
        } else {
            $data_headers = $this->data_headers;
        }
        $this->output_data[] = $data_headers;

        foreach($this->users_data as $user) {
            /** @var WP_User $user */
            $data_row = $this->get_item_row($user);

            $row_empty = true;
            foreach($data_row as $item) {
                if($item !== '')  {
                    $row_empty = false;
                    break;
                }
            }
            if(!$row_empty)
                $this->output_data[$user->ID] = $data_row;
        }
    }

    /**
     * @param WP_User $user
     * @return array
     */
    private function get_item_row($user)
    {
        $row = [];
        foreach($this->data_headers as $key => $header_name) {
            if(empty($this->filter_column) || in_array($key, $this->filter_column)) {
                $row[$key] = $this->get_column_data($key, $user);
            }
        }
        return $row;
    }

    /**
     * @param $key
     * @param WP_User $user
     * @return string
     */
    public function get_column_data($key, $user)
    {
        $data = '';
        switch($key) {
            case 'user_id':
                $data = $user->ID;
                break;
            case 'user_login':
                $data = $user->user_login;
                break;
            case 'user_email':
                $data = $user->user_email;
                break;
            case 'user_registered':
                $data = $user->user_registered;
                break;
            case 'billing_email':
                $data = $user->get('billing_email');
                break;
            case 'billing_first_name':
                $data = $user->get('billing_first_name');
                break;
            case 'billing_last_name':
                $data = $user->get('billing_last_name');
                break;
            case 'billing_phone':
                $data = $user->get('billing_phone');
                break;
            case 'billing_address':
                $data = $user->get('billing_address_1');
                break;
            case 'billing_postcode':
                $data = $user->get('billing_postcode');
                break;
            case 'billing_country':
                $data = $user->get('billing_country');
                break;
            case 'billing_state':
                $data = $user->get('billing_state');
                break;
            case 'billing_city':
                $data = $user->get('billing_city');
                break;
            case 'shipping_first_name':
                $data = $user->get('shipping_first_name');
                break;
            case 'shipping_last_name':
                $data = $user->get('shipping_last_name');
                break;
            case 'shipping_postcode':
                $data = $user->get('shipping_postcode');
                break;
            case 'shipping_address':
                $data = $user->get('shipping_address');
                break;
            case 'shipping_country':
                $data = $user->get('shipping_country');
                break;
            case 'shipping_state':
                $data = $user->get('shipping_state');
                break;
            case 'shipping_city':
                $data = $user->get('shipping_city');
                break;
        }

        return apply_filters('woce_customer_column_data', $data, $key, $user);
    }

    private function get_file_name()
    {
        $file_name = __('Customers', 'woce') . '-' . time();
        return apply_filters('woce_export_customer_file_name', $file_name);
    }

    public function download_csv()
    {
        $this->output_data = [];
        $this->prepare_users()->prepare_output_data();
        try {
            $downloader = Woce_Downloader_Factory::get_downloader('csv');
            $downloader->download($this->get_file_name(), $this->output_data);
        } catch (Exception $e) {

        }
    }

    public function download_excel()
    {
        $this->output_data = [];
        $this->prepare_users()->prepare_output_data();
        $page = [
            'title' => '會員總覽',
            'data' => []
        ];

        foreach ($this->output_data as $row => $output_data) {
            array_push($page['data'], array_values($output_data));
        }
        $sheet_data = [$page];
        try {
            $downloader = Woce_Downloader_Factory::get_downloader('excel');
            $downloader->download($this->get_file_name(), $sheet_data);
        } catch (Exception $e) {

        }
    }
}