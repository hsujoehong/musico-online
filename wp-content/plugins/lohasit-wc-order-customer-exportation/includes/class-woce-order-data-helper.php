<?php
class Woce_Order_Data_Helper extends Woce_Data_Helper
{
    private $posts_data = [];

    public function __construct()
    {
        $this->set_default_query_args();
        $this->reset_data_headers();
    }

    public function get_default_data_headers()
    {
        $data_headers = [
            'order_id'              => __('ID', 'woce'),
            'order_status'          => __('Status', 'woce'),
            'order_date'            => __('Date', 'woce'),
            'product_id'            => __('Product ID', 'woce'),
            'product_name'          => __('Product Name', 'woce'),
            'product_variations'    => __('Product Variations', 'woce'),
            'product_regular_price' => __('Product Regular Price', 'woce'),
            'product_sale_price'    => __('Product Sale Price', 'woce'),
            'product_sku'           => __('SKU', 'woce'),
            'item_qty'              => __('Quantity', 'woce'),
            'item_price'            => __('Item Price', 'woce'),
            'item_subtotal'         => __('Item Subtotal', 'woce'),
            'total_discount'        => __('Total Discount', 'woce'),
            'order_shipping_fee'    => __('Shipping Fee', 'woce'),
            'order_amount'          => __('Amount', 'woce'),
            'order_refund'          => __('Refunded Amount', 'woce'),
            'customer_account'      => __('Account', 'woce'),
            'billing_email'         => __('Billing Email', 'woce'),
            'billing_first_name'    => __('Billing First Name', 'woce'),
            'billing_last_name'     => __('Billing Last Name', 'woce'),
            'billing_phone'         => __('Billing Phone', 'woce'),
            'billing_country'       => __('Billing Country', 'woce'),
            'billing_postcode'      => __('Billing Postcode', 'woce'),
            'billing_state'         => __('Billing State', 'woce'),
            'billing_city'          => __('Billing City', 'woce'),
            'billing_address'       => __('Billing Address', 'woce'),
            'shipping_first_name'   => __('Shipping First Name', 'woce'),
            'shipping_last_name'    => __('Shipping Last Name', 'woce'),
            'shipping_country'      => __('Shipping Country', 'woce'),
            'shipping_postcode'     => __('Shipping Postcode', 'woce'),
            'shipping_state'        => __('Shipping State', 'woce'),
            'shipping_city'         => __('Shipping City', 'woce'),
            'shipping_address'      => __('Shipping Address', 'woce'),
            'payment_method'        => __('Payment Method', 'woce'),
            'shipping_method'       => __('Shipping Method', 'woce'),
	        'customer_note'         => __('Customer Note', 'woce'),
        ];
        $data_headers = apply_filters('woce_order_data_headers', $data_headers);
        return $data_headers;
    }

    public function reset_data_headers()
    {
        $this->data_headers = $this->get_default_data_headers();
    }

    private function set_default_query_args()
    {
        $this->query_args=  [
            'posts_per_page'    => -1,
            'paged'             => 1,
            'post_type'         => 'shop_order',
            'post_status'       => array_keys( wc_get_order_statuses() ),
            'meta_query'        => [
                'relation' => 'AND',
            ]
        ];
    }

    public function set_range($start_time = null, $end_time = null)
    {
        if($start_time) {
            $this->query_args['date_query'][] = [
                'column' => 'post_date_gmt',
                'after'  => $start_time,
            ];
        }

        if($end_time) {
            $this->query_args['date_query'][] = [
                'column' => 'post_date_gmt',
                'before'  => $end_time,
            ];
        }
        return $this;
    }

    public function search_post_in($post_ids)
    {
        if($post_ids) {
            return $this->set_query_arg('post__in', explode(',', $post_ids));
        }
        return $this;
    }

    public function add_meta_query($args)
    {
        array_push($this->query_args['meta_query'], $args);
        return $this;
    }

    public function limit($limit, $page = 1)
    {
        if($limit > 0) {
            $this->set_query_arg('posts_per_page', $limit);
        }
        if($page > 1) {
            $this->set_query_arg('paged', $page);
        }
        return $this;
    }

    public function status($status)
    {
        return $this->set_query_arg('post_status', $status);
    }

    public function post_in($post_in)
    {
        return $this->set_query_arg('post__in', $post_in);
    }

    public function prepare_posts()
    {
        $query_args = apply_filters('woce_order_post_query_args', $this->query_args);
        $this->posts_data = (new WP_Query($query_args))->get_posts();
        return $this;
    }

    private function wc_gt_3()
    {
        return version_compare(WC()->version, '3.0.0', '>=');
    }

    public function prepare_output_data()
    {
        $data_headers = [];
        if(!empty($this->filter_column)) {
            foreach($this->filter_column as $filter_key) {
                $data_headers[$filter_key] = $this->data_headers[$filter_key];
            }
        } else {
            $data_headers = $this->data_headers;
        }
        $this->output_data[] = $data_headers;

        foreach($this->posts_data as $post) {
            $order = wc_get_order($post->ID);
            $order_items = $order->get_items();
            $order_id = $this->wc_gt_3() ? $order->get_id() : $order->id;
            foreach($order_items as $order_item_id => $order_item) {
                if($order_item === reset($order_items))
                    $data_row = $this->get_item_row($order, $order_item_id, $order_item, true);
                else
                    $data_row = $this->get_item_row($order, $order_item_id, $order_item);

                $row_empty = true;
                foreach($data_row as $item) {
                    if($item !== '')  {
                        $row_empty = false;
                        break;
                    }
                }
                if(!$row_empty)
                    $this->output_data[$order_id . '_' . $order_item_id] = $data_row;
            }

        }
        return $this;
    }

    public function get_output_data()
    {
        $this->output_data = [];
        $this->prepare_posts()->prepare_output_data();

        return $this->output_data;
    }

    private function get_file_name()
    {
        $file_name = __('Orders', 'woce') . '-' . time();
        return apply_filters('woce_export_order_file_name', $file_name);
    }

    public function download_csv()
    {
        $this->output_data = [];
        $this->prepare_posts()->prepare_output_data();
        try {
            $downloader = Woce_Downloader_Factory::get_downloader('csv');
            $downloader->download($this->get_file_name(), $this->output_data);
        } catch (Exception $e) {

        }
    }

    public function download_excel()
    {
        $this->output_data = [];
        $this->prepare_posts()->prepare_output_data();
        $page = [
            'title' => '訂單總覽',
            'data' => []
        ];

        foreach ($this->output_data as $row => $output_data) {
            array_push($page['data'], array_values($output_data));
        }
        $sheet_data = [$page];
        try {
            $downloader = Woce_Downloader_Factory::get_downloader('excel');
            $downloader->download($this->get_file_name(), $sheet_data);
        } catch (Exception $e) {

        }
    }

    public function clear_filter_column()
    {
        $this->filter_column = [];
        return $this;
    }

    public function get_item_row($order, $order_item_id, $order_item, $first_item = false)
    {
        $row = [];
        foreach($this->data_headers as $key => $header_name) {
            if(empty($this->filter_column) || in_array($key, $this->filter_column)) {
                $row[$key] = $this->get_column_data($key, $order, $order_item_id, $order_item, $first_item);
            }
        }
        return $row;
    }

    /**
     * @param mixed $order_item
     *
     * @return false|null|WC_Product
     */
    private function get_product_from_order_item($order_item)
    {
        $product_id = ($order_item['variation_id'] != 0) ? $order_item['variation_id'] : $order_item['product_id'];
        $product = wc_get_product($product_id);
        return $product;
    }

    /**
     * @param $key
     * @param WC_Order $order
     * @param array $order_item
     *
     * @return bool
     */
    private function get_column_data($key, $order, $order_item_id, $order_item, $first_item = false)
    {
        $data = '';
        switch($key) {
            case 'order_id':
                $data = $this->wc_gt_3() ? $order->get_id() : $order->id;
                break;
            case 'order_status':
                $data = wc_get_order_status_name($order->get_status());
                break;
            case 'order_date':
                $data = $this->wc_gt_3() ? $order->get_date_created() : $order->order_date;
                break;
            case 'order_amount':
                $data = $order->get_total();
                break;
            case 'order_shipping_fee':
                $data = $this->wc_gt_3() ? $order->get_shipping_total() : $order->get_total_shipping();
                break;
            case 'order_refund':
                $data = $order->get_total_refunded();
                break;
                break;
            case 'total_discount':
                $data = $order->get_total_discount();
                break;
            case 'customer_account':
                $customer = $order->get_user();
                $data = ($customer != false) ? $customer->user_login : '';
                break;
            case 'billing_email':
                $data = $this->wc_gt_3() ? $order->get_billing_email() : $order->billing_email;
                break;
            case 'billing_first_name':
                $data = $this->wc_gt_3() ? $order->get_billing_first_name() : $order->billing_first_name;
                break;
            case 'billing_last_name':
                $data = $this->wc_gt_3() ? $order->get_billing_last_name() : $order->billing_last_name;
                break;
            case 'billing_phone':
                $data = $this->wc_gt_3() ? $order->get_billing_phone() : $order->billing_phone;
                break;
            case 'billing_postcode':
                $data = $this->wc_gt_3() ? $order->get_billing_postcode() : $order->billing_postcode;
                break;
            case 'billing_address':
                $data = $this->wc_gt_3() ? $order->get_billing_address_1() : $order->billing_address_1;
                break;
            case 'billing_country':
                $data = $this->wc_gt_3() ? $order->get_billing_country() : $order->billing_country;
                break;
            case 'billing_state':
                $data = $this->wc_gt_3() ? $order->get_billing_state() : $order->billing_state;
                break;
            case 'billing_city':
                $data = $this->wc_gt_3() ? $order->get_billing_city() : $order->billing_city;
                break;
            case 'shipping_first_name':
                $data = $this->wc_gt_3() ? $order->get_shipping_first_name() : $order->shipping_first_name;
                break;
            case 'shipping_last_name':
                $data = $this->wc_gt_3() ? $order->get_shipping_last_name() : $order->shipping_last_name;
                break;
            case 'shipping_postcode':
                $data = $this->wc_gt_3() ? $order->get_shipping_postcode() : $order->shipping_postcode;
                break;
            case 'shipping_address':
                $data = $this->wc_gt_3() ? $order->get_shipping_address_1() : $order->shipping_address_1;
                break;
            case 'shipping_country':
                $data = $this->wc_gt_3() ? $order->get_shipping_country() : $order->shipping_country;
                break;
            case 'shipping_state':
                $data = $this->wc_gt_3() ? $order->get_shipping_state() : $order->shipping_state;
                break;
            case 'shipping_city':
                $data = $this->wc_gt_3() ? $order->get_shipping_city() : $order->shipping_city;
                break;
            case 'payment_method':
                $data = $this->wc_gt_3() ? $order->get_payment_method_title() : $order->payment_method_title;
                break;
            case 'shipping_method':
                $data = $order->get_shipping_method();
                break;
	        case 'customer_note':
		        $data = $this->wc_gt_3() ? $order->get_customer_note() : $order->customer_note;
	        	break;
        }

        switch($key) {
            case 'product_id':
                $data = $order_item['product_id'];
                break;
            case 'product_name':
                $data = $order_item['name'];
                break;
            case 'product_variations':
                $product = $this->get_product_from_order_item($order_item);
                $variations = '';
                if($product && $product->is_type('variation')) {
                    /** @var WC_Product_Variable $product */
                    $variation_attributes = $product->get_variation_attributes();
                    foreach ( $variation_attributes as $attr_name => $attr_value ) {
                        $attr_name = str_replace('attribute_', '', $attr_name);
                        $attr_name = str_replace('pa_', '', $attr_name);
                        $attr_name = urldecode($attr_name);
                        $variations .= $attr_name . ':' . $attr_value;
                        if($attr_value !== end($variation_attributes))
                            $variations .= '/';
                    }
                }
                $data = $variations;
                break;
            case 'product_sku':
                $product = $this->get_product_from_order_item($order_item);
                $data = $product ? $product->get_sku() : '';
                break;
            case 'product_regular_price':
                $product = $this->get_product_from_order_item($order_item);
                $data = $product ? $product->get_regular_price() : '';
                break;
            case 'product_sale_price':
                $product = $this->get_product_from_order_item($order_item);
                $data = $product ? $product->get_sale_price() : '';
                break;
            case 'item_price':
                $data = $order_item['line_subtotal'] / $order_item['qty'];
                break;
            case 'item_qty':
                $data = $order_item['qty'];
                break;
            case 'item_subtotal':
                $data = $order_item['line_subtotal'];
                break;
        }

        $item_column_data = apply_filters('woce_order_item_columns', [
            'product_id',
            'product_name',
            'product_variations',
            'product_regular_price',
            'product_sale_price',
            'product_sku',
            'item_qty',
            'item_price',
            'item_subtotal',
        ]);

        if(!$first_item && !in_array($key, $item_column_data) && !apply_filters('woce_first_order_item_empty', false)) {
            $data = '';
        }

        $data = apply_filters('woce_order_column_data', $data, $key, $order, $order_item_id, $order_item, $first_item);
        return $data;
    }
}