<?php
/*
 * Plugin Name: Lohasit WC Order/Customer Exportation
 * Description: 樂活壓板系統 - Woocommerce - 匯出訂單及顧客資料
 * Version: 3.0.0
 * Author URI: https://www.lohaslife.cc/
 * Author: Lohas IT
 */

require_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';

class Wc_Order_Customer_Exportation
{
	private static $instance;

	public function __construct() {
		load_plugin_textdomain('woce', false, basename(dirname(__FILE__)) . '/languages');
		$this->init();
	}

	public static function get_instance()
    {
		if(is_null(self::$instance))
			self::$instance = new self;
		return self::$instance;
	}

	private function init()
    {
		$this->includes();
		$this->register_hooks();
	}

	private function includes()
    {
		require_once('includes/class-woce-excel-downloader.php');
		require_once('includes/class-woce-csv-downloader.php');
		require_once('includes/class-woce-downloader-factory.php');
        require_once('includes/class-woce-data-helper.php');
		require_once('includes/class-woce-order-data-helper.php');
		require_once('includes/class-woce-customer-data-helper.php');
	}

	private function register_hooks()
    {
		add_action('admin_menu', array($this, 'add_submenu_pages'));
		add_action('admin_enqueue_scripts', array($this, 'register_scripts'), 99);
	}

	public function add_submenu_pages()
    {
		add_submenu_page('woocommerce', __('Export Orders', 'woce'), __('Export Orders', 'woce') ,'manage_woocommerce', 'woce-export-orders', array($this, 'export_orders_page'));
		add_submenu_page('woocommerce', __('Export Customers', 'woce'), __('Export Customers', 'woce') ,'manage_woocommerce', 'woce-export-customers', array($this, 'export_customers_page'));
	}

	private function enqueue_scripts()
    {
        wp_enqueue_script('woce-js');
        wp_enqueue_script('woce-bootstrap');
        wp_enqueue_style('woce-css');
        wp_enqueue_style('woce-jquery-ui');
        wp_enqueue_style('woce-bootstrap');
        wp_enqueue_style('woce-jquery-data-table');
        wp_enqueue_style('woce-select2');
    }

	public function export_orders_page()
    {
        $this->enqueue_scripts();
		include_once('templates/export-orders-page.php');
	}

    public function export_customers_page()
    {
        $this->enqueue_scripts();
        include_once('templates/export-customers-page.php');
	}

    public function register_scripts()
    {
        wp_register_script('woce-js', plugin_dir_url(__file__) . 'assets/js/woce.js', array('jquery', 'woce-select2', 'woce-jquery-data-table', 'jquery-ui-datepicker'));
        wp_localize_script('woce-js', 'Woce', [
            'string' => [
                'datatable' => [
                    "processing"=>   __('Processing...', 'woce'),
                    "loadingRecords"=> __('Loading...', 'woce'),
                    "lengthMenu"=>   __('Showing _MENU_ records', 'woce'),
                    "zeroRecords"=>  __('No records found', 'woce'),
                    "info"=>         __('Showing _START_ to _END_ records, totally _TOTAL_ records', 'woce'),
                    "infoEmpty"=>    __('Showing 0 to 0 record, totally 0 record', 'woce'),
                    "infoFiltered"=> __('(filter from _MAX_ records)', 'woce'),
                    "infoPostFix"=>  "",
                    "search"=>       __('Search:', 'woce'),
                    "paginate"=> [
                        "first"=>    __('First page', 'woce'),
                        "previous"=> __('Last page', 'woce'),
                        "next"=>     __('Next page', 'woce'),
                        "last"=>     __('Final page', 'woce')
                    ],
                    "aria"=> [
                        "sortAscending"=>  __(': Ascending order', 'woce'),
                        "sortDescending"=> __(': Descending order', 'woce')
                    ]
                ]
            ]
        ]);
        wp_register_script('woce-select2', plugin_dir_url(__file__) . 'assets/js/select2.full.min.js', array('jquery'));
        wp_register_script('woce-jquery-data-table', plugin_dir_url(__file__) . 'assets/js/datatables.min.js', array('jquery'));
        wp_register_script('woce-bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js');
        wp_register_style('woce-css', plugin_dir_url(__file__) . 'assets/css/woce.css');
        wp_register_style('woce-select2', plugin_dir_url(__file__) . 'assets/css/select2.min.css');
        wp_register_style('woce-jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
        wp_register_style('woce-bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css');
        wp_register_style('woce-jquery-data-table', plugin_dir_url(__file__) . 'assets/css/datatables.min.css');
	}
}
$GLOBALS['Wc_Order_Customer_Exportation'] = Wc_Order_Customer_Exportation::get_instance();