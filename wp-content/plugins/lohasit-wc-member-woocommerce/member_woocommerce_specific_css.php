<style type="text/css">
  body {
  background-color: #f1f1f1;
}

h1 {
  padding: 20px 0px;
}

h3 {
  text-align: center;
  padding: 20px 0px;
}
tr.danny_display {
    display: none;
}
.danny_display_phone {
    display: none;
}
select {
  border: 1px solid #d1d1d1;
  padding: 5px 5px;
  background-color: transparent;
  width: 100%;
}

input {
  border: 1px solid #d1d1d1;
  padding: 2px 5px;
  background-color: transparent;
  width: 80%;
  text-align: center;
}

input:disabled {
  border: 0px;
}

div.ok {
  width: 80%;
  margin: 0 auto;
}

div.edit {
  display: none;
  position: fixed;
  top: 0px;
  opacity: 0.4;
  width: 100%;
  height: 100vh;
  background-color: #000;
}

div.editinfo {
  display: none;
  width: 50%;
  position: absolute;
  padding: 30px 0;
  top: 80px;
  left: 25%;
  border-radius: 5px;
  background-color: #fff;
}

div.ok table {
  margin: 0 auto;
  width: 100%;
  background-color: #fff;
  color: #333333;
}
div.ok table tr {
  border: 1px solid #e1e1e1;
}
div.ok table tr:nth-child(even) {
  background-color: #f9f9f9;
}
div.ok table th {
  font-size: 14px;
  padding: 20px;
}
div.ok table th {
  width: 15%;
  text-align: center;
}
div.ok table th:nth-child(1), div.ok table th:nth-child(6), div.ok table th:nth-child(7), div.ok table th:nth-child(8) {
  width: 10%;
  text-align: center;
}
div.ok table i.fa.fa-power-off.fa-2x.block {
  color: #9b9b9b;
  cursor: pointer;
  transition: 0.5s;
}
div.ok table i.fa.fa-power-off.fa-2x.gree {
  color: #58e61f;
  cursor: pointer;
  transition: 0.5s;
}
div.ok table i.fa.fa-pencil {
  cursor: pointer;
  color: #0073aa;
}
div.ok table i.fa.fa-cog.fa-2x.fa-fw {
  color: #333333;
  cursor: pointer;
}
div.ok table i.fa.fa-times-circle {
  cursor: pointer;
  color: #ff0000;
}
div.ok table tr.add i.fa.fa-times-circle, div.ok table tr.add i.fa.fa-pencil, div.ok table tr.add i.fa.fa-power-off.fa-2x {
  color: #999999;
  cursor: default;
}

div.editinfo table {
  margin: 0 auto;
  width: 70%;
  background-color: #fff;
  color: #333333;
}
div.editinfo table th {
  font-size: 14px;
  padding: 10px;
  text-align: right;
}
div.editinfo table th:nth-child(even) {
  width: 30%;
}
div.editinfo table th:nth-child(even) input {
  width: 100%;
  text-align: left;
}

a.btn-group.open.btn.btn-primary {
  margin: 20px 10px 0px 0px;
  color: #fff;
  cursor: pointer;
}

div.editinfophone {
  position: absolute;
  border-radius: 5px;
  top: 80px;
  left: 10%;
  background-color: #fff;
  width: 80%;
  margin: 0 auto;
  padding: 20px 0;
  display: none;
}
div.editinfophone table {
  margin: 10px auto;
  width: 70%;
}
div.editinfophone table th {
  width: 20%;
  text-align: right;
  padding: 5px 20px 5px 0;
}
div.editinfophone table th:nth-child(odd) {
  width: 8%;
}
div.editinfophone table th input {
  width: 100%;
  text-align: left;
}

div.phone {
  margin: 0 auto;
  display: none;
  width: 80%;
}
div.phone table {
  margin: 20px auto;
  width: 100%;
  background-color: #fff;
  color: #333333;
}
div.phone table th {
  font-size: 14px;
  padding: 20px;
  border: 1px solid #e1e1e1;
}
div.phone table th {
  width: 15%;
  text-align: center;
}
div.phone table th:nth-child(odd) {
  background-color: #f9f9f9;
  text-align: left;
  width: 10%;
}
div.phone table th:nth-child(6), div.phone table th:nth-child(7), div.phone table th:nth-child(8) {
  width: 10%;
  text-align: center;
}
div.phone table th input {
  text-align: left;
}
div.phone table i.fa.fa-power-off.fa-2x.block {
  color: #9b9b9b;
  cursor: pointer;
  transition: 0.5s;
}
div.phone table i.fa.fa-power-off.fa-2x.gree {
  color: #58e61f;
  cursor: pointer;
  transition: 0.5s;
}
div.phone table i.fa.fa-pencil {
  cursor: pointer;
  color: #0073aa;
}
div.phone table i.fa.fa-cog.fa-2x.fa-fw {
  color: #333333;
  cursor: pointer;
}
div.phone table i.fa.fa-times-circle {
  cursor: pointer;
  color: #ff0000;
}
div.phone table tr.add i.fa.fa-times-circle, div.phone table tr.add i.fa.fa-pencil, div.phone table tr.add i.fa.fa-power-off.fa-2x {
  color: #999999;
  cursor: default;
}

@media screen and (max-width: 980px) {
  div.ok {
    display: none;
  }

  div.phone {
    display: block;
  }
}

</style>