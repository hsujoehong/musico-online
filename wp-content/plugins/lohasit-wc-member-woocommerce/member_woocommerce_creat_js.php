
<script type="text/javascript">
	
	jQuery(document).ready( function ($){
    
    $('body').on('click',".btn-from-submit", function(){
        $("#from_member_up").find("input[name='role_duration[]']").each(function() {
            var value = $(this).val();
            if (value=="") {
                alert("角色期限為必填!");
                exit();
            }
        });
        /*$("#from_member_up").find("input[name^='year']").each(function() {

            if ($(this).attr('checked')==true ) {
                alert("1234");
                exit();
            }
        });*/

      $("tr :input").prop("disabled", false);
      $( "#from_member_up" ).submit();
    });
    $('body').on('click',".btn-from-submit-de", function(){
        $("#from_member_de").find("input[name='downgrade_role_duration[]']").each(function() {
            var value = $(this).val();
            if (value=="") {
                alert("角色期限為必填!");
                exit();
            }
        });
      $("tr :input").prop("disabled", false);
      $( "#from_member_de" ).submit();
    });
    $('body').on('click',".btn-from-submit-de-phone", function(){
        $("#from_member_de_phone").find("input[name='downgrade_role_duration[]']").each(function() {
            var value = $(this).val();
            if (value=="") {
                alert("角色期限為必填!");
                exit();
            }
        });
      $("tr :input").prop("disabled", false);
      $( "#from_member_de_phone" ).submit();
    });
    $('body').on('click',".btn-from-submit-phone", function(){
        $("#from_member_up_phone").find("input[name='role_duration[]']").each(function() {
            var value = $(this).val();
            if (value=="") {
                alert("角色期限為必填!");
                exit();
            }
        });
      $("tr :input").prop("disabled", false);
      $( "#from_member_up_phone" ).submit();
    });
    $('body').on('focus',".datepicker", function(){
        $(this).datepicker();
    });

     $(".addwebrow").click(function() {

            var intId = $(".show_hide_web1").length +1;
		        var fieldWrapper = $('<tr class="add"><th><div class="show_hide_web1"><select name="member[]"><?php global $wp_roles; ?><?php  foreach ( $wp_roles->roles as $key=>$value ):
                        $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                        if(!in_array($key, $key_danny_array))
                        { ?><option value="<?php echo $key; ?>"><?php echo $value['name']; ?></option><?php } endforeach; ?></select></div></th> <th><select name="upgrade[]" class="target"><option value="cumulative_full">累積消費滿</option> <option value="single_full">單筆消費滿</option></select></th> <th><input name="upgrade_number[]" placeholder="請輸入金額" class="money"> <label>元</label></th> <th><input name="start[]" placeholder="" class="datepicker"> <label>至</label> <input name="end[]" placeholder="" class="datepicker"></th> <th><select name="change_role[]"><?php global $wp_roles; ?> <?php  foreach ( $wp_roles->roles as $key=>$value ):
                        $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                        if(!in_array($key, $key_danny_array))
                        { ?><option value="<?php echo $key; ?>"><?php echo $value['name']; ?></option><?php } endforeach; ?></select></th> <th><input type="text" id="fif-input" name="role_duration[]" value=""> <input type="radio" name="year['+intId+']" value="year" class="radioch"><label>年</label> <input type="radio" name="year['+intId+']" value="month" class="radioch"><label>月</label></th> <th><i class="fa fa-pencil fa-2x"></i></th> <th><i class="fa fa-times-circle fa-2x"></i></th></tr>');
		        $("#buildyourformweb").append(fieldWrapper);
       
});
     $(".addwebrow2").click(function() {

            var intId1 = $(".show_hide_web2").length +1;
		        var fieldWrapper = $('<tr class="add"><th><div class="show_hide_web2"><select name="member_downgrade[]"><?php global $wp_roles; ?><?php  foreach ( $wp_roles->roles as $key=>$value ):
                        $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                        if(!in_array($key, $key_danny_array))
                        { ?><option value="<?php echo $key; ?>"><?php echo $value['name']; ?></option><?php } endforeach; ?></select></div></th> <th><select name="upgrade_downgrade[]" id="upgrade-<?php echo $loop_table_upgrade;?>"><option value="cumulative_full">累積消費滿</option><option value="single_full">單筆消費滿</option></select></th> <th><input type="text" name="downgrade_number[]" value=""  class="money" placeholder="請輸入金額"/><label>元</label></th> <th><select name="downgrade_change_role[]"><?php global $wp_roles; ?><?php  foreach ( $wp_roles->roles as $key=>$value ):
                        $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                        if(!in_array($key, $key_danny_array))
                        { ?><option value="<?php echo $key; ?>"><?php echo $value['name']; ?></option><?php } endforeach; ?></select></th> <th><input type="text" id="fif-input" name="downgrade_role_duration[]"><input type="radio" name="year_downgrade['+intId1+']" value="year"><label>年</label><input type="radio" name="year_downgrade['+intId1+']" value="month"><label>月</label></th> <th><i class="fa fa-pencil fa-2x"></i></th> <th><i class="fa fa-times-circle fa-2x"></i></th></tr>');
		        $("#buildyourformweb2").append(fieldWrapper);
       
});
    
$( "i.fa.fa-question-circle.fa-2.question-up" ).hover(
  function() {
    $("div.helpout").css("display","block");
  },
  
  function() {
    $("div.helpout").css("display","none");
  }
);
$( "i.fa.fa-question-circle.fa-2.question-degree" ).hover(
  function() {
    $("div.helpout-degree").css("display","block");
    $("i.fa.fa-question-circle.fa-2.question-degree").addClass("coo");
  },
  
  function() {
    $("div.helpout-degree").css("display","none");
    $("i.fa.fa-question-circle.fa-2.question-degree").removeClass("coo");
  }
);
$( "i.fa.fa-question-circle.fa-2.question-term" ).hover(
  function() {
    $("div.helpout-term").css("display","block");
    $("i.fa.fa-question-circle.fa-2.question-term").addClass("coo");
  },
  
  function() {
    $("div.helpout-term").css("display","none");
    $("i.fa.fa-question-circle.fa-2.question-term").removeClass("coo");
  }
);
    
$( "i.fa.fa-question-circle.fa-2.question-up-phone" ).click(
  function() {
    if($("div.helpoutphone").css("display")=="none")
      {
        $("div.helpoutphone").css("display","block");
      }
    else
    {
      $("div.helpoutphone").css("display","none");
    }
  }
);
$( "i.fa.fa-question-circle.fa-2.question-term-phone" ).click(
  function() {
    if($("div.helpout-term-phone").css("display")=="none")
      {
        $("div.helpout-term-phone").css("display","block");
        $("i.fa.fa-question-circle.fa-2.question-term-phone").addClass("coo");
      }
    else
    {
      $("div.helpout-term-phone").css("display","none");
      $("i.fa.fa-question-circle.fa-2.question-term-phone").removeClass("coo");
    }
  }
);
$( "i.fa.fa-question-circle.fa-2.question-term-phone-two" ).click(
  function() {
    if($("div.helpout-term-phone").css("display")=="none")
      {
        $("div.helpout-term-phone").css("display","block");
        $("i.fa.fa-question-circle.fa-2.question-term-phone-two").addClass("coo");
      }
    else
    {
      $("div.helpout-term-phone").css("display","none");
      $("i.fa.fa-question-circle.fa-2.question-term-phone-two").removeClass("coo");
    }
  }
);
     $(".addtablephone").click(function() {
            var intId3 = $(".show_hide_web1_phone").length +1;
		        var fieldWrapper = $('<div class="phone1" id="phone"><div><table><tr><th>目前會員角色</th><th> <div class="show_hide_web1_phone"><select name="member[]"><?php global $wp_roles; ?><?php  foreach ( $wp_roles->roles as $key=>$value ):
                        $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                        if(!in_array($key, $key_danny_array))
                        { ?><option value="<?php echo $key; ?>"><?php echo $value['name']; ?></option><?php } endforeach; ?></select></div></th></tr><tr><th>升級條件</th><th> <select class="target" name="upgrade[]"><option value="cumulative_full">累積消費滿</option><option value="single_full">單筆消費滿</option></select></th></tr><tr><th>金額</th><th><input name="upgrade_number[]" class="money" placeholder="請輸入金額"/><label>元</label></th></tr><tr><th>累積期限</th><th><input class="datepicker" name="start[]"  placeholder=""/><label>至</label><input class="datepicker" name="end[]" placeholder=""/></th></tr><tr><th>變換角色為</th><th><select name="change_role[]"><?php global $wp_roles; ?><?php  foreach ( $wp_roles->roles as $key=>$value ):
                        $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                        if(!in_array($key, $key_danny_array))
                        { ?><option value="<?php echo $key; ?>"><?php echo $value['name']; ?></option><?php } endforeach; ?></select></th></tr><tr> <th> 角色期限<i class="fa fa-question-circle fa-2 question-term-phone"></i></th><div class="helpout-term-phone"><div class="help-term-phone">角色期限是變換角色的期限。</div></div><th> <input type="text" id="fif-input" name="role_duration[]" value="<?php echo $sql_role_duration[$loop_table_upgrade];?>" ><input type="radio" name="year['+intId3+']" value="year" class="radioch"><label>年</label><input type="radio" name="year['+intId3+']" value="month" class="radioch"><label></div>');
		        $(".phone1").append(fieldWrapper);
       
});
     $(".addtablephone2").click(function() {
            var intId4 = $(".show_hide_web2_phone").length +1;
		        var fieldWrapper = $('<div class="phone2" id="phone"><div><table><tr><th>目前會員角色</th><th> <div class="show_hide_web2_phone"><select name="member_downgrade[]"><?php global $wp_roles; ?><?php  foreach ( $wp_roles->roles as $key=>$value ):
                        $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                        if(!in_array($key, $key_danny_array))
                        { ?><option value="<?php echo $key; ?>"><?php echo $value['name']; ?></option><?php } endforeach; ?></select></div></th></tr><tr><th>續角色條件</th><th> <select name="upgrade_downgrade[]" id="upgrade-<?php echo $loop_table_upgrade;?>"><option value="cumulative_full">累積消費滿</option><option value="single_full">單筆消費滿</option> </select></th></tr><tr><th>金額/次數</th><th> <input type="text" name="downgrade_number[]"  class="money"  placeholder="請輸入金額"/><label>元</label></th></tr><tr><th>未達續條件降級後等級</th><th> <select name="downgrade_change_role[]"><?php global $wp_roles; ?><?php  foreach ( $wp_roles->roles as $key=>$value ):
                        $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                        if(!in_array($key, $key_danny_array))
                        { ?><option value="<?php echo $key; ?>"<?php
                    if($sql_downgrade_change_role[$loop_table_upgrade]==$key) echo 'selected="selected"';?>><?php echo $value['name']; ?></option><?php } endforeach; ?></select></th></tr><tr><th> 角色期限<i class="fa fa-question-circle fa-2 question-term-phone"></i></th><div class="helpout-term-phone"><div class="help-term-phone-de">降級後會員等級維持時間。</div></div><th><input type="text" id="fif-input" name="downgrade_role_duration[]" value=""><input type="radio" name="year_downgrade['+intId4+']" value="year" class="radioch" ><label>年</label><input type="radio" name="year_downgrade['+intId4+']" value="month" class="radioch" ><label>月</label></th></tr></table></div></div>');
		        $(".phone2").append(fieldWrapper);
       
});
      $(".fa-spin-web").click(function() {
        $(".editinfo").css("display","block");
        $(".edit").css("display","block");
      });
      $(".fa-spin-phone").click(function() {
        $(".editinfophone").css("display","block");
        $(".edit").css("display","block");
      });
      $(".btn-primary").click(function() {
        $(".editinfophone").css("display","none");
        $(".editinfo").css("display","none");
        $(".edit").css("display","none");
      });

        $(".fa-pencil").click(function() {
            $(this).parents("tr").find("input").css("border-style","solid");
            $(this).parents("tr").find("input").prop("disabled", false);
            $(this).parents("tr").find("select").prop("disabled", false);
        });
        $(".fa-pencil-phone_table-edit").click(function() {
            $(this).parents("table").find("input").css("border-style","solid");
            $(this).parents("table").find("input").prop("disabled", false);
            $(this).parents("table").find("select").prop("disabled", false);
        });
    
      $("div.ok :input").prop("disabled", true);
      $("tr.add :input").prop("disabled", false);
        $("div.phone_table :input").prop("disabled", true);


	});
var vm = new Vue({
  el: "#app",
  data: {
    roomdatas:[
      {
      "id" : 5,
      "turn_off" : true,
      "delimoth" : "黑貓宅急便",
      "shopping_method_name" : "黑貓宅急便",
      "fright" : 60,
      "free_fright": 1000
      },
      {
      "id" : 2,
      "turn_off" : true,
      "delimoth" : "黑貓宅急",
      "shopping_method_name" : "黑宅急便",
      "fright" : 60,
      "free_fright": 1000
      },
    ]
  },methods: {
    delete_room: function(){
      alert(this.room.turn_off)
        
    }
  }
  
});
</script>