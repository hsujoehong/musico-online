<?
	global $wpdb;
		$sql = "SELECT * FROM wp_vip_downgrade  ";
		$result = $wpdb->get_results($sql);
		$downgrade_number=0;
		$member_downgrade="";
		$downgrade_change_role="";
		foreach( $result as $test ) {
				$member_downgrade=$test->member_downgrade;
				$downgrade_change_role=$test->downgrade_change_role;
				$downgrade_role_duration=$test->downgrade_role_duration;
				$upgrade_downgrade=$test->upgrade_downgrade;
				
				$downgrade_number=$test->downgrade_number;
				/*sql 撈出限制 */
				
					$user = get_userdatabylogin($username);						
					$user_id=$user->ID;
				
				$user_info = get_userdata($user_id);	
				$user_login=$user_info->user_login;
				$capabilities=implode(', ', $user_info->roles);
				
				/*累積消費*/
			$customer_orders = get_posts( array(
				'numberposts' => - 1,
				'meta_key'    => '_customer_user',
				'meta_value'  => $user_id,
				'post_type'   => array( 'shop_order' ),
				'post_status' => array( 'wc-completed' )
			) );

			$add_total = 0;
			$current_time=current_time('Y-m-d' );
			$freq=0;
			foreach ( $customer_orders as $customer_order ) {
					$add_order = wc_get_order( $customer_order );
					$freq++;
					$order_date =$add_order->order_date;
					$add_total+=$add_order->get_total();
				
					

		}
	
	
		
		$user_lastdate = get_user_meta( $user_id, 'last_date', true ); 
		
		
		
		/*累積消費_角色降級*/	
		if($add_total<$downgrade_number && $capabilities==$member_downgrade  &&  strtotime($current_time)>strtotime($user_lastdate) && $upgrade_downgrade=='cumulative_full'){
			
			$u = new WP_User( $user_id );			
			$u->remove_role( $member_downgrade );
			$u->add_role($downgrade_change_role);
			
			$first_date=current_time('Y-m-d' );
			$first_dates = strtotime($first_date);
			
			$time='+'.$downgrade_role_duration.'month';
			$last_date  =date('Y-m-d', strtotime($time, $first_dates));
			
			 update_user_meta( $user_id , 'first_date', $first_date );
			 update_user_meta( $user_id , 'last_date', $last_date);
		}	
		
	/*單筆次數_角色降級*/		
		 if($freq<$downgrade_number && $capabilities==$member_downgrade  &&  strtotime($current_time)>strtotime($user_lastdate) && $upgrade_downgrade=='single_full'){
			
			$u = new WP_User( $user_id );			
			$u->remove_role( $member_downgrade );
			$u->add_role($downgrade_change_role);
			
			$first_date=current_time('Y-m-d' );
			$first_dates = strtotime($first_date);
			
			$time='+'.$downgrade_role_duration.'month';
			$last_date  =date('Y-m-d', strtotime($time, $first_dates));
			update_user_meta( $user_id , 'first_date', $first_date);
			update_user_meta( $user_id , 'last_date', $last_date );
		}	


	}
?>