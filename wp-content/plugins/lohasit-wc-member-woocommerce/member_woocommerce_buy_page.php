
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css'>

      <link rel="stylesheet" href="css/style.css">
  
<div id="app">
  <div class="ok">
      <h1 class="h1">買A送B<i class="fa fa-question-circle fa-2"></i></h1>

    <form method="post" action="admin.php?page=member_customer_woocommerce_buy_page_option" id="from_member_de">
    <table id="buildyourformweb">
      <div class="helpout">
        <div class="help">搜尋貨品ID(可至商品列找到)，不能設定商品數量，故只適用買A送B活動，如要買多件 贈送某商品時，應到商品區新增欲作活動之商品。<br>例如貨品ID 199要買三送一， 需到商品區新增商品，將ID199的商品數量 跟價錢X3輸入至新增商品內，此時此新增商 品將有自己的ID名稱，將其輸入至搜尋框內即可。<br>Note：如有庫存管理之疑問，應屬額外客製功能，此版本不提供此服務。<br>贈送之商品可當作一般商品販賣，如贈品不 想販賣時，可將其隱藏。(商品編輯內可設定)</div>
      </div>
      <tr>
        <th>排序</th>
        <th>購買</th>
        <th>送</th>
        <th>編輯</th>
        <th>刪除</th>
      </tr>
          <?php 
          $total_number=sizeof($sql_id);
            for($loop_table_upgrade=0;$loop_table_upgrade<sizeof($sql_id);$loop_table_upgrade++)
            {
          ?>
      <tr>
        <th> <i class="fa fa-bars fa-2x block" @click="chang_color()"></i></th>
        <th> 
          <input name="buy[]" value="<?php echo $sql_buy[$loop_table_upgrade];?>" class="tags"  />

        </th>
        <th> 
          <input  name="give[]" value="<?php echo $sql_give[$loop_table_upgrade];?>" class="tags"  />
          
        </th>
        <th><i class="fa fa-pencil fa-2x"></i></th>
        <th><a href="admin.php?page=member_customer_woocommerce_buy_page_option&id=<?php echo $sql_id[$loop_table_upgrade];?>&action=del" ><i class="fa fa-times-circle fa-2x"></i></a></th>
      </tr>
      <?php 
      }?>
      <tr class="add">
        <th> <i class="fa fa-bars fa-2x block"></i></th>
        <th>
          <input  name="buy[]" placeholder="輸入商品ID或商品名稱" class="tags"  />
        </th>
        <th>
          <input name="give[]" placeholder="輸入商品ID或商品名稱" class="tags" />
        </th>
        <th><i class="fa fa-pencil fa-2x"></i></th>
        <th><i class="fa fa-times-circle fa-2x"></i></th>
      </tr>
    </table>
  </div>
    <div style="
    text-align: center;
"> <input type="checkbox" value="true" name="check_true_danny" <?php if($check_true_danny)echo "checked";?>>不啟用此按鈕，贈送的商品消費者可自行選擇數量</div>
    <div class="ok"><a class="btn-group open btn btn-primary addwebrow">新增條件設定</a><a class="btn-group open btn btn-primary btn-from-submit-de">儲存變更</a></div>
  </form>
  <div class="phone">
    <form method="post" action="admin.php?page=member_customer_woocommerce_buy_page_option"  id="from_member_de_phone">
    <div id="phone">
      <h1>買A送B <i class="fa fa-question-circle fa-2"></i></h1>
      <div class="helpoutphone">
        <div class="help">搜尋貨品ID(可至商品列找到)，不能設定商品數量，故只適用買A送B活動，如要買多件 贈送某商品時，應到商品區新增欲作活動之商品。<br>例如貨品ID 199要買三送一， 需到商品區新增商品，將ID199的商品數量 跟價錢X3輸入至新增商品內，此時此新增商 品將有自己的ID名稱，將其輸入至搜尋框內即可。<br>Note：如有庫存管理之疑問，應屬額外客製功能，此版本不提供此服務。<br>贈送之商品可當作一般商品販賣，如贈品不 想販賣時，可將其隱藏。(商品編輯內可設定)</div>
      </div>
          <?php 
          $total_number=sizeof($sql_id);
            for($loop_table_upgrade=0;$loop_table_upgrade<sizeof($sql_id);$loop_table_upgrade++)
            {
          ?>
      <div class="phone_edit">
        <table>
          <tr>
            <th>購買</th>
            <th> 
          <input name="buy[]" value="<?php echo $sql_buy[$loop_table_upgrade];?>" class="tags" />
            </th>
          </tr>
          <tr>
            <th>送</th>
            <th> 
          <input  name="give[]" value="<?php echo $sql_give[$loop_table_upgrade];?>" class="tags" />
            </th>
          </tr>
          <tr>
            <th>編輯</th>
            <th><i class="fa fa-pencil fa-2x fa-pencil-edit"></i></th>
          </tr>
          <tr>
            <th>刪除</th>
            <th><a href="admin.php?page=member_customer_woocommerce_buy_page_option&id=<?php echo $sql_id[$loop_table_upgrade];?>&action=del" ><i class="fa fa-times-circle fa-2x"></i></a></th>
          </tr>
        </table>
      </div>
      <?php 
      }?>
      <table> 
        <tr class="add">
          <th>購買</th>
          <th><input name="buy[]" placeholder="輸入商品ID或商品名稱" class="tags" />
          </th>
        </tr>
        <tr class="add">
          <th>送</th>
          <th>
            <input name="give[]" placeholder="輸入商品ID或商品名稱" class="tags" />

          </th>
        </tr>
      </table>
        <div style="
    text-align: center;
"> <input type="checkbox" value="true" name="check_true_danny" <?php if($check_true_danny)echo "checked";?>>不啟用此按鈕，贈送的商品消費者可自行選擇數量</div>
    </div><a class="btn-group open btn btn-primary addtablephone">新增條件設定</a><a class="btn-group open btn btn-primary btn-from-submit-de-phone">儲存變更</a>
    </form>
  </div>
  <div class="edit"> </div>
  <div class="editinfo">
    <h3>黑貓</h3>
    <table>
      <tr>
        <th>運送國家</th>
        <th>
          <select>
            <option>請選擇運送區域</option>
          </select>
        </th>
        <th>特定國家</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>商店所在縣市</th>
        <th>
          <input/>
        </th>
        <th>商店代號</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>HushKey</th>
        <th>
          <input/>
        </th>
        <th>HushIV</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>寄件人*</th>
        <th>
          <input/>
        </th>
        <th>寄件人手機*</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>寄件人郵遞區號*</th>
        <th>
          <input/>
        </th>
        <th>寄件人地址*</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th> </th>
        <th></th>
        <th> </th>
        <th></th>
      </tr>
      <tr>
        <th> </th>
        <th><a class="btn-group open btn btn-primary">更新</a></th>
        <th> <a class="btn-group open btn btn-primary">取消</a></th>
        <th></th>
      </tr>
      <tr>
        <th> </th>
        <th></th>
        <th> </th>
        <th></th>
      </tr>
    </table>
  </div>
  <div class="editinfophone">
    <table>
      <h3>黑貓</h3>
      <tr>
        <th>運送國家</th>
        <th>
          <select>
            <option>請選擇運送區域</option>
          </select>
        </th>
      </tr>
      <tr>
        <th>特定國家</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>商店所在縣市</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>商店代號</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>HushKey</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>HushIV</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>寄件人*</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>寄件人手機*</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>寄件人郵遞區號*</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th>寄件人地址*</th>
        <th>
          <input/>
        </th>
      </tr>
      <tr>
        <th> </th>
        <th></th>
      </tr>
      <tr>
        <th><a class="btn-group open btn btn-primary">更新</a></th>
        <th> <a class="btn-group open btn btn-primary">取消</a></th>
      </tr>
      <tr>
        <th> </th>
        <th></th>
      </tr>
    </table>
  </div>
</div>