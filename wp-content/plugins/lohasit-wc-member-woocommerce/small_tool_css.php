<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2017/5/5
 * Time: 下午 02:52
 */
?>
<style>
    .woocommerce-account .woocommerce-MyAccount-content {
        margin-top: -150px;
    }
    .vip_member{
        width: 60%;
        min-width: 200px;
        position: relative;
        float: left;
    }
    .member_info{
        width: 7vw;
        height: 7vw;
        min-width: 100px;
        min-height: 100px;
        overflow: hidden;
        float: left;
        border-radius: 50%;
        border: 2px solid #52575f;
    }
    @keyframes imgfadein{
        0%{opacity: 0;}
        100%{opacity: 1;}
    }
    .member_img{
        width: 100%;
        height: 100%;
        background-color: #faa;
        border-radius: 50%;
        animation: imgfadein 2.5s ease;
    }
    .member_center_tool{
        width: 70%;
        font-family: "微軟正黑體";
        min-width: 150px;
        right: 0;
        position: absolute;
        /*outline: 1px solid #afa;*/
    }
    .member_center_tool h5{
        font-size: 28px;
        margin: 0 0 10px 0;
        font-weight: 500;
    }
    .member_center_tool h6{
        font-size: 18px;
        line-height: 24px;
        color: #52575f;
        margin: 0;
        font-weight: 500;
    }
    .member_center_tool p{
        font-size: 1em;
        color: #52575f;
    }
    .member_center_vipvalue{
        width: 90%;
        height: 20px;
        border: 1px solid #52575f;
        margin: 10px 0 40px 0;
        position: relative;
        color: #52575f;
    }
    .member_center_vipvalue span{
        position: absolute;
        top: -25px;
        right:-20px;
    }
    .vipvalue_percent{
        height: 100%;
        width: 0%;
        background-color: #52575f;
        position: relative;
        transition: 1.5s ease-in-out;
    }
    #expense{
        position: absolute;
        right: -18px;
        bottom: -25px;
    }
    @media only screen and (max-width:1326px){
        .vip_member{
            width: 20%;
            float: left;
        }
        .woocommerce-account .woocommerce-MyAccount-content {
            margin-top: 400px;
        }
        .member_info{
            position: absolute;
            left: 0;
            right: 0;
            margin: auto;
        }
        .member_center_tool{
            bottom: -340px;
            right: 0;
            width: 100%;
        }
    }



    .vip_member {
        width: 60%;
        min-width: 200px;
        position: relative;
        float: left;
    }

    .member_center_tool {
        margin-left: 100px;
        float: left;
        width: 70%;
        font-family: "微軟正黑體";
        min-width: 150px;
        right: 0;
        position: relative;
    }
    .woocommerce-account .woocommerce-MyAccount-navigation {
        float: left;
        width: 30%;
    }

    .member_center_tool {
        bottom: inherit;
        right: 0;
        width: 70%;
        float: left;
        margin-left: 0px;
    }
    .member_info {

        position: relative;
        left: 0;
        right: 0;
        margin: auto;
        float: left;
    }
    .woocommerce-account .woocommerce-MyAccount-content {
        margin-top: 0px;
    }
</style>
