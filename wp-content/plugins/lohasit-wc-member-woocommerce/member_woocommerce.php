<?php 
/*
 * Plugin Name: Lohasit WC Member Woocommerce
 * Description: 樂活壓板系統 - Woocommerce - 自訂Woocommerce 優惠設定的頁面，方便客戶操作
 * Version: 1.0.0
 * Author URI: https://www.lohaslife.cc/
 * Author: Lohas IT
*/
// Add the code below to your theme's functions.php file to add a confirm password field on the register form under My Accounts.

add_action('admin_menu', 'member_woocommerce_option_menu_page');


function member_woocommerce_option_menu_page(){

	add_menu_page( '優惠設定', '優惠設定', 'manage_options', 'member_customer_woocommerce_page_option', 'member_customer_creat_page', 'dashicons-cart', 50 );
	add_submenu_page( 'member_customer_woocommerce_page_option', '會員升級', '會員升級', 'manage_options', 'member_customer_woocommerce_page_option', 'member_customer_creat_page' );
	add_submenu_page( 'member_customer_woocommerce_page_option', '買A送B', '買A送B', 'manage_options', 'member_customer_woocommerce_buy_page_option', 'member_customer_buy_page' );
	add_submenu_page( 'custom_customer_woocommerce_page_option', '自訂義物流運費', '自訂義物流運費', 'manage_options', 'member_customer_woocommerce_specific_page_option', 'member_customer_specific_page' );
}


function member_woocommerce_order_status_completed($order_id) {
    global $wpdb;
    $sql = "SELECT * FROM wp_vip_upgrade";
    $result = $wpdb->get_results($sql);
    $upgrade_number=0;
    $member="";
    $change_role="";
    foreach( $result as $test ) {
        $member=$test->member;
        $change_role=$test->change_role;
        $start=$test->start;
        $end=$test->ends;
        $upgrade=$test->upgrade;
        $upgrade_number=$test->upgrade_number;
        $role_duration=$test->role_duration;
        $year=$test->year;
        /*sql 撈出限制 */

        $order = new WC_Order($order_id);
        $order_date = $order->order_date;
        $user_id = $order->user_id;
        $total=$order->get_total();
        $user_info = get_userdata($user_id);
        $user_login=$user_info->user_login;
        $capabilities=implode(', ', (array)$user_info->roles);
        /*累積消費*/
        $customer_orders = get_posts( array(
            'numberposts' => - 1,
            'meta_key'    => '_customer_user',
            'meta_value'  => $user_id,
            'post_type'   => array( 'shop_order' ),
            'post_status' => array( 'wc-completed' )
        ) );

        $add_total = 0;
        $current_time=current_time('Y-m-d' );
        foreach ( $customer_orders as $customer_order ) {
            $add_order = wc_get_order( $customer_order );

            $order_date =$add_order->order_date;
            $add_total+=$add_order->get_total();



        }

        /*累積消費_角色升級*/
        if($add_total>$upgrade_number && $capabilities==$member  && strtotime($end)>strtotime($current_time) && strtotime($current_time)>strtotime($start) && $upgrade=='cumulative_full'){

            $u = new WP_User( $user_id );
            $u->remove_role( $capabilities );
            $u->add_role($change_role);
            $first_date=current_time('Y-m-d' );
            $first_dates = strtotime($first_date);

            $time='+'.$role_duration.'month';
            $last_date  =date('Y-m-d', strtotime($time, $first_dates));
            add_user_meta( $user_id , 'first_date', $first_date, true );
            add_user_meta( $user_id , 'last_date', $last_date, true );
        }
        /*單筆消費_角色升級*//*ymlin_fixed_20171024  取消時間活動限制*/
        if($total>$upgrade_number && $capabilities==$member  && $upgrade=='single_full'){


            $u = new WP_User( $user_id );
            $u->remove_role( $capabilities );
            $u->add_role($change_role);
            $first_date=current_time('Y-m-d' );
            $first_dates = strtotime($first_date);
            $tmp=$role_duration;
            if($year=='month'){
                $time='+'.$tmp.'month';
            }
            else if($year=='year'){
                $time='+'.$tmp.'year';

            }
            $last_date  =date('Y-m-d', strtotime($time, $first_dates));
            add_user_meta( $user_id , 'first_date', $first_date, true );
            add_user_meta( $user_id , 'last_date', $last_date, true );

        }



    }
}
add_action( 'woocommerce_order_status_completed', 'member_woocommerce_order_status_completed' );

function check_custom_authentications ( $username ) {
    global $wpdb;
    $sql = "SELECT * FROM wp_vip_downgrade  ";
    $result = $wpdb->get_results($sql);
    $downgrade_number=0;
    $member_downgrade="";
    $downgrade_change_role="";
    foreach( $result as $test ) {
        $member_downgrade=$test->member_downgrade;
        $downgrade_change_role=$test->downgrade_change_role;
        $downgrade_role_duration=$test->downgrade_role_duration;
        $upgrade_downgrade=$test->upgrade_downgrade;

        $downgrade_number=$test->downgrade_number;
        /*sql 撈出限制 */

        $user = get_userdatabylogin($username);
        $user_id=$user->ID;

        $user_info = get_userdata($user_id);
        //$user_login=$user_info->user_login;
   
        $capabilities=implode(', ', (array)$user_info->roles)?implode(', ', (array)$user_info->roles):"";
        /*累積消費*/
        $customer_orders = get_posts( array(
            'numberposts' => - 1,
            'meta_key'    => '_customer_user',
            'meta_value'  => $user_id,
            'post_type'   => array( 'shop_order' ),
            'post_status' => array( 'wc-completed' )
        ) );

        $add_total = 0;
        $current_time=current_time('Y-m-d' );
        $freq=0;
        foreach ( $customer_orders as $customer_order ) {
            $add_order = wc_get_order( $customer_order );
            $freq++;
            $order_date =$add_order->order_date;
            $add_total+=$add_order->get_total();



        }



        $user_lastdate = get_user_meta( $user_id, 'last_date', true );



        /*累積消費_角色降級*/
        if($add_total<$downgrade_number && $capabilities==$member_downgrade  &&  strtotime($current_time)>strtotime($user_lastdate) && $upgrade_downgrade=='cumulative_full'){

            $u = new WP_User( $user_id );
            $u->remove_role( $member_downgrade );
            $u->add_role($downgrade_change_role);

            $first_date=current_time('Y-m-d' );
            $first_dates = strtotime($first_date);

            $time='+'.$downgrade_role_duration.'month';
            $last_date  =date('Y-m-d', strtotime($time, $first_dates));

            update_user_meta( $user_id , 'first_date', $first_date );
            update_user_meta( $user_id , 'last_date', $last_date);
        }

        /*單筆次數_角色降級*/
        if($freq<$downgrade_number && $capabilities==$member_downgrade  &&  strtotime($current_time)>strtotime($user_lastdate) && $upgrade_downgrade=='single_full'){

            $u = new WP_User( $user_id );
            $u->remove_role( $member_downgrade );
            $u->add_role($downgrade_change_role);

            $first_date=current_time('Y-m-d' );
            $first_dates = strtotime($first_date);

            $time='+'.$downgrade_role_duration.'month';
            $last_date  =date('Y-m-d', strtotime($time, $first_dates));
            update_user_meta( $user_id , 'first_date', $first_date);
            update_user_meta( $user_id , 'last_date', $last_date );
        }


    }
}
add_action( 'wp_authenticate' , 'check_custom_authentications' );

function member_customer_creat_page(){

    include('member_woocommerce_creat_head.php');
    include('member_woocommerce_creat_css.php');
	include('member_woocommerce_creat_page.php');
    include('member_woocommerce_creat_js.php');
	?>
<?php }
function member_customer_buy_page(){
	include('member_woocommerce_buy_head.php');
    include('member_woocommerce_buy_css.php');
    include('member_woocommerce_buy_js.php');
    include('member_woocommerce_buy_page.php');
}
function member_customer_specific_page(){
	include('member_woocommerce_specific_head.php');
    include('member_woocommerce_specific_css.php');
	include('member_woocommerce_specific_page.php');
    include('member_woocommerce_specific_js.php');
}


?>