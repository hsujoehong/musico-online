<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css'>
<link rel='stylesheet prefetch' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'>

<div id="app">
    <div class="ok">
        <h1 class="h1">會員升級設定<i class="fa fa-question-circle fa-2 question-up"></i></h1>
        <form method="post" action="admin.php?page=member_customer_woocommerce_page_option" id="from_member_up">
            <table id="buildyourformweb">
                <div class="helpout">
                    <div class="help">最多只能新增20個欄位!如有三個以上的會員等級，每個會員等級都需要設定。<Br>例如：普通會員升級成VIP或是VVIP有兩種方式，因系統無法同時判定“單筆消費滿”與“累積消費滿，故每個會員升級條件都需要個別設定。會員升級日起算會員腳色維持期限。<Br>例如2017/04/05會員升級條件符合，期限 設定一年，2018/04/06年會員升級失效。</div>
                </div>
                <tr>
                    <th>目前會員角色</th>
                    <th>升級條件</th>
                    <th>金額</th>
                    <th>累積期限</th>
                    <th>變換角色為</th>
                    <th>角色期限<i class="fa fa-question-circle fa-2 question-term"></i></th>
                    <div class="helpout-term">
                        <div class="help-term">角色期限是變換角色的期限。</div>
                    </div>
                    <th>編輯</th>
                    <th>刪除</th>
                </tr>
                <?php
                $total_number=sizeof($sql_member);
                for($loop_table_upgrade=0;$loop_table_upgrade<sizeof($sql_member);$loop_table_upgrade++)
                {
                    ?>
                    <tr>
                        <th> <div class="show_hide_web1">
                                <select name="member[]">
                                    <?php global $wp_roles; ?>
                                    <?php foreach ( $wp_roles->roles as $key=>$value ):
                                        $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                                        if(!in_array($key, $key_danny_array))
                                        {
                                            ?>
                                            <option value="<?php echo $key; ?>"
                                                <?php
                                                if($sql_member[$loop_table_upgrade]==$key) echo 'selected="selected"';
                                                ?>
                                            ><?php echo $value['name']; ?></option>
                                        <?php } endforeach; ?>
                                </select>
                            </div>
                        </th>
                        <th>
                            <select class="target" name="upgrade[]" id="upgrade-<?php echo $loop_table_upgrade;?>">
                                <option value="cumulative_full">累積消費滿</option>
                                <option value="single_full"
                                    <?php
                                    if($sql_upgrade[$loop_table_upgrade]=="single_full") echo 'selected="selected"';?>
                                >單筆消費滿</option>
                            </select>
                        </th>
                        <th>
                            <input type="text" name="upgrade_number[]" placeholder="請輸入金額" value="<?php echo $sql_upgrade_number[$loop_table_upgrade];?>" class="money" />
                            <label>元</label>
                        </th>
                        <th>
                            <input type="text" name="start[]" value="<?php echo $sql_start[$loop_table_upgrade];?>" class="fif-input datepicker">

                            <label>至</label>
                            <input type="text" value="<?php echo $sql_end[$loop_table_upgrade];?>" name="end[]" class="fif-input datepicker">
                        </th>
                        <th>
                            <select name="change_role[]">
                                <?php global $wp_roles; ?>
                                <?php foreach ( $wp_roles->roles as $key=>$value ):
                                    $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                                    if(!in_array($key, $key_danny_array))
                                    {
                                        ?>
                                        <option value="<?php echo $key; ?>"
                                            <?php
                                            if($sql_change_role[$loop_table_upgrade]==$key) echo 'selected="selected"';
                                            ?>
                                        ><?php echo $value['name']; ?></option>
                                    <?php } endforeach; ?>
                            </select>
                        </th>
                        <th>

                            <input type="text" id="fif-input" name="role_duration[]" value="<?php echo $sql_role_duration[$loop_table_upgrade];?>" required="required" >
                            <input type="radio" name="year[<?php echo $loop_table_upgrade;?>]" value="year"
                                <?php
                                if($sql_year_upgrade[$loop_table_upgrade]=="year") echo 'checked="checked"';
                                ?> class="radioch"
                            ><label>年</label>
                            <input type="radio" name="year[<?php echo $loop_table_upgrade;?>]" value="month"
                                <?php
                                if($sql_year_upgrade[$loop_table_upgrade]=="month") echo 'checked="checked"';
                                ?> class="radioch"
                            ><label>月</label>
                        </th>
                        <th><i class="fa fa-pencil fa-2x"></i></th>
                        <th><a href="admin.php?page=member_customer_woocommerce_page_option&id=<?php echo $sql_id[$loop_table_upgrade];?>&action=del" ><i class="fa fa-times-circle fa-2x"></i></a></th>
                    </tr>
                    <?php
                }
                ?>

            </table>
    </div>
    <div class="ok"><a class="btn-group open btn btn-primary addwebrow">新增條件設定</a><a class="btn-group open btn btn-primary btn-from-submit">儲存變更</a></div>
    </form>
    <!-- 
    <div class="ok">
        <h1 class="h1">會員降級設定</h1>

        <form method="post" id="from_member_de">
            <table id="buildyourformweb2">
                <div class="helpout"></div>
                <tr>
                    <th>目前會員角色</th>
                    <th>續角色條件</th>
                    <th>金額/次數</th>
                    <th>未達續條件降級後等級</th>
                    <th>角色期限<i class="fa fa-question-circle fa-2 question-degree"></i></th>
                    <div class="helpout-degree">
                        <div class="help-term-degree">降級後會員等級維持時間。</div>
                    </div>
                    <th>編輯</th>
                    <th>刪除</th>
                </tr>
                <?php

                for($loop_table_upgrade=0;$loop_table_upgrade<sizeof($sql_member_downgrade);$loop_table_upgrade++)
                {
                    ?>
                    <tr>
                        <th>
                            <div class="show_hide_web2">
                                <select name="member_downgrade[]">
                                    <?php global $wp_roles; ?>
                                    <?php foreach ( $wp_roles->roles as $key=>$value ):
                                        $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                                        if(!in_array($key, $key_danny_array))
                                        {
                                            ?>
                                            <option value="<?php echo $key; ?>"
                                                <?php
                                                if($sql_member_downgrade[$loop_table_upgrade]==$key) echo 'selected="selected"';?>
                                            ><?php echo $value['name']; ?></option>
                                        <?php } endforeach; ?>
                                </select>
                            </div>
                        </th>
                        <th>
                            <select name="upgrade_downgrade[]" id="upgrade-<?php echo $loop_table_upgrade;?>">
                                <option value="cumulative_full">累積消費滿</option>
                                <option value="single_full"
                                    <?php
                                    if($sql_upgrade_downgrade[$loop_table_upgrade]=="single_full") echo 'selected="selected"';?>
                                >單筆消費滿</option>
                            </select>
                        </th>
                        <th>
                            <input type="text" name="downgrade_number[]" value="<?php echo $sql_downgrade_number[$loop_table_upgrade]; ?>" class="money"  placeholder="請輸入金額"/>
                            <label>元</label>
                        </th>
                        <th>
                            <select name="downgrade_change_role[]">
                                <?php global $wp_roles; ?>
                                <?php foreach ( $wp_roles->roles as $key=>$value ):
                                    $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                                    if(!in_array($key, $key_danny_array))
                                    {
                                        ?>
                                        <option value="<?php echo $key; ?>"
                                            <?php
                                            if($sql_downgrade_change_role[$loop_table_upgrade]==$key) echo 'selected="selected"';?>
                                        ><?php echo $value['name']; ?></option>
                                    <?php } endforeach; ?>
                            </select>
                        </th>
                        <th>
                            <input type="text" id="fif-input" name="downgrade_role_duration[]" value="<?php echo $sql_downgrade_role_duration[$loop_table_upgrade];?>">
                            <input type="radio" name="year_downgrade[<?php echo $loop_table_upgrade;?>]" value="year" class="radioch"
                                <?php
                                if($sql_year_downgrade[$loop_table_upgrade]=="year") echo 'checked="checked"';
                                ?>
                            >
                            <label>年</label>
                            <input type="radio" name="year_downgrade[<?php echo $loop_table_upgrade;?>]" value="month" class="radioch"
                                <?php
                                if($sql_year_downgrade[$loop_table_upgrade]=="month") echo 'checked="checked"';
                                ?>
                            >
                            <label>月</label>
                        </th>
                        <th><i class="fa fa-pencil fa-2x"></i></th>
                        <th><a href="admin.php?page=member_customer_woocommerce_page_option&id=<?php echo $sql_id_downgrade[$loop_table_upgrade];?>&action=del1"><i class="fa fa-times-circle fa-2x"></i></a></th>
                    </tr>
                    <?php
                }
                ?>

            </table>
    </div>
    <div class="ok"><a class="btn-group open btn btn-primary addwebrow2">新增條件設定</a><a class="btn-group open btn btn-primary btn-from-submit-de">儲存變更</a></div>
    -->
    </form>
    <div class="phone">
        <div id="phone">
            <form method="post" action="admin.php?page=member_customer_woocommerce_page_option" id="from_member_up_phone">
                <h1>會員升級設定 <i class="fa fa-question-circle fa-2 question-up-phone"></i>
                    <div class="helpoutphone">
                        <div class="helpphone">最多只能新增20個欄位!如有三個以上的會員等級，每個會員等級都需要設定。<Br>例如：普通會員升級成VIP或是VVIP有兩種方式，因系統無法同時判定“單筆消費滿”與“累積消費滿，故每個會員升級條件都需要個別設定。會員升級日起算會員腳色維持期限。<Br>例如2017/04/05會員升級條件符合，期限 設定一年，2018/04/06年會員升級失效。</div>
                    </div>
                </h1>
                <div>
                    <?php
                    $total_number=sizeof($sql_member);
                    for($loop_table_upgrade=0;$loop_table_upgrade<sizeof($sql_member);$loop_table_upgrade++)
                    {
                        ?>
                        <div class="phone_table">
                            <table>
                                <tr>
                                    <th>目前會員角色</th>
                                    <th> <div class="show_hide_web1_phone">
                                            <select name="member[]">
                                                <?php global $wp_roles; ?>
                                                <?php foreach ( $wp_roles->roles as $key=>$value ):
                                                    $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                                                    if(!in_array($key, $key_danny_array))
                                                    {
                                                        ?>
                                                        <option value="<?php echo $key; ?>"
                                                            <?php
                                                            if($sql_member[$loop_table_upgrade]==$key) echo 'selected="selected"';
                                                            ?>
                                                        ><?php echo $value['name']; ?></option>
                                                    <?php } endforeach; ?>
                                            </select></div>
                                    </th>
                                </tr>
                                <tr>
                                    <th>升級條件</th>
                                    <th>
                                        <select class="target" name="upgrade[]" id="upgrade-<?php echo $loop_table_upgrade;?>">
                                            <option value="cumulative_full">累積消費滿</option>
                                            <option value="single_full"
                                                <?php
                                                if($sql_upgrade[$loop_table_upgrade]=="single_full") echo 'selected="selected"';?>
                                            >單筆消費滿</option>
                                        </select>
                                    </th>
                                </tr>
                                <tr>
                                    <th>金額</th>
                                    <th>
                                        <input type="text" name="upgrade_number[]" placeholder="請輸入金額" value="<?php echo $sql_upgrade_number[$loop_table_upgrade];?>" class="money" />
                                        <label>元</label>
                                    </th>
                                </tr>
                                <tr>
                                    <th>累積期限</th>
                                    <th>
                                        <input type="text" name="start[]" value="<?php echo $sql_start[$loop_table_upgrade];?>" class="datepicker">

                                        <label>至</label>
                                        <input type="text" value="<?php echo $sql_end[$loop_table_upgrade];?>" name="end[]" class="datepicker">
                                    </th>
                                </tr>
                                <tr>
                                    <th>變換角色為</th>
                                    <th>
                                        <select name="change_role[]">
                                            <?php global $wp_roles; ?>
                                            <?php foreach ( $wp_roles->roles as $key=>$value ):
                                                $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                                                if(!in_array($key, $key_danny_array))
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"
                                                        <?php
                                                        if($sql_change_role[$loop_table_upgrade]==$key) echo 'selected="selected"';
                                                        ?>
                                                    ><?php echo $value['name']; ?></option>
                                                <?php } endforeach; ?>
                                        </select>
                                    </th>
                                </tr>
                                <tr>
                                    <th> 角色期限<i class="fa fa-question-circle fa-2 question-term-phone"></i></th>
                                    <div class="helpout-term-phone">
                                        <div class="help-term-phone">角色期限是變換角色的期限。</div>
                                    </div>
                                    <th>
                                        <input type="text" id="fif-input" name="role_duration[]" value="<?php echo $sql_role_duration[$loop_table_upgrade];?>" required >
                                        <input type="radio" name="year[<?php echo $loop_table_upgrade;?>]" value="year"
                                            <?php
                                            if($sql_year_upgrade[$loop_table_upgrade]=="year") echo 'checked="checked"';
                                            ?> class="radioch"
                                        ><label>年</label>
                                        <input type="radio" name="year[<?php echo $loop_table_upgrade;?>]" value="month"
                                            <?php
                                            if($sql_year_upgrade[$loop_table_upgrade]=="month") echo 'checked="checked"';
                                            ?> class="radioch"
                                        ><label>月</label>
                                    </th>
                                </tr>
                                <tr>
                                    <th>編輯</th>
                                    <th><i class="fa fa-pencil fa-2x fa-pencil-phone_table-edit"></i></th>
                                </tr>
                                <tr>
                                    <th>刪除</th>
                                    <th><a href="admin.php?page=member_customer_woocommerce_page_option&id=<?php echo $sql_id[$loop_table_upgrade];?>&action=del" ><i class="fa fa-times-circle fa-2x"></i></a></th>
                                </tr>
                            </table>
                        </div>
                        <?php
                    }
                    ?>
                </div>
        </div>
        <div class="phone1" id="phone">
            <div>
            </div>
        </div><a class="btn-group open btn btn-primary addtablephone">新增條件設定</a><a class="btn-group open btn btn-primary btn-from-submit-phone">儲存變更</a>
        </form>
        <div id="phone">
            <h1>會員降級設定 </h1>
            <form method="post" id="from_member_de_phone">
                <?php

                for($loop_table_upgrade=0;$loop_table_upgrade<sizeof($sql_member_downgrade);$loop_table_upgrade++)
                {
                    ?>
                    <div class="phone_table">
                        <table>
                            <tr>
                                <th>目前會員角色</th>
                                <th>
                                    <div class="show_hide_web2_phone">
                                        <select name="member_downgrade[]">
                                            <?php global $wp_roles; ?>
                                            <?php foreach ( $wp_roles->roles as $key=>$value ):
                                                $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                                                if(!in_array($key, $key_danny_array))
                                                {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"
                                                        <?php
                                                        if($sql_member_downgrade[$loop_table_upgrade]==$key) echo 'selected="selected"';?>
                                                    ><?php echo $value['name']; ?></option>
                                                <?php } endforeach; ?>
                                        </select>
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th>續角色條件</th>
                                <th>
                                    <select name="upgrade_downgrade[]" id="upgrade-<?php echo $loop_table_upgrade;?>">
                                        <option value="cumulative_full">累積消費滿</option>
                                        <option value="single_full"
                                            <?php
                                            if($sql_upgrade_downgrade[$loop_table_upgrade]=="single_full") echo 'selected="selected"';?>
                                        >單筆消費滿</option>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th>金額/次數</th>
                                <th>
                                    <input type="text" name="downgrade_number[]" value="<?php echo $sql_downgrade_number[$loop_table_upgrade]; ?>" class="money"  placeholder="請輸入金額"/>
                                    <label>元</label>
                                </th>
                            </tr>
                            <tr>
                                <th>未達續條件降級後等級</th>
                                <th>
                                    <select name="downgrade_change_role[]">
                                        <?php global $wp_roles; ?>
                                        <?php foreach ( $wp_roles->roles as $key=>$value ):
                                            $key_danny_array=array("administrator","author","contributor","editor","subscriber","translator","shop_manager");
                                            if(!in_array($key, $key_danny_array))
                                            {
                                                ?>
                                                <option value="<?php echo $key; ?>"
                                                    <?php
                                                    if($sql_downgrade_change_role[$loop_table_upgrade]==$key) echo 'selected="selected"';?>
                                                ><?php echo $value['name']; ?></option>
                                            <?php } endforeach; ?>
                                    </select>
                                </th>
                            </tr>
                            <tr>
                                <th> 角色期限<i class="fa fa-question-circle fa-2 question-term-phone"></i></th>
                                <div class="helpout-term-phone">
                                    <div class="help-term-phone-de">降級後會員等級維持時間。</div>
                                </div>
                                <th>
                                    <input type="text" id="fif-input" name="downgrade_role_duration[]" value="<?php echo $sql_downgrade_role_duration[$loop_table_upgrade];?>">
                                    <input type="radio" name="year_downgrade[<?php echo $loop_table_upgrade;?>]" value="year" class="radioch"
                                        <?php
                                        if($sql_year_downgrade[$loop_table_upgrade]=="year") echo 'checked="checked"';
                                        ?>
                                    >
                                    <label>年</label>
                                    <input type="radio" name="year_downgrade[<?php echo $loop_table_upgrade;?>]" value="month" class="radioch"
                                        <?php
                                        if($sql_year_downgrade[$loop_table_upgrade]=="month") echo 'checked="checked"';
                                        ?>
                                    >
                                    <label>月</label>
                                </th>
                            </tr>
                            <tr>
                                <th>編輯</th>
                                <th><i class="fa fa-pencil fa-2x fa-pencil-phone_table-edit"></i></th>
                            </tr>
                            <tr>
                                <th>刪除</th>
                                <th><a href="admin.php?page=member_customer_woocommerce_page_option&id=<?php echo $sql_id_downgrade[$loop_table_upgrade];?>&action=del1"><i class="fa fa-times-circle fa-2x"> </i></a></th>
                            </tr>
                        </table>
                    </div>
                    <?php
                }
                ?>
        </div>
        <div class="phone2" id="phone">
            <div>

            </div>
        </div><a class="btn-group open btn btn-primary addtablephone2">新增條件設定</a><a class="btn-group open btn btn-primary btn-from-submit-de-phone">儲存變更</a>
    </div>
    </form>
    <div class="edit"> </div>
    <div class="editinfo">
        <h3>黑貓</h3>
        <table>
            <tr>
                <th>運送國家</th>
                <th>
                    <select>
                        <option>請選擇運送區域</option>
                    </select>
                </th>
                <th>特定國家</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>商店所在縣市</th>
                <th>
                    <input/>
                </th>
                <th>商店代號</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>HushKey</th>
                <th>
                    <input/>
                </th>
                <th>HushIV</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>寄件人*</th>
                <th>
                    <input/>
                </th>
                <th>寄件人手機*</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>寄件人郵遞區號*</th>
                <th>
                    <input/>
                </th>
                <th>寄件人地址*</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th> </th>
                <th></th>
                <th> </th>
                <th></th>
            </tr>
            <tr>
                <th> </th>
                <th><a class="btn-group open btn btn-primary">更新</a></th>
                <th> <a class="btn-group open btn btn-primary">取消</a></th>
                <th></th>
            </tr>
            <tr>
                <th> </th>
                <th></th>
                <th> </th>
                <th></th>
            </tr>
        </table>
    </div>
    <div class="editinfophone">
        <table>
            <h3>黑貓</h3>
            <tr>
                <th>運送國家</th>
                <th>
                    <select>
                        <option>請選擇運送區域</option>
                    </select>
                </th>
            </tr>
            <tr>
                <th>特定國家</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>商店所在縣市</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>商店代號</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>HushKey</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>HushIV</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>寄件人*</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>寄件人手機*</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>寄件人郵遞區號*</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>寄件人地址*</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th> </th>
                <th></th>
            </tr>
            <tr>
                <th><a class="btn-group open btn btn-primary">更新</a></th>
                <th> <a class="btn-group open btn btn-primary">取消</a></th>
            </tr>
            <tr>
                <th> </th>
                <th></th>
            </tr>
        </table>
    </div>
</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.4/vue.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js'></script>

<script src="js/index.js"></script>
