<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css'>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link rel="stylesheet" href="css/style.css">

<div id="app">
    <form method="post" action="admin.php?page=member_customer_woocommerce_specific_page_option" id="from_member_de">
        <div class="ok">
            <h1>自訂義物流運費</h1>
            <table id="buildyourformweb">
                <tr>
                    <th>啟用</th>
                    <th>運送方式</th>
                    <th>運費</th>
                    <th>免運金額</th>
                    <th>進階設定</th>
                    <th>編輯</th>
                    <th>刪除</th>
                </tr>

                <?php
                $danny_right_count_1=0;
                for($danny_icount=0;$danny_icount<sizeof($array_danny);$danny_icount++)
                {
                    for($danny_right_count=0;$danny_right_count<sizeof($array_danny);$danny_right_count++)
                        if($danny_icount==$danny_shipping[$danny_right_count]["is_order"])$danny_right_count_1=$danny_right_count;
                    ?>
                    <tr <?php if($danny_shipping[$danny_right_count_1]["availability"]=="" &
                        $danny_shipping[$danny_right_count_1]["title"]=="" &
                        $danny_shipping[$danny_right_count_1]["free_cost"]=="" &
                        $danny_shipping[$danny_right_count_1]["countries"]=="" )echo "class='danny_display'";?>>
                        <th> <i class="fa fa-power-off fa-2x
        <?php if($danny_shipping[$danny_right_count_1]["enabled"]=="yes") echo "gree";
                            else echo " block"; ?>" @click="chang_color()"></i>
                            <input type="hidden" name="danny_enabled[]" value="<?php echo $danny_shipping[$danny_right_count_1]["enabled"];?>" class="danny_enabled" >
                            <input type="hidden" name="danny_availability[]" value="<?php echo $danny_shipping[$danny_right_count_1]["availability"];?>" class="danny_availability" >
                            <input type="hidden" name="danny_countries[]" value="<?php echo $danny_shipping[$danny_right_count_1]["countries"];?>" class="danny_countries" >
                        </th>
                        <th>
                            <input name="danny_title[]" value="<?php echo $danny_shipping[$danny_right_count_1]["title"];?>" class="danny_title"  />
                            <input type="hidden" name="danny_is_order[]" class="danny_is_order" value="<?php echo $danny_shipping[$danny_right_count_1]["is_order"];?>" />
                        </th>
                        <th>
                            <input name="danny_cost_title[]" value="<?php echo $danny_shipping[$danny_right_count_1]["cost"];?>" class="danny_cost_title"  />
                        </th>
                        <th>
                            <input name="danny_cost_free_cost[]" value="<?php echo $danny_shipping[$danny_right_count_1]["free_cost"];?>" class="danny_cost_free_cost" />
                        </th>
                        <th><i class="fa fa-cog fa-2x fa-fw fa-spin-web"></i></th>
                        <th><i class="fa fa-pencil fa-2x"></i></th>
                        <th><a href="admin.php?page=member_customer_woocommerce_specific_page_option&id=<?php echo $danny_shipping[$danny_right_count_1]["is_order"];?>&action=del" ><i class="fa fa-times-circle fa-2x"></i></a></th>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
        <div class="ok"><a class="btn-group open btn btn-primary addwebrow">新增條件設定</a><a class="btn-group open btn btn-primary btn-from-submit-de">儲存變更</a></div>

    </form>
    <div class="phone">
        <div id="phone">
            <h1>自訂義物流運費</h1>
            <form method="post" action="admin.php?page=member_customer_woocommerce_specific_page_option" id="from_member_phone">
                <?php
                $danny_right_count_1=0;
                for($danny_icount=0;$danny_icount<sizeof($array_danny);$danny_icount++)
                {
                    for($danny_right_count=0;$danny_right_count<sizeof($array_danny);$danny_right_count++)
                        if($danny_icount==$danny_shipping[$danny_right_count]["is_order"])$danny_right_count_1=$danny_right_count;
                    ?>
                    <div class='phone-edit <?php if($danny_shipping[$danny_right_count_1]["availability"]=="" &
                        $danny_shipping[$danny_right_count_1]["title"]=="" &
                        $danny_shipping[$danny_right_count_1]["free_cost"]=="" &
                        $danny_shipping[$danny_right_count_1]["countries"]=="" )echo "danny_display_phone";?>'>
                        <table>
                            <tr>
                                <th>啟用</th>
                                <th> <i class="fa fa-power-off fa-2x
        <?php if($danny_shipping[$danny_right_count_1]["enabled"]=="yes") echo "gree";
                                    else echo " block"; ?>" @click="chang_color()"></i><input type="hidden" name="danny_enabled[]" value="<?php echo $danny_shipping[$danny_right_count_1]["enabled"];?>" class="danny_enabled" >
                                    <input type="hidden" name="danny_availability[]" value="<?php echo $danny_shipping[$danny_right_count_1]["availability"];?>" class="danny_availability" >
                                    <input type="hidden" name="danny_countries[]" value="<?php echo $danny_shipping[$danny_right_count_1]["countries"];?>" class="danny_countries" ></th>
                            </tr>
                            <tr>
                                <th>運送方式</th>
                                <th>
                                    <input name="danny_title[]" value="<?php echo $danny_shipping[$danny_right_count_1]["title"];?>" class="danny_title"  />
                                    <input type="hidden" name="danny_is_order[]" class="danny_is_order" value="<?php echo $danny_shipping[$danny_right_count_1]["is_order"];?>" />
                                </th>
                            </tr>
                            <tr>
                                <th>運費</th>
                                <th>
                                    <input name="danny_cost_title[]" value="<?php echo $danny_shipping[$danny_right_count_1]["cost"];?>" class="danny_cost_title"  />
                                </th>
                            </tr>
                            <tr>
                                <th>免運金額</th>
                                <th>
                                    <input name="danny_cost_free_cost[]" value="<?php echo $danny_shipping[$danny_right_count_1]["free_cost"];?>" class="danny_cost_free_cost" />
                                </th>
                            </tr>
                            <tr>
                                <th>編輯</th>
                                <th><i class="fa fa-pencil fa-2x phone-edit-fa-pencil"></i></th>
                            </tr>
                            <tr>
                                <th>刪除</th>
                                <th><a href="admin.php?page=member_customer_woocommerce_specific_page_option&id=<?php echo $danny_shipping[$danny_right_count_1]["is_order"];?>&action=del" ><i class="fa fa-times-circle fa-2x"></i></a></th>
                            </tr>
                        </table>
                    </div>
                    <?php
                }
                ?>
            </form>
        </div><a class="btn-group open btn btn-primary addtablephone">新增條件設定</a><a class="btn-group open btn btn-primary btn_submit_phone">儲存變更</a>
    </div>
    <div class="edit"> </div>
    <div class="editinfo">
        <form method="post" action="admin.php?page=member_customer_woocommerce_specific_page_option" id="from_member_update">
            <h3>運送區域</h3>
            <table>
                <tr>
                    <th>運送國家</th>
                    <th>
                        <select name="availability" class="speclfic_country_danny">
                            <option>請選擇運送區域</option>
                            <option value="all" selected="selected">所有允許的國家</option>
                            <option value="specific">特定國家</option>
                        </select>
                    </th>
                    <th><label class="danny_specific_1234">特定國家</label></th>
                    <th>
                        <div class="danny_specific_1234"><?php
                            /*ymlin_fixed_201709 新增下拉國家方式*/
                            $arr = new WC_Countries();

                            $arr1 = $arr->get_countries();
                            $method = get_option('woocommerce_my_shipping_method_settings');

                            ?>
                            <select id="tags" name="countries">
                                <?php
                                foreach ($arr1 as $key => $value) {
                                    ?>
                                    <option name="countries" <?php if($method["countries"][0] == $key)
                                        echo 'selected="selected"'; ?>
                                            value="<?php echo $key ?>"><?php echo $value ?></option>
                                    <?php
                                }
                                /*ymlin_fixed_201709 新增下拉國家方式*/
                                ?>
                            </select>
                        </div>
                        <input type="hidden" name="danny_id_specific_enabled" id="danny_id_specific_enabled" />
                        <input type="hidden" name="danny_id_specific_title" id="danny_id_specific_title" />
                        <input type="hidden" name="danny_id_specific_is_order" id="danny_id_specific_is_order" />
                        <input type="hidden" name="danny_id_specific_cost_title" id="danny_id_specific_cost_title" />
                        <input type="hidden" name="danny_id_specific_1234_free_cost" id="danny_id_specific_1234_free_cost" />
                        <input type="hidden" name="danny_id_specific_1234" id="danny_id_specific_1234" />
                        <input type="hidden" name="danny_id_action" id="danny_update_specific_1234" />
                    </th>
                </tr>

                <tr>
                    <th></th>
                    <th></th>
                    <th> </th>
                    <th></th>
                </tr>
                <tr>
                    <th> </th>
                    <th><a class="btn-group open btn btn-primary from_member_update">更新</a></th>
                    <th> <a class="btn-group open btn btn-primary">取消</a></th>
                    <th></th>
                </tr>
                <tr>
                    <th> </th>
                    <th></th>
                    <th> </th>
                    <th></th>
                </tr>
            </table>
        </form>
    </div>
    <div class="editinfophone">
        <table>
            <h3>運送區域</h3>
            <tr>
                <th>運送國家</th>
                <th>
                    <select>
                        <option>請選擇運送區域</option>
                    </select>
                </th>
            </tr>
            <tr>
                <th>特定國家</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>商店所在縣市</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>商店代號</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>HushKey</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>HushIV</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>寄件人*</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>寄件人手機*</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>寄件人郵遞區號*</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th>寄件人地址*</th>
                <th>
                    <input/>
                </th>
            </tr>
            <tr>
                <th> </th>
                <th></th>
            </tr>
            <tr>
                <th><a class="btn-group open btn btn-primary">更新</a></th>
                <th> <a class="btn-group open btn btn-primary">取消</a></th>
            </tr>
            <tr>
                <th> </th>
                <th></th>
            </tr>
        </table>
    </div>
</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.4/vue.min.js'></script>

<script src="js/index.js"></script>
